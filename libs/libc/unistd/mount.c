#include <unistd.h>
#include <types.h>
#include <stddef.h>
#include <string.h>
#include <math.h>
#include <sys/syscall.h>


int mount(const char *source, const char *targe, const char *fstype, uint64_t flags)
{
    if (!source || !targe || !fstype)
        return -1;
    return syscall4(int, SYS_MOUNT, (uintptr_t)source, (uintptr_t)targe, (uintptr_t)fstype, flags);
}

int unmount(const char *source, uint64_t flags)
{
    if (!source)
        return -1;
    return syscall2(int, SYS_UNMOUNT, (uintptr_t)source, flags);
}

int mkfs(const char *source, const char *fstype, uint64_t flags)
{
    if (!source || !fstype)
        return -1;
    return syscall3(int, SYS_MKFS, (uintptr_t)source, (uintptr_t)fstype, flags);
}

int mount_info(mount_t *mt,uint32_t count)
{
    return syscall2(int,SYS_MOUNTINFO,(uintptr_t)mt,count);
}