#include <unistd.h>
#include <string.h>

// open a free device from assign class
int openclass(const char *cname, int flags)
{
    char _devname[32];
    char devname[32];
    int fd = 0;

    memset(devname, 0, 32);
    memset(_devname, 0, 32);

    if (probedev(cname, _devname, 32) < 0)
        return -1;

    strcat(devname, "/dev/");
    strcat(devname, _devname);
    
    fd = open(devname, flags);
    return fd;
}