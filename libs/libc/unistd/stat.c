#include <unistd.h>
#include <types.h>
#include <stddef.h>
#include <string.h>
#include <sys/status.h>
#include <sys/syscall.h>

int stat(const char *path, status_t *buff)
{
    char abs[MAX_PATH_LEN+1];

    if (!path)
        return -1;

    memset(abs, 0, MAX_PATH_LEN);
    buildpath((char *)path, abs);
    return syscall2(int, SYS_STATUS, (uintptr_t)abs, (uintptr_t)buff);
}

int chmod(char *path, mode_t mode)
{
    char abs[MAX_PATH_LEN+1];

    if (!path)
        return -1;
    buildpath((char *)path, abs);
    return syscall2(int, SYS_CHMOD, (uintptr_t)path, mode);
}
