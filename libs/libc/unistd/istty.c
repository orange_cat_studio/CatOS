#include<unistd.h>
#include<sys/ioctl.h>

int istty(int desc)
{
    int arg;

    if (desc < 0)
        return 0;
    if (ioctl(desc, TTYIO_ISTTY, &arg) < 0)
        return 0;
    return arg;
}


