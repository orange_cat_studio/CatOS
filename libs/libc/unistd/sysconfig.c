#include<unistd.h>
#include<sys/syscall.h>

int64_t sysconfig(int name)
{
    return syscall1(int64_t,SYS_SYSCONFIG,name);
}