#include<unistd.h>
#include<sys/ioctl.h>
#include<sys/udev.h>
#include<string.h>


char *ttyname(int desc)
{
    static char name[DEVICE_NAME_LEN+1];

    if(desc<0)
        return NULL;

    memset(name,0,DEVICE_NAME_LEN);
    if(ioctl(desc,TTYIO_NAME,name)<0)
        return NULL;
    
    return name;
}


