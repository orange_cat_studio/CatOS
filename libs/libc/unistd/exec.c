#include <unistd.h>
#include <types.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/proc.h>
#include <sys/syscall.h>

extern char **__environ;

int execve(const char *path, const char *argv[], const char *envp[])
{
    return syscall3(int, SYS_EXECVE, (uintptr_t)path, (uintptr_t)argv, (uintptr_t)envp);
}

int execv(const char *path, const char *argv[])
{
    return execve(path, argv, __environ);
}

int execle(const char *path, const char *envp[],const char *arg, ...)
{
    va_list *pargs = (va_list *)&arg;
    return execve(path, pargs, envp);
}

int execl(const char *path, const char *arg, ...)
{
    va_list *pargs = (va_list *)(&arg);
    const char **p = (const char **)pargs;

    return execv(path, p);
}

int execvp(const char *file, const char *argv[])
{
    return execv(file, argv);
}

int execlp(const char *file, const char *arg, ...)
{
    va_list *parg = (va_list *)&arg;
    const char **p = (const char **)parg;

    return execvp(file, (char **)(p));
}
