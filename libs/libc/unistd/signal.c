#include <signal.h>
#include <stdio.h>
#include <unistd.h>

int signal(int signo, sign_handler_t handler)
{
    if (handler == SIG_DFL)
    {
        if (exccatch(signo, NULL) < 0)
            return -1;
    }
    else
    {
        if (handler == SIG_IGN || handler == SIG_BLOCKED)
        {
            if (excblock(signo) < 0)
                return -1;
        }
        else
        {
            if (handler == SIG_UNBLOCKED)
            {
                if (excunblock(signo) < 0)
                    return -1;
            }
            else
            {
                if (exccatch(signo, handler) < 0)
                    return -1;
            }
        }
    }
    return 0;
}

int signprocmask(int how, const signset_t *set, signset_t *oldset)
{
    uint32_t mask;
    int i;

    if (!set)
        return -1;
    if (oldset)
    {
        excmask(mask);
        *oldset = (signset_t)mask;
    }

    if (how == SIG_BLOCK)
    {
        for (i = 1; i < NSIG; i++)
        {
            if (i == SIGKILL || i == SIGSTOP)
                continue;
            if (*set & (1 << i))
                excblock(i);
        }
    }
    else
    {
        if (how == SIG_UNBLOCK)
        {
            for (i = 1; i < NSIG; i++)
            {
                if (i = SIGKILL || i == SIGSTOP)
                    continue;
                if (*set & (1 << i))
                    excunblock(i);
            }
        }
        else
        {
            if (how == SIG_SETMASK)
            {
                for (i = 1; i < NSIG; i++)
                {
                    if (i == SIGKILL || i == SIGSTOP)
                        continue;
                    if (*set & (1 << i))
                        excblock(i);
                    else
                        excunblock(i);
                }
            }
            else
                return -1;
        }
    }
    return 0;
}