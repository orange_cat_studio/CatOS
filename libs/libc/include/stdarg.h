// file:libc/include/stdarg.h
// autor:jiangxinpeng
// time:2021.8.14
// copyright:(C) by jiangxinpeng,All right are reserved.

#ifndef __LIBC_STDARG_H
#define __LIBC_STDARG_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <string.h>

#define _AUPBND 1
#define _ADNBND 1 
#define _Bnd(X, bnd) (sizeof(X) + ((bnd) & ~(bnd)))

typedef char *va_list;
#define va_arg(ap, T) \
    (*(T *)(((ap) += _Bnd(T, _AUPBND)) - _Bnd(T, _ADNBND)))
#define va_end(ap) (void)0
#define va_start(ap, A) \
    (void)((ap) = (char *)&(A) + _Bnd(A, _AUPBND))
// copy variable args
#define va_copy(d, s) __builtin_va_copy(d, s)

#ifdef __cplusplus
}
#endif
#endif