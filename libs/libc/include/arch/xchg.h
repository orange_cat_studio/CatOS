#ifndef __LIBC_ARCH_XCHG_H
    #define __LIBC_ARCH_XCHG_H

    #ifdef __cplusplus
    extern "C"{
    #endif 

    #include"config.h"
    #include"x86/const.h"

    #define test_and_set __test_and_set

    #ifdef __cplusplus
    }
    #endif

#endif