#ifndef __LIBC_ARCH_CONFIG_H
#define __LIBC_ARCH_CONFIG_H

#define LIB_ARCH_X86 1
#define LIB_ARCH_X64 2

//lib arch swith
#define LIB_ARCH LIB_ARCH_X86

#if LIB_ARCH == LIB_ARCH_X86
#include "x86/atomic.h"
#include "x86/const.h"
#include "x86/xchg.h"
#elif LIB_ARCH == LIB_ARCH_X64
#include "x64/atomic.h"
#include "x64/const.h"
#include "x64/xchg.h"
#else 
#error "libc arch config error!\n"
#endif 
#endif
