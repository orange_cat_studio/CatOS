#ifndef __LIBC_DIRENT_H
    #define __LIBC_DIRENT_H

    #include<types.h>

    typedef struct 
    {
       int flags;
       dir_t diridx;
    }DIR;

    #define DIR_IN_RANAGE(dir) !((dir)<__dirdes_table||(dir)>=__dirdes_table+_MAX_DIRDES_NR)
    
#endif
