#ifndef __LIBC_STDINT_H
#define __LIBC_STDINT_H

#include<sys/arch.h>

//define standard c type to another type,in order to we can assume size of this type.
#if __ARCH==__ARCH_X86
//define uint_t type
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long uint64_t;

//define signed int_t type
typedef char int8_t;
typedef short int16_t;
typedef int int32_t;
typedef long long int64_t;
#elif __ARCH==__ARCH_X64
//define uint_t type
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long uint64_t;
typedef unsigned long long uint64_t;

//define signed int_t type
typedef char int8_t;
typedef short int16_t;
typedef int int32_t;
typedef long int64_t;
typedef long long int64_t;
#else
#error "[config] system arch config errror!\nplease check include file "sys/arch.h"!\n"
#endif

//max integer
typedef uint64_t uintmax_t;
typedef int64_t intmax_t;



#endif