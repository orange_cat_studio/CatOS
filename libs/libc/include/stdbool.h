#ifndef __LIBC_STDBOOL_H
#define __LIBC_STDBOOL_H

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef _STDBOOL
#define _STDBOOL

#ifndef __cpluplus
#define bool char //c lanuage bool type
#define true 1
#define false 0
#else
#define bool _Bool //cplusplus bool type
#endif

#endif

#ifdef __cplusplus
}
#endif
#endif