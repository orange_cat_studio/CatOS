#ifndef __LIBC_EXIT_H
    #define __LIBC_EXIT_H


    #ifdef __cplusplus
    extern "C" {
    #endif

    void exit(int status);

   

    #ifdef __cplusplus
    }
    #endif
#endif