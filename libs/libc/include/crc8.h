#ifndef __LIBC_CRC8_H
    #define __LIBC_CRC8_H

    #ifdef __cplusplus
    extern "C"{
    #endif

    #include<stdint.h>

    uint8_t crc8_sum(uint8_t crc, const uint8_t * buf, int len);


    #ifdef __cplusplus
    }
    #endif

#endif