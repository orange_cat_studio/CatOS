#ifndef __LIBC_GETOPT_H
#define __LIBC_GEtOPT_H

extern char *optarg;
extern int optind;
extern int opterr;
extern int optopt;

typedef struct option
{
    const char *name;
    int has_arg;
    int *flag;
    int val;
} option_t;

#define no_argument 0
#define required_argument 1
#define optional_argument 2

extern int getopt();
extern int getopt_long(int argc, char *const *argv, const char *shortopts,
                       const struct option *longopts, int *longind);
extern int getopt_long_only(int argc, char *const *argv,
                            const char *shortopts,
                            const struct option *longopts, int *longind);

/* Internal only.  Users should not call this directly.  */
extern int _getopt_internal(int argc, char *const *argv,
                            const char *shortopts,
                            const struct option *longopts, int *longind,
                            int long_only);
#endif