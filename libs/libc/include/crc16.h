#ifndef __LIBC_CRC16_H
    #define __LIBC_CRC16_H

    #ifdef __cplusplus
    extern "C"{
    #endif

    #include<stdint.h>


    uint16_t crc16_sum(uint16_t crc, const uint8_t * buf, int len);

    #ifdef __cplusplus
    }
    #endif
#endif