#ifndef __LIBC_SETJMP_H
#define __LIBC_SETJMP_H

typedef long jmp_buf[10];

int setjmp(jmp_buf env);
void longjmp(jmp_buf env,int val);

#endif