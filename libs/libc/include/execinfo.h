#ifndef __LIBC_EXECINFO_H
#define __LIBC_EXECINFO_H

int backtrace(void **buffer,int size);
int backtrace2(void **buffer,int size);

#endif