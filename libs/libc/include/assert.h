#ifndef __LIBC_ASSERT_H
#define __LIBC_ASSERT_H

#include<stdio.h>
#include<stdlib.h>

#define ASSERT_VOID_CAST (void)

#define assert(exp) ((exp) ? ASSERT_VOID_CAST(0) : assertion_failure(#exp, __FILE__, __LINE__))

static inline void assertion_failure(char *exp, char *file, int line)
{
    printf("Assert failed:%s file:%s,line:%d\n",exp,file,line);
    abort();
}

#endif