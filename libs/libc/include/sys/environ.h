#ifndef __SYS_ENVIRON_H
#define __SYS_ENVIRON_H

#include <sys/list.h>

#define ENV_NAME_MAX 16
#define ENV_VAL_MAX 64

//system environ var
typedef struct env
{
    char name[ENV_NAME_MAX+1];
    char val[ENV_VAL_MAX+1];
    list_t list;
} env_t;

//get a env from system
env_t *env(env_t *env, env_t *out);

#endif