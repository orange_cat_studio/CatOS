#ifndef _SYS_TYPES_H
    #define _SYS_TYPES_H

    #ifdef __cplusplus
    extern "C"{
    #endif

    #include<stdint.h>
    #include<types.h>
    #include<stddef.h>

    #define DIR_NAME_LEN 256

    #define DE_RDONLY 0x01  //read only
    #define DE_HIDDEN 0x02  //hidden
    #define DE_SYSTEM 0x04  //system
    #define DE_DIR    0x08 //dir
    #define DE_ARCHIVE 0x10 //archive
    #define DE_BLOCK  0x20 //block
    #define DE_CHAR   0x40  //char

    typedef struct dirent
    {
        size_t d_size;
        uint32_t d_time;    //size
        uint32_t d_date;    //date
        mode_t d_attr;      //attr
        char d_name[DIR_NAME_LEN];  //name
    }dirent_t;

    typedef int64_t ssize_t;
    typedef char * caddr_t;
    
    #ifdef __cplusplus
    }
    #endif
#endif