#ifndef __SYS_LOG_H
#define __SYS_LOG_H

#define LOG_TITLE_LEN 15
#define LOG_CONTEXT_LEN 64

typedef enum
{
    LOG_TYPE_SYSTEM,
    LOG_TYPE_APP,
}log_type_t;

typedef enum
{
    LOG_EVNET_TYPE_ERR,
    LOG_EVENT_TYPE_WARN,
    LOG_EVNET_TYPE_NORMAL,
    LOG_EVENT_TYPE_DEBUG,
}log_event_type_t;

typedef struct
{
    uint32_t id;    //log id
    uint32_t year;  //log year
    uint32_t month; //log month
    uint32_t day;   //log day
    uint32_t hour;  //log hour
    uint32_t minute; //log minute
    uint32_t second; //log second
    log_type_t type; //log type
    log_event_type_t event_type; //event type
    uint32_t error_code;         //log error code
    char title[LOG_TITLE_LEN];   //log title
    char context[LOG_CONTEXT_LEN]; //log context
}log_t;

int log_write(int type, char* title, char* context, int err, int event_type);
int log_read(int type, log_t* buff);
int log_del(int type, int title);

#endif