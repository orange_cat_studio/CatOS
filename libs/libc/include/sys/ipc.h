//file:libc/include/sys/ipc.h
//autor:jiangxinpeng
//time:2021.12.24
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef __LIBC_IPC_H
#define __LIBC_IPC_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <types.h>

    typedef enum ipc_flags
    {
        IPC_CREATE = 0x01,  //create a ipc
        IPC_EXCL = 0x02,    //must open a no exist ipc
        IPC_READ = 0x04,    //reader for pipe
        IPC_WRITE = 0x08,   //writer for pipe
        IPC_NOWAIT = 0x10,  //no wiat
        IPC_EXCEPT = 0x20,  //except something
        IPC_NOERROR = 0x40, //no error
        IPC_NOSYNC = 0x80,  //no sync
        IPC_RND = 0x100,    //addr round page aligned
        IPC_REMAP = 0x200,  //remap memory
    } ipc_local_flags;

    typedef enum
    {
        IPC_SHM = 0x100000,  //share mem lpc
        IPC_SEM = 0x200000,  //semaphore lpc
        IPC_MSG = 0x400000,  //message queue lpc
        IPC_PIPE = 0x800000, //pipe lpc
    } ipc_slaver_flags;

    typedef enum
    {
        IPC_DEL = 0x01,   //del a lpc from kernel
        IPC_SETRW = 0x02, //set lpc reader/writer
        IPC_SET = 0x03    //set lpc
    } ipc_cmd;

    int shmget(char *name, uint64_t size, uint64_t flags);
    int shmput(int shmid);
    void *shmmap(int shmid, void *shmaddr, int shmflag);
    int shmunmap(void *shmaddr, int shmflag);
    int semget(char *name, int value, int semflag);
    int semput(int semid);
    int semdown(int semid, int semflag);
    int semup(int semid);
    int msgget(char *name, unsigned long flags);
    int msgput(int msgid);
    int msgsend(int msgid, void *msgbuf, size_t size, int msgflg);
    int msgrecv(int msgid, void *msgbuf, size_t msgsz, int msgflg);

#ifdef __cplusplus
}
#endif
#endif