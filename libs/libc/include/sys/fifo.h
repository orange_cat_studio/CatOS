#ifndef _SYS_FIFO_H
    #define _SYS_FIFO_H

    #ifdef __cplusplus
    extern "C"
    {
    #endif

    #include "spinlock.h"

    typedef struct fifo
    {
        uint8_t *buffer;
        uint64_t size;
        uint64_t in;
        uint64_t out;
        spinlock_t lock;
    } fifo_t;

    uint64_t fifo_get(fifo_t *fifo, uint8_t *buff, uint64_t len);
    uint64_t fifo_put(fifo_t *fifo, uint8_t *buff, uint64_t len);
    uint64_t fifo_len(fifo_t *fifo);
    void fifo_reset(fifo_t *fifo);
    void fifo_free(fifo_t *fifo);
    fifo_t *fifo_alloc(uint64_t size);

    #ifdef __cplusplus
    }
    #endif
#endif