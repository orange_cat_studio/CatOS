#ifndef __SYS_TIME_H
#define __SYS_TIME_H

#include <types.h>
#include <sys/walltime.h>

#define CLOCK_REALTIME            1 /*系统统当前时间，从1970年1.1日算起*/
#define CLOCK_MONOTONIC           2 /*系统的启动时间，不能被设置*/
#define CLOCK_PROCESS_CPUTIME_ID  3 /* 本进程运行时间*/
#define CLOCK_THREAD_CPUTIME_ID   4 /*本线程运行时间*/


#define CLOCKS_PER_SEC  (100 * 5)   /* 1秒的时钟数 */
#define HZ_PER_CLOCKS   (CLOCKS_PER_SEC / 100)   /* 每个时钟的HZ数 */

//micro seconds per ticks
#define MS_PER_TICKS (1000 / CLOCKS_PER_SEC)

#define MSEC_TO_TICKS(msec) ((msec) / MS_PER_TICKS)

#define TICHS_TO_MSEC(ticks) ((ticks)*MS_PER_TICKS)


//tm block define
typedef struct tm
{
    int tm_sec;  //second
    int tm_min;  //minute
    int tm_hour; //hour
    int tm_mday; //days from this month
    int tm_mon;  //month
    int tm_year; //year
    int tm_wday; //days from sunday
    int tm_yday; //days from this year start(1.1)
    int tm_isdst;
    long int __tm_gmtoff;  /* Seconds east of UTC.  */
    const char *__tm_zone; /* Timezone abbreviation.  */
} tm_t;

//time value struct
typedef struct timeval
{
    uint64_t tv_sec;  //second
    uint64_t tv_usec; //micro second
} timeval_t;

typedef struct timezone
{
    uint64_t tz_minutewest;
    uint64_t tz_dsttime;
} timezone_t;

typedef struct timespec
{
    uint64_t tv_sec;
    uint64_t tv_nsec;
} timespec_t;

uint64_t alarm(uint64_t second);
int walltime(walltime_t *walltime);
clock_t getticks();
int gettimeofday(timeval_t *tv, timezone_t *tz);
int clock_gettime(clock_type_t clockid, timespec_t *ts);
int walltime_switch(walltime_t *walltime, tm_t *tm);
int usleep(uint32_t usec);
void Mdelay(time_t msec);

#endif