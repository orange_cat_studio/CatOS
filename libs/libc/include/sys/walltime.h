#ifndef __LIBC_WALLTIME_H
#define __LIBC_WALLTIME_H

#include <types.h>

// write
#define WALLTIME_WR_TIME(hour, min, sec) ((unsigned short)((((hour)&0x1f) << 11) | \
                                                           (((min)&0x3f) << 5) | (((sec) / 2) & 0x1f)))
#define WALLTIME_WR_DATE(year, mon, day) (((unsigned short)(((year)-1980) & 0x7f) << 9) | \
                                          (((mon)&0xf) << 5) | ((day)&0x1f))

// read
#define WALLTIME_RD_HOUR(date) ((unsigned int)(((date) >> 11) & 0x1f))
#define WALLTIME_RD_MIN(date) ((unsigned int)(((date) >> 5) & 0x3f))
#define WALLTIME_RD_SEC(date) ((unsigned int)(((date)&0x1f) * 2))
#define WALLTIME_RD_YEAR(date) ((unsigned int)((((date) >> 9) & 0x7f) + 1980))
#define WALLTIME_RD_MON(date) ((unsigned int)(((date) >> 5) & 0xf))
#define WALLTIME_RD_DAY(date) ((unsigned int)((date)&0x1f))
#define MINUTE 60
#define HOUR (60 * MINUTE)
#define DAY (24 * HOUR)
#define YEAR (365 * DAY)

typedef struct
{
    uint64_t second;    // (0-59)
    uint64_t minute;    // (0-59)
    uint64_t hour;      // (0-23)
    uint64_t day;       // (1-31)
    uint64_t month;     // (1-12)
    uint64_t year;      //  year
    uint64_t week_days; // [0-6]
    uint64_t year_days; // [0-366]
} walltime_t;

int walltime(walltime_t *walltime);

#endif