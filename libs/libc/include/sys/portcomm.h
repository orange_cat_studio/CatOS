#ifndef __LIBC_PORTCOMM_H
    #define __LIBC_PORTCOMM_H

    #ifdef __cplusplus
    extern "C"{
    #endif

    #include"types.h"
  
    #define PORT_MSG_HEADER_SIZE (sizeof(port_msg_header_t))
    #define PORT_MSG_SIZE (4096)

    typedef struct 
    {
       uint32_t port;
       uint32_t id;
       uint32_t size;
    }port_msg_header_t;

    typedef struct 
    {
        port_msg_header_t header;
        uint8_t data[PORT_MSG_SIZE];
    }port_msg_t;
    
    #define PORT_COM_UNNAME_START (PORT_COM_MAX-4)

    enum 
    {
        PORT_COM_TEST=0,
        PORT_COM_NET,
        PORT_COM_GRAPH,
        PORT_COM_LAST=8,
    }port_com_reserved;

    enum
    {
        PORT_BIND_GROUP=0x01,   //bind to group
        PORT_BIND_ONCE=0x02,    //only bind once
    }port_com_bind;

    int bind_port(int port,int flags);
    int unbind_port(int port);
    int reply_port(int port,port_msg_t *msg);
    int receive_port(int port,port_msg_t *msg);
    int request_port(int port,port_msg_t *msg);
    void port_msg_reset(port_msg_t *msg);
    void port_msg_copy_header(port_msg_t *src,port_msg_t *dest);

    #ifdef __cplusplus
    }
    #endif
#endif