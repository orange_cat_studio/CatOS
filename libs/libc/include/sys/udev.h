#ifndef __LIBC_UDEV_H
#define __LIBC_UDEV_H

#ifdef __cplusplus
extern "C"
{
#endif

#include<types.h>

#define DEVICE_NAME_LEN 32

    //enum all device type in there
    typedef enum _device_type_t
    {
        DEVICE_TYPE_ANY = 0,            //any device
        DEVICE_TYPE_DISK,               //disk device
        DEVICE_TYPE_PROCESS,            //process device
        DEVICE_TYPE_VOL,                //disk volume device
        DEVICE_TYPE_BEEP,               //beep device
        DEVICE_TYPE_KEYBOARD,           //keyborad device
        DEVICE_TYPE_NETWORK,            //network device
        DEVICE_TYPE_MOUSE,              //mouse device
        DEVICE_TYPE_NULL,               //null device
        DEVICE_TYPE_SCREEN,             //screen device
        DEVICE_TYPE_VIDEO,              //video device
        DEVICE_TYPE_SOUND,              //sound device
        DEVICE_TYPE_WAVE_IN,            //sound input device
        DEVICE_TYPE_WAVE_OUR,           //sound output device
        DEVICE_TYPE_STREAM,             //stream device
        DEVICE_TYPE_VIRTUAL_DISK,       //virtual disk device
        DEVICE_TYPE_VIRTUAL_CHAR,       //virtual char device
        DEVICE_TYPE_UNKNOW,             //unknow device
        DEVICE_TYPE_PORT,               //port device
        DEVICE_TYPE_SERIAL,             //serial device
        DEVICE_TYPE_PARALLEL,           //parallel   device
        DEVICE_TYPE_PHYSIC_NETWORKCARD, //networkcard
        DEVICE_TYPE_BUS_EXTERN,         //bus extern
        DEVICE_TYPE_VIEW,               //view device
        DEVICE_TYPE_8042_PORT,          //8042 port
        DEVICE_TYPE_OTHER               //other more device
    } device_type_t;

    typedef struct devent
    {
        uint16_t de_type;
        char de_name[DEVICE_NAME_LEN+1];
    } devent_t;

    int scandev(devent_t *de, device_type_t type, devent_t *out);
    int fastio(int fd, int cmd, void *arg);
    int fastread(int fd, void *buff, size_t size);
    int fastwrite(int fd, void *buff, size_t size);

#ifdef __cplusplus
}
#endif
#endif