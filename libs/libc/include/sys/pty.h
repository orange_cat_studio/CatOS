#ifndef SYS_PTY_H
#define SYS_PTY_H
    int posix_openpt(int flags);
    char *ptsname(int fd);
    int grantpt(int fd_master);
    int unlockpt(int fd_master);
    int openpty(int *amster,int *aslave,char *name);
    int ptym_open(char *pts_name,int namelen);
    int ptys_open(const char *name);
#endif