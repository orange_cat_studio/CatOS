#ifndef _SYS_CONFIG_H
#define _SYS_CONFIG_H

#ifdef __cplusplus
extern "C"
{
#endif

//os name
#define SYS_NAME "CatOS"
//os version
#define SYS_VER "0.1"
//cpu arch
#define SYS_ARCH "X86"
//os platform type
#define SYS_PLATFORM "PC"
//local host name
#define SYS_HOST "TEST-PC"
//sys build time 
#define SYS_BUILD_TIME "2022.5.16"
//reserved
#define SYS_RESERVED "CatOS Studio-JiangXinpeng"

#ifdef __cplusplus
}
#endif
#endif