#ifndef __SYS_DIR_H
#define __SYS_DIR_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <dirent.h>

    void buildpath(char *path, char *out);
    int chdir(const char *path);

#ifdef __cplusplus
}
#endif
#endif