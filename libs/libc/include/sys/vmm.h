#ifndef _SYS_VMM_H
    #define _SYS_VMM_H

    #include<types.h>
    #include<stddef.h>

    typedef struct 
    {
       void *addr;
       size_t length;
       int prot;
       int flags;
       int fd;
       offset_t off;
    }mmap_args_t;


    typedef struct 
    {
        uint64_t mst_total; //pymem total
        uint64_t mst_free;  //pymem free
        uint64_t mst_used;  //pymem used 
    }mstatus_t;
    
    void *heap(void *heap);
    int mstate(mstatus_t *mst);
#endif