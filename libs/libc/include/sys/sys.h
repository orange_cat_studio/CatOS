#ifndef __SYS_SYS_H
#define __SYS_SYS_H

#include <types.h>

#define SYS_VER_LEN 64

int login(const char *name, char *password);
int logout(const char *name);
int account_register(const char *name, char *password);
int account_unregister(const char *name);
int account_name(char *buff, size_t len);
int account_verity(char *password);
int account_chpwd(const char *name, const char *old_password, const char *new_password);
int group_create(const char *name, int attr);
int group_remove(const char *name);
int group_adduser(const char *name, const char *user);
int group_deluser(const char *name, const char *user);
int get_fireware(const char *tablename, uint32_t namelen, uint64_t *table_len);

#endif