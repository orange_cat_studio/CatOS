// file: lib/type.h
// autor: jiang xinpeng
// time:2021.1.17
// copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef __LIBC_TYPE_H
#define __LIBC_TYPE_H

#include <stdint.h>
#include <stdbool.h>
#include <arch/const.h>

// os data type define
typedef long pid_t;
typedef unsigned int clock_t;
typedef unsigned int address_t;
typedef unsigned int cpuid_t;
typedef unsigned int flags_t;
typedef unsigned int time_t;
typedef uint32_t offset_t;
typedef uint64_t dir_t;
typedef void *buffer_t;
typedef uint64_t clock_type_t;
typedef uint32_t size_t;
typedef uint32_t mode_t;
typedef uint64_t inode_t;
typedef uint64_t nlink_t;
typedef uint64_t dev_t;
typedef uint64_t uid_t;
typedef uint64_t gid_t;
typedef uint64_t blkcnt_t;
typedef uint64_t blksize_t;
typedef uint32_t lba_t;
typedef uint64_t kobjid_t; // kernel object id
typedef uint64_t pthread_t;

typedef uint32_t ptrdiff_t;

// os specific type define
typedef char CHAR;
typedef uint8_t BYTE;
typedef uint16_t WCHAR;
typedef uint16_t WORD;
typedef uint32_t DWORD;
typedef WCHAR TCHAR;

typedef int8_t s8_t;
typedef uint8_t u8_t;
typedef int16_t s16_t;
typedef uint16_t u16_t;
typedef int s32_t;
typedef uint32_t u32_t;
typedef int64_t s64_t;
typedef uint64_t u64_t;

#if WORDSZ == 2
typedef int32_t intptr_t;
typedef uint32_t uintptr_t;
#else
typedef int64_t intptr_t;
typedef uint64_t uintptr_t;
#endif

typedef int64_t intmax_t;
typedef uint64_t uintmax_t;

// bool value
#define TRUE true
#define FALSE false

#define PRIVATE static
#define PUBLIC extern

#define DIRENT_RDONLY 0x01
#define DIRENT_HIDDEN 0x02
#define DIRENT_SYSTEM 0x04
#define DIRENT_DIR 0x08
#define DIRENT_ARCHIVE 0x10
#define DIRENT_BLOCK 0x20
#define DIRENT_CHAR 0x40

#define DIR_NAME_LEN 256

#endif