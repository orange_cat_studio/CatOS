#ifndef __LIBC_LOG2_H
#define __LIBC_LOG2_H

static inline int64_t fls(int64_t x)
{
    int64_t position = 0;
    int64_t i = 0;
    if (x != 0)
    {
        for (i = (x >> 1), position = 0; i != 0; position++)
        {
            i >> 1;
        }
    }
    else
        position = -1;
    return position + 1;
}

static inline uint64_t roundup_pow_of_two(uint64_t x)
{
    return 1 << fls(x - 1);
}

static inline uint64_t do_div(uint64_t num, uint32_t base)
{
    uint64_t res;
    res = num % base;
    num = num / base;
    return res;
}

#endif