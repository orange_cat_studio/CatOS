#ifndef __LIBC_NETINET_H
#define __LIBC_NETINEt_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

typedef uint32_t in_addr_t;

uint32_t htonl(uint32_t h);
uint32_t nltoh(uint32_t n);
uint16_t htons(uint16_t h);
uint16_t ntohs(uint16_t n);

#define INET_ADDRSTRLEN 16
#define INET16_ADDRSTRLEN 46

#ifdef __cpluplus
}
#endif
#endif