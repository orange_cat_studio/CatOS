#ifndef __LIBC_UNISTD_H
    #define __LIBC_UNISTD_H

    #ifdef __cpluplus
    extern "C"
    {
    #endif

    #include <types.h>
    #include <stddef.h>
    #include <fcntl.h>
    #include <sys/status.h>
    #include <sys/dir.h>
    #include <sys/types.h>

    typedef struct mount
    {
        char devpath[MAX_PATH_LEN];
        char abspath[MAX_PATH_LEN];
        char path[MAX_PATH_LEN];
        int driver;
    }mount_t;

    extern char **__environ;

    #define M_IREAD (1 << 2)  //file readable
    #define M_IWRITE (1 << 1) //file writeable
    #define M_IEXEC (1 << 0)  //file execvalbe

    #ifndef SEEK_SET
    //seek
    #define SEEK_SET 0
    #define SEEK_CUR 1
    #define SEEK_END 2
    #endif

    //file access
    #define R_OK 0
    #define W_OK 1
    #define X_OK 2
    #define F_OK 3

    #define STDIN_FILENO 0  //standard input fileno
    #define STDOUT_FILENO 1 //standard ouput fileno
    #define STDERR_FILENO 2 //standard error fileno

    int mkfifo(const char *name, mode_t mode);
    int probedev(const char *name, char *buff, size_t len);

    int setpgid(pid_t pid, pid_t pgid);
    int setpgrp(void);
    int getpgrp();
    int getpgid(pid_t pid);
    int getuid();
    int setuid(int uid);
    int setgid(gid_t gid);
    gid_t getgid();

    char *ttyname(int desc);
    char *ttyname(int desc);
    int gethostname(char *name, size_t len);

    int execve(const char *path, const char *argv[], const char *envp[]);
    int execv(const char *path, const char *argv[]);
    int execle(const char *path, const char *envp[], const char *arg, ...);
    int execl(const char *path, const char *arg, ...);
    int execvp(const char *file, const char *argv[]);
    int execlp(const char *file, const char *arg, ...);

    int istty(int desc);

    int openclass(const char *cname, int flags);
    int chmod(char *path, mode_t mode);
    int stat(const char *path, status_t *buff);
    int64_t sysconfig(int name);

    DIR *opendir(const char *path);
    int open(const char *path, int flags, ...);
    int close(int fd);
    int read(int fd, void *buff, size_t bytes);
    int write(int fd, void *buff, size_t bytes);
    int ioctl(int fd, int cmd, void *arg);
    int fcntl(int fd, int cmd, ...);
    int lseek(int fd, offset_t off, int whence);
    int access(const char *path, mode_t mode);
    int unlink(const char *path);
    int ftruncate(int fd, offset_t off);
    int fsync(int fd, int mode);
    int fchmode(int fd, mode_t mode);
    int64_t tell(int fd);
    int fstat(int fd, status_t *buff);
    int dup(int fd);
    int dup2(int oldfd, int newfd);
    int pipe(int fd[2]);
    int probedev(const char *name, char *buff, size_t len);
    int mkfifo(const char *name, mode_t mode);
    int _rename(const char *source, const char *targe);
    dirent_t *readdir(DIR *dir);
    int rewinddir(DIR *dir);
    int closedir(DIR *dir);
    void make_abs_path(char *path, char *abs);

    int mount(const char *source, const char *targe, const char *fstype, uint64_t flags);
    int unmount(const char *source, uint64_t flags);
    int mount_info(mount_t *mt,uint32_t count);
    int mkfs(const char *source, const char *fstype, uint64_t flags);
    int mkdir(const char *path, mode_t mode);
    int rmdir(const char *path);

    int brk(void *addr);
    void *sbrk(int inc);

    char *getcwd(char *buff, int size);
    char *realpath(const char *path, char *resolved_path);

    #ifdef __cplusplus
    }
    #endif
#endif