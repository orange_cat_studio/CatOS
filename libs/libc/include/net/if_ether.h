#ifndef __NET_IF_ETHER_H
#define __NET_IF_ETHER_H

#ifdef __cplusplus
extern "C" {
#endif 

#define ETH_ALEN 6 //ethernet mac address length 
#define ETH_HLEN 16 //ethernet header length
#define ETH_ZLEN 60 //ethernet frame minimum length 
#define ETH_DATA_LEN 1500 //ethernet frame maximum payload
#define ETH_FRAME_LEN 1514 //ethernet frame maximum length 

typedef struct ethhdr ethhdr_t;

struct ethhdr
{
    uint8_t h_dest[ETH_ALEN]; //destination MAC address
    uint8_t h_source[ETH_ALEN]; //source MAC address
    uint16_t h_proto; //network layer protocol
}__attribute__((packed));

#ifdef __cplusplus
}
#endif 

#endif 