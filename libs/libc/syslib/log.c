#include<sys/syscall.h>
#include<sys/log.h>
#include<errno.h>

int log_write(int type, char* title, char* context, int err, int event_type)
{
    return syscall5(int, SYS_LOGWRITE, type, title, context, err, event_type);
}

int log_read(int type, log_t* buff)
{
    return syscall2(int, SYS_LOGREAD, type, buff);
}

int log_del(int type, int title)
{
    return syscall2(int, SYS_LOGDEL, type, title);
}