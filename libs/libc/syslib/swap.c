#include<sys/syscall.h>
#include<sys/swap.h>


int SwapMem(swap_info_t *swap_info)
{
    return syscall1(int,SYS_SWAPMEM,swap_info);
}