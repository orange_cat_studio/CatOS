#include <sys/udev.h>
#include <sys/syscall.h>

int scandev(devent_t *de, device_type_t type, devent_t *out)
{
    return syscall3(int, SYS_SCANDEV, (uintptr_t)de, type,(uintptr_t) out);
}

int fastio(int fd, int cmd, void *arg)
{
    return syscall3(int, SYS_FASTIO, fd, cmd,(uintptr_t) arg);
}

int fastread(int fd, void *buff, size_t size)
{
    return syscall3(int, SYS_FASTREAD, fd, (uintptr_t)buff, size);
}

int fastwrite(int fd, void *buff, size_t size)
{
    return syscall3(int, SYS_FASTWRITE, fd, (uintptr_t)buff, size);
}
