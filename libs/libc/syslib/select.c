#include <sys/select.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <errno.h>

int select(int maxfd, fd_set_t *readfds, fd_set_t *writefds, fd_set_t *exceptfds, timeval_t *timeout)
{
    int ret = syscall5(int, SYS_SELECT, maxfd, (uintptr_t)readfds, (uintptr_t)writefds, (uintptr_t)exceptfds, (uintptr_t)timeout);
    if (ret < 0)
    {
        _set_errno(ret);
        ret = -1;
    }
    return ret;
}