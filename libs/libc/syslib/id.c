#include <unistd.h>
#include <sys/syscall.h>
#include <errno.h>

int getuid()
{
    int ret = syscall0(int, SYS_GETUID);
    if (ret < 0)
    {
        _set_errno(-ret);
        return -1;
    }
    return ret;
}

int setuid(int uid)
{
    int ret=syscall1(int,SYS_SETUID,uid);
    if(ret<0)
    {
        _set_errno(-ret);
        return -1;
    }
    return ret;
}

int setgid(gid_t gid)
{
    int ret=syscall1(int,SYS_SETGID,gid);
    if(ret<0)
    {
        _set_errno(-ret);
        return -1;
    }
    return ret;
}

gid_t getgid()
{
    int ret=syscall0(int,SYS_GETGID);
    if(ret<0)
    {
        _set_errno(-ret);
        return -1;
    }
    return ret;
}

int setpgid(pid_t pid, pid_t pgid)
{
    int ret = syscall2(int, SYS_SETPGID, pid, pgid);
    if (ret < 0)
    {
        _set_errno(-ret);
        return -1;
    }
    return ret;
}

int setpgrp(void)
{
    return setpgid(0, 0);
}

int getpgid(pid_t pid)
{
    pid_t ret = syscall1(pid_t, SYS_GETPGID, pid);
    if (ret < 0)
    {
        _set_errno(-ret);
        return -1;
    }
    return ret;
}

int getpgrp()
{
    return getpgid(0);
}
