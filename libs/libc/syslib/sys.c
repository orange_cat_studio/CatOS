#include <sys/syscall.h>
#include <sys/sys.h>
#include <stddef.h>

int login(const char *name, char *password)
{
    return syscall2(int, SYS_LOGIN, (uintptr_t)name, (uintptr_t)password);
}

int logout(const char *name)
{
    return syscall1(int, SYS_LOGOUT, (uintptr_t)name);
}

int account_register(const char *name, char *password)
{
    return syscall2(int, SYS_USERREGISTER, (uintptr_t)name, (uintptr_t)password);
}

int account_unregister(const char *name)
{
    return syscall1(int, SYS_USERUNREGISTER, (uintptr_t)name);
}

int account_name(char *buff, size_t len)
{
    return syscall2(int, SYS_USERNAME, (uintptr_t)buff, len);
}

int account_verity(char *password)
{
    return syscall1(int, SYS_USERVERITFY, (uintptr_t)password);
}

int account_chpwd(const char *name, const char *old_password, const char *new_password)
{
    return syscall3(int, SYS_USERCHPWD, (uintptr_t)name, (uintptr_t)old_password, (uintptr_t)new_password);
}

int group_create(const char *name, int attr)
{
    return syscall2(int, SYS_GROUPCREATE, (uintptr_t)name, attr);
}

int group_remove(const char *name)
{
    return syscall1(int, SYS_GROUPREMOVE, (uintptr_t)name);
}

int group_adduser(const char *name, const char *user)
{
    return syscall2(int, SYS_GROUPADDUSER, name, user);
}

int group_deluser(const char *name, const char *user)
{
    return syscall2(int, SYS_GROUPDELUSER, name, user);
}

int get_fireware(const char *tablename, uint32_t namelen, uint64_t *table_len)
{
    return syscall3(int, SYS_GETFIREWORK, tablename, namelen, table_len);
}