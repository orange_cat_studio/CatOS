#include <sys/times.h>
#include <sys/syscall.h>

clock_t times(tms_t *buff)
{
    return syscall1(clock_t, SYS_TIMES, (uintptr_t)buff);
}