#include <sys/syscall.h>
#include <sys/ipc.h>

int shmget(char *name, uint64_t size, uint64_t flags)
{
    return syscall3(int, SYS_SHAREMEMGET, (uintptr_t)name, size, flags);
}

int shmput(int shmid)
{
    return syscall1(int, SYS_SHAREMEMPUT, shmid);
}

void *shmmap(int shmid, void *shmaddr, int shmflag)
{
    return syscall3(void *, SYS_SHAREMEMMAP, shmid, (uintptr_t)shmaddr, shmflag);
}

int shmunmap(void *shmaddr, int shmflag)
{
    return syscall2(int, SYS_SHAREMEMUNMAP, (uintptr_t)shmaddr, shmflag);
}

int semget(char *name, int value, int semflag)
{
    return syscall3(int, SYS_SEMGET, (uintptr_t)name, value, semflag);
}

int semput(int semid)
{
    return syscall1(int, SYS_SEMDOWN, semid);
}

int semup(int semid)
{
    return syscall1(int, SYS_SEMUP, semid);
}

int semdown(int semid, int semflag)
{
    return syscall2(int, SYS_SEMDOWN, semid, semflag);
}

int msgget(char *name,unsigned long flags)
{
    return syscall2(int,SYS_MSGQUEUEGET,name,flags);
}

int msgput(int msgid)
{
    return syscall1(int,SYS_MSGQUEUEPUT,msgid);
}

int msgsend(int msgid,void *msgbuf,size_t size,int msgflags)
{
    return syscall4(int,SYS_MSGQUEUESEND,msgid,msgbuf,size,msgflags);
}

int msgrecv(int msgid,void *msgbuf,size_t size,int msgflags)
{
    return syscall4(int,SYS_MSGQUEUERECV,msgid,msgbuf,size,msgflags);
}