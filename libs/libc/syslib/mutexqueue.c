#include <sys/syscall.h>
#include <sys/mutexqueue.h>

int mutex_queue_alloc()
{
    return syscall0(int, SYS_MUTEX_QUEUE_CREATE);
}

int mutex_queue_free(int handle)
{
    return syscall1(int, SYS_MUTEX_QUEUE_DESTORY, handle);
}

int mutex_queue_wait(int handle, void *addr, uint32_t wqfalgs, uint32_t value)
{
    return syscall4(int, SYS_MUTEX_QUEUE_WATI, handle, (uintptr_t)addr, wqfalgs, value);
}

int mutex_queue_wake(int handle, void *addr, uint32_t wqflags, uint32_t value)
{
    return syscall4(int, SYS_MUTEX_QUEUE_WAKE, handle, (uintptr_t)addr, wqflags, value);
}
