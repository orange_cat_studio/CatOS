[bits 32]
[section .text]

%define SYSCALL_INT 0x40

global __syscall0
__syscall0:
    mov eax,[esp+4] ;sycall num
    int SYSCALL_INT
    ret

global __syscall1
__syscall1:
    push ebx
    mov eax,[esp+4+4]   ;syscall num
    mov ebx,[esp+4+8]   ;args0
    int SYSCALL_INT
    pop ebx
    ret

global __syscall2
__syscall2:
    push ecx
    push ebx

    mov eax,[esp+8+4]   ;syscall num
    mov ebx,[esp+8+8]   ;args0
    mov ecx,[esp+8+12]  ;args1
    int SYSCALL_INT

    pop ebx
    pop ecx
    ret

global __syscall3
__syscall3:
    push edx
    push ecx
    push ebx
    
    mov eax,[esp+12+4]  ;syscall num
    mov ebx,[esp+12+8]  ;args0
    mov ecx,[esp+12+12] ;args1
    mov edx,[esp+12+16] ;args2
    int SYSCALL_INT

    pop ebx
    pop ecx
    pop edx
    ret

global __syscall4
__syscall4:
    push esi
    push edx
    push ecx
    push ebx

    mov eax,[esp+16+4]  ;syscall num
    mov ebx,[esp+16+8]  ;args0
    mov ecx,[esp+16+12] ;args1
    mov edx,[esp+16+16] ;args2
    mov esi,[esp+16+20] ;args3
    int SYSCALL_INT

    pop ebx
    pop ecx
    pop edx
    pop esi
    ret

global __syscall5
__syscall5:
    push edi
    push esi
    push edx
    push ecx
    push ebx

    mov eax,[esp+20+4]  ;syscall num
    mov ebx,[esp+20+8]  ;args0
    mov ecx,[esp+20+12] ;args1
    mov edx,[esp+20+16] ;args2
    mov esi,[esp+20+20] ;args3
    mov edi,[esp+20+24] ;args4  
    int SYSCALL_INT

    pop ebx
    pop ecx
    pop edx
    pop esi
    pop edi
    ret