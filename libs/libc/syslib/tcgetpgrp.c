#include<unistd.h>
#include<errno.h>
#include<termios.h>
#include<sys/syscall.h>

pid_t tcgetpgrp(int fd)
{
    pid_t pid=-1;
    if(ioctl(fd,TTYIO_GETPGROUP,&(pid))<0)
        return -1;
    return pid;
}

