#include <sys/syscall.h>
#include <sys/proc.h>

void _exit(int status)
{
    syscall1(int, SYS_EXIT, status);
}

pid_t fork()
{
    return syscall0(pid_t, SYS_FORK);
}

int wait(int *status)
{
    return syscall3(int, SYS_WAITPID, -1, (uintptr_t)status, 0);
}

int waitpid(pid_t pid, int *status, int options)
{
    return syscall3(int, SYS_WAITPID, pid, (uintptr_t)status, options);
}

pid_t getpid()
{
    return syscall0(pid_t, SYS_GETPID);
}

pid_t getppid()
{
    return syscall0(pid_t, SYS_GETPPID);
}

pid_t gettid()
{
    return syscall0(pid_t, SYS_GETTID);
}

uint64_t sleep(uint64_t second)
{
    return syscall1(int, SYS_SLEEP, second);
}

void sched_yeild()
{
    return syscall0(void, SYS_SCHED_YIELD);
}

int tstatus(tstatus_t *ts, int *idx)
{
    return syscall2(int, SYS_TSTATUS, (uintptr_t)ts, (uintptr_t)idx);
}

int getver(char *buff, int len)
{
    if (!buff || len < 1)
        return -1;
    return syscall2(int, SYS_GETVER, (uintptr_t)buff, len);
}

int create_process(char *const argv[], char *const envp[], uint32_t flags)
{
    return syscall3(int, SYS_CREATEPROCESS, (uintptr_t)argv, (uintptr_t)envp, flags);
}

int resume_process(pid_t pid)
{
    return syscall1(int, SYS_RESUMEPROCESS, pid);
}
