#include <sys/syscall.h>
#include <sys/vmm.h>

/** 
 * @function: heap()
 * @heap:   heap value
 * @info:   if heap=0,retun current heap. 
 *          if heap>old,expand heap up.
 *          if head<old,shrink heap down]
 *          if heap=old,do nothing.
 * @return：return new heap value
 * 
*/
void *heap(void *heap)
{
    return syscall1(void *, SYS_HEAP, (uintptr_t)heap);
}

void *mmap(void *addr, size_t length, int prot, int flags, int fd, offset_t off)
{
    mmap_args_t args = {addr, length, prot, flags, fd, off};
    return syscall1(void *, SYS_MMAP, (uintptr_t)&args);
}

void *cmap(int fd, size_t len, int flags)
{
    return mmap(NULL, len, 0, flags, fd, 0);
}

int munmap(void *addr, size_t len)
{
    return syscall2(int, SYS_MUNMAP, (uintptr_t)addr, len);
}

int cmunmap(void *addr, size_t len)
{
    return munmap(addr, len);
}

int mstate(mstatus_t *mst)
{
    return syscall1(int, SYS_MSTATUS, (uintptr_t)mst);
}
