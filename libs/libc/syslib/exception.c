#include <sys/syscall.h>
#include <sys/exception.h>
#include <sys/proc.h>
#include <stddef.h>
#include <errno.h>

int excsend(pid_t pid, uint32_t code)
{
    return syscall2(int, SYS_EXCSEND, pid, code);
}

int excraise(uint32_t code)
{
    return excsend(getpid(), code);
}

int excmask(uint32_t mask)
{
    if (!mask)
        return -1;
    return syscall1(int, SYS_EXCMASK, mask);
}

int exccatch(uint32_t code, exception_handler_t handler)
{
    return syscall2(int, SYS_EXCCATCH, code, (uint32_t)handler);
}

int excblock(uint32_t code)
{
    return __excblock(code, 1);
}

int excunblock(uint32_t code)
{
    return __excblock(code, 0);
}

void *exchandler(uint32_t code)
{
    return syscall1(void *, SYS_EXCHANDLER, code);
}

static int __excblock(uint32_t code, uint32_t status)
{
    return syscall2(int, SYS_EXCBLOCK, code, status);
}
