#include<unistd.h>
#include<errno.h>
#include<sys/syscall.h>
#include<termios.h>

//set current process pgid to current process pid
int tcsetpgrp(int fd,pid_t pgid)
{
    return ioctl((fd),TTYIO_SETPGROUP,&(pgid));
}

