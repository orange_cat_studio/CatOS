#include <sys/disk.h>
#include <sys/syscall.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int disk_info_get(char *name, disk_t *buff)
{
    return syscall2(int, SYS_GETDISK, name, buff);
}
