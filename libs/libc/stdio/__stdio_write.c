#include <malloc.h>
#include <stdio.h>
#include <sys/syscall.h>

static size_t __unbuffered_write(FILE *f, const uint8_t *buff, size_t size)
{
    uint8_t *p = (uint8_t *)buff;
    size_t cnt = 0;
    size_t bytes = 0;

    while (size > 0)
    {
        bytes = f->write(f, p, size);
        if (bytes < 0)
        {
            f->err = 1;
            break;
        }
        size -= bytes;
        cnt += bytes;
        f->pos += bytes;
        p += bytes;
    }
    return cnt;
}

size_t __stdio_write(FILE *f, const uint8_t *buff, size_t size)
{
    uint8_t *p = (uint8_t *)buff;
    size_t cnt = 0;
    size_t bytes = 0;
    size_t buffsz = 0;
    int i = 0, ret = 0;

    if (!f->write)
        return -EINVAL;

    switch (f->mode)
    {
    case _IONBF:
    {
        bytes = __unbuffered_write(f, buff, size);
        f->pos += bytes;
        return bytes;
    }
    case _IOLBF:
    {
        for (i = size; i > 0; i--)
        {
            if (p[i - 1] == '\n')
            {
                ret = __stdio_write_flush(f);
                if (ret)
                    return cnt;

                bytes = __unbuffered_write(f, p, i);
                if (bytes <= 0)
                    return cnt;

                size -= bytes;
                cnt += bytes;
                f->pos += bytes;
                p += bytes;

                break;
            }
        }

        if (size > 0)
        {
            buffsz = f->fifo_write->size;

            if (fifo_len(f->fifo_write) + size > buffsz)
            {
                ret = __stdio_write_flush(f);
                if (ret)
                    return cnt;

                // write file
                while (size > buffsz)
                {
                    bytes = f->write(f, p, size);
                    if (bytes < 0)
                    {
                        f->err = 1;
                        return cnt;
                    }
                    size -= bytes;
                    cnt += bytes;
                    f->pos += bytes;
                    p += bytes;
                }
            }
            // write to fifo
            bytes = fifo_put(f->fifo_write, p, size);
            size -= bytes;
            cnt += bytes;
            f->pos += bytes;
            p += bytes;

            f->rwflush = &__stdio_write_flush;
        }
        return cnt;
    }
    case _IOFBF:
    {
        buffsz = f->fifo_write->size;

        if (fifo_len(f->fifo_write) + size > buffsz)
        {
            ret = __stdio_write_flush(f); // fifo write
            if (ret)
                return cnt;

            // write file
            while (size > buffsz)
            {
                bytes = f->write(f, p, size);
                if (bytes < 0)
                {
                    f->err = 1;
                    return cnt;
                }
                size -= bytes;
                cnt += bytes;
                f->pos += bytes;
                p += bytes;
            }
        }
        // write to fifo
        bytes = fifo_put(f->fifo_write, p, size);
        size -= bytes;
        cnt += bytes;
        f->pos += bytes;
        p += bytes;

        f->rwflush = &__stdio_write_flush;
        return cnt;
    }
    default:
        break;
    }
}