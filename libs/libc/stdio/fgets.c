#include <stddef.h>
#include <stdio.h>

char *fgets(char *s, int n, FILE *f)
{
    uint8_t *p = s;
    uint8_t *ret = s;
    size_t res = 0;

    while (n--)
    {
        res = __stdio_read(f, p, 1);
        if (!res)
            break;
        else
        {
            if (res < 0)
                return NULL;
        }

        // deal with '\b'
        if (*p == '\b')
        {
            *p = '\0';
            if (p > (uint8_t *)s)
            {
                --p;
                *p = '\0';
            }
        }
        else
        {
            if (*p == '\n') // press enter end input
                break;
            else 
                p++; // next char
        }
    }
    *p = 0;
    return ret;
}
