#include <stdio.h>

int putchar(const char ch)
{
    fputc(ch, __stdio_get_stdout());
}