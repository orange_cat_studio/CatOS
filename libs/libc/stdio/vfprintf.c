#include <stdarg.h>
#include <sizes.h>
#include <stdio.h>

int vfprintf(FILE *f, const char *fmt, va_list arg)
{
    char buff[SZ_4K];
    int ret = vsnprintf(buff, SZ_4K, fmt, arg);
    ret = fputs(buff, f) < 0 ? 0 : ret;
    return ret;
}