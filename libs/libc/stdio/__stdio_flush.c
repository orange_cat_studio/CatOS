// file:libc/stdio/__stdio_flush.c
// autor:jiangxinpeng
// time:2021.8.14
// copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#include <stddef.h>
#include <stdio.h>

int __stdio_no_flush(FILE *f)
{
    return 0;
}

int __stdio_read_flush(FILE *f)
{
    f->seek(f, f->pos, SEEK_SET);
    fifo_reset(f->fifo_read);

    f->rwflush = &__stdio_no_flush;
    return 0;
}

int __stdio_write_flush(FILE *f)
{
    uint8_t *p;
    size_t size;
    size_t ret;

    p = f->buff;
    size = fifo_get(f->fifo_write, p, f->buffsize);

    while (size > 0)
    {
        ret = f->write(f, p, size);
        if (ret <= 0)
        {
            f->err = 1;
            fifo_put(f->fifo_write, p, size);
            return -1;
        }
        size -= ret;
        p += ret;
    }

    f->rwflush = &__stdio_no_flush;
    return 0;
}
