//file:libc/stdio/fprintf.c
//autor:jiangxinpeng
//time:2021.8.14
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#include <stdarg.h>
#include <sizes.h>
#include <stdio.h>

int fprintf(FILE *f, const char *fmt, ...)
{
    va_list args;
    char buff[SZ_4K];
    int rv;

    va_start(args,fmt);
    rv=vsnprintf(buff,SZ_4K,fmt,args);
    va_end(args);

    rv=fputs(buff,f);
    return rv;
}





