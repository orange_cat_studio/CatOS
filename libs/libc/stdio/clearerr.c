#include <stdio.h>

void clearerr(FILE * f)
{
	f->err = 0;
	f->eof = 0;
}
