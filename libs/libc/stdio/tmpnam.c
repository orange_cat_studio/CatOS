#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/status.h>

char *tmpnam(char *buff)
{
    static char internal[L_tmpnam];
    status_t st;
    char path[MAX_PATH_LEN+1];
    memset(path,0,MAX_PATH_LEN);

    do
    {
        sprintf(path, "%s/tmp%d", "/tmp", rand());
    } while (stat(path, &st) < 0);

    return strcpy(buff ? buff : internal, path);
}
