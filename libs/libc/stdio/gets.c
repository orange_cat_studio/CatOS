#include <stdio.h>

int gets(char *str)
{
    char *p = str;
    while ((*p = fgetc(stdin)) != '\n')
    {
        if (*p == '\b')
        {
            *p = '\0';
            if (p > (uint8_t *)str)
            {
                --p;
                *p = '\0';
            }
        }
        else
            p++;
    }
    *p = '\0';
}