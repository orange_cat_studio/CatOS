#include <stdio.h>
#include <stdarg.h>

int fputs(const char *s, FILE *f)
{
    return ((__stdio_write(f, s, strlen(s)) <= 0) ? EOF : 0);
}
