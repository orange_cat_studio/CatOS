#include<stdio.h>

int snprintf(char *buff,size_t n,const char *fmt,...)
{
    va_list ap;
    int rv;

    va_start(ap,fmt);
    rv=vsnprintf(buff,n,fmt,ap);
    va_end(ap);
    
    return rv;
}