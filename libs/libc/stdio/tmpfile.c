#include <stdio.h>
#include <stdlib.h>
#include <sys/status.h>

FILE *tmpfile()
{
    status_t st;
    char path[MAX_PATH_LEN+1];

    do
    {
       sprintf(path,"%s/tmpfile_%d","/tmp",rand());
    } while (stat(path,&st)<0);

    return fopen(path,"wb+");
}




