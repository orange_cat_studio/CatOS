#include<stdio.h>

int sprintf(char *buff,const char *fmt,...)
{
    va_list ap;
    int rv;

    va_start(ap,fmt);
    rv=vsnprintf(buff,~(size_t)0,fmt,ap);
    va_end(ap);
    
    return rv;
}