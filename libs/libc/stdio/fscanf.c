#include<stdio.h>

int fscanf(FILE *f,const uint8_t *fmt,...)
{
    va_list args;
    uint8_t *buff;
    int rv;

    buff=(uint8_t*)malloc(SZ_4K);
    if(!buff)
        return 0;

    memset(buff,0,SZ_4K);
    fread(buff,1,SZ_4K,f);

    va_start(args,fmt);
    rv=vsscanf(buff,fmt,args);
    va_end(args);

    free(buff);
    return rv;
}

