#include <stdarg.h>
#include <sizes.h>
#include <stdio.h>
#include <stdlib.h>

int vasprintf(char **s, const char *fmt, va_list ap)
{
    va_list ap_;
    int i;

    va_copy(ap_, ap);
    i = vsnprintf(0, 0, fmt, ap_);
    va_end(ap_);

    if ((i < 0) || (*s = (char *)malloc(i + 1))) //malloc buff
        return -1;
    return vsnprintf(*s, i + 1, fmt, ap);
}
