#include <stdio.h>
#include <sizes.h>
#include <string.h>

int scanf(const char *fmt, ...)
{
    va_list args;
    char *buff;
    int ret;

    buff = malloc(SZ_4K);
    if (!buff)
        return 0;

    memset(buff, 0, SZ_4K);

    char *p = buff;
    char ch;

    do
    {
        ch = fread(p++, 1, 1, stdin);
        if (ch < 0)
            break;
    } while (ch != '\n');
    *p = 0;

    va_start(args, fmt);
    ret = vsscanf(buff, fmt, args);
    va_end(args);

    free(buff);
    return ret;
}
