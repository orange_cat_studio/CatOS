#include<stdio.h>

size_t fwrite(const void *buff,size_t size,size_t count,FILE *f)
{
    const uint8_t *p=buff;
    size_t i;

    for(i=0;i<count;i++)    //write block 
    {
        if(__stdio_write(f,p,size)!=size)
            break;
        p+=size;
    }
    return i;
}

