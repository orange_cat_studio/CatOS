#include <stdarg.h>
#include <sizes.h>
#include <stdio.h>

int printf(const char *fmt, ...)
{
    va_list args = NULL;
    char buff[SZ_1K] = {0};
    int ret = -1;

    va_start(args, fmt);
    ret = vsnprintf(buff, SZ_1K, fmt, args);
    va_end(args);

    ret = fputs(buff, stdout) < 0 ? 0 : ret;
    fflush(stdout);
    return ret;
}
