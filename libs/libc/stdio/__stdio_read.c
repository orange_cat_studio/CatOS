#include <types.h>
#include <errno.h>
#include <stdio.h>

static size_t __unbuffered_read(FILE *f, uint8_t *buff, size_t size)
{
    uint8_t *p = buff;
    size_t cnt = 0;
    size_t bytes = 0;

    while (size > 0)
    {
        bytes = f->read(f, p, size);
        if (bytes <= 0)
        {
            if (bytes == 0)
                f->eof = 1;
            else
                f->err = 1;
            break;
        }
        size -= bytes;
        cnt += bytes;
        p += bytes;
    }
    return cnt;
}

size_t __stdio_read(FILE *f, uint8_t *buff, size_t size)
{
    uint8_t *p = buff;
    size_t cnt = 0;
    size_t bytes = 0;
    size_t div, rem, tmp;

    if (!f->read)
        return -EINVAL;

    switch (f->mode) // swith io mode
    {
    case _IONBF:
    case _IOLBF:
        bytes = __unbuffered_read(f, buff, size);
        f->pos += bytes;
        return bytes;
    case _IOFBF:
    {
        // read from fifo buff
        bytes = fifo_get(f->fifo_read, p, size);
        size -= bytes;
        cnt += bytes;
        p += bytes;
        f->pos += bytes;

        // read from file
        if (size > 0)
        {
            f->rwflush = &__stdio_no_flush;

            div = (size / f->buffsize);
            rem = (size % f->buffsize);

            if (div > 0) // read block bytes
            {
                bytes = __unbuffered_read(f, p, div * f->buffsize);
                size -= bytes;
                cnt += bytes;
                f->pos += bytes;
                p += bytes;

                if (bytes < 0)
                    break;
            }

            if (rem > 0) // read less bytes
            {
                bytes = __unbuffered_read(f, f->buff, rem);

                if (bytes < rem)
                    tmp = bytes;
                else
                    tmp = rem;

                memcpy(p, f->buff, tmp);
                size -= tmp;
                cnt += tmp;
                f->pos += tmp;
                p += tmp;

                if (bytes >= tmp)
                {
                    if(bytes>tmp)
                    {
                        // write to fifo buff
                        fifo_put(f->fifo_read, f->buff + tmp, bytes - tmp);
                        f->rwflush = &__stdio_read_flush;
                        f->eof = 0;
                    }
                }
                else 
                {
                    f->eof=1;
                }
            }
        }
    }
    break;
    default:
        break;
    }

    return cnt;
}
