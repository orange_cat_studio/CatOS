#include <stdio.h>
#include<stdlib.h>

int fclose(FILE *f)
{
    int err;

    if (!f)
        return EINVAL;
    if (!f->close)
        return EINVAL;

    //flush sync
    if ((err = f->rwflush(f)))
        return err;

    if ((err = f->close(f)))
        return err;

    //free fifo
    if (f->fifo_read)
        fifo_free(f->fifo_read);
    if (f->fifo_write)
        fifo_free(f->fifo_write);

    //free buffer
    if (f->buff)
        free(f->buff);

    if (f)
        free(f);

    return err;
}


