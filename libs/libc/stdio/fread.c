#include <stdio.h>

size_t fread(void *buff, size_t size, size_t count, FILE *f)
{
    uint8_t *p = buff;
    size_t i;

    for (i = 0; i < count; i++)
    {
        if (__stdio_read(f, p, size) != size)
            break;
        p += size;
    }
    return i;
}