#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int remove(const char *path)
{
    return unlink(path);
}