// file:libc/ansi/exit.c
// autor:jiang xinpeng
// time:2021.8.12
// copyright:(C) 2020-2050 by jiang xinpeng,All right are reserved.

#include <exit.h>
#include <stddef.h>
#include <sys/proc.h>

int (*_clean)(void) = NULL;

extern void _exit_cleanup();

void exit(int status)
{
    if (_clean)
        _clean();
    _exit_cleanup();
    _exit(status);
}
