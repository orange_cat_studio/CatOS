//file: libs/libc/path/dirname.c
//autor: jiangxinpeng
//time: 2021.12.24 
//copyright: (C) 2020-2050 by jiangxinpeng,All right are reserved.


#include <string.h>
#include <path.h>

char *dirname(char *path)
{
    int i;
    if (!path || !path[0])
        return ".";

    i = strlen(path) - 1;

    for (; path[i] == '/'; i--)
    {
        if (!i) //check if all '/' before
            return "/";
    }

    for (; path[i] != '/'; i--)
    {
        if (!i) //check if first not is '/'
            return ".";
    }

    for (; path[i] == '/'; i--)
    {
        if (!i) //check if all is '/' before
            return "/";
    }

    //end of string and return
    path[i + 1] = 0;
    return path;
}



