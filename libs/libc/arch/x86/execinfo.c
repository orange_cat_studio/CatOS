#include<execinfo.h>
#include<types.h>

int backtrace(void **buffer,int size)
{
    int i=0;
    int n=0;
    uint32_t _ebp=0;
    uint32_t _eip=0;

    __asm__ __volatile__("movl %%ebp,%0" : "=g"(_ebp)::"memory");
    for(i=0;i<size && _ebp && *(uint32_t*)_ebp && *(uint32_t*)_ebp!=_ebp;i++)
    {
        _eip=(uint32_t)((uint32_t*)_ebp+1);
        _eip=*(uint32_t*)_eip;
        _ebp=*(uint32_t*)_ebp;
        buffer[i]=(void*)_eip;
        n++;
    }   
    return n;
}

int backtrace2(void **buffer,int size)
{
    int n=0;
    int *p=&n;
    int i;
    int ebp=p[5];
    int eip=p[5+1];
    for(i=0;i<size&&ebp&&*(uint32_t*)ebp&&*(uint32_t*)ebp!=ebp;i++)
    {
        buffer[i]=(void*)eip;
        p=(int*)ebp;
        ebp=p[0];
        eip=p[1];
        n++;
    }
    return n;
}