#include <errno.h>
#include <string.h>
#include <malloc.h>
#include <environ.h>
#include <sys/syscall.h>

int unsetenv(const char *name)
{
    //remove from system
    return syscall1(int, SYS_UNSETENV, name);
}