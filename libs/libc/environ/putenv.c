#include <string.h>
#include <stddef.h>
#include <malloc.h>
#include <environ.h>
#include <sys/syscall.h>

int __put_env(char *str, int overwrite)
{
    char *name = str;
    str = strchr(str, '=');
    if (str)
    {
        *str = '\0';
        str++;
    }
    char *val = str;

    return syscall3(int, SYS_PUSHENV, name, val, overwrite);
}

int putenv(const char *str, int overwrite)
{
    return __put_env(str, overwrite);
}
