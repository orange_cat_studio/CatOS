#include <environ.h>
#include <stdio.h>
#include <sys/syscall.h>

static char env_data[128]; //buffer used to get environ value 

char *getenv(const char *name)
{
    //get env from system
    return syscall3(char *, SYS_GETENV, name,env_data,128);
}