#include <malloc.h>
#include <environ.h>
#include <sys/syscall.h>

int clearenv()
{
    //clear env from system
    return syscall0(int, SYS_CLEARENV);
}
