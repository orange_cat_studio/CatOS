#include <string.h>
#include <malloc.h>
#include <environ.h>
#include <sys/syscall.h>

int setenv(const char *name, const char *val, int overwrite)
{
    return syscall3(int, SYS_SETENV, name, val, overwrite);
}
