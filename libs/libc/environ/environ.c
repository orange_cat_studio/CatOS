#include <stddef.h>
#include <environ.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/environ.h>
#include <sys/syscall.h>>

env_t *env(env_t *env, env_t *out)
{
    return syscall2(env_t *, SYS_ENV, env, out);
}