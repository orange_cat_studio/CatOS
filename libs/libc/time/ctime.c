#include <time.h>
#include <sys/time.h>

char *ctime(const char *t)
{
    return asctime((tm_t *)localtime((time_t *)t));
}
