#include <time.h>
#include <sys/time.h>
#include <sys/times.h>
#include <stddef.h>

tm_t *gmtime(const time_t *time)
{
    static tm_t tm;

    if (__secs_to_tm(*time, &tm) < 0)
        return NULL;

    tm.tm_isdst = 0;
    tm.__tm_gmtoff = 0;
    tm.__tm_zone = "UTC";
    return &tm;
}
