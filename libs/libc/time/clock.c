#include<time.h>
#include<sys/time.h>

clock_t clock()
{
    timeval_t tv;

    if(gettimeofday(&tv,0))
        return -1;
    return tv.tv_sec*1000000+tv.tv_usec;
}