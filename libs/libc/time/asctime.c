#include <stddef.h>
#include <stdio.h>
#include <time.h>

static const char *week_days[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
static const char *month_days[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

char *asctime(const tm_t *tm)
{
    static uint8_t ascbuff[26];

    if (!tm)
        return NULL;
    snprintf(ascbuff, 26, "%.3s %.3s%3d %.2d:%.2d:%.2d %d\n",
             week_days[tm->tm_wday], month_days[tm->tm_mday-1], tm->tm_mday,
             tm->tm_hour, tm->tm_min, tm->tm_sec, tm->tm_year);
    return ascbuff;
}


