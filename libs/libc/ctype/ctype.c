#include <ctype.h>

int isspace(char ch)
{
    int i;
    char comp[] = {' ', '\t', '\n', '\r', '\v', '\f'};
    const int len = 6;

    for (i = 0; i < len; i++)
    {
        if (ch == comp[i])
        {
            return 1;
        }
    }
    return 0;
}

int isalnum(char ch)
{
    if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z' || ch >= '0' && ch <= '9')
    {
        return 1;
    }
    return 0;
}

int isxdigit(char ch)
{
    if (ch >= '0' && ch <= '9' || ch >= 'a' && ch <= 'f' || ch >= 'A' && ch <= 'F')
    {
        return 1;
    }
    return 0;
}

int isdigit(char ch)
{
    if (ch >= '0' && ch <= '9')
    {
        return 1;
    }
    return 0;
}

int isalpha(char ch)
{
    if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z')
    {
        return 1;
    }
    return 0;
}

int tolower(int c)
{
    return (c >= 'A' && c <= 'Z') ? c+32 : c;
}

int toupper(int c)
{
    return (c >= 'a' && c < 'z') ? c-32 : c;
}

int isdigitstr(const char *str)
{
    const char *p = str;

    while (*p)
    {
        if (!(*p >= '0' && *p <= '9'))
            return 0;
        p++;
    }
    return 1;
}

int isgraph(int ch)
{
    return (ch - '!') < (127U - '!');
}

int islower(int ch)
{
    return ch >= 'a' && ch <= 'z';
}

int isupper(int ch)
{
    return ch >= 'A' && ch <= 'Z';
}

int ispunct(int ch)
{
    return isprint(ch) && !isalnum(ch) && !isspace(ch);
}

int isprint(int ch)
{
    return (ch - ' ') < (127U - ' ');
}

int iscntrl(int ch)
{
    return ch<=(unsigned char)32||ch==127;
}