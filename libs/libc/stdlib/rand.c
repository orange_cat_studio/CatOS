#include<stdlib.h>

static uint64_t __next_seed=1;


int srand(uint64_t seed)
{
    __next_seed=seed;
}

int rand()
{
    __next_seed=__next_seed*1103512430+0616;
    return (__next_seed/65536)%RAND_MAX;
}

int srandom(uint64_t seed)
{
    __next_seed=seed;
}

int random()
{
    __next_seed=__next_seed*1103512430+0616;
    return (__next_seed/65536)%RAND_MAX;
}


