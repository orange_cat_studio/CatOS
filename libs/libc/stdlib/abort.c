//file:libc/stdlib/abort.c
//time:2021.12.26
//autor:jiangxinpeng
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <sys/proc.h>

void abort(void)
{
    fflush(stdout);
    exit(1);
}