#include <errno.h>
#include <unistd.h>
#include <string.h>

errno_t volatile __errno = 0;

static char *__error_string[] = {
    "E2BIG",
    "EACCES",
    "EADDRINUSE",
    "EADDRNOTAVAI",
    "EFANOSUPPORT",
    "EAGAIN",
    "EFANOSUPPORT",
    "EALREADY",
    "EDOM",
    "EILSEQ",
    "ERANGE"
    "EBADF",
    "EBADMSG",
    "EBUSY",
    "ECANCELED",
    "ECHILD",
    "ECONNABORTED",
    "ECONNREFUSED",
    "ECONNRESET",
    "EDEADLK",
    "EDESTADDRREQ",
    "EEXIST",
    "EFAULT",
    "EFBIG",
    "EHOSTUNREACH",
    "EIDRM",
    "EINPROGRESS",
    "EINTR",
    "EINVAL",
    "EIO",
    "EISCONN",
    "EISDIR",
    "ELOOP",
    "EMFILE",
    "EMLINK",
    "EMSGSIZE",
    "EMULTIHOP",
    "ENAMETOOLONG",
    "ENETDOWN",
    "ENETRESET",
    "ENETUNREACH",
    "ENFILE",
    "ENOBUFS",
    "ENODATA",
    "ENODEV",
    "ENOFILE OR ENODIRECTOR",
    "ENOEXEC",
    "ENOLOCK",
    "ENOLINK"
    "ENOMEM",
    "ENOMSG",
    "ENOPROTOOPT",
    "ENOSPC",
    "ENOSR",
    "ENOSTR",
    "ENOSYS",
    "ENOTCONN",
    "ENOTDIR",
    "ENOTEMPTY",
    "ENOTSOCK",
    "ENOTSUP",
    "ENOTTY",
    "ENXIO",
    "EOPNOTSUPP",
    "EOVERFLOW",
    "EOWNERDIED",
    "EPERM",
    "EPIPE",
    "EPROTO",
    "EPROTONOSUPPORT",
    "EPROTOTYPE",
    "EROFS",
    "ESPIPE",
    "ESRCH",
    "ESTALE",
    "ESTREAMTIME",
    "ETIMEDOUT",
    "ETEXTBSY",
    "EWOULDBLOCK",
    "EXDEV"};

char *strerror(int errnum)
{
    if (errnum >= 0 && errnum <= EMAXNR)
    {
        return (char *)__error_string[errnum];
    }
    return (char *)__error_string[0];
}

extern int *_errno(void)
{
    return (int *)&__errno;
}

errno_t _set_errno(int value)
{
    __errno = value;
    return __errno;
}

errno_t _get_errno(int *value)
{
    if (value)
        *value = __errno;
    return __errno;
}
