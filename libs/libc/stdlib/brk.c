#include <types.h>
#include <stdio.h>
#include <sys/vmm.h>

static void *__curbrk = NULL;

// set break value
int brk(void *addr)
{
    void *newbrk = NULL;

    __curbrk = newbrk = heap(addr); // set new break

    if (newbrk < addr)
    {
        return -1;
    }
    return 0;
}

// move break position
void *sbrk(int inc)
{
    void *oldbrk = NULL;

    // if no init
    if (__curbrk == NULL)
        if (brk(0) < 0) 
            return NULL;

    // if no increment
    if (!inc)
        return __curbrk;

    // get brk
    oldbrk = __curbrk;

    if ((inc > 0) ? ((uint32_t)oldbrk + (uint32_t)inc < (uint32_t)oldbrk) : ((uint32_t)oldbrk + (uint32_t)inc < (uint32_t)oldbrk))
        return NULL;

    if (brk((uint8_t *)oldbrk + inc) < 0)
        return NULL;

    return oldbrk;
}