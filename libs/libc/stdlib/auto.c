#include <sys/dir.h>
#include <malloc.h>
#include <stdio.h>

// system enviroment variable global
char **__environ = NULL;

// callback function needed be procude before exit
extern void __atexit_callback();

void _enter_preload(int argc, char *const argv[], char *const envp[])
{
    // set environ
    __environ = (char **)envp;
}

void _exit_cleanup()
{
    __atexit_callback();
    fflush(stdout); // flush stdout buffer
}
