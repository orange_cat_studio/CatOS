#include <stdlib.h>

typedef void (*atexit_func_t)(void);

#define __ATEXIT_FUNC_NR 32

atexit_func_t __at_exit_func_table[__ATEXIT_FUNC_NR] = {0};

// exit callback function register
void atexit(void (*func)(void))
{
    int i;
    for (i = 0; i < __ATEXIT_FUNC_NR; i++)
    {
        if (!__at_exit_func_table[i])
        {
            __at_exit_func_table[i] = func;
            break;
        }
    }
}

// exec callback function
void __atexit_callback()
{
    int i;
    atexit_func_t func = NULL;

    for (i = 0; i < __ATEXIT_FUNC_NR; i++)
    {
        func = __at_exit_func_table[i];
        if (func)
            func();
        else
            break;
    }
}