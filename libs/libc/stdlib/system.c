#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/proc.h>
#include <sys/wait.h>

int system(const char *cmd)
{
    pid_t pid;
    int status;

    if (!cmd)
        return EINVAL;

    if ((pid = fork()) < 0)
        status = -1;
    else
    {
        //sub process start shell
        if (pid == 0)
        {
            execl("/bin/sh", "sh", "-c", cmd, 0);
            exit(ENOTRECOVERABLE);
        }
        else
        {
            //father process wait sub process exit
            while (waitpid(pid, &status, 0) < 0)
            {
                if (errno != EINTR)
                {
                    status = -1;
                    break;
                }
            }
        }
    }
    return status;
}
