#include<stddef.h>
#include<stdlib.h>

int64_t atol(const char *nptr)
{
    return (int64_t)strtol(nptr,NULL,10);
}