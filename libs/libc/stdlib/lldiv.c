#include<stdlib.h>

lldiv_t lldiv(int64_t num,int64_t den)
{
    return (lldiv_t){num/den,num%den};
}