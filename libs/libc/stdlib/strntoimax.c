//file:libs/libc/stdlib/stntoimax.c
//time:2021.11.16
//autor:jiangxinpeng
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#include <stdlib.h>

intmax_t strntoimax(const char *nptr, char **endptr, int base, size_t n)
{
    return (intmax_t)strntoumax(nptr, endptr, base, n);
}
