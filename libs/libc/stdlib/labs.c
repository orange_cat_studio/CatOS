#include <stdlib.h>

int64_t labs(int64_t const i)
{
    return i > 0 ? i : -i;
}
