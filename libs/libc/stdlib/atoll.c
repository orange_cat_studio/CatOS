#include <stddef.h>
#include <stdlib.h>

int64_t atoll(const char *nptr)
{
    return (int64_t)strtoll(nptr, NULL, 10);
}
