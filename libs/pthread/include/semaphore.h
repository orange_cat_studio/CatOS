
#ifndef __LIBC_PTHREAD_SEMAPHORE_H
#define __LIBC_PTHREAD_SEMAPHORE_H

#include <sys/time.h>
#include <stdint.h>
#include <pthread.h>

#define SEM_VAL_MAX (2147483647)

typedef struct sem
{
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    int value;
    int valid;
} sem_t;

#define SEM_VALID (0x19980325)

#define SEM_INIT(val)                                                             \
    {                                                                             \
        PHTREAD_MUTEX_INITIALIZER, PTHREAD_COND_INITIALIZER, val, COND_SYNC_VALID \
    }

int sem_init(sem_t *sem, int pshared, uint32_t value);
int sem_destroy(sem_t *sem);
int sem_getvalue(sem_t *sem, int *val);
int sem_wait(sem_t *sem);
int sem_timeout(sem_t *sem, const timespec_t *abs_timeout);
int sem_trywait(sem_t *sem);
int sem_post(sem_t *sem);

#endif