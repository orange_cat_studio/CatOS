[bits 32]
[section .text]

extern __pthread_exit      
global __pthread_entry

__pthread_entry:
    push ebx    ;push args
    call ecx    ;pthread start
    push eax    ;push exit state
    call __pthread_exit ;call pthread exit function
    jmp $       ;failed just do loop
