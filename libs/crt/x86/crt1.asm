;i386 C runtime libray 1
extern __libc_start_main ;c libc start point
extern __libc_csu_fini
extern __libc_csu_init
extern main              ;c program main function
extern debug

[bits 32]
[section .text]

global _start
_start:
    call debug
    xor ebp,ebp
    push esp
    push __libc_csu_fini
    push __libc_csu_init
    ;push arg on stack
    push edx
    push ebx
    push ecx
    push main
    call __libc_start_main
    ;should never be here
    hlt