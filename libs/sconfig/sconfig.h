#ifndef __LIBC_SCONFIG_H
#define __LIBC_SCONFIG_H

#ifdef __cplusplus
extern "C"
{
#endif

    int sconfig_set_separtor(char separator);
    char sconfig_get_separtor();
    int sconfig_bool(const char *str);
    int sconfig_int(const char *str);
    int sconfig_write(char *line, const char *str);
    char *sconfig_read(char *line, const char *str, int len);
    int sconfig_writeline(char *line);
    char *sconfig_read_line(char *buff, const char *line, int len);
    char *sconfig_trim(const char *str);

#ifdef __cplusplus
}
#endif
#endif