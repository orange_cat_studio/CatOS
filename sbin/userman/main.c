#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/sys.h>
#include <sys/time.h>

void print_usage()
{
    printf("usage: userman [opt]\n");
    printf("  opt:  -a [user] [passsword]  add user\n");
    printf("        -d [user] del user\n");
    printf("        -p [user] [old_password] [new_password] change use password\n");
    printf("        -h show help page\n");
}

int main(int argc, char **argv)
{

    if (argc == 1) //show all user list
    {
        printf("------ user list -------\n");
        setpwent();
        passwd_t *entry = getpwent();
        while (entry)
        {
            printf("%s", entry->pw_name);
            printf("      ");
        }
    }

    char *p = argv[1];
    int user_add = 0;
    int user_del = 0;
    int user_chpwd = 0;

    if (*p == '-') //process opt
    {
        p++;
        switch (*p)
        {
        case 'a':           //add user
            user_add = 1;
            break;
        case 'd':           //del user
            user_del = 1;
            break;
        case 'p':           //change password
            user_chpwd = 1;
            break;
        case 'h':           //show help
            print_usage();
            return 0;
        default:
            printf("userman: unknow option '%c'\n", *p);
            return -1;
        }
    }
    else
    {
        print_usage();
        return -1;
    }

    if (user_add)
    {
        if (argc != 4)
        {
            printf("userman: argument no invalid!\n");
            return -1;
        }
        char *name = argv[2];
        char *password = argv[3];
        if (account_register(name, password) < 0)
        {
            printf("userman: create new user %s failed!\n", name);
            return -1;
        }
        printf("[userman] add new user %s ok\n",name);
        return 0;
    }
    else
    {
        if (user_del)
        {
            if (argc != 3)
            {
                printf("userman: arguement no invalid\n");
                return -1;
            }
            char *name = argv[2];
            if (account_unregister(name) < 0)
            {
                printf("userman: remove user %s failed!\n", name);
                return -1;
            }
            printf("[userman] del user %s ok\n",name);
            return 0;
        }
        else
        {
            if (user_chpwd)
            {
                if (argc != 5)
                {
                    printf("userman: argument no invalid!\n");
                    return -1;
                }
                char *name = argv[2];
                char *old_pwd = argv[3];
                char *new_pwd = argv[4];
                char buff[32];
                if (account_name(buff, 32) < 0)
                {
                    printf("userman: maybe no login any user!\n");
                    return -1;
                }
                //no admin user cannot to change password
                if (strcmp(buff, "admin") != 0)
                {
                    printf("userman: no permission to process this operator!\n");
                    return -1;
                }
                //no can change password for login user
                if (!strcmp(buff, name))
                {
                    printf("userman: no can change login user passwd!\n");
                    return -1;
                }
                if (account_chpwd(name, old_pwd, new_pwd) < 0)
                {
                    printf("userman: change user %s password failed!\n", name);
                    return -1;
                }
                printf("[userman] chpwd user %s successful\n",name);
            }
        }
    }
    return 0;
}