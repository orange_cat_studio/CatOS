#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <sys/sys.h>
#include <sys/ioctl.h>

#define BUFF_SIZE 32 + 1

void readline(char *buff, uint32_t count)
{
    int len = 0;
    char *pos = buff;
    while (len < count)
    {
        // read a char from stdin file
        read(STDIN_FILENO, pos, 1);
        if (*pos == '\n') // end read
        {
            *(pos) = '\0';
            break;
        }
        else
        {
            if (*pos == '\b') // del a char before
            {
                if (pos > buff)
                {
                    *(pos) = '\0';
                    --pos;
                    *(pos) = '\0';
                    len--;
                }
                else
                {
                    len = 0;
                }
            }
            else // next char
            {
                len++;
                pos++;
            }
        }
    }
}

int main(int argc, char *argv[])
{
    char *p;
    int i = 0;
    int flags = 0;
    char *username = NULL;
    char *password = NULL;
    char *shellpath = NULL;
    char pwdbuff[BUFF_SIZE] = {0};
    char namebuff[BUFF_SIZE] = {0};

    printf("hello,welcome to use this OS\n");
    if (argc > 1)
        p = argv[i];

    if (p)
    {
        if (*p == '-' && *(p + 1))
        {
            if (*(p + 1) == 'u') //-u
            {
                flags = 1;
                i++;
            }
            else
            {
                if (*(p + 1) == 'p') //-p
                {
                    flags = 2;
                    i++;
                }
                else
                {
                    if (*(p + 1) == 's') //-s
                    {
                        flags = 3;
                        i++;
                    }
                }
            }
        }
    }


    if (flags == 1)
    {
        username = argv[i];
        flags = 0;
        if ((username == NULL) || !(*username) || (*username == '-'))
        {
            printf("user:");
            readline(namebuff, BUFF_SIZE);
            username = namebuff;
            printf("\n");
        }
    }
    else
    {
        if (flags == 2)
        {
            password = argv[i];
            flags = 0;
            if ((password == NULL) || !(*password) || (*password == '-'))
            {
                // get password
                printf("password:");
                uint32_t oldflags = 0;
                ioctl(STDIN_FILENO, TTYIO_GETFLAGS, &oldflags);
                uint32_t newflags = oldflags & ~TTYFLAG_ECHO;
                ioctl(STDIN_FILENO, TTYIO_SETFLAGS, &newflags);
                readline(pwdbuff, BUFF_SIZE);
                ioctl(STDIN_FILENO, TTYIO_SETFLAGS, &oldflags);
                printf("\n");
                password = pwdbuff;
            }
        }
        else
        {
            if (flags == 3)
            {
                shellpath = argv[i];
            }
        }
    }

    // login
    if (username && password)
    {
        if (login(username, password) < 0)
        {
            printf("login failed!\n please check your user:%s passwd:%s\n", username, password);
            return -1;
        }
    }
    else
    {
        if (!username && !password) // need input user and passwd
        {
            printf("please input user name admin password to login\n");
            username = NULL;
            password = NULL;
            memset(namebuff, 0, BUFF_SIZE);
            printf("user:");
            while (!namebuff[0])
            {
                readline(namebuff, BUFF_SIZE);
                printf("\n");
            }
            memset(pwdbuff, 0, BUFF_SIZE);
            uint32_t oldflags = 0;
            ioctl(STDIN_FILENO, TTYIO_GETFLAGS, &oldflags);
            uint32_t newflags = oldflags & ~TTYFLAG_ECHO;
            ioctl(STDIN_FILENO, TTYIO_SETFLAGS, &newflags);
            printf("password:");
            while (!pwdbuff[0])
            {
                readline(pwdbuff, BUFF_SIZE);
                printf("\n");
            }
            ioctl(STDIN_FILENO, TTYIO_SETFLAGS, &oldflags);
            username = namebuff;
            password = pwdbuff;
            if (login(username, password) < 0)
            {
                printf("login failed!\nplease check user:%s and password:%s!\n", username, password);
                return -1;
            }
        }
        else
        {
            if (username && !password)
            {
                // get password
                printf("password:");
                uint32_t oldflags = 0;
                ioctl(STDIN_FILENO, TTYIO_GETFLAGS, &oldflags);
                uint32_t newflags = oldflags & ~TTYFLAG_ECHO;
                ioctl(STDIN_FILENO, TTYIO_SETFLAGS, &newflags);
                readline(pwdbuff, BUFF_SIZE);
                ioctl(STDIN_FILENO, TTYIO_SETFLAGS, &oldflags);
                printf("\n");
                password = pwdbuff;

                if (login(username, password) < 0)
                {
                    printf("login failed!\nplease check user:%s and password:%s!\n", username, password);
                    return -1;
                }
            }
        }
    }
    printf("user %s login success!\n", username);
    if (shellpath)
    {
        exit(execv(shellpath, NULL));
    }
}
