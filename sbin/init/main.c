// file:src/sysapp/init/main.c
// autor:jiangxinpeng
// time:2021.4.26
// copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termio.h>
#include <sys/proc.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <fcntl.h>

// #define _HAS_LOGIN
// #define _HAS_NET

#define SHELL_PATH "/bin"
#define SHELL_NAME "sh"

int main(int argc, char *argv[])
{
    int tty0 = open("/dev/tty0", O_RDONLY);
    if (tty0 < 0)
    {
        return -1;
    }
    int tty1 = open("/dev/tty0", O_WRONLY);
    if (tty1 < 0)
    {
        close(tty0);
        return -1;
    }
    int tty2 = dup(tty1);

    // fork process
    int pid = fork();
    if (pid < 0)
    {
        printf("[init]: fork process error! stop service.\n");
        close(tty0);
        close(tty1);
        close(tty2);
    }
    // father process wait sub process wait
    if (pid > 0)
    {
        while (1)
        {
            int status = 0;
            int _pid;
            _pid = waitpid(-1, &status, 0);
            if (_pid > 1)
                printf("[init]: process[%d] exit with status %d\n", _pid, status);
        }
    }

#ifdef _HAS_NET // configuration netserv
    pid = fork();
    if (pid < 0)
    {
        printf("[init]: fork process error!\n stop service.\n");
        close(tty2);
        close(tty1);
        close(tty0);
        return -1;
    }
    else
    {
        if (pid == 0) // sub process start netserv
        {
            printf("start netserv\n");
            exit(execv("/sysapp/netserv", NULL));
        }
    }
#endif

    setpgrp();
    tcsetpgrp(STDIN_FILENO, getpgrp());
#ifdef _HAS_GRAPH
    exit(execv("/system/programs/desktops", NULL));
#else
#ifdef _HAS_LOGIN
    printf("login system\n");
    char *_argv[3] = {"/sbin/login", "-s", SHELL_PATH "/" SHELL_NAME, NULL};
    exit(execv("/sbin/login", _argv));
#else
    printf("welcome to using shell\n");
    char *_argv[3] = {"/bin/sh", NULL};
    exit(execv("/bin/sh", _argv));
#endif
#endif
    return 0;
}
