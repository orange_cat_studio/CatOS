#include<stdio.h>
#include<unistd.h>
#include<sys/proc.h>
#include<unistd.h>

int main(int argc,char *argv[])
{
    if(argc<2)
    {
        printf("[server] please input server name.\n");
        return -1;
    }
    char *servername=argv[1];
    if(access(servername,F_OK)<0)
    {
        printf("[server] server %s failed!\n",servername);
        return -1;
    }
    char *_argv[2]={servername,NULL};
    pid_t pid=create_process(_argv,__environ,PROC_CREATE_STOP);
    if(pid<0)
    {
        printf("[server] start server %s failed!\n",servername);
        return -1;
    }
    resume_process(pid);
    printf("start server %s sucess!\n",servername);
    return 0;
}



