#include <lib/ctype.h>

int isspace(char ch)
{
    int i;
    char comp[] = {' ', '\t', '\n', '\r', '\v', '\f'};
    const int len = 6;

    for (i = 0; i < len; i++)
    {
        if (ch == comp[i])
        {
            return 1;
        }
    }
    return 0;
}

int isalnum(char ch)
{
    if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z' || ch >= '0' && ch <= '9')
    {
        return 1;
    }
    return 0;
}

int isxdight(char ch)
{
    if (ch >= '0' && ch <= '9' || ch >= 'a' && ch <= 'f' | ch >= 'A' && ch <= 'F')
    {
        return 1;
    }
    return 0;
}

int isdigit(char ch)
{
    if (ch >= '0' && ch <= '9')
    {
        return 1;
    }
    return 0;
}

int isalpha(char ch)
{
    if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z')
    {
        return 1;
    }
    return 0;
}

int issuper(char ch)
{
    if (ch >= 'A' && ch <= 'Z')
        return 1;
    return 0;
}
