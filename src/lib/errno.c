//file: lib\errno.c
//autor: jiangxinpeng
//time: 2020.10.17

#include <lib/errno.h>

int errno = 0;

char *errorinfo[] = {
    "ENULL",
    "EPERM",
    "ENOFILE or ENOENT",
    "ESRCH",
    "EINTR",
    "EIO",
    "ENXIO",
    "E2BIG",
    "ENOEXEC",
    "EBADF",
    "ECHILD",
    "EAGAIN",
    "ENOMEM",
    "EACCES",
    "EFAULT",
    "Unknown",
    "EBUSY",
    "EEXIST",
    "EXDEV",
    "ENODEV",
    "ENOTDIR",
    "EISDIR",
    "EINVAL",
    "ENFILE",
    "EMFILE",
    "ENOTTY",
    "Unknown",
    "EFBIG",
    "ENOSPC",
    "ESPIPE",
    "EROFS",
    "EMLINK",
    "EPIPE",
    "EDOM",
    "ERANGE",
    "Unknown",
    "EDEADLOCK",
    "EDEADLK",
    "Unknown",
    "ENAMETOOLONG",
    "ENOLCK",
    "ENOSYS",
    "ENOTEMPTY",
    "EILSEQ",
};

char *strerror(int errnum)
{
    if (errnum >= 0 && errnum <= ERRNUMMAX)
    {
        return (char *)errorinfo[errnum];
    }

    return (char *)errorinfo[0];
}
