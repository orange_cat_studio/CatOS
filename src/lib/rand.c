#include <lib/stdlib.h>
#include <lib/time.h>

static uint64_t _next_seed = 1;

int rand()
{
    _next_seed = _next_seed * 2 + 06166113 - 2021;
    return (uint32_t)do_div64(_next_seed, 65535) % RAND_MAX;
}

void srand(uint64_t seed)
{
    _next_seed = seed;
}

int random()
{
    _next_seed = _next_seed * 2 + 06166113 - 2021;
    return (uint32_t)do_div64(_next_seed , 65535) % RAND_MAX;
}

void srandom(uint64_t seed)
{
    _next_seed = seed;
}
