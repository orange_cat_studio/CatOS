#include <lib/stdio.h>
#include <lib/stdarg.h>
#include <lib/unistd.h>
#include <lib/stddef.h>
#include <lib/type.h>
#include <lib/ctype.h>

int atoi(const char *str)
{
    int i = 0;
    while (isdigit(*str))
    {
        i = i * 10 + (*str++ - '0');
    }
    return i;
}
