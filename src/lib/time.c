#include <lib/time.h>
#include <lib/stddef.h>
#include <sys/time.h>

double difftime(time_t time_end, time_t time_beg)
{
    return (double)(time_end - time_beg);
}

time_t time(time_t *time)
{
    timeval_t tv;
    if (SysGetTimeOfDay(&tv, NULL))
        return -1;
    if (time)
        *time = tv.tv_sec;
    return *time;
}

clock_t clock()
{
    timeval_t tv;

    if(SysGetTimeOfDay(&tv,0))
        return -1;
    return tv.tv_sec*1000000+tv.tv_usec;
}

