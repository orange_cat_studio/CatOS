#include<os/debug.h>
#include<arch/interrupt.h>
#include<arch/x86.h>
#include<arch/cpu.h>
#include<lib/assert.h>

void assertion_failure(char *exp,char *file,char *basefile,int line)
{
    KPrint(PRINT_ERR"assert(%s)failed:\nfile:%s\nbasefile:%s\nline:%d\n",exp,file,basefile,line);
}

