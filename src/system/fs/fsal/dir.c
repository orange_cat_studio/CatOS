// file:src/kernel/fs/dir.c
// autor:jiangxinpeng
// time:2021.2.23
// copyright:(C) by jiangxinpeng,All right are reserved.

#include <os/memcache.h>
#include <os/dir.h>
#include <os/fsal.h>
#include <lib/string.h>

fsal_dir_t *fsal_dir_table;

int FsalDirTableInit()
{
    fsal_dir_table = KMemAlloc(sizeof(fsal_dir_t) * FSAL_DIR_MAX);
    if (fsal_dir_table != NULL)
    {
        memset(fsal_dir_table, 0, sizeof(fsal_dir_t) * FSAL_DIR_MAX);
        return 0;
    }
    return -1;
}

fsal_dir_t *FsalDirAlloc()
{
    fsal_dir_t *dir = NULL;
    for (int i = 0; i < FSAL_DIR_MAX; i++)
    {
        dir = &fsal_dir_table[i];
        if (!dir->flags)
        {
            dir->flags = FSAL_DIR_USING;
            dir->fsal = NULL;
            dir->extension = NULL;
        }
        return dir;
    }
    return NULL;
}

int FsalDirFree(fsal_dir_t *dir)
{
    if (dir != NULL)
    {
        if (dir->flags == FSAL_DIR_USING)
        {
            dir->flags = 0;
            return 0;
        }
    }
    return -1;
}