#include <os/fsal.h>
#include <os/file.h>
#include <os/dir.h>
#include <os/path.h>
#include <os/cdfsal.h>
#include <os/memcache.h>
#include <os/diskman.h>
#include <lib/stddef.h>
#include <lib/stdio.h>
#include <lib/stdlib.h>
#include <lib/type.h>
#include <lib/arch.h>
#include <lib/errno.h>
#include <os/iso9660.h>

#define CDFS_DEBUG

#define PDRV_TO_PATH(pdrv) ((pdrv) + '0')
#define PATH_TO_PDRV(path) ((path) - '0')

static int FsalCdFsOpen(char *path, mode_t mode)
{
    fsal_file_t *file = FsalFileAlloc();
    if (!file)
        return -ENOMEM;

    file->fsal = &cdfs_fsal;
    file->extension = KMemAlloc(sizeof(l9660_file_t));
    if (!file->extension)
    {
        FsalFileFree(file);
        return -ENOMEM;
    }

    l9660_status lstu = l9660_openfile((l9660_fs_t *)cdfs_fsal.extension, (l9660_file_t *)file->extension, path);
    if (lstu != L9660_OK)
    {
#ifdef CDFS_DEBUG
        KPrint("[cdfs] open file failed\n");
#endif
        KMemFree(file->extension);
        FsalFileFree(file);
        return -1;
    }
    return FSAL_FILE_FILE2IDX(file);
}

static int FsalCdFsClose(int handle)
{
    fsal_file_t *fp = FSAL_FILE_IDX2FILE(handle);
    if (FSAL_FILE_BAD_IDX(handle))
        return -1;
    if (FSAL_FILE_BAD_FILE(fp))
        return -1;

    fp->fsal = NULL;
    if (fp->extension)
        KMemFree(fp->extension);
    if (FsalFileFree(fp) < 0)
        return -1;
    return 0;
}

static int FsalCdFsMount(char *source, char *targe, char *fstype, uint64_t flags)
{
#ifdef CDFS_DEBUG
    KPrint("[cdfsif] mount path %s\n", source);
#endif
    int solt = DiskSoltFindByPath((char *)source); // find disk solt
    if (solt < 0)
    {
        KPrint("[cdfs]not find device %s.\n", source);
        return -1;
    }
    // check whether device had map
    if (FsalPathFindDevice(source))
    {
        KPrint("[cdfs]device %s had been mount!\n", source);
        return -1;
    }

    // translation to pysical driver
    int pdrv, i;
    for (i = 0; i < DISK_MAX; i++)
    {
        if (driver_map[i] == solt)
        {
            break;
        }
    }
    // get pysical driver
    pdrv = i;
    char path[3] = {PDRV_TO_PATH(pdrv), ':', '\0'};

    if (!strcmp(fstype, "iso9660"))
    {
        KPrint("[cdfsif] mount type is iso9660\n");
        l9660_fs_t *fs = KMemAlloc(sizeof(l9660_fs_t));
        if (!fs)
        {
            return -1;
        }
        l9660_status lstu = l9660_mount(fs, solt);
        if (lstu != L9660_OK)
        {
            KPrint("[cdfsif] mount iso9660 failed\n");
            KMemFree(fs);
            return -1;
        }
        FsalPathInsert(&cdfs_fsal, source, path, targe);
        // bind fsal fs extension
        cdfs_fsal.extension = fs;
    }
    else
    {
        KPrint("[cdfsif] mount type is unknown\n");
    }
    return 0;
}

static int FsalCdFsUnMount(char *path)
{
    int i;

    // check path whether is virtual path or pysical path
    if (FsalPathFind(path, 0) < 0 && FsalPathFindDevice(path) < 0)
    {
#if DEBUG_KERNEL
        KPrint("%s: path %s no found!\n", __func__, path);
#endif
        return -1;
    }
    // find disk info
    int solt = DiskSoltFindByPath(path);
    if (solt < 0)
    {
        KPrint("no found device %s!\n", path);
        return -1;
    }
    // translation pysical driver
    for (i = 0; i < DISK_MAX; i++)
    {
        if (driver_map[i] == solt)
            break;
    }
    int pdrv = i;
    char _path[3] = {PDRV_TO_PATH(pdrv), ':'};
    const TCHAR *p = (const TCHAR *)_path;

    l9660_status lstu = l9660_umount((l9660_fs_t *)cdfs_fsal.extension);
    if (lstu != L9660_OK)
    {
        KPrint("[cdfs] umount iso9660 error\n");
        return -1;
    }

    if (FsalPathRemove(p) < 0)
    {
        KPrint("[cdfsif] unmount error\n");
        return -1;
    }
    return 0;
}

static int FsalCdFsRead(int handle, uint8_t *buff, uint64_t len)
{
    if (FSAL_FILE_BAD_IDX(handle))
    {
        return -1;
    }
    fsal_file_t *file = FSAL_FILE_IDX2FILE(handle);
    if (FSAL_FILE_BAD_FILE(file))
    {
        return -1;
    }
    l9660_status lstu = l9660_readfile(&cdfs_fsal.extension, file, buff, len);
    if (lstu != L9660_OK)
    {
        return -1;
    }
    return 0;
}

static int FsalCdFsOpenDir(char *dir_path)
{
    fsal_dir_t *dir = FsalDirAlloc();
    dir->extension = KMemAlloc(sizeof(l9660_dir_t));
    if (!dir->extension)
        return -1;
    dir->fsal = &cdfs_fsal;

    if (*dir_path)
    {
        l9660_status lstu = l9660_opendir((l9660_dir_t *)dir->extension, (l9660_fs_t *)cdfs_fsal.extension, dir_path);
        if (lstu != L9660_OK)
        {
            KPrint("[cdfs] open dir failed\n");
            FsalDirFree(dir);
            return -1;
        }
        return FSAL_DIR2IDX(dir);
    }
}

static int FsalCdFsCloseDir(int idx)
{
    fsal_dir_t *dir = FSAL_IDX2DIR(idx);
    if (FSAL_BAD_DIR_IDX(idx) || FSAL_BAD_DIR(dir))
    {
        return -1;
    }
    if (dir->extension)
    {
        KMemFree(dir->extension);
        dir->fsal = NULL;
    }
    KMemFree(dir);
}

static int FsalCdFsReadDir(int handle, uint8_t *buff)
{
    fsal_dir_t *fdir = FSAL_IDX2DIR(handle);
    if (FSAL_BAD_DIR_IDX(handle) || FSAL_BAD_DIR(fdir))
        return -1;

    // clear dirty data
    memset(buff, 0, sizeof(dirent_t));

    l9660_dir_t *dir = fdir->extension;
    l9660_dirent_t *dirent = NULL;
    l9660_status lstu = l9660_readdir(dir, &dirent);
    if (lstu != L9660_OK)
    {
        KPrint("[cdfs] readdir error\n");
        return -1;
    }
    if (dirent == NULL)
    {
        KPrint("[cdfs] read dir end\n");
        return -1;
    }

    dirent_t _dirent;
    memcpy(_dirent.name, dirent->name, dirent->name_len);
    _dirent.name[dirent->name_len] = '\0';
    _dirent.size = READ32(dirent->size);
    if (dirent->flags & DENT_ISDIR)
    {
        _dirent.attr |= DIRENT_DIR;
    }
    // copy to buffer
    memcpy(buff, &_dirent, sizeof(dirent_t));
    return 0;
}

static uint32_t FsalCdFsFsize(int handle)
{
    fsal_file_t *fp = FSAL_FILE_IDX2FILE(handle);
    if (FSAL_FILE_BAD_IDX(handle) || FSAL_FILE_BAD_FILE(fp))
        return -1;

    l9660_dir_t *file = fp->extension;
    if (!file)
        return -1;
    uint32_t size = 0;
    l9660_status stu = l9660_fsize(file, &size);
    if (stu != L9660_OK)
        return 0;
    return size;
}

// TODO
static int FsalCdFsStatus(char *path, status_t *status)
{
    if (!path || !status || !*path)
        return -1;
    l9660_status st = l9660_getstatus((l9660_fs_t*)cdfs_fsal.extension,path, status);
    if(st!=L9660_OK)
        return -1;
    return 0;
}

static int FsalCdFsRewindDir(int diridx)
{
    fsal_dir_t *dir = FSAL_IDX2DIR(diridx);
    if (FSAL_BAD_DIR_IDX(diridx) || FSAL_BAD_DIR(dir))
        return -1;
    l9660_dir_t *obj = dir->extension;
    if (!dir->extension)
        return -1;
    // rewind dir object(deal as file)
    l9660_status st = REWIND_DIR(obj);
    if (st != L9660_OK)
    {
        return -1;
    }
    return 0;
}

static int FsalFsRewind(int idx)
{
    fsal_file_t *file = FSAL_FILE_IDX2FILE(idx);
    if (FSAL_FILE_BAD_FILE(file) || FSAL_FILE_BAD_IDX(idx))
        return -1;

    if (!file->extension)
        return -1;

    l9660_status st = REWIND((l9660_file_t *)file->extension);
    if (st != L9660_OK)
    {
        return -1;
    }
    return 0;
}

static int FsalCdFsLseek(int idx, int pos, int off)
{
    fsal_file_t *file = FSAL_FILE_IDX2FILE(idx);
    if (FSAL_FILE_BAD_FILE(file) || FSAL_FILE_BAD_IDX(idx))
        return -1;

    if (!file->extension)
        return -1;

    l9660_status st = l9660_seek(file->extension, pos, off);
    if (st != L9660_OK)
        return -1;
    return 0;
}

fsal_t cdfs_fsal = {
    .name = "iso9660",
    .subtable = NULL,
    .mkfs = NULL,
    .mount = FsalCdFsMount,
    .unmount = FsalCdFsUnMount,
    .open = FsalCdFsOpen,
    .close = FsalCdFsClose,
    .read = FsalCdFsRead,
    .write = NULL,
    .fastio = NULL,
    .fastread = NULL,
    .fastwrite = NULL,
    .rename = NULL,
    .lseek = FsalCdFsLseek,
    .opendir = FsalCdFsOpenDir,
    .closedir = FsalCdFsCloseDir,
    .mkdir = NULL,
    .readdir = FsalCdFsReadDir,
    .rewinddir = FsalCdFsRewindDir,
    .fsize = FsalCdFsFsize,
    .fsync = NULL,
    .incref = NULL,
    .decref = NULL,
    .status = FsalCdFsStatus,
    .chmod = NULL,
    .fchmod = NULL,
    .feof = NULL,
    .error = NULL,
};