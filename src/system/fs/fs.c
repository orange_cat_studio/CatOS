// file:kernel/fs.c
// autor:jiang xinpeng
// time:2020.12.24
// copyright:(C) 2020-2050 by jiang xinpeng,All right are reserved.

#include <os/fs.h>
#include <os/fstype.h>
#include <os/fsal.h>
#include <os/diskman.h>
#include <os/debug.h>

void FsInit()
{
    // init fstype
    if (FsTypeInit() < 0)
        Panic("fs: init fstype failed!\n");
        
    // init fsal
    if (FsalInit() < 0)
        Panic("fs: init fsal failed!\n");

    KPrint(PRINT_INFO "[fs] init fs done.\n");
}