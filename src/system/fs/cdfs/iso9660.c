#include <os/iso9660.h>
#include <os/driver.h>
#include <os/diskio.h>
#include <os/debug.h>
#include <lib/string.h>
#include <lib/type.h>
#include <lib/errno.h>

static bool CDROM_Read(int sector, int count, uint8_t *buff, int driver);
static l9660_status ISO_InitFs(l9660_fs_t *fs, uint8_t disk_number);
static bool ISO_Check(uint8_t disk_number);
static bool read_sector(l9660_fs_t *fs, void *buf, uint32_t sector);

static char *strchrnul(const char *s, int c)
{
    while (*s)
    {
        if ((*s++) == c)
            break;
    }
    return (char *)s;
}

static char *strdup(char *s)
{
    char *p;
    if (!s)
        return NULL;

    p = KMemAlloc(strlen(s) + 1);
    if (p)
        return strcpy(p, s);
    return NULL;
}

static char *strtoupper(char *s)
{
    int i;
    for (i = 0; *(s + i); i++)
    {
        ((s[i] > 'A') && (s[i] <= 'Z')) ? s[i] -= 32 : s[i];
    }
}

l9660_status l9660_mount(l9660_fs_t *fs, int driver)
{
    bool ret = ISO_Check(driver);
    if (ret == false)
    {
        KPrint("[iso9660] mount: check error\n");
        return L9660_EIO;
    }

    l9660_status stu = ISO_InitFs(fs, driver);
    if (stu != L9660_OK)
    {
        KPrint("[iso9660] mount: try to openfs error\n");
        return L9660_EBADFS;
    }
    KPrint("[iso9660] mount: mount iso9660 ok\n");
    return L9660_OK;
}

l9660_status l9660_umount(l9660_fs_t *fs)
{
    if (fs)
    {
        fs->disk_number = 0;
        if (fs->fs_m)
        {
            KMemFree(fs->fs_m);
        }
        fs->fs_m = NULL;
        KMemFree(fs);
        return L9660_OK;
    }
    return L9660_EIO;
}

static l9660_status l9660_openfs(l9660_fs_t *fs,
                                 bool (*read_sector)(l9660_fs_t *fs, void *buf,
                                                     uint32_t sector),
                                 uint8_t disk_number)
{
    fs->read_sector = read_sector;
    fs->disk_number = disk_number;
#ifndef L9660_SINGLEBUFFER
    l9660_vdesc_primary_t *pvd = PVD(&fs->pvd);
#else
    last_file = NULL;
    l9660_vdesc_primary_t *pvd = PVD(gbuf);
#endif
    uint32_t idx = 0x10;
    for (;;)
    {
        // Read next sector
        if (!read_sector(fs, pvd, idx))
            return L9660_EIO;

        // Validate magic
        if (memcmp(pvd->hdr.magic, "CD001", 5) != 0)
            return L9660_EBADFS;

        if (pvd->hdr.type == 1)
            break; // Found PVD
        else if (pvd->hdr.type == 255)
            return L9660_EBADFS;
    }

#ifdef L9660_SINGLEBUFFER
    memcpy(&fs->root_dir_ent, &pvd->root_dir_ent, pvd->root_dir_ent.length);
#endif

    return L9660_OK;
}

l9660_status l9660_fs_open_root(l9660_dir_t *dir, l9660_fs_t *fs)
{
    l9660_file_t *f = &dir->file;
#ifndef L9660_SINGLEBUFFER
    l9660_dirent_t *dirent = &PVD(&fs->pvd)->root_dir_ent;
#else
    l9660_dirent_t *dirent = &fs->root_dir_ent;
#endif

    f->fs = fs;
    f->first_sector = READ32(dirent->sector);
    f->length = READ32(dirent->size);
    f->position = 0;

    return L9660_OK;
}

static l9660_status buffer(l9660_file_t *f)
{
#ifdef L9660_SINGLEBUFFER
    last_file = f;
#endif

    if (!f->fs->read_sector(f->fs, BUF(f), f->first_sector + f->position / 2048))
        return L9660_EIO;
    else
        return L9660_OK;
}

static l9660_status prebuffer(l9660_file_t *f)
{
    if (!HAVEBUFFER(f) || (f->position % 2048) == 0)
        return buffer(f);
    else
        return L9660_OK;
}

static l9660_status find_path(l9660_fs_t *fs, char *path, l9660_dirent_t **dirent)
{
    l9660_status rv = L9660_ENOENT;
    l9660_dir_t *parent = &fs->fs_m->root_dir;
    l9660_file_t *child = NULL;
    l9660_dirent_t *dent = NULL;

    if ((rv = l9660_seekdir(parent, 0)) != L9660_OK)
        return rv;

    do
    {
        path = strchrnul(path, '/');
        if (*path == '\0') // no name,return
            break;
        const char *seg = path;
        const char *seg_end = strchr(path, '/');
        uint32_t seglen = 0;
        if (!seg_end)
            seglen = strlen(path);
        else
            seglen = seg_end - seg;

        /* ISO9660 stores '.' as '\0' */
        if (seglen == 1 && *seg == '.')
            seg = "\0";

        /* ISO9660 stores ".." as '\1' */
        if (seglen == 2 && seg[0] == '.' && seg[1] == '.')
        {
            seg = "\1";
            seglen = 1;
        }

        for (;;)
        {
            if ((rv = l9660_readdir(parent, &dent)))
                return rv;

            /* EOD */
            if (!dent)
                return L9660_ENOENT;

#ifdef DEBUG
            print_dirent(dent);
#endif

            /* wrong length */
            if (seglen > dent->name_len)
                continue;

            /* check name */
            if (memcmp(seg, dent->name, seglen) != 0)
                continue;

            /* check for a revision tag */
            if (dent->name_len > seglen && dent->name[(int)seglen] != ';')
                continue;

            /* all tests pass */
            break;
        }

        // get child file object info
        child->fs = parent->file.fs;
        child->first_sector = READ32(dent->sector) + dent->xattr_length;
        child->length = READ32(dent->size);
        child->position = 0;
        parent->file.position = 0;

        // if is file but path try to as dir to open,error
        if (dent->flags & DENT_ISDIR != 0)
        {
            if (*strchrnul(path, '/') != '\0')
                return L9660_ENOENT;
        }

        parent = (l9660_dir_t *)child;
    } while (*path);

    // return dirent point
    *dirent = dent;
    return L9660_OK;
}

static l9660_status openat_raw(l9660_file_t *child, l9660_dir_t *parent,
                               const char *name, bool isdir)
{
    l9660_status rv;
    l9660_dirent_t *dent = NULL;
    if ((rv = l9660_seekdir(parent, 0)))
        return rv;

    // deal with path layer for each subpath
    do
    {
        name = strchrnul(name, '/');
        if (*name == '\0') // no name,return
            break;
        const char *seg = name;
        const char *seg_end = strchr(name, '/');
        uint32_t seglen = 0;
        if (!seg_end)
            seglen = strlen(name);
        else
            seglen = seg_end - seg;

        /* ISO9660 stores '.' as '\0' */
        if (seglen == 1 && *seg == '.')
            seg = "\0";

        /* ISO9660 stores ".." as '\1' */
        if (seglen == 2 && seg[0] == '.' && seg[1] == '.')
        {
            seg = "\1";
            seglen = 1;
        }

        for (;;)
        {
            if ((rv = l9660_readdir(parent, &dent)))
                return rv;

            /* EOD */
            if (!dent)
                return L9660_ENOENT;

#ifdef DEBUG
            print_dirent(dent);
#endif

            /* wrong length */
            if (seglen > dent->name_len)
                continue;

            /* check name */
            if (memcmp(seg, dent->name, seglen) != 0)
                continue;

            /* check for a revision tag */
            if (dent->name_len > seglen && dent->name[(int)seglen] != ';')
                continue;

            /* all tests pass */
            break;
        }

        // get child file object info
        child->fs = parent->file.fs;
        child->first_sector = READ32(dent->sector) + dent->xattr_length;
        child->length = READ32(dent->size);
        child->position = 0;
        parent->file.position = 0;

        // if is file but path try to as dir to open,error
        if (dent->flags & DENT_ISDIR != 0)
        {
            if (*strchrnul(name, '/') != '\0')
                return L9660_ENOENT;
        }

        // child as new dir to contiue find
        parent = (l9660_dir_t *)child;
    } while (*name);

    // according type to which return errcode to use
    if (isdir)
    {
        if ((dent->flags & DENT_ISDIR) == 0)
            return L9660_ENOTDIR;
    }
    else
    {
        if ((dent->flags & DENT_ISDIR) != 0)
            return L9660_ENOTFILE;
    }

    return L9660_OK;
}

l9660_status l9660_opendirat(l9660_dir_t *dir, l9660_dir_t *parent,
                             const char *path)
{
    return openat_raw(&dir->file, parent, path, true);
}

static inline uint16_t fsectoff(l9660_file_t *f) { return f->position % 2048; }

static inline uint32_t fsector(l9660_file_t *f) { return f->position / 2048; }

static inline uint32_t fnextsectpos(l9660_file_t *f)
{
    return (f->position + 2047) & ~2047;
}

static inline unsigned aligneven(unsigned v) { return v + (v & 1); }

l9660_status l9660_readdir(l9660_dir_t *dir, l9660_dirent_t **pdirent)
{
    l9660_status rv;
    l9660_file_t *f = &dir->file;

rebuffer:
    if (f->position >= f->length)
    {
        *pdirent = NULL;
        f->position = 0;
        return L9660_OK;
    }

    if ((rv = prebuffer(f)))
        return rv;
    char *off = BUF(f) + fsectoff(f);
    if (*off == 0)
    {
        // Padded end of sector
        f->position = fnextsectpos(f);
        goto rebuffer;
    }

    l9660_dirent_t *dirent = (l9660_dirent_t *)off;
    f->position += aligneven(dirent->length);

    if (dirent->flags & DENT_EXISTS == 0 || *dirent->name == '\0' || *dirent->name == '\1')
        goto rebuffer;

    *pdirent = dirent;
    return L9660_OK;
}

static l9660_status l9660_openat(l9660_file_t *child, l9660_dir_t *parent,
                                 const char *name)
{
    return openat_raw(child, parent, name, false);
}

/*! Seek the file to \p offset from \p whence */
l9660_status l9660_seek(l9660_file_t *f, int whence, int32_t offset)
{
    l9660_status rv;
    uint32_t cursect = fsector(f);

    switch (whence)
    {
    case SEEK_SET:
        f->position = offset;
        break;

    case SEEK_CUR:
        f->position = f->position + offset;
        break;

    case SEEK_END:
        f->position = f->length - offset;
        break;
    }

    if (fsector(f) != cursect && fsectoff(f) != 0)
    {
        if ((rv = buffer(f)))
            return rv;
    }

    return L9660_OK;
}

uint32_t l9660_tell(l9660_file_t *f)
{
    return f->position;
}

l9660_status l9660_read(l9660_file_t *f, void *buf, size_t size, size_t *read)
{
    l9660_status rv;

    if ((rv = prebuffer(f)))
        return rv;

    uint16_t rem = 2048 - fsectoff(f);
    if (rem > f->length - f->position)
        rem = f->length - f->position;
    if (rem < size)
        size = rem;

    memcpy(buf, BUF(f) + fsectoff(f), size);

    *read = size;
    f->position += size;

    return L9660_OK;
}

static bool read_sector(l9660_fs_t *fs, void *buf, uint32_t sector)
{
    return CDROM_Read(sector, 1, buf, fs->disk_number);
}

static bool CDROM_Read(int sector, int count, uint8_t *buff, int driver)
{
    disk_status_t dst = CDROMOpen(driver);
    if (dst != DSTU_OK)
    {
        KPrint("[iso9660] open disk error\n");
        return false;
    }
    dst = CDROMRead(driver, buff, sector, 1);
    if (dst != DSTU_OK)
    {
        KPrint("[iso9660] read disk error\n");
        return false;
    }
    CDROMClose(driver);
    return true;
}

static bool ISO_Check(uint8_t disk_number)
{
    unsigned char *buffer = KMemAlloc(2049); // 假设扇区大小为 2048 字节
    bool ok = CDROM_Read(16, 1, buffer, disk_number);

    if (buffer[0] == 0x01 && buffer[1] == 'C' && buffer[2] == 'D' &&
        buffer[3] == '0' && buffer[4] == '0' && buffer[5] == '1')
    {
        KMemFree(buffer);
        return true; // 是 ISO9660 文件系统
    }
    else
    {
        KMemFree(buffer);
        return false; // 不是 ISO9660 文件系统
    }
}

static l9660_status ISO_InitFs(l9660_fs_t *fs, uint8_t disk_number)
{
    fs->fs_m = KMemAlloc(sizeof(l9660_fs_status_t));
    if (!fs->fs_m)
        return L9660_EIO;
    l9660_fs_status_t *fs_m = fs->fs_m;
    fs_m->fs = fs;
    l9660_openfs(fs_m->fs, read_sector, disk_number);
    l9660_fs_open_root(&fs_m->root_dir, fs_m->fs);
    fs_m->now_dir = fs_m->root_dir;
    return L9660_OK;
}

l9660_status l9660_openroot(l9660_dir_t *dir, l9660_fs_t *fs)
{
    if (!dir || !fs)
        return L9660_EIO;
    l9660_fs_open_root(dir, fs);
}

static char *stripVol(char *path)
{
    if (!path || *path == '\0')
    {
        return NULL;
    }
    char *p = path;
    while (*p != '/')
    {
        p++;
    }
    return p;
}

l9660_status l9660_opendir(l9660_dir_t *dir, l9660_fs_t *fs, char *dictname)
{
    // get actual director
    dictname = stripVol(dictname);

    if (strcmp(dictname, "/") == 0)
    {
        l9660_fs_status_t *fs_m = (l9660_fs_status_t *)fs->fs_m;
        fs->cur_dir = &fs_m->root_dir;
        memcpy(dir, &fs_m->root_dir, sizeof(l9660_dir_t)); // copy root dir object to buffer
        return L9660_OK;
    }
    l9660_dir_t *dinfo = dir;
    l9660_status stu;
    stu = l9660_opendirat(dinfo, &fs->fs_m->root_dir, dictname);
    if (stu != L9660_OK)
    {
        return stu;
    }
    fs->cur_dir = dinfo;
    memcpy(dir, dinfo, sizeof(l9660_dir_t));
    return L9660_OK;
}

l9660_status l9660_openfile(l9660_fs_t *fs, l9660_file_t *file, char *path)
{
    if (!fs)
        return -1;
    if (!path || *path == '\0')
        return -1;
    l9660_status stu = l9660_openat(file, &fs->fs_m->root_dir, path);
    if (stu != L9660_OK)
    {
        KPrint("[iso9660] open file %s error\n", path);
        return L9660_ENOTFILE;
    }
    return L9660_OK;
}

l9660_status l9660_readfile(l9660_fs_t *fs, l9660_file_t *file, char *buffer, int len)
{
    for (;;)
    {
        size_t read;
        l9660_read(file, buffer, len, &read);
        if (read == 0)
            break;
        buffer += read;
    }
    return L9660_OK;
}

l9660_status l9660_fsize(l9660_file_t *file, uint32_t *fsize)
{
    if (!file || !fsize)
        return L9660_EIO;
    *fsize = file->length;
    return L9660_OK;
}

l9660_status l9660_getstatus(l9660_fs_t *fs, char *path, status_t *status)
{
    path = stripVol(path);

    status_t _status;
    l9660_dirent_t *dirent;
    l9660_status stu = find_path(fs, path, &dirent);
    if (stu != L9660_OK)
        return stu;

    _status.st_size = READ32(dirent->size);
    memcpy(status, &_status, sizeof(status_t));
    return L9660_OK;
}