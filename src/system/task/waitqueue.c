// file: system/task/mutexqueue.c
// autor: jiangxinpeng
// time: 2021.4.3
// copyright: (C) 2020-2050 by jiangxinpeng,All right are reserved.

#include <os/mutexqueue.h>
#include <os/memcache.h>
#include <os/debug.h>
#include <os/mutexlock.h>
#include <os/schedule.h>
#include <os/safety.h>
#include <os/clock.h>
#include <os/timer.h>
#include <lib/list.h>
#include <os/waitqueue.h>
#include <os/task.h>
#include <os/schedule.h>
#include <os/spinlock.h>

void WaitQueueAdd(wait_queue_t *wait_queue, void *task)
{
    uint32_t eflags;

    SpinLockDisInterruptSave(&wait_queue->lock,eflags);
    if (!list_find(&((task_t *)task)->list, &wait_queue->wait_list))
    {
        list_add_tail(&((task_t *)task)->list, &wait_queue->wait_list);
        TASK_INTO_WAITLIST((task_t *)task);
    }
    SpinUnlockEnInterruptRestore(&wait_queue->lock,eflags);
}

void WaitQueueExit(wait_queue_t *wait_queue, void *task)
{
    uint32_t eflags;

    SpinLockDisInterruptSave(&wait_queue->lock,eflags);
    if (list_find(&((task_t *)task)->list, &wait_queue->wait_list))
    {
        list_del_init(&((task_t *)task)->list);
        TASK_EXIT_WAITLIST((task_t *)task);
    }
    SpinUnlockEnInterruptRestore(&wait_queue->lock,eflags);
}

void WaitQueueSleep(wait_queue_t *wait_queue)
{
    task_t *cur = cur_task;

    // add current task to wait queue
    WaitQueueAdd(wait_queue, cur);
    // task block
    TaskBlock(TASK_BLOCKED);
}

void WaitQueueWakeup(wait_queue_t *wait_queue)
{
    task_t *task = NULL;
    uint32_t eflags;

    SpinLockDisInterruptSave(&wait_queue->lock,eflags);
    if (!list_empty(&wait_queue->wait_list))
    {
        task = list_first_owner(&wait_queue->wait_list, task_t, list);
        if (task != NULL)
        {
            list_del_init(&task->list);
            TASK_EXIT_WAITLIST(task);
            TaskUnBlock(task);
        }
    }
    SpinUnlockEnInterruptRestore(&wait_queue->lock,eflags);
}

void WaitQueueWakeupAll(wait_queue_t *wait_queue)
{
    task_t *task = NULL;
    uint32_t eflags;

    SpinLockDisInterruptSave(&wait_queue->lock,eflags);
    list_traversal_all_owner_to_next(task, &wait_queue->wait_list, list)
    {
        // del task from watting list
        list_del_init(&task->list);
        TASK_EXIT_WAITLIST(task);
        TaskUnBlock(task);
    }
    SpinUnlockEnInterruptRestore(&wait_queue->lock,eflags);
}

uint64_t WaitQueueLen(wait_queue_t *queue)
{
    task_t *task;
    uint64_t count=0;
    uint32_t eflags;

    SpinLockDisInterruptSave(&queue->lock,eflags);
    list_traversal_all_owner_to_next(task, &queue->wait_list, list)
    {
        count++;
    }
    SpinUnlockEnInterruptRestore(&queue->lock,eflags);
    return count;
}