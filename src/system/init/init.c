#include <arch/interrupt.h>
#include <arch/config.h>
#include <os/mdl.h>
#include <os/driver.h>
#include <os/memcache.h>
#include <os/waitqueue.h>
#include <os/walltime.h>
#include <os/virmem.h>
#include <os/fs.h>
#include <os/hardirq.h>
#include <os/exception.h>
#include <os/softirq.h>
#include <os/mutexlock.h>
#include <os/schedule.h>
#include <os/timer.h>
#include <os/syscall.h>
#include <os/share.h>
#include <os/msgqueue.h>
#include <os/mutexqueue.h>
#include <os/account_group.h>
#include <os/fifo.h>
#include <os/vmm.h>
#include <os/clock.h>
#include <os/account.h>
#include <os/config.h>
#include <os/initcall.h>
#include <os/timer.h>
#include <os/task.h>
#include <os/msgqueue.h>
#include <os/sem.h>
#include <os/debug.h>
#include <os/swap.h>
#include <os/portcom.h>
#include <os/test.h>
#include <os/fifo.h>
#include <os/environ.h>

extern void SysCallTest();

// kernel main
// init system environment and into kernel
int KernelMain()
{
    InterruptDisable();
    KPrint(PRINT_INFO "Welcome to CatOS Kernel!\n");
    MemCachesInit();
    VirMemInit();
    IrqDescriptInit();
    SoftIrqInit();
    SysCallInit();
    AcpiInitAfter(); //init acpi interrupt
    ShareMemInit();
    WallTimeInit();
    SemInit();
    FifoInit();
    MutexQueueInit();
    MsgQueueInit();
    DiskManagerInit();
    #ifdef ENABLE_SMP
    // map APIC memory to virtual address space
    APICMapVbase();
    #endif
    ScheduleInit();
    TaskInitKernel();
    InterruptEnable();
    TimersInit();
    ClockInit();
    DriverFrameInit();
    InitCallsExec();
    DriverFrameDump();
    DiskInfoPrint();
    FsInit();
    EnvInit();
    AccountManagerInit();
    /*AccountGroupManagerInit();
    LogInit();*/
#if SWAP_ENABLE
    SwapInit();
#endif
    Test();
#if CONFIG_NET
    NetworkInit();
#endif
#ifdef ENABLE_SMP
    MpStartInit();
#endif
    KPrint(PRINT_INFO "OS init done.Start OS finished!\n");
    // start user process
    TaskStartUser();
}