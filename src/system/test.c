#include <os/kernelif.h>
#include <os/fs.h>
#include <os/fsal.h>
#include <os/task.h>
#include <os/swap.h>
#include <os/schedule.h>
#include <os/syscall.h>
#include <os/process.h>
#include <os/test.h>
#include <os/timer.h>
#include <os/driver.h>
#include <os/debug.h>
#include <lib/unistd.h>
#include <lib/type.h>
#include <sys/fcntl.h>
#include <os/dma.h>
#include <os/path.h>
#include <os/dirent.h>

// // /*
// // extern void AsmTest();

// // static void FsTest()
// // {
// //     int fd = KFileOpen("/test", O_CREATE | O_RDWR);
// //     if (fd < 0)
// //     {
// //         KPrint("[test] file create failed!\n");
// //         return 0;
// //     }
// //     uint32_t *w_buff = KMemAlloc(1024 * 4), *r_buff = KMemAlloc(1024 * 4);
// //     for (int i = 0; i < 1024; i++)
// //     {
// //         w_buff[i] = i;
// //     }
// //     memset(r_buff, 0, 1024 * 4);
// //     if (KFileWrite(fd, w_buff, 1024 * 4) < 0)
// //     {
// //         KPrint("[test] file write failed!\n");
// //         return;
// //     }
// //     KFileRewind(fd);
// //     if (KFileRead(fd, r_buff, 1024 * 4) < 0)
// //     {
// //         KPrint("[test] file read failed!\n");
// //         return;
// //     }
// //     if (memcmp(w_buff, r_buff, 1024 * 4))
// //     {
// //         KPrint("[test] fs r/w test err\n");
// //         return;
// //     }
// //     KPrint("[test] fixed r/w test ok\n");

// //     memset(r_buff, 0, 1024 * 4);
// //     if (KFileLSeek(fd, 20 * 4, SEEK_SET) < 0)
// //     {
// //         KPrint("[test] fs lseek err\n");
// //         return;
// //     }
// //     KFileRead(fd, r_buff, 5 * 4);
// //     if (memcmp(r_buff, &w_buff[20], 5 * 4))
// //     {
// //         KPrint("[test] fs r/w test err\n");
// //         return;
// //     }
// //     if (KFileLSeek(fd, 32 * 4, SEEK_SET) < 0)
// //     {
// //         KPrint("[test] fs lseek err\n");
// //         return;
// //     }
// //     KFileRead(fd, r_buff, 120 * 4);
// //     if (memcmp(r_buff, &w_buff[32], 120 * 4))
// //     {
// //         KPrint("[test] fs r/w test err\n");
// //         return;
// //     }
// //     KPrint("[test] fs rand read test ok\n");

// //     KFileRewind(fd);
// //     memset(w_buff, 0, 1024 * 4);
// //     memset(r_buff, 0, 1024 * 4);
// //     KFileWrite(fd, w_buff, 1024 * 4);
// //     KFileRewind(fd);
// //     KFileRead(fd, r_buff, 1024 * 4);
// //     if (memcmp(r_buff, w_buff, 1024 * 4))
// //     {
// //         KPrint("[test] fs r/w test err\n");
// //         return;
// //     }
// //     KPrint("[test] file clear ok\n");

// //     memset(w_buff, 0, 1024 * 4);
// //     memset(r_buff, 0, 1024 * 4);
// //     for (int i = 0; i < 1024; i++)
// //     {
// //         w_buff[i] = i;
// //     }
// //     KFileLSeek(fd, 512 * 4, SEEK_SET);
// //     KFileWrite(fd, &w_buff[512], 800 * 4);
// //     KFileLSeek(fd, 512 * 4, SEEK_SET);
// //     KFileRead(fd, r_buff, 800 * 4);
// //     if (memcmp(r_buff, &w_buff[512], 800 * 4))
// //     {
// //         KPrint("[test] fs r/w test err\n");
// //         KFileClose(fd);
// //         return;
// //     }
// //     KPrint("[test] rand write test ok\n");
// //     KFileClose(fd);
// // }

// // static void TimerHandler1(void *arg)
// // {
// //     timer_t *timer = (timer_t *)arg;
// //     KPrint("[test] timer out! ticks %d\n", timer->timeout);
// // }

// // static void TimerHandler2(void *arg)
// // {
// //     timer_t *timer = (timer_t *)arg;
// //     KPrint("[test] timer out! ticks %d\n", timer->timeout);
// // }

// // static void TimerTest()
// // {
// //     timer_t *timer = TimerAlloc();
// //     if (!timer)
// //         return -1;
// //     TimerModify(timer, 20);
// //     TimerSetHandler(timer, TimerHandler1);
// //     TimerSetArgument(timer, timer);
// //     TimerAdd(timer);

// //     timer_t *timer1 = TimerAlloc();
// //     TimerModify(timer1, 50);
// //     TimerSetHandler(timer1, TimerHandler2);
// //     TimerSetArgument(timer1, timer1);
// //     TimerAdd(timer1);
// //     KPrint("timer create ok!\n");
// //     TimerDump();
// // }

// // static void TTYFun()
// // {
// //     int fd = SysOpen("/dev/tty0", 0);
// //     if (fd < 0)
// //     {
// //         KPrint("[tty] open error\n");
// //         return;
// //     }

// //     char buff[32];
// //     SysRead(fd, buff, 23);
// //     KPrint("tty read end\n");
// //     CPUhlt();
// // }

// // static void TTYTest()
// // {
// //     TaskCreate("tt", TASK_PRIOR_LEVEL_NORMAL, TTYFun, NULL);
// //     while (1)
// //         ;
// // }*/

// static void SwapTest()
// {
//     uint8_t *page = KMemAlloc(PAGE_SIZE);
//     if (!page)
//     {
//         KPrint("[swap] alloc err\n");
//         return;
//     }
//     KPrint("[swap] alloc vaddr %x pyaddr %x pte %x\n",page,PAGE_ALIGN(Vbase2Pybase(page)),*(GetPteVptr(page))&PAGEBASE_MASK);
//     // uint32_t data[PAGE_SIZE / 4];
//     // for (int i = 0; i < PAGE_SIZE / 4; i++)
//     // {
//     //     data[i] = i;
//     // }
//     // memcpy(page, data, PAGE_SIZE);
//     // if (SwapOut(page) < 0)
//     // {
//     //     KPrint("[swap] swap out err\n");
//     //     return;
//     // }
//     // pte_t *pte=GetPteVptr(page);
//     // KPrint("[swap] swap out vpage %x pte %x present %d ok\n", page,*pte,*pte&PAGE_PRESENT);

//     // memset(page,0xff,PAGE_SIZE);

//     if (SwapIn(page) < 0)
//         return;
//     KPrint("[swap] swap in vpage %x ok\n", page);
//     // if (memcmp(page, data, PAGE_SIZE))
//     // {
//     //     KPrint("[swap] check err\n");
//     //     return;
//     // }
//     KPrint("[swap] swap test ok\n");
// }

// /*void LogTest()
// {
//     KPrint("--log test---\n");
//     LogDump();
// }

// void InputTest()
// {
//     input_event_buff_t buffer;
//     int ret=InputEventInit(&buffer);
//     if(ret<0)
//         return -1;
//     input_event_t event;
//     for(int i=0;i<6;i++)
//     InputEventPut(&buffer,&event);
// }*/

// // static void sleep_test_func()
// // {
// //     KPrint("sleep start\n");
// //     /*for(int i=0;i<10;i++)
// //         TaskSleepByTicks(2000);*/
// //     //ClockDelayByTicks(200);
// //     KPrint("wakeup\n");
// // }

// // void SleepTest()
// // {
// //     TaskCreate("sleep test",TASK_PRIOR_LEVEL_NORMAL,sleep_test_func,NULL);
// //     while(1);
// // }

// /*void DmaTest()
// {
//     KPrint("----dma test-----\n");
//     dma_region_t dma;
//     dma.p.size=PAGE_SIZE;
//     dma.flags=DMA_REGION_SPECIAL;
//     dma.p.alignment=PAGE_SIZE;
//     uint32_t res=DmaAllocBuffer(&dma);
//     if(res<0)
//     {
//         KPrint("dma alloc error\n");
//         return -1;
//     }
//     KPrint("Dma alloc ok\n");
//     DmaBufferDump(&dma);
// }*/

// static void IsoTest()
// {
//     int res=KFileMount("/dev/cdrom0","/mnt","iso9660",0);
//     if(res<0)
//     {
//         KPrint("[iso test] mount error\n");
//         return -1;
//     }
//     KPrint("[iso test] mount ok\n");
// }

void Test()
{
    // int handle = DeviceOpen("cdrom0", 0);
    // if (handle < 0)
    // {
    //     KPrint("[test] open cdrom0 ok");
    //     return -1;
    // }
    // char buff[2048];
    // DeviceRead(handle, buff, 2048, 16);
    // if (buff[1] == 'C' && buff[2] == 'D' && buff[3] == '0' && buff[4] == '0' & buff[5] == '1')
    // {
    //     KPrint("[cdrom] is ISO9660 filesystem\n");
    //     return 1;
    // }
    if (KFileMount("/dev/cdrom0", "/mnt", "iso9660", 0) < 0)
    {
        KPrint("[cdrom] mount fs error\n");
        return -1;
    }
    int dir = KFileOpenDir("/mnt");
    if (dir < 0)
    {
        KPrint("[cdrom] open dir error\n");
        return;
    }
    dirent_t dirent;
    KFileReadDir(dir, &dirent);

    // FsTest();
    // SwapTest();
    // AsmTest();
    // ExceptionTest();
    /*LogTest();
    CPUhlt();*/
    // InputTest();
    // SleepTest();
    // DmaTest();
    // CPUhlt();
    // IsoTest();
}
