#include <os/initcall.h>
#include <os/debug.h>

extern initcall_t __initcall_start[];
extern initcall_t __initcall_end[];
extern exitcall_t __exitcall_start[];
extern exitcall_t __exitcall_end[];

void InitCallsExec()
{
    initcall_t *func = __initcall_start;

    for (; func < __initcall_end; func++)
    {
        (*func)();
    }
}

void ExitCallsExec()
{
    exitcall_t *func = __exitcall_start;

    for (; func < __exitcall_end; func++)
    {
        (*func)();
    }
}
