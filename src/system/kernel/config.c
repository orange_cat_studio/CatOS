#include <os/config.h>
#include <os/cpu.h>
#include <arch/page.h>
#include <os/file.h>
#include <os/task.h>
#include <os/fs.h>
#include <os/driver.h>
#include <arch/acpi.h>
#include <arch/page.h>
#include <arch/smbios.h>
#include <os/kernel.h>
#include <os/safety.h>
#include <lib/errno.h>
#include <lib/type.h>
#include <os/config.h>

int SysConfig(int name)
{
    switch (name)
    {
    case SC_HOST_NAME_LEN: // host name len
        return HOST_NAME_LEN;
    case SC_PAGESIZE: // page size
        return PAGE_SIZE;
    case SC_OPENMAX: // file max opens
        return LOCAL_FILE_MAX;
    case SC_TTY_NAME_LEN: // tty name len
        return DEVICE_NAME_LEN;
    case SC_VERSION: // version
        return (uint32_t)KERNEL_VERSION;
    case SC_CLK_TCK: // clock ticks
        return HZ;
    case SC_ARG_MAX: // task arg max
        return TASK_STACK_ARG_MAX;
    case SC_ARCH:
        return (uint32_t)__ARCH_STR;
    case SC_CPUWIDTH:
        return CONFIG_CPU_WIDTH;
    default:
        break;
    }
    return -1;
}

int SysGetHostName(char *name, size_t len)
{
    if (!name || !len)
        return -EINVAL;
    strncpy(name, KERNEL_NAME, min(len, KERNEL_NAME_LEN));
    return 0;
}

// support acpi and smbios table
void *SysGetFireworkTable(char *table, int namelen, uint64_t *table_len)
{
    if (!table || !namelen || !table_len)
        return NULL;


    if (!strcmp(table, "RSDT"))
    {
        *table_len = 0;
        return ACPI_TABLE_RSDT;
    }
    else
    {
        if (!strcmp(table, "FACP"))
        {
            *table_len = 0;
            return ACPI_TABLE_FACP;
        }
        else
        {
            if (!strcmp(table, "SMBIOS"))
            {
                *table_len = smbios->table_total_len;
                return SMBIOS_TABLE;
            }
        }
    }
    return NULL;
}

int SysGetVer(char *buff, int len)
{
    char tmp_buff[32];

    if (!buff || !len)
        return -EINVAL;

    memset(tmp_buff, 0, 32);
    strcpy(tmp_buff, KERNEL_NAME);
    strcat(tmp_buff, "-");
    strcat(tmp_buff, KERNEL_VERSION);

    MemCopyToUser(buff, tmp_buff, min(len, strlen(tmp_buff)));

    return 0;
}