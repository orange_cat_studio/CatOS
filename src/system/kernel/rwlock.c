//file:system/kernel/rwlock.c
//autor:jiangxinpeng
//time:2021.4.12
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#include<os/rwlock.h>
#include<lib/stddef.h>

void RwLockInit(rwlock_t *lock,rwlock_arg_t arg)
{
    lock->count=0;

    MutexlockInit(&lock->count_mutex);
    MutexlockInit(&lock->rw_mutex);
    MutexlockInit(&lock->write_mutex);

    switch(arg)
    {
        case RWLOCK_RD_FIRST:
            lock->read_lock=_RwLockReadLockRdFirst;
            lock->read_unlock=_RwLockReadUnlockRdFirst;
            lock->write_lock=_RwLockWriteLockRdFirst;
            lock->write_unlock=_RwLockWriteUnLockRdFirst;
            break;
        case RWLOCK_WR_FIRST:
            lock->read_lock=_RwLockReadLockWrFirst;
            lock->read_unlock=_RwLockReadUnLockWrFirst;
            lock->write_lock=_RwLockWriteLockWrFirst;
            lock->write_unlock=_RwLockWriteUnLockWrFirst;
            break;
        case RWLOCK_RW_FAIR:
            lock->read_lock=_RwLockReadLockRwFair;
            lock->read_unlock=_RwLockReadUnLockRwFair;
            lock->write_lock=_RwLockWriteLockRwFair;
            lock->write_unlock=_RwLockWriteUnLockRwFair;
            break;
        default:
            lock->read_lock=lock->read_unlock=lock->write_lock=lock->write_unlock=NULL;
            break;
    } 
}