
#ifndef __ARCH_ADDRESS_H
#define __ARCH_ADDRESS_H

#include <lib/type.h>

//how much ards block
#define ARDS_MAX 12

typedef struct ards
{
    uint32_t base_low;
    uint32_t base_high;
    uint32_t size_low;
    uint32_t size_high;
    uint32_t type;
} ards_t;

typedef struct ardsinfo
{
    uint32_t count;
    struct ards ards[ARDS_MAX];
}ardsinfo_t;

uint64_t GetPymemSize(); //init ards
#endif