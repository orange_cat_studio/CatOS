#ifndef ARCH_EFLAGS_H
#define ARCH_EFLAGS_H

#include <lib/type.h>
#include <arch/interrupt.h>
#include <arch/x86.h>

#define EFLAGS_CF 0x0
#define EFLAGS_PF 0x4
#define EFLAGS_AF 0x10
#define EFLAGS_ZF 0x40
#define EFLAGS_SF 0x80
#define EFLAGS_TF 0x100
#define EFLAGS_IF 0x200
#define EFLAGS_DF 0x400
#define EFLAGS_OF 0x800
#define EFLAGS_IODPL0 0x1000
#define EFLAGS_IODPL1 0x2000
#define EFLAGS_IODPL2 0x3000
#define EFLAGS_IODPL3 0x4000
#define EFLAGS_NT 0x8000
#define EFLAG_RF 0x20000
#define EFLAGS_VM 0x40000
#define EFLAGS_AC 0x80000
#define EFLAGS_VIF 0x100000
#define EFLAGS_VIP 0x200000
#define EFLAGS_ID 0x400000

static inline void SetEflags(uint32_t eflags)
{
    LoadEflag(eflags);
}

static inline void SaveEfalgs(uint32_t *eflags)
{
    *(uint32_t *)eflags = StoreEflag();
}

#endif