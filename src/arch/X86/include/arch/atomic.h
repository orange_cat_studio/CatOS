//file:arch/x86/include/arch/atomic.h
//autor:jiangxinpeng
//time:2021.1.15
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef ARCH_ATOMIC_H
#define ARCH_ATOMIC_H

#include <lib/type.h>

typedef struct
{
    volatile int value;
} atomic_t;

#define AtomicInit(atomic, val) (atomic->value = val)
#define AtomicRead(atomic) ((atomic)->value)

#define ATOMIC_INIT(val) \
    {                    \
        (val)              \
    }

extern void MemAtomicAdd(volatile int *a, int b);
extern void MemAtomicSub(volatile int *a, int b);
extern void MemAtomicInc(volatile int *a);
extern void MemAtomicDec(volatile int *a);
extern void MemAtomicAnd(volatile int *a, int b);
extern void MemAtomicOr(volatile int *a, int b);
extern int MemAtomicXCHG(volatile int *a, int val);
extern void MemAtomicSet(volatile int *a,int val);

int __attribute__((optimize("O0"))) AtomicAdd(atomic_t *atomic, int val);
int __attribute__((optimize("O0"))) AtomicSub(atomic_t *atomic, int val);
int __attribute__((optimize("O0"))) AtomicInc(atomic_t *atomic);
int __attribute__((optimize("O0"))) AtomicDec(atomic_t *atomic);
int __attribute__((optimize("O0"))) AtomicAnd(atomic_t *atomic, int val);
int __attribute__((optimize("O0"))) AtomicOr(atomic_t *atomic, int val);
int __attribute__((optimize("O0"))) AtomicXCHG(atomic_t *atomic, int val);
int __attribute__((optimize("O0"))) AtomicGet(atomic_t *atomic);
int __attribute__((optimize("O0"))) AtomicSet(atomic_t *atomic,int val);


#endif