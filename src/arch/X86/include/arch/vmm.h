#ifndef ARCH_VMM_H
    #define ARCH_VMM_H

    #include<os/vmm.h>

    void VmmActive(vmm_t *vmm);
    void VmmActiveKernel();
    void VmmActiveUser(uint32_t pdir);
#endif