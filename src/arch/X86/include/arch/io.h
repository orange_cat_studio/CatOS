#ifndef X86_IO_H
    #define X86_IO_H

    #include<arch/x86.h>

    #define In8 _In8
    #define In16 _In16
    #define In32 _In32
    #define Out8 _Out8
    #define Out16 _Out16
    #define Out32 _Out32
    
    #define IoWrite OutBytes
    #define IoRead  InBytes

    static void OutBytes(uint16_t port,uint8_t *buff,uint64_t bytes)
    {
        uint8_t *p=buff;

        while(bytes--)
        {
            Out8(port,*p++);
        }
    }

    static void InBytes(uint16_t port,uint8_t *buff,uint64_t bytes)
    {
        uint8_t *p=buff;

        while(bytes--)
        {
            *buff++=In8(port);
        }
    }

#endif