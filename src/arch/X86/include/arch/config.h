#ifndef X86_CONFIG_H
#define X86_CONFIG_H

// hareware config
//#define X86_CONSOLE_HW // enable console hardware
#define X86_SERIAL_HW // enable serial hardware

// enable smp support
// #define ENABLE_SMP

// enable driver debug
#define DRIVER_DEBUG

// enable pci debug
// #define DEBUG_PCI

// enable vbe mode
// #define KERNEL_VBE_MODE

#endif