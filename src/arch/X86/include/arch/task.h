//file: arch/x86/include/arch/task.h
//autor: jiangxinpeng
//time: 2021.1.19
//copyright: (C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef _ARCH_TASK_H
    #define _ARCH_TASK_H

    #include<lib/type.h>
    #include<arch/interrupt.h>
    #include<os/task.h>

    #define UserSetEntryPoint(frame,entry) ((frame)->eip=(entry))

    void UserFrameInit(trap_frame_t *frame);
    void UserThreadFrameBuild(trap_frame_t *fram,void *arg,void *func,void *thread_entry,void *stack_top);
    void KernelFrameInit(trap_frame_t *frame);
    void KernelThreadEntry(task_func_t function, void *arg);
    void TaskSwitchToNext(void *prev,void *next);
    void TaskStackBuild(task_t *task,task_func_t fun,void *arg);
    int TaskStackBuildWhenForking(task_t *child);
    int ProcessFrameInit(task_t *task, trap_frame_t *frame, char **argv, char **envp);
    extern void KernelSwitchToUser(trap_frame_t *frame);
    void ThreadStackDump(thread_stack_t *kstack);

#endif