// file: arch/x86/include/arch/cpu.h
// autor:jiangxinpeng
// time:2021.1.17
// copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef X86_CPU_H
#define X86_CPU_H

#include <lib/type.h>
#include <arch/time.h>

// max support cpu number
#define CPU_NUM_MAX 4

// cpuid instruction
// function
#define CPU_GETVENDORID 0x00
#define CPU_GETMODEL 0x01
#define CPU_GETSTEPPING 0x01
#define CPU_GETFAMILY 0x01
#define CPU_GETTYPE 0x01

// brand
#define CPU_SUPPORTBRAND 0x80000000
#define CPU_GETBRAND1 0x80000002
#define CPU_GETBRAND2 0x80000003
#define CPU_GETBRAND3 0x80000004

// CPU feature
#define CPU_FEATURE_LOCAL_APIC 0x100 // support local APIC

// cpu vendor id MICRO
#define CPU_VENDOR_INTER GenuineIntel
#define CPU_VENDOR_AMD AuthenticAMD
#define CPU_VENDOR_VMware VMwareVMware
#define CPU_VENDOR_HV Microsoft Hv

// cpu exec nop instruction
static inline void CpuIdle()
{
    __asm__ __volatile__("nop");
}

static inline void CpuPause()
{
    __asm__ __volatile__("pause");
}

static inline void CpuSleep()
{
    __asm__ __volatile__("hlt");
}

void CpuInit();
uint32_t CpuGetAttachList(cpuid_t *list);
cpuid_t CpuGetMyId();

// global cpu nubmers
extern uint32_t cpunum;
extern cpuid_t cpu_list[CPU_NUM_MAX];

#define CpuDoNoting CpuIdle
#define CpuDoDelay UDelay
#define CpuDoPause CpuPause
#define CpuDoSleep CpuSleep
#endif