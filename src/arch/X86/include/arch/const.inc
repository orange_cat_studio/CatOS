;file: x86/include/arch/const.inc
;autor: jiangxinpeng
;time: 2021.10.16
;copyright: (C) 2020-2050 by jiangxinpeng,All right are reserved.

%ifndef _CONST
%define _CONST

%define KERNEL_VIR_ADDR  0x80000000
%define KERNEL_STACK_TOP 0x0009f000

%define ENABLE_SMP 0

%define KERNEL_VIR_STACK_TOP KERNEL_STACK_TOP+KERNEL_VIR_ADDR
%endif
