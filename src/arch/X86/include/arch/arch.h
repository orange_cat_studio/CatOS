//filename:arch\x86\include\kernel\core\arch.h
//autor:jiang xinpeng
//time:2020.12.7
//copyright:(C) 2020-2050 by Jiang xinpeng. All rights reserved.

#ifndef X86_ARCH_H
#define X86_ARCH_H

//arch define
#define __ARCH_X86
//#define __ARCH_X64

#ifndef ARCH
    //arch list
    #define X86 0
    #define X64 1

    #ifdef __ARCH_X86
        //kernel arch
        #define __ARCH X86
    #else
        #ifdef __ARCH_X64
            #define __ARCH X64
        #else
            #error "__ARCH error!"
        #endif
    #endif
#endif

//init arch
void ArchInit();
void ArchInitAfterPage();

#endif
