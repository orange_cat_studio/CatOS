// file:arch/x86/include/arch/cmos.h
// autor:jiang xinpeng
// time:2020.12.7
// copyright:(C) 2020-2050 by Jiang xinpeng. All rights reserved.
#ifndef X86_CMOS_HEADER
#define X86_CMOS_HEADER

#include <lib/type.h>

// cmos io port
#define CMOS_ADPORT 0x70
#define CMOS_DATAPORT 0x71

// comos data offset
#define CMOS_SEC 0x0
#define CMOS_MIN 0x2
#define CMOS_HOUR 0x4
#define CMOS_WEEKDAY 0x6
#define CMOS_DAYS 0x7
#define CMOS_MON 0x8
#define CMOS_YEAR 0x9
#define CMOS_CEN 0x32

PUBLIC uint32_t CMOS_GetHour_HEX();
PUBLIC uint32_t CMOS_GetMinute_HEX();
PUBLIC uint32_t CMOS_GetSecond_HEX();
PUBLIC uint32_t CMOS_GetDayOfMonth_HEX();
PUBLIC uint32_t CMOS_GetDayOfWeek_HEX();
PUBLIC uint32_t CMOS_GetMon_HEX();
PUBLIC uint32_t CMOS_GetYear_HEX();
PUBLIC void DumpCMOS();


#define BCD_HEX(num) ((((num) >> 4) * 10) + ((num)&0xf))

#define BCD_ASCII_FIRST(num) (((num)&0xf) + 0x30)
#define BCD_ASCII_SECOND(num) (((num) >> 4) + 0x30)

#endif
