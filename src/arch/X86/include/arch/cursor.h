//file:arch/x86/include/arch/cursor.h
//autor: jiang xinpeng
//time:2021.2.1
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#include<lib/type.h>

#ifndef CURSOR
    #define CURSOR

    #define CRT_ADDRESS_PORT 0x3d4
    #define CRT_DATA_POST 0x3d5
    
    #define CURSOR_POSITON_HIGH8_PORT 0xE
    #define CURSOR_POSITION_LOW8_PORT 0xF
    
        
    #define MAXLINE 25
    #define MAXLIST 80
    #define MAXPAGE 8
    #define MINLINE 1
    #define MINLIST 1
    #define CHARS_PER_LINE MAXLIST


    //cursor init position
    #define CURSOR_INIT 0       

    struct cursor_info
    {
        uint8_t line;
        uint8_t list;
    };

    //InitCursor to 0x0
    void InitCursor(int x,int y);
    //set cursor 
    void SetCursor(int x,int y);
    //return cursor info use a struct block
    void GetCursor(int x,int y);
    //write cursor position to io port
    int WriteCursor(uint16_t position);
    //read position data from io port
    uint16_t ReadCursor();
#endif