//file：arch/x86/hardware/text.h
//autor:jiangxinpeng
//time:2021.2.2
//copyright:(C) 2020-2050 by Jiang xinpeng. All rights reserved.

#include <lib/type.h>
#include <arch/page.h>

#ifndef __ARCH_TEXT_H
#define __ARCH_TEXT_H

#define TEXT_BUFFER_ADDRESS ((uint8_t*)0xb8000)
#define TEXT_BUFFER_SIZE 0x8000

//text attribute filed format
#define TEXT_CHAR_OFF 0
#define TEXT_COLOR_OFF 1

//max chars
#define TEXT_CHARS_MAX (80 * 25)

//standard color micro
#define TEXT_FRONTCOLOR_BLACK 0x0
#define TEXT_FRONTCOLOR_BULE 0x1
#define TEXT_FRONTCOLOR_GREEN 0x2
#define TEXT_FRONTCOLOR_CYAN 0x3
#define TEXT_FRONTCOLOR_RED 0x4
#define TEXT_FRONTCOLOR_MAGENTA 0x5
#define TEXT_FRONTCOLOR_BROWN 0x6
#define TEXT_FRONTCOLOR_LIGHTGRAY 0x7
#define TEXT_FRONTCOLOR_DARKGRAY 0x8
#define TEXT_FRONTCOLOR_LIGHTBLUE 0X9
#define TEXT_FRONTCOLOR_LIGHTGREEN 0xA
#define TEXT_FRONTCOLOR_LIGHTCYAN 0xB
#define TEXT_FRONTCOLOR_LIGHTRED 0xC
#define TEXT_FRONTCOLOR_LIGHTMAGENTA 0xD
#define TEXT_FRONTCOLOR_YELLOW 0xE
#define TEXT_FRONTCOLOR_WHITE 0xF

#define TEXT_BACKGROUNTCOLOR_BLACK 0x0
#define TEXT_BACKGROUNTCOLOR_BULE 0x1
#define TEXT_BACKGROUNTCOLOR_GREEN 0x2
#define TEXT_BACKGROUNTCOLOR_CYAN 0x3
#define TEXT_BACKGROUNTCOLOR_RED 0x4
#define TEXT_BACKGROUNTCOLOR_MAGENTA 0x5
#define TEXT_BACKGROUNTCOLOR_BROWN 0x6
#define TEXT_BACKGROUNTCOLOR_LIGHTGRAY 0x7
#define TEXT_BACKGROUNTCOLOR_DARKGRAY 0x8
#define TEXT_BACKGROUNTCOLOR_LIGHTBLUE 0X9
#define TEXT_BACKGROUNTCOLOR_LIGHTGREEN 0xA
#define TEXT_BACKGROUNTCOLOR_LIGHTCYAN 0xB
#define TEXT_BACKGROUNTCOLOR_LIGHTRED 0xC
#define TEXT_BACKGROUNTCOLOR_LIGHTMAGENTA 0xD
#define TEXT_BACKGROUNTCOLOR_YELLOW 0xE
#define TEXT_BACKGROUNTCOLOR_WHITE 0xF

void TextPutChar(uint8_t ch, uint8_t color);
void TextPutString(uint8_t *str, uint8_t color);
void TextCleanScreen(uint8_t color);
void TextCopy(int srcx, int srcy, int x, int y);
void TextClearArea(int x, int y, int size, int color);

#endif
