#ifndef __ARCH_CONST_H
#define __ARCH_CONST_H

#define KB 1024
#define MB (KB * 1024)
#define GB (MB * 1024)
#define TB (GB * 1024)

#ifndef _WORDSZ
#define _WORDSZ 2
#endif

#define WORDSZ _WORDSZ

#endif