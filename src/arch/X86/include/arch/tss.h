// file:arch/x86/include/arch/tss.h
// autor:jiangxinpeng
// time:2021.1.16
// copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef X86_TSS_H
#define X86_TSS_H

#include <lib/type.h>
#include <arch/descript.h>

typedef struct tss
{
    struct tss *pre_tss; // point prior tss
    uint32_t esp0;       // DPL0 stack
    uint32_t ss0;
    uint32_t esp1; // DPL1 stack
    uint32_t ss1;
    uint32_t esp2; // DPL2 stack
    uint32_t ss2;
    uint32_t cr3;    // current PDT base register
    uint32_t eip;    // current PC
    uint32_t eflags; // eflags register
    uint32_t eax;    // general register
    uint32_t ecx;
    uint32_t edx;
    uint32_t ebx;
    uint32_t esp;
    uint32_t ebp;
    uint32_t esi;
    uint32_t edi;
    uint32_t es; // segment register
    uint32_t cs;
    uint32_t ss;
    uint32_t ds;
    uint32_t fs;
    uint32_t gs;
    uint32_t ldt_sel; // LDT sel
    uint32_t io_map;  // io map base
} tss_t;

extern volatile tss_t tss0; // tss for BSP

#define DISIOMAP ((sizeof(tss_t) - 1) << 16)

void TssInit();
tss_t *GetCurrentTSS();
void UpdateTSS(address_t task);
void DumpTss(tss_t *tss);

#endif