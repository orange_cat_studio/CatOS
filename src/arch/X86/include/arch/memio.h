#ifndef X86_MEMIO_H
    #define X86_MEMIO_H

    #include<lib/type.h>

    void HalMemIoReMap(uint32_t vbase,uint32_t pybase,uint32_t len);
    void HalMemIoUnMap(uint32_t vbase,uint32_t len);
#endif