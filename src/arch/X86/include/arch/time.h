//file:arch/x86/include/arch/time.h
//autor:jiangxinpeng
//time:2021.1.19
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef ARCH_TIME_H
    #define ARCH_TIME_H

    #include<arch/cmos.h>

    //#define HZ (uint32_t)(100*10) //clock HZ
    #define HZ (uint32_t)(100) //clock HZ
    #define TimerHZ CurTimerFrequery 
    
    void _PitClockInit();
    void _delay(int usec);

    #define ClockHardwareInit  _PitClockInit
    #define TimeGetHour   CMOS_GetHour_HEX
    #define TimeGetSecond CMOS_GetSecond_HEX
    #define TimeGetMinute CMOS_GetMinute_HEX
    #define TimeGetDay    CMOS_GetDayOfMonth_HEX
    #define TimeGetWeek   CMOS_GetDayOfWeek_HEX
    #define TimeGetMonth  CMOS_GetMon_HEX
    #define TimeGetYear   CMOS_GetYear_HEX
    #define UDelay        _delay
#endif