// file:arch\x86\include\arch\pymem.h
// autor:jiang xinpeng
// time:2020.12.18
// copyright:(C) 2020-2050 by jiang xinpeng,All right are reserved.

#ifndef X86_PYMEM_HEADER
#define X86_PYMEM_HEADER

#include <os/config.h>
#include <os/memcache.h>
#include <lib/type.h>
#include <arch/page.h>
#include <lib/stddef.h>
#include <lib/math.h>

// file:arch\x86\include\arch\pymem.h
// autor:jiang xinpeng
// time:2020.12.18
// copyright:(C) 2020-2050 by jiang xinpeng,All right are reserved.

#ifndef X86_PYMEM_HEADER
#define X86_PYMEM_HEADER

#include <os/config.h>
#include <os/memcache.h>
#include <lib/type.h>
#include <arch/page.h>
#include <lib/stddef.h>
#include <lib/math.h>

// file:arch\x86\include\arch\pymem.h
// autor:jiang xinpeng
// time:2020.12.18
// copyright:(C) 2020-2050 by jiang xinpeng,All right are reserved.

#ifndef X86_PYMEM_HEADER
#define X86_PYMEM_HEADER

#include <os/config.h>
#include <os/memcache.h>
#include <lib/type.h>
#include <arch/page.h>
#include <lib/stddef.h>
#include <lib/math.h>

// pymem area space situation
// arch mem area 0-1MB (1MB)
#define BIOS_MEM_BASE 0x00000000
#define BIOS_MEM_SIZE (1 * MB)
// kernel image area 1-3MB (2MB)
#define KERNELSELF_MEM_BASE BIOS_MEM_BASE + BIOS_MEM_SIZE
#define KERNELSELF_MEM_SIZE (2 * MB)
// system data struct area 3-5MB (2MB)
#define SYSDATA_MEM_BASE KERNELSELF_MEM_BASE + KERNELSELF_MEM_SIZE
#define SYSDATA_MEM_SIZE (2 * MB)
// dma area 5-16MB(11MB)
#define DMA_MEM_BASE SYSDATA_MEM_BASE + SYSDATA_MEM_SIZE
#define DMA_MEM_SIZE (11 * MB)
// above dma area is command mem area(start of 16MB above)
#define KERNEL_MEM_BASE DMA_MEM_BASE + DMA_MEM_SIZE


// vmm space
#define KERNEL_SPACE_TOP_ADDR 0xffffffff
#define USER_SPACE_START_ADDR 0x00000000
#define USER_SPACE_SIZE 0x80000000

// page dir pyaddress map(4MB)
// map to vmem high 4MB
#define NULL_MEM_SIZE (256 * MB) // 4MB

#define KERNEL_LIMIT_MEM_ADDR (KERNEL_SPACE_TOP_ADDR - NULL_MEM_SIZE + 1)

// boot mem
#define BOOT_MEM_SIZE (42 * MB)
#define BOOT_MEM_BASE KERNEL_MEM_BASE

// dynamic map mem area
#define DYNAMIC_MAP_MEM_SIZE (128 * MB)
#define DYNAMIC_MAP_MEM_VBASE (KERNEL_LIMIT_MEM_ADDR - DYNAMIC_MAP_MEM_SIZE)
#define DYNAMIC_MAP_MEM_END (KERNEL_LIMIT_MEM_ADDR)

// kernel stack
#define MEM_KERNEL_STACK_TOP (KERNEL_VMM_BASE + 0x9f000)
#define MEM_KERNEL_STACK_BOTTOM (MEM_KERNEL_STACK_TOP - TASK_KERNEL_STACK_SIZE)

// mem node alloc status
#define MODE_NODE_FREE 0
#define MEM_NODE_USE 1

// mem mode
#define MEM_NODE_DMA MEM_RANGE_DMA
#define MEM_MODE_KERNEL MEM_RANGE_KERNEL
#define MEM_MODE_USER MEM_RANGE_USER

void PymemInit();
void CutBootMem();
uint32_t GetPymemPageCount();

#define MemGetTotalSize() (GetPymemPageCount() * PAGE_SIZE)

#endif

// vmm space
#define KERNEL_SPACE_TOP_ADDR 0xffffffff
#define USER_SPACE_START_ADDR 0x00000000
#define USER_SPACE_SIZE 0x80000000

// page dir pyaddress map(4MB)
// map to vmem high 4MB
#define NULL_MEM_SIZE (256 * MB) // 4MB

#define KERNEL_LIMIT_MEM_ADDR (KERNEL_SPACE_TOP_ADDR - NULL_MEM_SIZE + 1)

// boot mem
#define BOOT_MEM_SIZE (42 * MB)
#define BOOT_MEM_BASE KERNEL_MEM_BASE

// dynamic map mem area
#define DYNAMIC_MAP_MEM_SIZE (128 * MB)
#define DYNAMIC_MAP_MEM_VBASE (KERNEL_LIMIT_MEM_ADDR - DYNAMIC_MAP_MEM_SIZE)
#define DYNAMIC_MAP_MEM_END (KERNEL_LIMIT_MEM_ADDR)

// kernel stack
#define MEM_KERNEL_STACK_TOP (KERNEL_VMM_BASE + 0x9f000)
#define MEM_KERNEL_STACK_BOTTOM (MEM_KERNEL_STACK_TOP - TASK_KERNEL_STACK_SIZE)

// mem node alloc status
#define MODE_NODE_FREE 0
#define MEM_NODE_USE 1

// mem mode
#define MEM_NODE_DMA MEM_RANGE_DMA
#define MEM_MODE_KERNEL MEM_RANGE_KERNEL
#define MEM_MODE_USER MEM_RANGE_USER

void PymemInit();
void CutBootMem();
uint32_t GetPymemPageCount();

#define MemGetTotalSize() (GetPymemPageCount() * PAGE_SIZE)

#endif

// vmm space
#define KERNEL_SPACE_TOP_ADDR 0xffffffff
#define USER_SPACE_START_ADDR 0x00000000
#define USER_SPACE_SIZE 0x80000000

// page dir pyaddress map(4MB)
// map to vmem high 4MB
#define NULL_MEM_SIZE (256 * MB) // 4MB

#define KERNEL_LIMIT_MEM_ADDR (KERNEL_SPACE_TOP_ADDR - NULL_MEM_SIZE + 1)

// boot mem
#define BOOT_MEM_SIZE (42 * MB)
#define BOOT_MEM_BASE KERNEL_MEM_BASE

// dynamic map mem area
#define DYNAMIC_MAP_MEM_SIZE (128 * MB)
#define DYNAMIC_MAP_MEM_VBASE (KERNEL_LIMIT_MEM_ADDR - DYNAMIC_MAP_MEM_SIZE)
#define DYNAMIC_MAP_MEM_END (KERNEL_LIMIT_MEM_ADDR)

// kernel stack
#define MEM_KERNEL_STACK_TOP (KERNEL_VMM_BASE + 0x9f000)
#define MEM_KERNEL_STACK_BOTTOM (MEM_KERNEL_STACK_TOP - TASK_KERNEL_STACK_SIZE)

// mem node alloc status
#define MODE_NODE_FREE 0
#define MEM_NODE_USE 1

// mem mode
#define MEM_NODE_DMA MEM_RANGE_DMA
#define MEM_MODE_KERNEL MEM_RANGE_KERNEL
#define MEM_MODE_USER MEM_RANGE_USER

void PymemInit();
void CutBootMem();
uint32_t GetPymemPageCount();

#define MemGetTotalSize() (GetPymemPageCount() * PAGE_SIZE)

#endif