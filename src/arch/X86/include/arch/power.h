#ifndef X86_POWER_H
    #define X86_POWER_H

    extern void PowerInit();

    void SysReboot();
    void SysPowerOff();
    void Reboot();
    void Halt();

    
#endif