// file:include/arch/fpu.h
// autor:jiangxinpeng
// time:2021.6.20
// copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.
#ifndef X86_FPU_H
#define X86_FPU_H

#include <lib/string.h>

typedef struct
{
    uint64_t cwd;
    uint64_t swd;
    uint64_t twd;
    uint64_t fip;
    uint64_t fcs;
    uint64_t foo;
    uint64_t fos;
    uint64_t st_space[20];
    uint64_t status;
} fpu_storage_t;

typedef struct fpu
{
    fpu_storage_t storage;
} fpu_t;

static inline void FpuInit(fpu_t *fpu, int reg)
{
    if (reg)
        __asm__ __volatile__("fninit");
    memset(&fpu->storage, 0, sizeof(fpu_storage_t));
}

static inline void FpuSave(fpu_t *fpu)
{
    __asm__ __volatile__("fnsave %0 ; fwait"
                         : "=m"(fpu->storage));
}

static inline void FpuRestore(fpu_t *fpu)
{
    __asm__ __volatile__("frstor %0	\n\t"
                         :
                         : "m"(fpu->storage));
}
#endif