#ifndef __ARCH_SWAPSWITCH_H
#define __ARCH_SWAPSWITCH_H

#include <os/config.h>

#if SWAP_ENABLE

#include <lib/type.h>
#include <lib/list.h>
#include <arch/page.h>

typedef struct
{
    pte_t *pte;
    uint32_t count;
    list_t list;
} page_info_t;

void SwapSwitchInit();
page_info_t *SwapSwitchPageAlloc(pte_t *pte);
void SwapSwitchPageFree(page_info_t *page_info);
page_info_t *SwapSwitchFindByPte(pte_t *pte);

#endif

#endif