// file：arch/x86/hardware/cursor.c
// autor:jiangxinpeng
// time:2021.2.2
// copyright:(C) 2020-2050 by Jiang xinpeng. All rights reserved.

#ifndef ARCH_CONSOLE_H
#define ARCH_CONSOLE_H

#include <lib/type.h>

// console mode
#define CONSOLE_MODE_TEXT 0
#define CONSOLE_MODE_GRAPHICS 1

// console show buffer
#define CONSOLE_TEXT_BUFFER 0xb8000
#define CONSOLE_GRAPHICS_BUFFER 0xe0000

// console windows size
#define CONSOLE_HIGH 25
#define CONSOLE_WEIGHT 80

// console position
#define CONSOLE_X 0
#define CONSOLE_Y 0

// console limit
#define CONSOLE_X_MAX CONSOLE_X + CONSOLE_WEIGHT
#define CONSOLE_Y_MAX CONSOLE_Y + CONSOLE_HIGH

// console color mask
#define CONSOLE_COLOR_MASK(back, front) ((back << 4) | front)

// console scroll director
#define CONSOLE_SCROLL_UP 0
#define CONSOLE_SCROLL_DOWN 1

// whether enable cursor
#define CONSOLE_HAS_CURSOR 1

// console
typedef struct
{
    uint32_t x;              // console position x
    uint32_t y;              // console position y
    uint32_t high;           // console high
    uint32_t weight;         // console weight
    uint32_t size;           // console windows size
    uint8_t background;      // console backgroud
    uint8_t front;           // console front
    uint32_t cur_line;       // current line
    uint32_t cur_list;       // current list
    uint32_t scrollposition; // console scroll position
    uint32_t mode;           // console show mode
} console_t;

void ConsoleInit();
void ConsolePageBuffInit();
void ConsolePutChar(char ch);
void ConsolePutString(char *str);
void ConsoleCleanScreen();
void ConsoleScrollScreen();
#endif