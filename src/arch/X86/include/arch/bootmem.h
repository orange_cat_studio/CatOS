#ifndef ARCH_BOOTMEM_H
    #define ARCH_BOOTMEM_H
    
    #include<lib/type.h>

    typedef struct
    {
        uint32_t start;
        uint32_t end;
        uint32_t cur;
    }boot_mem_t;

    void BootMemInit(uint32_t start, uint32_t size);
    uint32_t BootMemAlloc(size_t size);
    uint32_t BootMemCurAddr();
    uint32_t BootMemSize();
#endif