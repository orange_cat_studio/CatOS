// file：arch/x86/arch/debug.c
// autor:jiangxinpeng
// time:2021.2.2
// copyright:(C) 2020-2050 by Jiang xinpeng. All rights reserved.

#include <arch/debug.h>
#include <os/debug.h>
#include <arch/console.h>
#include <arch/serial.h>
#include <arch/arch.h>
#include <arch/config.h>

// init debug driver
void DebugInit()
{
    // init console hardware windows
    ConsoleInit();

// init serial hardware
#ifdef X86_SERIAL_HW
    SerialInit();
#endif

    KPrint("[debug] debug init done\n");
}

void DebugPutC(char ch)
{
#ifdef X86_CONSOLE_HW
    ConsolePutChar(ch);
#endif

#ifdef X86_SERIAL_HW
    SERIAL_SEND(ch);
#endif
}