%include "arch/const.inc"

[bits 32]
[section .text]

extern KernelMain
extern ArchInit
extern SetupPage
extern ArchInitAfterPage

global KernelStart
KernelStart:
    mov esp,KERNEL_STACK_TOP 

    ;init arch
    call ArchInit
    ;setup page
    call SetupPage

    xor ebp,ebp 
    jmp .page+KERNEL_VIR_ADDR  
.page:
    mov esp,KERNEL_VIR_STACK_TOP
    call ArchInitAfterPage
    call KernelMain 

global KernelStop
KernelStop:
    hlt
    jmp $
