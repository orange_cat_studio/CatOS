//file: arch/x86/arch/mm/atomic.c
//autor:jiangxinpeng
//time:2021.1.17
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#include <arch/atomic.h>
#include <lib/type.h>
#include <arch/x86.h>


int AtomicAdd(atomic_t *atomic, int val)
{
    MemAtomicAdd(&atomic->value, val);
}

int AtomicSub(atomic_t *atomic, int val)
{
    MemAtomicSub(&atomic->value, val);
}

int AtomicInc(atomic_t *atomic)
{
    MemAtomicInc(&atomic->value);
}

int AtomicDec(atomic_t *atomic)
{
    MemAtomicDec(&atomic->value);
}

int AtomicAnd(atomic_t *atomic, int val)
{
    MemAtomicAnd(&atomic->value, val);
}

int AtomicOr(atomic_t *atomic, int val)
{
    MemAtomicOr(&atomic->value, val);
}

int AtomicSet(atomic_t *atomic, int val)
{
    MemAtomicSet(&atomic->value, val);
}

int AtomicGet(atomic_t *atomic)
{
    return atomic->value;
}

int AtomicXCHG(atomic_t *atomic, int val)
{
    return MemAtomicXCHG(&atomic->value, val);
}
