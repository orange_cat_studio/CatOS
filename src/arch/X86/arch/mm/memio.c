#include <arch/memio.h>
#include <arch/page.h>
#include <lib/type.h>

void HalMemIoReMap(uint32_t vbase, uint32_t pybase, uint32_t len)
{
    len = PAGE_ALIGN(len);
    if (!len)
        return;

    uint32_t pages = len / PAGE_SIZE;

    KPrint("[memio] remap vbase %x len %d\n", vbase, len);
    while (pages--)
    {
        VbaseLinkPybase(vbase, pybase, KERNEL_PAGE_ATTR);

        vbase += PAGE_SIZE;
        pybase += PAGE_SIZE;
    }
}

void HalMemIoUnMap(uint32_t vbase, uint32_t len)
{
    len = PAGE_ALIGN(len);
    if (!len)
        return;

    uint32_t pages = len / PAGE_SIZE;
    KPrint("[memio] unmap vbase %x len %d\n", vbase, len);
    while (vbase < vbase + len)
    {
        VbaseUnlinkPybase(vbase);
        vbase += PAGE_SIZE;
    }
}
