#include <arch/ards.h>
#include <lib/type.h>
#include <arch/address.h>
#include <arch/page.h>

#define GRUB

// grub loader ards address
#define ARDS_LOAD_ADDRESS (0X1000)

#ifdef GRUB
PRIVATE uint32_t *memsize = (void *)ARDS_LOAD_ADDRESS;
#else
PRIVATE struct ardsinfo *ards_info = (void *)ARDS_LOAD_ADDRESS;
#endif

PUBLIC uint64_t GetPymemSize()
{

    // in order to fit page feature
    if (page_enable)
    {
#ifdef GRUB
        memsize += PAGE_HIGH_BASE;
#else
        ards_info += PAGE_HIGH_BASE;
#endif
    }
#ifdef GRUB
    uint32_t totolmem = *(uint32_t *)memsize;
#else
    uint32_t ardsnum = ards_info->count;
    struct ards *ards = ards_info->ards;
    uint64_t totolmem = 0;
    int i;

    // check if is ardsnum above limit,if above limit,we need do change
    if (ardsnum > ARDS_MAX)
    {
        ardsnum = ARDS_MAX;
    }

    // get total mem size
    for (i = 0; i < ardsnum; i++)
    {
        // type is 1 just is used mem size
        if ((ards + i)->type == 1)
        {
            // search max used mem
            if ((ards + i)->base_low + (ards + i)->size_low > totolmem)
            {
                // get current size
                totolmem = (ards + i)->size_low;
            }
        }
        KPrint("[ards] ards base=%x length %x type=%d\n", ards->base_low, ards->size_low, ards->type);
    }
#endif
    KPrint("[ards] pymem totol size: %u bytes,%d Mb\n", totolmem, totolmem / MB);
    return totolmem;
}