#include <arch/vmm.h>
#include <arch/x86.h>
#include <arch/page.h>
#include <arch/x86.h>
#include <arch/tss.h>
#include <os/schedule.h>

// active kernel vmm
void VmmActiveKernel()
{
    uint32_t pdir = PAGEDIR_PYBASE;
    WriteCR3(pdir);
    UpdateTSS((uint32_t)cur_task);
}

// active user vmm
// use user provide pdir table
void VmmActiveUser(uint32_t pdir)
{
    WriteCR3(pdir);
    UpdateTSS((uint32_t)cur_task);
}

void VmmActive(vmm_t *vmm)
{
    if (!vmm) //kernel vmm
    {
       
        VmmActiveKernel();
    }
    else
    {
        if (vmm->page_storge)  //user vmm
        {
            VmmActiveUser((uint32_t)(vmm->page_storge));
        }
    }
}