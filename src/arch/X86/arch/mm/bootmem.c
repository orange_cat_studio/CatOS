#include <arch/bootmem.h>
#include <arch/page.h>
#include <os/debug.h>

static boot_mem_t boot_mem;

void BootMemInit(uint32_t start, uint32_t size)
{
    boot_mem.start = start;
    boot_mem.end = start + size;
    boot_mem.cur = boot_mem.start;
}

uint32_t BootMemAlloc(size_t size)
{
    size = PageAlign(size);
    uint32_t addr = boot_mem.cur;
    boot_mem.cur += size;
    if (boot_mem.cur >= boot_mem.end)
    {
        boot_mem.cur -= size; // rollback
        Panic("%s: alloc failed! cur=%x size=%x end=%x\n", __func__, boot_mem.cur, size, boot_mem.end);
        return 0;
    }
    return addr;
}

uint32_t BootMemCurAddr()
{
    return boot_mem.cur;
}

uint32_t BootMemSize()
{
    return boot_mem.cur - boot_mem.start;
}