[section .text]
[bits 32]

global MemAtomicAdd
;MemAtomicAdd(int *,int)
MemAtomicAdd:
    mov eax,[esp+4]
    mov ebx,[esp+8]
    lock add [eax],ebx
    ret 
global MemAtomicSub
MemAtomicSub:
    mov eax,[esp+4]
    mov ebx,[esp+8]
    lock sub [eax],ebx
    ret 
global MemAtomicInc
MemAtomicInc:
    mov eax,[esp+4]
    lock inc dword [eax]
    ret 
global MemAtomicDec
MemAtomicDec:
    mov eax,[esp+4]
    lock dec dword [eax]
    ret 
global MemAtomicAnd
MemAtomicAnd:
    mov eax,[esp+4]
    mov ebx,[esp+8]
    lock and [eax],ebx
    ret 
global MemAtomicOr
MemAtomicOr:
    mov eax,[esp+4]
    mov ebx,[esp+8]
    lock or [eax],ebx
    ret 
global MemAtomicXCHG
MemAtomicXCHG:
    mov eax,[esp+4]
    mov ebx,[esp+8]
    lock xchg [eax],ebx
    mov eax,ebx
    ret 
global MemAtomicSet
MemAtomicSet:
    mov eax,[esp+4]
    mov ebx,[esp+8]
    mov [eax],ebx
    ret 