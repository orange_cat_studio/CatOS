#include <os/debug.h>
#include <arch/acpi.h>
#include <arch/x86.h>
#include <arch/power.h>
#include <lib/type.h>

void SysReboot()
{
    KPrint(PRINT_INFO "ready reboot system!\n");
    Reboot();
}

void Reboot()
{
    KPrint(PRINT_INFO "Reboot System.\n");
    __asm__ __volatile__("cli");
    // call acpi reset
    DoReset();
}
