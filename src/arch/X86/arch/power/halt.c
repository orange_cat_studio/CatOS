#include<arch/acpi.h>
#include<arch/halt.h>
#include<os/debug.h>


int SysPowerOff()
{
    KPrint(PRINT_INFO"power off!\n");
    Halt();
}

void Halt()
{
    //send "Exit" signal to OS
    DoPowerOff();
}