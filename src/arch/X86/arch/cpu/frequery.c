// file: arch/X86/arch/cpu/frequery.c
// autor: jiangxinpeng
// time: 2022.6.15
// copyright: (c) 2020-2050 by jiangxinpeng,All right are reserved.

#include <arch/x86.h>
#include <arch/debug.h>
#include <arch/cpu.h>
#include <arch/frequery.h>
#include <os/clock.h>
#include <os/debug.h>
#include <lib/math.h>
#include <lib/stddef.h>

uint64_t __frequery = -1;

void FrequeryInit()
{
    uint64_t t1, t2;

    t1 = RdTsc();
    CpuDoDelay(1000);
    t2 = RdTsc();

    __frequery = do_div64((t2 - t1), HZ);
    KPrint("CPU Frequery %uMHZ %uGHZ\n",(uint32_t)__frequery, (uint32_t)__frequery / 1000 / 1000);
}






