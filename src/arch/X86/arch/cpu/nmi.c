#include <arch/nmi.h>
#include <arch/io.h>
#include <arch/cmos.h>

//What is NMI? 
//while extern hardware happend error and assert NMI spin,cpu will respond no-mask interrupt.  
//cmos address register bits7 can set NMI maskhj
void EnableNMI()
{
    uint8_t data;

    data=In8(CMOS_ADPORT);
    Out8((uint16_t)CMOS_ADPORT, data | 0x80);
}

void DisableNMI()
{
    uint8_t data;

    data=In8(CMOS_ADPORT);
    Out8((uint16_t)CMOS_ADPORT, data & ~0x80);
}
