#include <arch/arch.h>
#include <arch/segment.h>
#include <arch/mem_pool.h>
#include <arch/debug.h>
#include <arch/tss.h>
#include <arch/frequery.h>
#include <arch/cpu.h>
#include <arch/pymem.h>
#include <arch/smbios.h>
#include <arch/gate.h>
#include <arch/acpi.h>
#include <arch/ioapic.h>
#include <arch/pic.h>
#include <arch/pci.h>
#include <arch/cmos.h>
#include <arch/apic.h>
#include <arch/mp.h>
#include <os/mp.h>
#include <os/debug.h>
#include <lib/string.h>

// init platform envorment,then go to execute kernel
void ArchInit()
{
    // init debug
    DebugInit();
    // init ACPI
    AcpiInit();
    // init all CPU
    CpuInit();
#ifdef ENABLE_SMP
    // init APIC
    APICInit();
    // init IOAPIC
    IoAPICInit();
    // init mp
    MpInit();
#endif
}

void ArchInitAfterPage()
{
    // inif cpu frequery
    FrequeryInit();
    // init pymem manager
    PymemInit();
    // init gate
    GateDescriptInit();
    // init system all segment
    SegmentDescriptInit();
    // init pic
    PicInit();
    // init pci
    PciInit();
    // init tss
    TssInit();
    // dump cmos
    DumpCMOS();
    // smbios init
    SmbiosInit();
}
