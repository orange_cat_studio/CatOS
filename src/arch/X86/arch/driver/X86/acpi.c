#include <os/initcall.h>
#include <arch/x86.h>
#include <arch/acpi.h>
#include <os/debug.h>
#include <os/driver.h>
#include <lib/type.h>
#include <lib/stddef.h>
#include <lib/list.h>
#include <driver/acpi.h>

static iostatus_t ACPIEnter(driver_object_t *driver)
{
}

static iostatus_t ACPIExit(driver_object_t *driver)
{
}

static iostatus_t ACPIDriverFunc(driver_object_t *driver)
{
    // create driver name
    string_new(&driver->name, DEVICE_NAME, DEVICE_NAME_LEN);

    driver->driver_enter = ACPIEnter;
    driver->driver_exit = ACPIExit;

    return IO_SUCCESS;
}

static __init ACPIDriverEntry()
{
    KPrint("[acpi] create acpi driver\n");
    if (DriverObjectCreate(ACPIDriverFunc) < 0)
    {
        KPrint("[driver] ACPI driver create failed!\n");
    }
}

driver_initcall(ACPIDriverEntry);