#include <os/debug.h>
#include <os/dma.h>
#include <arch/config.h>
#include <arch/pci.h>
#include <driver/usb/hub.h>
#include <driver/usb.h>

uint32_t UsbRegRead32(device_extension_t *extension, uint32_t off)
{
    return *(uint32_t *)(extension->iobase + off);
}

uint16_t UsbRegRead16(device_extension_t *extension, uint32_t off)
{
    return *(uint16_t *)(extension->iobase + off);
}

uint8_t UsbRegRead8(device_extension_t *extension, uint32_t off)
{
    return *(uint8_t *)(extension->iobase + off);
}

void UsbRegWrite32(device_extension_t *extension, uint32_t off, uint32_t data)
{
    *(uint32_t *)(extension->iobase + off) = data;
}

void UsbRegWrite16(device_extension_t *extension, uint32_t off, uint16_t data)
{
    *(uint16_t *)(extension->iobase + off) = data;
}

void UsbRegWrite8(device_extension_t *extension, uint32_t off, uint8_t data)
{
    *(uint8_t *)(extension->iobase + off) = data;
}

