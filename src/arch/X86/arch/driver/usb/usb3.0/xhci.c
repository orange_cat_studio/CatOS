#include <driver/usb/hub.h>
#include <driver/usb/xhci.h>
#include <driver/usb.h>
#include <os/debug.h>
#include <os/spinlock.h>

// r/w usb port status and config register
void UsbWritePortSC(uint32_t port, device_extension_t *extension, uint32_t data)
{
    UsbRegWrite32(extension, extension->hub_port.XHCI.port_base + (port * 0x10), data);
}

uint32_t UsbReadPortSC(uint32_t port, device_extension_t *extension)
{
    return UsbRegRead32(extension, extension->hub_port.XHCI.port_base + (port * 0x10));
}

// write usb comand register
void UsbWriteCommand(device_extension_t *extension, uint32_t cmd)
{
    UsbRegWrite32(extension, extension->hub_port.XHCI.usb_command, cmd);
}

// r/w usb cmd ring control register
void UsbWriteCMDRingCR(device_extension_t *extension, uint64_t data)
{
    UsbRegWrite32(extension, extension->hub_port.XHCI.command_ring_ctrl, data);
}

uint64_t UsbReadCMDRingCR(device_extension_t *extension)
{
    return UsbRegRead32(extension, extension->hub_port.XHCI.command_ring_ctrl);
}

// check assigned port whether was connected to usb device
bool UsbPortConnect(uint32_t port, device_extension_t *extension)
{
    uint32_t status = UsbReadPortSC(port, extension);
    if (status & USB_XHCI_PORT_STATUS_CURCON)
        return true;
    else
        return false;
}

// set command ring control register (CRCR) command ring pointer
void UsbCRCRSetCRP(device_extension_t *extension, uint32_t address)
{
    uint64_t crcr = UsbReadCMDRingCR(extension);
    // only use 32bits address,but it is can support 64bits address
    crcr |= (address & 0xffffffff) << 6;
    UsbWriteCMDRingCR(extension, crcr);
}

// write usb device context base adddress array pointer register
void UsbWriteDCBAAP(device_extension_t *extension, uint64_t data)
{
    UsbRegWrite32(extension, extension->hub_port.XHCI.device_context_base_array, data);
}

uint64_t UsbReadDCBAAP(device_extension_t *extension)
{
    return UsbRegRead32(extension, extension->hub_port.XHCI.device_context_base_array);
}

void UsbDoorRingSetTargeAndStreamID(device_extension_t *extension, uint16_t targe, uint16_t streamid)
{
    uint32_t DR_data = ((uint32_t)streamid & USB_XHCI_DB_STREAM_ID_MASK << USB_XHCI_DB_STREAM_ID_SHIFT) | ((uint32_t)targe & USB_XHCI_DB_TARGE_MASK << USB_XHCI_DB_TARGE_SHIFT);
    UsbRegWrite32(extension, extension->hub_port.XHCI.doorbell_off, DR_data);
}
