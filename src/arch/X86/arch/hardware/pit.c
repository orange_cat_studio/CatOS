// file:arch/x86/arch/hardware/pit.c
// autor:jiangxinpeng
// time:2021.1.19
// copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.
#include <arch/time.h>
#include <arch/pit.h>
#include <arch/io.h>

void _PitClockInit()
{
    Out8(PIT_CTRL, PIT_MODE_2 | PIT_MODE_MSB_LSB | PIT_MODE_COUNTER0 | PIT_MODE_BINARY);
    Out8(PIT_COUNTER0, COUNTER0_VALUE & 0xff);        // low  bytes
    Out8(PIT_COUNTER0, (COUNTER0_VALUE >> 8) & 0xff); // high bytes
    KPrint("[pit] set pit to %d\n", (uint32_t)COUNTER0_VALUE);
}
