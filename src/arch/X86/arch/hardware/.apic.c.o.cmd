cmd_arch/X86/arch/hardware/apic.c.o := gcc -march=i386 -fno-builtin -w -fno-PIE -m32 -std=gnu99 -fno-stack-protector -fno-omit-frame-pointer -fno-strict-aliasing -g  -DKERNEL_NAME=\"CATOS\" -DKERNEL_VERSION=\"0.1.9\" -fno-strict-aliasing -O0  -MD -MF arch/X86/arch/hardware/.apic.c.o.d  -I /media/sf_CatOS/src/include/  -I /media/sf_CatOS/src/arch/X86/include/  -I /media/sf_CatOS/src/include/lib   -c arch/X86/arch/hardware/apic.c -o arch/X86/arch/hardware/apic.c.o

source_arch/X86/arch/hardware/apic.c.o := arch/X86/arch/hardware/apic.c

deps_arch/X86/arch/hardware/apic.c.o := \
  /usr/include/stdc-predef.h \
  /media/sf_CatOS/src/arch/X86/include/arch/apic.h \
  /media/sf_CatOS/src/include/lib/type.h \
  /media/sf_CatOS/src/arch/X86/include/arch/arch.h \
  /media/sf_CatOS/src/include/lib/stdint.h \
    $(wildcard include/config/arch.h) \
  /media/sf_CatOS/src/arch/X86/include/arch/const.h \
  /media/sf_CatOS/src/arch/X86/include/arch/cpu.h \
  /media/sf_CatOS/src/arch/X86/include/arch/time.h \
  /media/sf_CatOS/src/arch/X86/include/arch/cmos.h \
  /media/sf_CatOS/src/arch/X86/include/arch/pit.h \
  /media/sf_CatOS/src/arch/X86/include/arch/x86.h \
  /media/sf_CatOS/src/arch/X86/include/arch/page.h \
    $(wildcard include/config/page_4mb.h) \
  /media/sf_CatOS/src/arch/X86/include/arch/config.h \
    $(wildcard include/config/h.h) \
  /media/sf_CatOS/src/arch/X86/include/arch/pymem.h \
  /media/sf_CatOS/src/include/os/config.h \
    $(wildcard include/config/pae.h) \
    $(wildcard include/config/cpu_width.h) \
    $(wildcard include/config/show_text.h) \
    $(wildcard include/config/show_graphic.h) \
    $(wildcard include/config/net.h) \
    $(wildcard include/config/cd.h) \
    $(wildcard include/config/large_mem.h) \
  /media/sf_CatOS/src/include/os/kernel.h \
  /media/sf_CatOS/src/include/lib/stddef.h \
  /media/sf_CatOS/src/include/lib/type.h \
  /media/sf_CatOS/src/include/lib/const.h \
  /media/sf_CatOS/src/include/lib/math.h \
  /media/sf_CatOS/src/include/os/memcache.h \
  /media/sf_CatOS/src/include/lib/list.h \
  /media/sf_CatOS/src/include/lib/bitmap.h \
  /media/sf_CatOS/src/include/os/mutexlock.h \
  /media/sf_CatOS/src/include/os/spinlock.h \
  /media/sf_CatOS/src/arch/X86/include/arch/atomic.h \
  /media/sf_CatOS/src/include/lib/math.h \
  /media/sf_CatOS/src/arch/X86/include/arch/interrupt.h \
  /media/sf_CatOS/src/arch/X86/include/arch/mem_pool.h \
  /media/sf_CatOS/src/include/os/debug.h \
  /media/sf_CatOS/src/arch/X86/include/arch/mp.h \
  /media/sf_CatOS/src/include/os/timer.h \
  /media/sf_CatOS/src/include/os/clock.h \
  /media/sf_CatOS/src/include/sys/time.h \
  /media/sf_CatOS/src/arch/X86/include/arch/memio.h \
  /media/sf_CatOS/src/include/os/softirq.h \
  /media/sf_CatOS/src/include/os/schedule.h \
  /media/sf_CatOS/src/include/os/task.h \
  /media/sf_CatOS/src/arch/X86/include/arch/fpu.h \
  /media/sf_CatOS/src/include/lib/string.h \
  /media/sf_CatOS/src/include/os/exception.h \
  /media/sf_CatOS/src/include/os/waitqueue.h \
  /media/sf_CatOS/src/include/os/alarm.h \
  /media/sf_CatOS/src/include/os/fsal.h \
  /media/sf_CatOS/src/include/os/dirent.h \
  /media/sf_CatOS/src/include/os/fd.h \
  /media/sf_CatOS/src/include/sys/status.h \
  /media/sf_CatOS/src/include/sys/select.h \
  /media/sf_CatOS/src/include/os/pthread.h \
  /media/sf_CatOS/src/include/os/semaphore.h \
  /media/sf_CatOS/src/include/os/portcom.h \
  /media/sf_CatOS/src/include/os/msgpool.h \
  /media/sf_CatOS/src/include/os/vmm.h \
  /media/sf_CatOS/src/include/sys/proc.h \
  /media/sf_CatOS/src/include/os/virmem.h \
  /media/sf_CatOS/src/include/lib/assert.h \
    $(wildcard include/config/assert.h) \
  /media/sf_CatOS/src/arch/X86/include/arch/gate.h \
  /media/sf_CatOS/src/arch/X86/include/arch/segment.h \
  /media/sf_CatOS/src/arch/X86/include/arch/pic.h \

arch/X86/arch/hardware/apic.c.o: $(deps_arch/X86/arch/hardware/apic.c.o)

$(deps_arch/X86/arch/hardware/apic.c.o):
