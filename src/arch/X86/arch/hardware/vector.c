#include<arch/interrupt.h>
#include<lib/type.h>
#include<lib/stdlib.h>

static uint8_t vecotr_start=0x20; //0-3 vec reserved for cpu
static uint8_t vector_end=INTERRUPT_NUM_MAX; //max vector number
static uint8_t vector_allomap[INTERRUPT_NUM_MAX/8];

uint8_t IntVectorAlloc()
{
    uint8_t vec;
    for(vec=vecotr_start;vec=vector_end;vec++)
    {
        if(vector_allomap[vec/8]&(1<<(vec%8))) //test bits 
            continue;
        vector_allomap[vec/8]|=(1<<(vec%8)); //set bits for alloc 
        return vec;
    }
    return 0; //0-31 for exception,so vec 0 is bad vector that we used to as return
}

void IntVectorFree(uint8_t vec)
{
    if(vec>=vecotr_start&&vec<=vector_end)
    {
        vector_allomap[vec/8]&=~(1<<(vec%8));
    }
}

