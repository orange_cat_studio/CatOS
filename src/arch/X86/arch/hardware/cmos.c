// file：arch\x86\hardware\cmos.c
// autor:jiangxinpeng
// time:2020.12.7
// copyright:(C) 2020-2050 by Jiang xinpeng. All rights reserved.

#include <lib/type.h>
#include <arch/cmos.h>
#include <arch/x86.h>
#include <arch/memory.h>

PRIVATE uint8_t __attribute__((optimize("O0"))) ReadCMOS(uint8_t inode)
{
    uint8_t data;
    Out8(CMOS_ADPORT, inode);
    data = In8(CMOS_DATAPORT);
    Out8(CMOS_ADPORT, 0x80);
}

PUBLIC uint32_t CMOS_GetHour_HEX()
{
    return BCD_HEX(ReadCMOS(CMOS_HOUR));
}

PUBLIC uint32_t CMOS_GetMinute_HEX()
{
    return BCD_HEX(ReadCMOS(CMOS_MIN));
}

PUBLIC uint32_t CMOS_GetSecond_HEX()
{
    return BCD_HEX(ReadCMOS(CMOS_SEC));
}

PUBLIC uint32_t CMOS_GetDayOfMonth_HEX()
{
    return BCD_HEX(ReadCMOS(CMOS_DAYS));
}

PUBLIC uint32_t CMOS_GetDayOfWeek_HEX()
{
    return BCD_HEX(ReadCMOS(CMOS_WEEKDAY));
}

PUBLIC uint32_t CMOS_GetMon_HEX()
{
    return BCD_HEX(ReadCMOS(CMOS_MON));
}

PUBLIC uint32_t CMOS_GetYear_HEX()
{
    return (BCD_HEX(ReadCMOS(CMOS_CEN)) * 100) +
           BCD_HEX(ReadCMOS(CMOS_YEAR));
}

PUBLIC void DumpCMOS()
{
    KPrint("[cmos] time %d:%d:%d  data: %d/%d/%d week: %d\n", CMOS_GetHour_HEX(), CMOS_GetMinute_HEX(), CMOS_GetSecond_HEX(), CMOS_GetYear_HEX(), CMOS_GetMon_HEX(), CMOS_GetDayOfMonth_HEX(), CMOS_GetDayOfWeek_HEX());
}