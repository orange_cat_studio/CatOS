global TaskSwitchToNext
global KernelSwitchToUser

[bits 32]
[section .text]

TaskSwitchToNext:
    ;restore register to current stack
    push esi
    push edi
    push ebx
    push ebp

    ;get prev task stack
    mov eax,[esp+20]
    mov [eax],esp ;save esp to prev kstack

    ;get next task stack
    mov eax,[esp+24]
    mov esp,[eax]   ;store esp from next kstack

    ;refresh register from new task stack
    pop ebp
    pop ebx
    pop edi
    pop esi
    ret

KernelSwitchToUser:
    ;get frame addr
    mov esp,[esp+4]
    add esp,4       ;jump vector num
    popad
    pop gs
    pop fs
    pop es
    pop ds
    add esp,4       ;jump error code
    iretd
