#ifndef SYS_SELECT_H
    #define SYS_SELECT_H

    #include<sys/time.h>

    #define FD_SETSIZE 64
    #define FD_SET(n, set) ((set)->fd_bits[(n) / 8] |= (1 << ((n)&7)))
    #define FD_CLEAR(n, set) ((set)->fd_bits[(n) / 8] &= (~(1 << ((n)&7))))
    #define FD_ISSET(n, set) ((set)->fd_bits[(n) / 8] &= (1 << ((n)&7)))
    #define FD_ZERO(set) memset((set), 0, sizeof(*(set)))

    typedef struct
    {
        uint8_t fd_bits[(FD_SETSIZE + 7) / 8];
    } fd_set_t;

    void FdSetOr(fd_set_t *dst, fd_set_t *src, int max);
    int FdSetEmpty(fd_set_t *set, int max);
    void FdSetDump(fd_set_t *set, int max);
    int SysSelect(int max, fd_set_t *readfds, fd_set_t *writefds, fd_set_t *exceptfds, timeval_t *timeout);
#endif