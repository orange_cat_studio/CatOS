#ifndef SYS_FCNTL_H
    #define SYS_FCTL_H


    #define F_DUPFD  0x0 //duplicate file descript
    #define F_GETFD  0x1 //get file descript
    #define F_SETFD  0x2 //set file descript
    #define F_GETFL  0x4 //get file flags
    #define F_SETFL  0x8 //set file flags


    #define FD_NCLOEXEC 0x0 
    #define FD_CLOSEEXC 0x1

    //file open
    #define O_RDONLY    0x01 
    #define O_WRONLY    0x02
    #define O_RDWR      0x04
    #define O_CREATE    0x08 
    #define O_TRUNC     0x10
    #define O_APPEND    0x20
    #define O_EXEC      0x80
    #define O_TEXT      0x100        
    #define O_BINARY    0x200   
    #define O_NONBLOCK  0x400   
    #define O_NOCTTY    0x800   
    #define O_EXCL      0x1000  
    #define O_DIRECTORY 0x0200000

    
#endif