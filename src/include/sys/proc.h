#ifndef SYS_PROC_H
#define SYS_PROC_H

#include <lib/type.h>

#define PROC_NAME_LEN 16

typedef struct
{
    pid_t ts_pid;
    pid_t ts_ppid;
    pid_t ts_pgid;
    pid_t ts_tgid;
    uid_t ts_uid;
    gid_t ts_gid;
    uint8_t ts_status;
    uint32_t ts_priority;
    uint32_t ts_timeslice;
    uint32_t ts_runticks;
    char ts_name[PROC_NAME_LEN];
} tstatus_t;

int waitpid(pid_t pid, int *status, int options);
void _exit(int status);
pid_t fork();
pid_t getpid();
pid_t getppid();
pid_t gettid();
int tstatus(tstatus_t *ts, int *idx);
uint64_t sleep(uint64_t second);
void sched_yeild();
int getver(char *buff, int len);
int create_process(char *const argv[], char *const envp[], uint32_t flags);
int resume_process(pid_t pid);

#endif