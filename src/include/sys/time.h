//file:sys/time.h
//autor:jiangxinpneg
//time:2021.3.23
//copyright:(C) by jiangxinpeng,All right are reserved.

#ifndef SYS_TIME_H  
    #define SYS_TIME_H
    
    #include<lib/type.h>

    //clock time type
    #define CLOCK_REALTIME         1  //system current time
    #define CLOCK_BOOTTIME         2  //system boot time
    #define CLOCK_PROCCESS_CPUTIME 3  //proces running time
    #define CLOCK_THREAD_CPUTIME   4  //thread running time


    #define SYSTICKS_VALUE_MAX ((~0UL)- 1)

    //time value struct 
    typedef struct timeval
    {
        uint64_t tv_sec;    //second
        uint64_t tv_usec;   //micro second
    }timeval_t;

    typedef struct timezone
    {
        uint64_t tz_minutewest;
        uint64_t tz_dsttime;
    }timezone_t;

    typedef struct timespec
    {
        uint64_t  tv_sec;
        uint64_t tv_nsec;
    }timespec_t;

    typedef struct tms
    {
        clock_t time_user_time; //user cpu time
        clock_t time_sys_time;  //system cpu time
        clock_t time_cu_time;   //sub process user cpu time
        clock_t time_cs_time;   //sub proces system cpu time
    }tms_t;

    uint64_t TimevalToSysticks(timeval_t *tv);
    void SysticksToTimeval(clock_t ticks,timeval_t *ts);
    uint64_t TimespecToSysticks(timespec_t *ts);
   
    clock_t SysTime(tms_t *tms); 
    int SysClockGetTime(clock_type_t clocktype,timespec_t *ts);
    int SysGetTimeOfDay(timeval_t *tv,timezone_t *tz);
#endif