//file:include/sys/ipc.h
//autor:jiangxinpeng
//time:2021.12.24
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef SYS_IPC_H
#define SYS_IPC_H

typedef enum ipc_flags
{
    IPC_CREATE = 0x01,  //create a ipc
    IPC_EXCL = 0x02,    //must open a no exist ipc
    IPC_READ = 0x04,    //reader for pipe
    IPC_WRITE = 0x08,   //writer for pipe
    IPC_NOWAIT = 0x10,  //no wiat
    IPC_EXCEPT = 0x20,  //except something
    IPC_NOERROR = 0x40, //no error
    IPC_NOSYNC = 0x80,  //no sync
    IPC_RND = 0x100,    //addr round page aligned
    IPC_REMAP = 0x200,  //remap memory
} ipc_local_flags_t;

typedef enum ipc_slaver_cmd
{
    IPC_SHM = 0x100000,  //share mem lpc
    IPC_SEM = 0x200000,  //semaphore lpc
    IPC_MSG = 0x400000,  //message queue lpc
    IPC_PIPE = 0x800000, //pipe lpc
} ipc_slaver_flags_t;

typedef enum ipc_cmd
{
    IPC_DEL = 0x01,   //del a lpc from kernel
    IPC_SETRW = 0x02, //set lpc reader/writer
    IPC_SET = 0x03    //set lpc
} ipc_cmd_t;
#endif