#ifndef __SYS_SWAP_H
#define __SYS_SWAP_H

typedef struct 
{
    uint64_t total_size;
    uint64_t free_size;
    uint64_t used_size;
}swap_info_t;

int SysSwapMem(swap_info_t* swap_info);

#endif 