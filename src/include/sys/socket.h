#ifndef __SYS_SOCKET_H
#define __SYS_SOCKET_H


#include <os/config.h>

#if CONFIG_NET
#include <lib/type.h>
#include <lwip/sockets.h>

int SysSocket(int domain, int type, int protocol);
int SysSocketBind(int fd, sockaddr_t *my_addr, int addrlen);
int SysSocketAccept(int fd, sockaddr_t *addr, socklen_t addrlen);
int SysSocketConnect(int fd, sockaddr_t *server_addr, int addrlen);
int SysSocketGetPeerName(int fd, sockaddr_t *serever_addr, socklen_t addrlen);
int SysSocketGetSockName(int fd, sockaddr_t *my_addr, socklen_t addrlen);
int SysSocketGetSocketOpt(int fd, int level, int optname, const void *optval, socklen_t optlen);
int SysSocketSetSocketOpt(int fd, int level, int optname, const void *optval, socklen_t optlen);
int SysSocketListen(int fd, int backlog);
int SysSocketRecv(int fd, void *buff, int len, int flags);
int SysSocketRecvFrom(int fd, void *buff, int len, int flags, sockaddr_t *from, int fromlen);
int SysSocketSend(int fd, void *buff, int len, int flags);
int SysSocketSendTo(int fd, void *buff, int len, int flags,sockaddr_t *to,socklen_t tolen);
int SysSocketShutdown(int fd, int how);
#endif

#endif