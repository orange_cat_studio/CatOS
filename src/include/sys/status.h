#ifndef SYS_STATUS_H
#define SYS_STATUS_H

#include <lib/type.h>

//st_mode filed

//file type
#define STU_IFMT 0xFF0000   //file mode mask
#define STU_IFDIR 0x10000   //director file
#define STU_IFCHR 0x20000   //char device file
#define STU_IFBLK 0x40000   //block file
#define STU_IFREG 0x80000   //normal file
#define STU_IFIFO 0x10000   //fifo file
#define STU_IFLINK 0x200000 //link file
#define STU_IFSOCK 0x400000 //sock file

//protect
#define STU_IREAD 0x100  //read
#define STU_IWRITE 0x200 //write
#define STU_IEXEC 0x400  //exec

#define STU_IRUSR (STU_IREAD)  //owner readable
#define STU_IWUSR (STU_IWRITE) //owner writeable
#define STU_IXUSR (STU_IEXEC)  //owner execable
#define STU_IRGRP 0x1          //user group readable
#define STU_IWGRP 0x2          //user group writeable
#define STU_IXGRP 0x4          //user group exec
#define STU_IROTH 0x10         //other user readable
#define STU_IWOTH 0x20         //other user writeable
#define STU_IXOTH 0x40         //other user execable

typedef struct
{
    mode_t st_mode;       //file access precission
    inode_t st_inode;     //file node number
    dev_t st_device;      //file device number
    dev_t st_rdevice;     //device file device number
    nlink_t st_nlink;     //file link number
    uid_t st_uid;         //user id
    gid_t st_gid;         //group id
    offset_t st_size;     //file size(bytes)
    time_t st_atime;      //last access time
    time_t st_mtime;      //last modifiy time
    time_t st_ctime;      //last status change time
    blksize_t st_blksize; //block size
    blkcnt_t st_blkcout;  //block count
} status_t;

int SysStatus(const char *path, status_t *status);

#endif