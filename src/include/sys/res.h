#ifndef SYS_RES_H
#define SYS_RES_H

#include <lib/type.h>

// this is  system resource define header

#define DEV_RES 0x1000000
#define IPC_RES 0x2000000

#define DEVICE_NAME_LEN 32

// enum all device type in there
typedef enum _device_type_t
{
        DEVICE_TYPE_ANY = 0,            // any device
        DEVICE_TYPE_DISK,               // disk device
        DEVICE_TYPE_PROCESS,            // process device
        DEVICE_TYPE_VOL,                // disk volume device
        DEVICE_TYPE_BEEP,               // beep device
        DEVICE_TYPE_KEYBOARD,           // keyborad device
        DEVICE_TYPE_NETWORK,            // network device
        DEVICE_TYPE_MOUSE,              // mouse device
        DEVICE_TYPE_NULL,               // null device
        DEVICE_TYPE_SCREEN,             // screen device
        DEVICE_TYPE_VIDEO,              // video device
        DEVICE_TYPE_SOUND,              // sound device
        DEVICE_TYPE_WAVE_IN,            // sound input device
        DEVICE_TYPE_WAVE_OUR,           // sound output device
        DEVICE_TYPE_STREAM,             // stream device
        DEVICE_TYPE_VIRTUAL_DISK,       // virtual disk device
        DEVICE_TYPE_VIRTUAL_CHAR,       // virtual char device
        DEVICE_TYPE_UNKNOW,             // unknow device
        DEVICE_TYPE_PORT,               // port device
        DEVICE_TYPE_SERIAL,             // serial device
        DEVICE_TYPE_PARALLEL,           // parallel   device
        DEVICE_TYPE_PHYSIC_NETWORKCARD, // networkcard
        DEVICE_TYPE_BUS_EXTERN,         // bus extern
        DEVICE_TYPE_VIEW,               // view device
        DEVICE_TYPE_8042_PORT,          // 8042 port
        DEVICE_TYPE_OTHER               // other more device
} device_type_t;

// define sys dev tern
typedef struct
{
        uint16_t dev_type;              // device type
        char dev_name[DEVICE_NAME_LEN+1]; // device name
} devent_t;
#endif