#ifndef SYS_IOCTL_H
#define SYS_IOCTL_H

#include <lib/type.h>

// device flags
#define DEVICE_NOWAIT 0x01 // no block

// device control code
#define DEVCTL_CODE(type, cmd) ((((unsigned int)((type)&0xffff)) << 16) | (unsigned int)((cmd)&0xffff))

#define TTYIO_RAW 7

// tty flags
#define TTYFLAG_ECHO 0x01
#define TTYFLAG_NOWAIT 0x02

// net device control
#define NETIO_GETMAC DEVCTL_CODE('n', 1)   // net get mac address
#define NETIO_SETFLAGS DEVCTL_CODE('n', 2) // net set flags
#define NETIO_GETFLAGS DEVCTL_CODE('n', 3) // net get flags

// sound device control
#define SOUNDIO_VOLADD DEVCTL_CODE('s', 1)          // volume add
#define SOUNDIO_VOLDEC DEVCTL_CODE('s', 2)          // volume dec
#define SOUNDIO_MUTE DEVCTL_CODE('s', 3)            // mute
#define SOUNDIO_STOP DEVCTL_CODE('s', 4)            // stop
#define SOUNDIO_PLAY DEVCTL_CODE('s', 5)            // play
#define SOUNDIO_PAUSE DEVCTL_CODE('s', 6)           // pause
#define SOUNDIO_GETVOL DEVCTL_CODE('s', 7)          // get current volume
#define SOUNDIO_SETVOL DEVCTL_CODE('s', 8)          // set volume
#define SOUNDIO_ENABLE DEVCTL_CODE('s', 9)          // enable
#define SOUNDIO_DISABLE DEVCTL_CODE('s', 10)        // disable
#define SOUNDIO_SET_SAMPLERATE DEVCTL_CODE('s', 11) // set sample rate
#define SOUNDIO_SET_CHANNEL DEVCTL_CODE('s', 12)    // set channel
#define SOUNDIO_SET_BLOCK DEVCTL_CODE('s', 13)      // set block size

// serial device control
#define SERIALIO_SETBAUD DEVCTL_CODE('s', 1) // serial set baud
#define SERIALIO_GETBAUD DEVCTL_CODE('s', 2) // serial get baud
#define SERIALIO_SETMODE DEVCTL_CODE('s', 3) // serial set mode(word lenght,stop bits,parity)
#define SERIALIO_GETMODE DEVCTL_CODE('s', 4) // serial get mode

// tty device control
#define TTYIO_SETPGROUP DEVCTL_CODE('t', 1)      // ttyio set process group
#define TTYIO_GETPGROUP DEVCTL_CODE('t', 2)      // ttyio get process group
#define TTYIO_SELECT DEVCTL_CODE('t', 3)         // ttyio select
#define TTYIO_SETFLAGS DEVCTL_CODE('t', 4)       // ttyio setflags
#define TTYIO_GETFLAGS DEVCTL_CODE('t', 5)       // ttyio getflags
#define TTYIO_GETNAME DEVCTL_CODE('t', 6)        // ttyio getname
#define TTYIO_ISTTY DEVCTL_CODE('t', 7)          // ttyio device is tty
#define TTYIO_CLEAR CONIO_CLEAN                  // ttyio clear screen
#define TTYIO_NAME DEVCTL_CODE('t', 9)           // ttyio get name
#define TTYIO_GETFRONTGROUP DEVCTL_CODE('t', 10) // ttyio get front group task
#define TTYIO_GETPTNUM DEVCTL_CODE('t', 11)      // ttyio get presudo tty num
#define TTYIO_SETPTLOCK DEVCTL_CODE('t', 12)     // ttyio set presudo tty lock

// console device control
#define CONIO_CLEAN DEVCTL_CODE('c', 1)          // console clean screen
#define CONIO_GETSIZE DEVCTL_CODE('c', 2)        // console size
#define CONIO_GETWIDTH DEVCTL_CODE('c', 3)       // console width
#define CONIO_GETHEIGHT DEVCTL_CODE('c', 4)      // console height
#define CONIO_ID DEVCTL_CODE('c', 5)             // console id
#define CONIO_GETCOLOR_BACK DEVCTL_CODE('c', 6)  // console background
#define CONIO_GETCOLOR_FRONT DEVCTL_CODE('c', 7) // console front
#define CONIO_SETCOLOR_BACK DEVCTL_CODE('c', 8)
#define CONIO_SETCOLOR_FRONT DEVCTL_CODE('c', 9)

// keyboard device control
#define KEYBOARDIO_GETLED DEVCTL_CODE('k', 1)   // keyboard getled
#define KEYBOARDIO_GETFLAGS DEVCTL_CODE('k', 2) // keyboard getflags
#define KEYBOARDIO_SETFLAGS DEVCTL_CODE('k', 3) // keyboard setflags

// mouse device control
#define MOUSEIO_GETFLAGS DEVCTL_CODE('m', 1) // mouse getflags
#define MOUSEIO_SETFLAGS DEVCTL_CODE('m', 2) // mouse setflags

// ps2 device control
#define PS2IO_GETDEVICETYPE DEVCTL_CODE('p', 1) // get ps2 device type
#define PS2IO_SETDEVICETYPE DEVCTL_CODE('p', 2) // set ps2 device type
#define PS2IO_DISDEVICE DEVCTL_CODE('p', 3)     // disable ps2 device
#define PS2IO_ENDEVICE DEVCTL_CODE('p', 4)      // enable ps2 device

// disk io control
#define DISKIO_GETSIZE DEVCTL_CODE('d', 1)      // get disk size
#define DISKIO_SETOFF DEVCTL_CODE('d', 2)       // set disk rwoffset
#define DISKIO_GETOFF DEVCTL_CODE('d', 3)       // get disk rwoffset
#define DISKIO_CLEAN DEVCTL_CODE('d', 4)        // clean disk
#define DISKIO_GET_DISKTYPE DEVCTL_CODE('d', 5) // get disk type
#define DISKIO_SETDOWN DEVCTL_CODE('d', 6)      // disk setdown
#define DISKIO_SETUP DEVCTL_CODE('d', 7)        // disk setup
#define DISKIO_EJECT DEVCTL_CODE('d', 8)        // eject device
#define DISKIO_GETMODEL DEVCTL_CODE('d', 9)     // get disk model(40 ascii character)

// video device control
#define VIDEOIO_GETINFO DEVCTL_CODE('v', 1) // get video info

// view device control
#define VIEWIO_SETTYPE DEVCTL_CODE('v', 1)
#define VIEWIO_GETTYPE DEVCTL_CODE('v', 2)
#define VIEWIO_SHOW DEVCTL_CODE('v', 3)
#define VIEWIO_HIDE DEVCTL_CODE('v', 4)
#define VIEWIO_GETPOS DEVCTL_CODE('v', 5)
#define VIEWIO_WRBMP DEVCTL_CODE('v', 6)
#define VIEWIO_RDBMP DEVCTL_CODE('v', 7)
#define VIEWIO_SETFLAGS DEVCTL_CODE('v', 8)
#define VIEWIO_GETFLAGS DEVCTL_CODE('v', 9)
#define VIEWIO_REFRESH DEVCTL_CODE('v', 10)
#define VIEWIO_ADDATTR DEVCTL_CODE('v', 11)
#define VIEWIO_DELATTR DEVCTL_CODE('v', 12)
#define VIEWIO_RESIZE DEVCTL_CODE('v', 13)
#define VIEWIO_GETSCREENSZ DEVCTL_CODE('v', 14)
#define VIEWIO_GETLASTPOS DEVCTL_CODE('v', 15)
#define VIEWIO_GETMOUSEPOS DEVCTL_CODE('v', 16)
#define VIEWIO_SETSIZEMIN DEVCTL_CODE('v', 17)
#define VIEWIO_SETDRAGREGION DEVCTL_CODE('v', 18)
#define VIEWIO_SETMOUSESTATUS DEVCTL_CODE('v', 19)
#define VIEWIO_SETMOUSESTATUSINFO DEVCTL_CODE('v', 20)
#define VIEWIO_GETVID DEVCTL_CODE('v', 21)
#define VIEWIO_ADDTIMER DEVCTL_CODE('v', 22)
#define VIEWIO_DELTIMER DEVCTL_CODE('v', 23)
#define VIEWIO_RESTARTTIMER DEVCTL_CODE('v', 24)
#define VIEWIO_SETMONITOR DEVCTL_CODE('v', 25)
#define VIEWIO_SETWINMAXIMRECT DEVCTL_CODE('v', 26)
#define VIEWIO_GETWINMAXIMRECT DEVCTL_CODE('v', 27)
#define VIEWIO_GETMOUSESTATUS DEVCTL_CODE('v', 28)
#define VIEWIO_GETMOUSESTATUSINFO DEVCTL_CODE('v', 29)
#define VIEWIO_SETPOS DEVCTL_CODE('v', 30)

// cpu device ctrlol
#define CPUIO_GETVENDOR DEVCTL_CODE('c', 1)
#define CPUIO_GETBRAND DEVCTL_CODE('c', 2)
#define CPUIO_GETSTEPPING DEVCTL_CODE('c', 3)
#define CPUIO_GETFAMILY DEVCTL_CODE('c', 4)
#define CPUIO_GETFREQUERY DEVCTL_CODE('c', 5)
#define CPUIO_GETMAXFREQUERY DEVCTL_CODE('c', 6)
#define CPUIO_GETCURFREQUERY DEVCTL_CODE('c', 7)

/* sockets */
#define SIOCGIFCONF DEVCTL_CODE('s', 1)
#define SIOCSIFADDR DEVCTL_CODE('s', 2)
#define SIOCGIFADDR DEVCTL_CODE('s', 3)
#define SIOCSIFFLAGS DEVCTL_CODE('s', 4)
#define SIOCGIFFLAGS DEVCTL_CODE('s', 5)
#define SIOCSIFBRDADDR DEVCTL_CODE('s', 6)
#define SIOCGIFBRDADDR DEVCTL_CODE('s', 7)
#define SIOCGIFNETMASK DEVCTL_CODE('s', 8)
#define SIOCSIFNETMASK DEVCTL_CODE('s', 9)
#define SIOCGIFMTU DEVCTL_CODE('s', 10)
#define SIOCSIFMTU DEVCTL_CODE('s', 11)
#define SIOCSIFNAME DEVCTL_CODE('s', 12)
#define SIOCGIFNAME DEVCTL_CODE('s', 13)
#define SIOCSIFHWADDR DEVCTL_CODE('s', 14)
#define SIOCGIFHWADDR DEVCTL_CODE('s', 15)
#define SIOCSIFHWBROADCAST DEVCTL_CODE('s', 16)
#define SIOCGIFHWBROADCAST DEVCTL_CODE('s', 17)
#define SIOCGPGRP DEVCTL_CODE('s', 18)
#define SIOCSPGRP DEVCTL_CODE('s', 19)
#define SIOCSARP DEVCTL_CODE('s', 20)
#define SIOCGARP DEVCTL_CODE('s', 21)
#define SIOCDARP DEVCTL_CODE('s', 22)
#define SIOCADDRT DEVCTL_CODE('s', 23)
#define SIOCDELRT DEVCTL_CODE('s', 24)

typedef struct video_info
{
    char bits_per_pixel;      // pixel bits
    char bytes_per_scan_line; // scan line bytes
    uint16_t x_res, y_res;    // x,y resolution
} video_info_t;

#endif