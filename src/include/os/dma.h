#ifndef OS_DMA_H
#define OS_DMA_H

#include <lib/type.h>

#define DMA_REGION_SPECIAL 0x01

typedef struct mm_physical_region
{
    address_t addr;
    size_t size;
    address_t alignment;
} mm_physical_region_t;

typedef struct dma_region
{
    mm_physical_region_t p;
    address_t v;
    flags_t flags;
} dma_region_t;

int DmaAllocBuffer(dma_region_t *dma);
int DmaFreeBuffer(dma_region_t *dma);
void DmaBufferDump(dma_region_t *dma);

#endif