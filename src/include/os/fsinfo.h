// file:src/include/os/fsinfo.h
// autor:jiangxinpeng
// time:2021.2.29
// copyright:(C) by jiangxinpeng,All right are reserved.

#ifndef KERNEL_FSINFO_H
#define KERNEL_FSINFO_H

#include <lib/type.h>

#define FSINFO_SIGN 0xaa550000
#define FSINFO_LEND_SIGN 0x41615252
#define FSINFO_ANOTHER_SIGN 0x61417272

typedef struct fsinfo
{
    uint32_t lead_signature;    // must be 0x41615252
    uint8_t reserved0[480];     // reserved 480bytes
    uint32_t another_signature; // anothor signature must be 0x61417272
    uint32_t free_cluster;      // free cluster count
    uint32_t next_cluster;      // next cluster
    uint8_t reserved1[12];
    uint32_t signature;         // signature 0xaa550000
} __attribute__((packed)) fsinfo_t;

#endif