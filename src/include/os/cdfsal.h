#ifndef OS_CDFSAL_H
#define OS_CDFSAL_H

#include<os/fsal.h>

extern fsal_t cdfs_fsal;

#define REWIND_DIR(dir) (l9660_seekdir(dir,0))
#define REWIND(file) l9660_seek(file,SEEK_SET,0)
#endif