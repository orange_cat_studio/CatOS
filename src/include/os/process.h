//file: os/process.h
//autor：jiangxinpeng
//time: 2021.3.31
//uptime:2021.6.23
//copyright: (C) by jiangxinpeng,All right are reserved.

#ifndef OS_PROCESS_H
    #define OS_PROCESS_H

    #include <os/task.h>
    #include <os/elf32.h>

    #define PROCESS_CREATE_INIT (1 << 0)
    #define PROCESS_CREATE_STOP (1 << 1)

    int ProcessVmmInit(task_t *task);
    int ProcessVmmExit(task_t *task);
    int ProcessRelease(task_t *task);
    int ProcessDestroy(task_t *task, int thread);
    int ProcessDealZombieChild(task_t *parent);
    void ProcessCloseOtherThread(task_t *thread);
    void ProcessCloseOneThread(task_t *thread);
    int ProcessThreadInit(task_t *ptask);
    int ProcessThreadExit(task_t *task);
    int ProcessLoadImage(vmm_t *vmm, elf32_header_t *header, int fd);
    int ProcessBuildArg(uint64_t arg_top, uint64_t *arg_bottom, const char *argv[], const char **dest_argv[]);
    void ProcessTrapFrameInit(task_t *task);
    int ProcessVmmExitWhenForking(task_t *child, task_t *parent);

    void ProcessExecInit(task_t *task);
    task_t *ProcessCreate(char **argv, char **envp, uint32_t flags);
    int SysResumeProcess(pid_t pid);
    int SysCreateProcess(char **argv, char **envp, uint32_t flags);
    int SysFork();
    pid_t SysWaitPid(pid_t pid, int *status, int op);


#endif