//file:include/os/msgpool.h
//autor:jiangxinpeng
//time:2021.6.25
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef OS_MSGPOOL_H
#define OS_MSGPOOL_H

#include <os/mutexlock.h>
#include <os/waitqueue.h>
#include <lib/list.h>
#include <lib/type.h>

typedef struct msgpool
{
    size_t msgsize;      //message size
    size_t msgcount;     //message count
    size_t msgmax;       //message max count
    uint8_t *head;       //message head
    uint8_t *tail;       //message tail
    uint8_t *msgbuff;    //message buffer
    mutexlock_t mutex;   //message mutex
    wait_queue_t waiter; //message waiter
} msgpool_t;

typedef void (*msgpool_func_t)(msgpool_t *, void *);

static inline int MsgpoolFull(msgpool_t *pool)
{
    return pool->msgcount > pool->msgmax;
}

static inline int MsgpoolEmpty(msgpool_t *pool)
{
    return !(pool->msgcount);
}

msgpool_t *MsgpoolCreate(size_t msgsize, size_t msgcount);
int MsgpoolDestroy(msgpool_t *pool);
int MsgpoolPut(msgpool_t *pool, void *buff, size_t size);
int MsgpoolTryPut(msgpool_t *pool, void *buff, size_t size);
int MsgpoolGet(msgpool_t *pool, void *buff, msgpool_func_t callback);
int MsgpoolTryGet(msgpool_t *pool, void *buff, msgpool_func_t callback);
#endif