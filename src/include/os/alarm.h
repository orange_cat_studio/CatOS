//file:include/os/alarm.h
//autor:jiangxinpeng
//time:2021.1.19
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.
#ifndef OS_ALARM_H
    #define OS_ALARM_H
    
    #include <lib/type.h>

    typedef struct alarm
    {
        int flags; //available flags
        uint32_t ticks;     //ticks count
        uint32_t second;    //second count
    }alarm_t;


static inline void AlarmInit(alarm_t *alarm)
{
    alarm->flags=0;
    alarm->ticks=0;
    alarm->second=0;
}

int SysAlarm(uint32_t second);
void AlarmUpdateTicks();
#endif