#ifndef OS_GUID_H
    #define OS_GUID_H

    #include<lib/type.h>
    #include<lib/string.h>
    #include<lib/time.h>
    #include<lib/math.h>

    #define GUID_BASIC_STRING 0x303631364348494E414A58504341544F
    #define GUID_BASIC_TIME   0x134628A

    #define GUID_LEN 16
    #define GUID_BITS 128

#endif