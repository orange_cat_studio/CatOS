// file: os/diskman.h
// autor: jiang xinpeng
// time:2021.2.14
// copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef OS_DISKMAN_H
#define OS_DISKMAN_H

#include <os/driver.h>
#include <sys/res.h>
#include <arch/atomic.h>

// max disk solt num
#define DISK_SOLT_NUM 10
#define DISK_MAX DISK_SOLT_NUM

#define DISK_PARITION_MAX 6

#define DISK_TYPE_DISK 0 // disk device
#define DISK_TYPE_VOl 1  // vol device

// disk info
typedef struct
{
    list_t list;                   // disk list
    int id;                        // disk id
    char virname[DEVICE_NAME_LEN+1]; // disk name
    int solt;                      // solt position
    devent_t devtern;              // device entry info
    int handle;                    // source handle
    atomic_t ref;                  // reference
    uint8_t type;                  // disk type
} disk_t;

typedef struct
{
    int (*open)();
    int (*close)();
    int (*read)();
    int (*write)();
    int (*ioctl)();
} disk_manager_t;

// global diskman
extern disk_manager_t diskman;
// pysical driver map table
extern int driver_map[DISK_MAX];

// disk manager function
int DiskManagerInit();
int DiskManagerProbeDevice(int type);
void DiskInfoPrint();
int DiskSoltFind(const char *name);
int DiskSoltFindByPath(const char *path);
int DiskSoltindById(int id);
int DiskSoltFindDiskById(int id);
int DiskSoltFindVolById(int id);

int DiskIsValid(int solt);
int DiskIdFind(const char *name);
int DiskDel(device_object_t *device);
int DiskAdd(device_object_t *device, uint8_t type);
disk_t *DiskFind(char *name);
disk_t *DiskFindBySolt(int solt);
disk_t *DiskFindById(int id);
int SysDiskInfoGet(char *name, disk_t *buff);

#endif