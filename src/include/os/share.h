#ifndef OS_SHARE_H
#define OS_SHARE_H

#include <lib/type.h>
#include <os/semaphore.h>

#define SHARE_MEM_NAME_LEN 24
#define SHARE_MEM_NUM 128

#define SHARE_MEM_MAX_SIZE 128

#define SHARE_MEM_PRIVATE 0x01

typedef struct share_mem
{
    uint32_t id;                   //share mem id
    uint32_t pybase;               //share pybase
    uint32_t count;                //pyhics pages count
    uint32_t flags;                //flags
    atomic_t ref;                  //ref count
    semaphore_t mutex;             //share mem semaphore
    char name[SHARE_MEM_NAME_LEN]; //share mem name
} share_mem_t;

void ShareMemInit();
share_mem_t *ShareMemAlloc(char *name, uint32_t size);
int ShareMemFree(share_mem_t *share);
share_mem_t *ShareMemFindByName(char *name);
share_mem_t *ShareMemFindById(uint32_t id);
share_mem_t *ShareMemFindByAddr(uint32_t pybase);
int ShareMemInc(int id);
int ShareMemDec(int id);
int ShareMemGet(char *name, uint32_t size, uint32_t flags);
int ShareMemUp(int id);
int ShareMemDown(int id, int flags);
int ShareMemPut(int id);
void *ShareMemMap(int shmid, void *shmaddr, int shmflags);
int ShareMemUnmap(const void *shmaddr, int shmflag);
int SysShareMemGet(char *name, uint32_t val, uint32_t flags);
int SysShareMemPut(int id);
int SysShareMemDown(int id, int flags);
int SysShareMemUp(int id);
void *SysShareMemMap(int shmid, void *shmaddr, int shmflag);
int SysShareMemUnmap(void *shmaddr, int shmflag);

#endif