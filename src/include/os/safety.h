#ifndef OS_SAFETY_H
    #define OS_SAFETY_H

    #include<lib/type.h>

    int SafetyCheckRange(void *src,uint64_t bytes);
    int MemCopyFromUser(void *des,void *src,uint64_t bytes);
    int MemCopyToUser(void *des,void *src,uint64_t bytes);
#endif