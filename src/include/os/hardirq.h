//file:include/os/hardirq.h
//autor:jiangxinpeng
//time:2021.3.22
//copyright:(C) by jiangxinpeng,All right are reserve.

#ifndef OS_HARDIRQ_H
#define OS_HARDIRQ_H

#include <arch/interrupt.h>
#include <arch/pic.h>
#include <arch/x86.h>
#include <lib/type.h>
#include <lib/list.h>

typedef int (*irq_handler_t)(irqno_t irq, void *data);

#define IRQ_DISABLE 0x01
#define IRQ_SHARE 0x02
#define IRQ_TIMER 0x03

#define IRQ_HANDLE 0
#define IRQ_NEXTONE -1

#define IRQ_NUM_MAX 16

#define INT_CTRL_NUM 2

#define INT_CTRL_PIC    0
#define INT_CTRL_IOAPIC 1

typedef struct
{
    void (*enable)(uint8_t irq);                 //enable  irq
    void (*disable)(uint8_t irq);                //disable irq
    void (*install)(uint8_t irq, irq_handler_t handler); //install handler
    void (*uninstall)(uint8_t irq);              //uninstall handler
    void (*ack)(uint8_t irq);                    //ack irq
    void (*init)();                              //init interrupt controller
} hardware_interrupt_controller_t;

extern volatile hardware_interrupt_controller_t interrupt_controller;

//irq descript struct
typedef struct
{
    volatile hardware_interrupt_controller_t *controller;
    list_t action_list_head;
    int flags;
    atomic_t device_count;
    char *irqname;
} irq_descript_t;

//irq action
typedef struct
{
    void *data;                      //action argument
    int (*handler)(irqno_t, void *); //action handler
    list_t list;                     //action list
    char *name;                      //device name
    int flags;
} irq_action_t;

void __attribute__((optimize("O0"))) IrqDescriptInit();
irq_descript_t *IrqDescriptGet(uint32_t irq);
int IrqRegister(irqno_t irq, irq_handler_t handler, uint64_t flags, char *irqname, char *actname, void *data);
int IrqUnregister(irqno_t irq, void *data);
int __attribute__((optimize("O2"))) IrqHandle(irqno_t irq, trap_frame_t *frame);


#endif
