#ifndef OS_CLOCK_H
#define OS_CLOCK_H

#include <arch/time.h>
#include <lib/type.h>

//clock time type
#define CLOCK_REALTIME 1         //system current time
#define CLOCK_BOOTTIME 2         //system boot time
#define CLOCK_PROCCESS_CPUTIME 3 //proces running time
#define CLOCK_THREAD_CPUTIME 4   //thread running time

//micro seconds per ticks
#define MS_PER_TICKS (1000 / HZ)

#define MSEC_TO_TICKS(msec) ((msec) / MS_PER_TICKS)

#define TICHS_TO_MSEC(ticks) ((ticks)*MS_PER_TICKS)

extern volatile clock_t sys_ticks;
extern volatile clock_t timer_ticks;

#define TimeAfter(cur, sys) (cur - sys > 0)
#define TimeBefor(cur, sys) (cur - sys < 0)
#define TimeAfterEqual(cur, sys) (cur - sys >= 0)
#define TimeBeforEqual(cur, sys) (cur - sys <= 0)

void ClockInit();
void Mdelay(time_t msec);
clock_t ClockDelayByTicks(clock_t ticks);
clock_t SysGetTicks();

#endif