//filename:os\hal.h
//autor:jiang xinpeng
//time:2020.12.7
//copyright:(C) 2020-2050 by Jiang xinpeng. All rights reserved.

#include<lib\type.h>
#include<os\list.h>

#ifndef OS_HARDWARE_HEARDER
    #define OS_HARDWARE_HEARDER

    //define hardware abstract layel,used to manage all hardware
    //define hardware io and operator function

    //define hareware all support function point
    struct hardware_operator
    {
        void (*Init)();                                     //init function
        void (*Open)();                                     //open function
        int (*Read)(unsigned char *name,uint32_t data);     //read function
        int (*Write)(unsigned char *name,uint32_t data);    //write function
        void (*Close)();                                    //close function
        void (*IOctrl)();                                   //IO control function
        void (*Destructor)();                               //destructor function
    };

    struct hardware_info
    {
        struct hardware_operator *haloperator;
        unsigned char *halname;
        uint8_t haltype;
        uint8_t status;
        struct list hallist;
    };

    void InitHareware();    
    struct hareware_info *SearchHardware(unsigned char *halname);

    //define hareware handel function 
    PUBLIC void HalCreate(unsigned char *name);
    PUBLIC void HalDestructor(unsigned char *name);
    PUBLIC void HalOpen(unsigned char *name);
    PUBLIC void HalClose(unsigned char *name);
    PUBLIC void HalWrite(unsigned char *name,uint32_t *buffer,uint32_t count);
    PUBLIC void HalRead(unsigned char *name,uint32_t *buffer,uint32_t count);
    PUBLIC void HalIOCtrl(unsigned char *name,uint32_t cmd,uint32_t argument);

    //init a hal
    #define HAL_INIT(op,value) {&op,value}

    #define HAL_CREATE(name,op,value) struct hardware_info hal=HAL_INIT(op,value)

    #define HAL_EXTERN(name) extern struct hardware_info (name)

    //include all hareware header file
    #include<kernel\hal\ram.h>
    #include<kernel\hal\cpu.h>

#endif