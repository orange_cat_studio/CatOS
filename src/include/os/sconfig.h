#ifndef OS_SCONFIG_H
    #define OS_SCONFIG_H

    int SConfigSetSeparator(char separator);
    char SConfigGetSeparator();
    int SConfigBool(const char *str);
    int SConfigInt(const char *str);
    int SConfigWrite(char *line, const char *str);
    char *SConfigRead(char *line, const char *str, int len);
    int SConfigTrim(char *str);
    int SConfigWriteLine(char *line);
    char *SConfigReadLine(char *buff, const char *line, int len);
#endif