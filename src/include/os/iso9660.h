#ifndef OS_ISO9660_H
#define OS_ISO9660_H

#ifdef _cplusplus
extern "C"
{
#endif

#include <lib/type.h>

#define L9660_SEEK_END -1
#define L9660_SEEK_SET 0
#define L9660_SEEK_CUR +1

  typedef enum
  {
    L9660_OK = 0,
    L9660_EIO,
    L9660_EBADFS,
    L9660_ENOENT,
    L9660_ENOTFILE,
    L9660_ENOTDIR,
  } l9660_status;

  typedef struct
  {
    uint8_t le[2];
  } l9660_luint16;

  typedef struct
  {
    uint8_t be[2];
  } l9660_buint16;

  typedef struct
  {
    uint8_t le[2], be[2];
  } l9660_duint16;

  typedef struct
  {
    uint8_t le[4];
  } l9660_luint32;

  typedef struct
  {
    uint8_t be[4];
  } l9660_buint32;

  typedef struct
  {
    uint8_t le[4], be[4];
  } l9660_duint32;

  /* Descriptor time format */
  typedef struct
  {
    char d[17];
  } l9660_desctime;

  /* File time format */
  typedef struct
  {
    char d[7];
  } l9660_filetime;

  /* Directory entry */
  typedef struct l9660_dirent
  {
    uint8_t length;
    uint8_t xattr_length;
    l9660_duint32 sector;
    l9660_duint32 size;
    l9660_filetime time;
    uint8_t flags;
    uint8_t unit_size;
    uint8_t gap_size;
    l9660_duint16 vol_seq_number;
    uint8_t name_len;
    char name[0];
  } l9660_dirent_t;

  /* Volume descriptor header */
  typedef struct l9660_vdesc_header
  {
    uint8_t type;
    char magic[5]; // CD001
    uint8_t version;
  } l9660_vdesc_header_t;

  /* Primary volume descriptor */
  typedef struct l9660_vdesc_primary
  {
    l9660_vdesc_header_t hdr;
    char pad0[1];
    char system_id[32];
    char volume_id[32];
    char pad1[8];
    l9660_duint32 volume_space_size;
    char pad2[32];
    l9660_duint16 volume_set_size;
    l9660_duint16 volume_seq_number;
    l9660_duint16 logical_block_size;
    l9660_duint32 path_table_size;
    l9660_luint32 path_table_le;
    l9660_luint32 path_table_opt_le;
    l9660_buint32 path_table_be;
    l9660_buint32 path_table_opt_be;
    union
    {
      l9660_dirent_t root_dir_ent;
      char pad3[34];
    };
    char volume_set_id[128];
    char data_preparer_id[128];
    char app_id[128];
    char copyright_file[38];
    char abstract_file[36];
    char bibliography_file[37];
    l9660_desctime volume_created, volume_modified, volume_expires, volume_effective;
    uint8_t file_structure_version;
    char pad4[1];
    char app_reserved[512];
    char reserved[653];
  } l9660_vdesc_primary_t;

  typedef struct l9660_fs l9660_fs_t;

  typedef struct l9660_file
  {
#ifndef L9660_SINGLEBUFFER
    /* single sector buffer */
    char buf[2048];
#endif
    l9660_fs_t *fs;
    uint32_t first_sector;
    uint32_t position;
    uint32_t length;
  } l9660_file_t;

  typedef struct l9660_dir
  {
    /* directories are mostly just files with special accessors, but we like type safetey */
    struct l9660_file file;
  } l9660_dir_t;

  typedef struct l9660_fs_status
  {
    l9660_fs_t *fs;
    l9660_dir_t root_dir;
    l9660_dir_t now_dir;
  } l9660_fs_status_t;

  /* A generic volume descriptor (i.e. 2048 bytes) */
  typedef union l9660_vdesc
  {
    l9660_vdesc_header_t hdr;
    char _bits[2048];
  } l9660_vdesc_t;

  typedef struct l9660_file l9660_file_t;

  typedef struct l9660_fs
  {
#ifdef L9660_SINGLEBUFFER
    union
    {
      l9660_dirent_t root_dir_ent;
      char root_dir_pad[34];
    };
#else
  /* Sector buffer to hold the PVD */
  l9660_vdesc_t pvd;
#endif

    l9660_dir_t *cur_dir;

    /* read_sector func */
    bool (*read_sector)(struct l9660_fs_t *fs, void *buf, uint32_t sector);
    int disk_number;
    struct l9660_fs_status *fs_m;
  } l9660_fs_t;

  l9660_status l9660_readdir(l9660_dir_t *dir, l9660_dirent_t **pdirent);
  l9660_status l9660_opendir(l9660_dir_t *dir, l9660_fs_t *fs, char *dictname);
  l9660_status l9660_mount(l9660_fs_t *fs, int driver);
  l9660_status l9660_seek(l9660_file_t *f, int whence, int32_t offset);
  l9660_status l9660_openroot(l9660_dir_t *dir, l9660_fs_t *fs);
  l9660_status l9660_opendir(l9660_dir_t *dir, l9660_fs_t *fs, char *dictname);
  l9660_status l9660_readfile(l9660_fs_t *fs, l9660_file_t *file, char *buffer, int len);
  l9660_status l9660_fsize(l9660_file_t *file,uint32_t *fsize);
  l9660_status l9660_openfile(l9660_fs_t *fs, l9660_file_t *file, char *path);

#define l9660_seekdir(dir, pos) (l9660_seek(&(dir)->file, L9660_SEEK_SET, (pos)))
#define l9660_telldir(dir) (l9660_tell(&(dir)->file))
#define get_root_dir(fs) (&(fs->cur_dir))

#define SEEK_END L9660_SEEK_END
#define SEEK_SET L9660_SEEK_SET
#define SEEK_CUR L9660_SEEK_CUR

#define DENT_EXISTS (1 << 0)
#define DENT_ISDIR (1 << 1)
#define DENT_ASSOCIATED (1 << 2)
#define DENT_RECORD (1 << 3)
#define DENT_PROTECTION (1 << 4)
#define DENT_MULTIEXTENT (1 << 5)

#define PVD(vdesc) ((l9660_vdesc_primary_t *)(vdesc))

#ifdef L9660_BIG_ENDIAN
#define READ16(v) (((v).be[1]) | ((v).be[0] << 8))
#define READ32(v) (((v).be[3]) | ((v).be[2] << 8) | ((v).be[1]) << 16 | ((v).be[0] << 24))
#else
#define READ16(v) (((v).le[0]) | ((v).le[1] << 8))
#define READ32(v) (((v).le[0]) | ((v).le[1] << 8) | ((v).le[2]) << 16 | ((v).le[3] << 24))
#endif

#ifndef L9660_SINGLEBUFFER
#define HAVEBUFFER(f) (true)
#define BUF(f) ((f)->buf)
#else
#define HAVEBUFFER(f) ((f) == last_file)
#define BUF(f) (gbuf)
    static l9660_file_t *last_file;
static char gbuf[2048];
#endif

#ifdef _cplusplus
}
#endif
#endif