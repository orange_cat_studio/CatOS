//file:src/include/os/fatconfig.h
//autor:jiangxinpeng
//time:2021.5.29
//copyright:(C) by jiangxinpeng,All right are reserved.

#ifndef OS_FATCONFIG_H
#define OS_FATCONFIG_H

#include <lib/stddef.h>
#include <os/semaphore.h>
#include <os/diskman.h>

//configuire LFN unicode characterset
//0: ANSI (tchar=char)
//1: unicode in UTF-16 (tchar=wchar)
//2: unicode in UTF-8 (tchar=char)
//3: unicode in UTF_32 (tchar=dword)
#define FS_LFN_UNICODE 0

//using int64_t file name
//0: no configuire LFN
//1:use static buff
//2:use dynamic buff
#define FS_USE_LFN 1

//buffer len of int64_t file name
#define FS_LFN_BUFF_LEN 255

//buffer len of alt file name
#define FS_SFN_BUFF_LEN 12

//general int16_t file name len
#define FS_BUFF_LEN 12

//file system sector max bytes
#define FS_SEC_MAX SECTOR_SIZE

//support max volume
#define FS_VOLUME_MAX DISK_MAX

//configuration GPT
#define FS_GPT_MIN 0x100000000

//specific the OEM code page to be used on the system
//437 - U.S.
//720 - Arabicd
//737 - Greek
//771 - KBL
//775 - Baltic
//850 - Latin 1
//852 - Latin 2
//855 - Cyrillic
//857 - Turkish
//860 - Portuguese
//861 - Icelandic
//862 - Hebrew
//863 - Canadian French
//864 - Arabic
//865 - Nordic
//866 - Russian
//869 - Greek 2
//932 - Japanese (DBCS)
//936 - Simplified Chinese (DBCS)
//949 - Korean (DBCS)
//950 - Traditional Chinese (DBCS)
//0 - Include all code pages above and configured by f_setcp()
#define FS_CODE_PAGE 437

//fs timeout configuration
#define FS_TIMEOUT 1000

#define FS_SYNC_T semaphore_t *
#endif