//file:include/os/pipe.h
//autor:jiangxinpeng
//time:2021.4.20
//copyright:(C) by jiangxinpeng,All right are reserved.

#ifndef OS_PIPE_H
#define OS_PIPE_H

#include <os/fifobuff.h>
#include <os/mutexlock.h>
#include <os/waitqueue.h>
#include <lib/type.h>
#include <lib/list.h>

#define PIPE_SIZE 4096

enum pipe_flag
{
    PIPE_NOWAIT = 0x01,
};

typedef struct
{
    list_t list;             //list
    kobjid_t id;             //id number
    fifobuff_t *fifo;        //data buffer
    flags_t flags;           //pipe flags
    uint8_t rdflags;         //reader flags
    uint8_t wrflags;         //writer flags
    atomic_t read_count;     //read refence count
    atomic_t write_count;    //write refence count
    mutexlock_t mutex;       //rd/wr mutex
    wait_queue_t wait_queue; //wait queue
} pipe_t;

pipe_t *CreatePipe();
int DestroyPipe(pipe_t *pipe);
pipe_t *PipeFind(kobjid_t id);
int PipeRead(kobjid_t id, void *buffer, size_t size);
int PipeWrite(kobjid_t id, void *buffer, size_t size);
int PipeClose(kobjid_t id, int rw);
int PipeIoCtl(kobjid_t id, uint32_t cmd, void *arg, uint32_t rw);
int PipeIncRef(kobjid_t id, int rw);
int PipeDecRef(kobjid_t id, int rw);
int PipeClear(pipe_t *pipe);

#endif