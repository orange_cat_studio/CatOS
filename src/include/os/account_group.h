#ifndef __OS_ACCOUNT_GROUP_H
#define __OS_ACCOUNT_GROUP_H

#include<os/mutexlock.h>
#include<lib/type.h>
#include<lib/list.h>

#define GROUP_MAX      32    //number of group
#define GROUP_USER_NUM 32   //number of users in group

#define GROUP_NAME_LEN 16   //group name len

#define GROUP_NAME_BUFF_LEN (GROUP_NAME_LEN+1)
#define GROUP_ATTR_BUFF_LEN 4
#define GROUP_USER_BUFF_LEN (sizeof(uid_t)*GROUP_USER_NUM)+1

#define GROUP_FILE_NAME "account_group"

//group config len
#define GROUP_CONFIG_LEN (GROUP_NAME_LEN+GROUP_ATTR_BUFF_LEN+GROUP_USER_BUFF_LEN) 

//group name valid
#define GROUP_NAME_VALID(name) (((name) >= '0' && (name) <= '9') || ((name) >= 'a' && (name) <= 'z') || ((name) >= 'A' && (name) <= 'Z') || ((name) == '_'))

typedef enum
{
    GROUP_ATTR_NONE = 0,
    GROUP_ATTR_ADMIN = 1,
    GROUP_ATTR_NORMAL = 2,
    GROUP_ATTR_GUESS = 3,
}group_attr_t;

typedef struct account_group
{
    gid_t gid;                       //group id
    char group_name[GROUP_NAME_LEN+1]; //group name
    uid_t user[GROUP_USER_NUM];     //user id
    uint32_t count;                 //user number
    uint8_t attr;                   //group attribute
    int flags;                      //in use
    mutexlock_t lock;               //group lock
}account_group_t;

extern account_group_t account_group[GROUP_MAX];

void AccountGroupManagerInit();
account_group_t* AccountGroupAlloc();
void AccountGroupFree(account_group_t* group);
account_group_t* AccountGroupFindByName(char* name);
int AccountGroupPush(uint32_t attr, char* name);
int AccountGroupPop(char* name);
int AccountGroupNameCheck(char* name);
int AccountGroupRegister(uint32_t attr, char* name);
int AccountGroupUnRegister(char* name);
int AccountGroupSync();
int AccountGroupScanLine(char* line);
int AccountGroupAddUser(char* user, char* name);
int AccountGroupDelUser(char* user, char* name);
int AccountGroupFindUserById(account_group_t* group, uid_t uid);
int SysCreateGroup(char* name, int attr);
int SysRemoveGroup(char *name);
int SysGroupAddUser(char *name,char *user);
int SysGroupDelUser(char *name,char *user);

#endif 