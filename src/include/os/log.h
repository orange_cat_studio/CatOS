#ifndef __OS_LOG_H
#define __OS_LOG_H

#include<os/walltime.h>
#include<lib/list.h>

#define LOG_DIR_PATH "/sys/log"

#define LOG_SYSTEM_PATH "/sys/log/system.log"
#define LOG_APP_PATH "/sys/log/app.log"

#define LOG_TITLE_LEN 15
#define LOG_CONTEXT_LEN 64

typedef enum
{
    LOG_TYPE_SYSTEM,
    LOG_TYPE_APP,
}log_type_t;

typedef enum
{
    LOG_EVNET_TYPE_ERR,
    LOG_EVENT_TYPE_WARN,
    LOG_EVNET_TYPE_NORMAL,
    LOG_EVENT_TYPE_DEBUG,
}log_event_type_t;

typedef struct
{
    uint32_t id;    //log id
    uint32_t year;  //log year
    uint32_t month; //log month
    uint32_t day;   //log day
    uint32_t hour;  //log hour
    uint32_t minute; //log minute
    uint32_t second; //log second
    log_type_t type; //log type
    log_event_type_t event_type; //event type
    uint32_t error_code;         //log error code
    char title[LOG_TITLE_LEN+1];   //log title
    char context[LOG_CONTEXT_LEN+1]; //log context
    char *extern_context; //long data context 
    list_t list; //list
}log_t;

void LogInit();
int LogWrite(char* name, char* context, uint32_t error, uint8_t type, uint8_t event_type);
int LogFind(int title, int type, log_t* buff);
int LogDel(int title, int type);
int LogRead(int type, log_t* buff);
void LogDump();
int SysLogWrite(int type,char *title,char *context,int event_type,int err);
int SysLogRead(int type,log_t *buff);
int SysLogDel(int type,char *title);
int SysLogFind(int type,char *title,char *buff);
#endif