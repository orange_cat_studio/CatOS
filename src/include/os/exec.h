#ifndef OS_EXEC_H
    #define OS_EXEC_H

    #include<os/elf32.h>

    void DumpHeader(elf32_header_t *header);
    int SysExec(const char *path, const char *argv[], const char *envp[]);
    

#endif