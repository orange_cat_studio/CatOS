#ifndef __OS_SWAP_H
#define __OS_SWAP_H

#include <lib/type.h>
#include <arch/const.h>

// swap file name
#define SWAP_FILE_NAME "/swap"

// swap file size
#define SWAP_FILE_SIZE (5 * MB)

// config block device used to swap space
#define SWAP_BLOCK_DEVICE "/dev/hd0p1"

// swap magic
#define SWAP_MAGIC 0x676584207983

// swap file header
typedef struct
{
    uint64_t total_size;  // total size
    uint64_t used_size;   // used size
    uint64_t free_size;   // free size
    uint64_t bitmap_size; // bitmap size
    uint64_t last_alloc;  // last alloc
    uint64_t free_off;    // free area offset
    uint64_t magic;       // magic
} swap_header_t;

int SwapInit();
int SwapIn(uint64_t addr);
int SwapOut(uint64_t addr);
int SwapTrySwapIn(address_t address);
int SwapTrySwapOut();
int SwapAllocBitmap();
int SwapFreeBitmap(uint64_t bit);
int SwapTestBitmap(uint64_t bit);
void SwapDump();

#endif