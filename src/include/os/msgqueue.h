#ifndef OS_MSGQUEUE_H
    #define OS_MSGQUEUE_H

    #include<lib/list.h>
    #include<os/waitqueue.h>
    #include<os/semaphore.h>

    //queue name len
    #define MSGQUEUE_NAME_LEN 24

    //message len max
    #define MSG_MAX_LEN 1000

    //message queue num max
    #define MSGQUEUE_MAX_NUM 128

    //how much messages in message queue
    #define MSGQUEUE_MAX_MSG 128
    
    typedef struct 
    {
        list_t list;    //message list
        int64_t type;      //message type
        uint64_t length;   //message length
        char *buff;         //message buffer 
    }msg_t; 

    typedef struct 
    {
        uint16_t id;                    //message queue id
        list_t list;                    //message list
        uint64_t msgnum;               //message number
        uint64_t msgsize;              //message size
        wait_queue_t sender;           //sender waitqueue
        wait_queue_t recver;            //recevicer waitqueue
        semaphore_t mutex;              //queu lock
        char name[MSGQUEUE_NAME_LEN];   //message queue name len
    }msgqueue_t;

    msgqueue_t *MsgQueueAlloc(char *name);
    void MsgQueueFree(msgqueue_t *msgqueue);
    void MsgQueueInit();
    void MsgInit(msg_t *msg,int64_t type,void *buff,size_t length);
    int MsgQueueGet(char *name, uint32_t flags);
    int MsgQueuePut(int msgqueueid);
    int MsgQueueSend(int msgqueueid,void *msgbuff,size_t size,int msgflags);
    int MsgQueueRecv(int msgqueueid,void *msgbuff,size_t size,int64_t msgtype,int64_t msgflags);
    int SysMsgQueueSend(int msgqueueid,void *msgbuff,size_t size,int msgflags);
    int SysMsgQueueRecv(int msgqueueid,void *msgbuff,size_t size,int msgflags);
    int SysMsgQueueGet(char *name,uint32_t flags);
    int SysMsgQueuePut(int msgqueueid);
#endif