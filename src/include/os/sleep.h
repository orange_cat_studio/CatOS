#ifndef OS_SLEEP_H
#define OS_SLEEP_H

#include <lib/type.h>

uint64_t TaskSleepByTicks(clock_t ticks);
uint32_t SysSleep(uint32_t second);
#endif