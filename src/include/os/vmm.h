// file:src/include/os/vmm.h
// autor:jiangxinpeng
// time:2021.3.4
// uptime:2021.3.16
// copyright:(C) by jiangxinpeng,All right are reserved.
#ifndef OS_VMM_H
#define OS_VMM_H

#include <lib/type.h>
#include <arch/pymem.h>
#include <arch/page.h>

#define USER_VMM_SIZE USER_SPACE_SIZE
#define USER_VMM_BASE_ADDR USER_SPACE_START_ADDR
#define USER_VMM_TOP_ADDR (USER_VMM_BASE_ADDR + USER_VMM_SIZE)
#define USER_STACK_TOP (USER_VMM_TOP_ADDR - PAGE_SIZE)

typedef struct
{
    void *page_storge;
    list_t page_info_list;
    list_t mem_space_head;
    void **envp;
    void **argu;
    void *argbuff;
    uint32_t code_start, code_end;   // code space
    uint32_t data_start, data_end;   // data space
    uint32_t heap_start, heap_end;   // heap space
    uint32_t map_start, map_end;     // map space
    uint32_t stack_start, stack_end; // stack space
} vmm_t;

// mme status struct
typedef struct
{
    uint64_t total;
    uint64_t free;
    uint64_t used;
} mstatus_t;

void VmmInit(vmm_t *vmm);
void VmmFree(vmm_t *vmm);
int VmmExit(vmm_t *vmm);

int VmmReleaseSpace(vmm_t *vmm);
int VmmUnMapSpace(vmm_t *vmm);
int VmmUnMapOnlyMapSpace(vmm_t *vmm);
int VmmBuildArgBuff(vmm_t *vmm, char **argv, char **envp);
void VmmDeBuildArgBuff(vmm_t *vmm);
void VmmDump(vmm_t *vmm);
int VmmExitWhenForkFailed(vmm_t *child_vmm, vmm_t *parent_vmm);
int VmmCopyMapSpace(vmm_t *child_vmm, vmm_t *parent_vmm);
int VmmCopyMapping(task_t *child, task_t *parent);
int SysMstatus(mstatus_t *ms);
#endif