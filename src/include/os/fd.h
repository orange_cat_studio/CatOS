#ifndef OS_FD_H
#define OS_FD_H

#include <lib/type.h>

#define FILE_FD_KERNEL 0x01
#define FILE_FD_DEVICE 0x02
#define FILE_FD_SOCKET 0x04
#define FILE_FD_FIFO 0x08
#define FILE_FD_PIPE0 0x10
#define FILE_FD_PIPE1 0x20

#define FILE_FD_TYPE_MASK (0xff)

#define FILE_FD_ALLOC (1 << 31)

#define FILE_FD_CLOSEEXEC 0x1000 //close fd when exec

#define FILE_FD_BAD(ffd) ((!ffd) || (ffd->handle < 0) || (!ffd->flags) || (!ffd->fsal))

typedef struct
{
    int handle;      //object handler
    uint32_t flags;  //object flags
    offset_t offset; //data offset
    fsal_t *fsal;    //file opeartor
} file_fd_t;

int FsFdInit(task_t *task);
int FsFdExit(task_t *task);
int FsFdCopy(task_t *des, task_t *src);
file_fd_t *FdToFile(int fd_idx);
int HandleToFd(int handle, uint32_t flags);
int FsFdAlloc(int base);
int FsFdFree(int idx);
void FsFdSetFsal(file_fd_t *fd, uint32_t flags);
int FsFdInstallTo(int resid, int newid, int flags);
int FsFdInstall(int resid, int flags);
int FsFdInstallBase(int resid, int flags, int base);
int FsFdUnInstall(int id);
int FsFdCopyOnly(task_t *des, task_t *src);

#endif