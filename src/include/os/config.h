// filename:os\config.h
// autor:jiang xinpeng
// time:2020.12.7
// copyright:(C) 2020-2050 by Jiang xinpeng. All rights reserved.

#ifndef OS_CONFIG_H
#define OS_CONFIG_H

#include <arch/arch.h>
#include <arch/config.h>
#include <os/kernel.h>

// this file used to define system config,config kernel envorment

// indiate system arch
#ifndef CONFIG_ARCH
#define CONFIG_ARCH __ARCH
#endif

#ifndef __ARCH_STR
#if __ARCH == X86
#define __ARCH_STR "X86"
#elif __ARCH == X64
#define __ARCH_STR "X86-64"
#else
#error "[config] arch string make error! unknow arch"
#endif
#endif

// if define micro enable PAE
#ifndef CONFIG_PAE
// #define CONFIG_PAE
#endif

// system name and version
#ifndef SYSNAME
#define SYSNAME KERNEL_NAME
#endif

#ifndef SYSVER
#define SYSVER KERNEL_VERSION
#endif

// host name length
#ifndef HOST_NAME_LEN
#define HOST_NAME_LEN 32
#endif

// cpu width
#ifndef CONFIG_CPU_WIDTH
#if __ARCH == X86
#define CONFIG_CPU_WIDTH 32
#elif
#define CONFIG_CPU_WIDTH 64
#else
#error "[config] unknow arch for cpu width"
#endif
#endif

// show mode
#define CONFIG_SHOW_TEXT
// #define CONFIG_SHOW_GRAPHIC

// net config
#ifndef CONFIG_NET
#define CONFIG_NET 0
#endif

// debug config
#ifndef DEBUG_KERNEL
#define DEBUG_KERNEL 1
#endif

// driver debug enable
#ifndef DEBUG_DRIVER
#define DEBUG_DRIVER 1
#endif

// fs debug enable
#ifndef DEBUG_FS
#define DEBUG_FS 0
#endif

// config CD boot
#ifndef CONFIG_CD
#define CONFIG_CD 0
#endif

// config swap space
#ifndef SWAP_ENABLE
#define SWAP_ENABLE 1
#endif

// enable mem debug output
// #define MEM_DEBUG

// enable large mem alloc
#define CONFIG_LARGE_MEM

#endif