#ifndef __OS_ENVIRON_H
#define __OS_ENVIRON_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <lib/list.h>

#define ENV_NAME_LEN_MAX 16
#define ENV_VAL_LEN_MAX 64
#define ENV_BUFF_LEN (ENV_NAME_LEN_MAX + ENV_VAL_LEN_MAX+1)

    typedef struct env
    {
        char name[ENV_NAME_LEN_MAX+1];
        char val[ENV_VAL_LEN_MAX+1];
        list_t list;
    } env_t;

    int EnvInit();
    int EnvPush(const char *name, const char *val, int overwrite);
    int EnvPop(const char *name);
    void EnvPopAll();
    env_t *EnvFindByName(const char *name);
    int EnvClear();
    char *EnvGet(const char *name);
    void EnvDump();
    int EnvSet(const char *name, const char *value, int overwrite);
    char **EnvTranslate(env_t *env);
    
    int EnvUnset(const char *name);
    int SysEnvSet(const char *name, const char *val, int overwrite);
    int SysEnvUnset(const char *name);
    char *SysEnvGet(const char *name,const char *val,int vlen);
    int SysEnvClear();
    int SysEnvPush(const char *name, const char *val, int overwrite);
    env_t *SysEnv(env_t *p, env_t *env);

#ifdef __cplusplus
}
#endif
#endif