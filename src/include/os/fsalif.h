//file:src/include/os/fsalio.h
//autor:jiangxinpeng
//time:2021.2.25
//copyright:(C) by jiangxinpeng,All right are reserved.


#ifndef OS_FSALIF_H
    #define OS_FSALIF_H

    #include<os/fsal.h>
    #include<arch/atomic.h>
    #include<lib/type.h>

    #define FSAL_FSTYPE_DEFAULT "fat32" //default mount fstype when assign "auto" type on mount

    extern fsal_t fsalif;
#endif 