#ifndef OS_WAITQUEUE_H
    #define OS_WAITQUEUE_H

    #include<lib/list.h>
    #include<os/spinlock.h>

    typedef struct wait_queue
    {
        list_t wait_list;   //record process wait list
        spinlock_t lock;    //wait queue lock
    }wait_queue_t;

    #define WAIT_QUEUE_INIT(wait_queue) \
        {     .wait_list=LIST_HEAD_INIT(wait_queue.wait_list) \
            , .lock=SPIN_LOCK_INIT_UNLOCK() \
        }


    void WaitQueueAdd(wait_queue_t *wait_queue,void *task);
    void WaitQueueDel(wait_queue_t *wait_queue,void *task);
    void WaitQueueSleep(wait_queue_t *wait_queue);
    void WaitQueueWakeupAll(wait_queue_t *wait_queue);
    void WaitQueueWakeup(wait_queue_t *wait_queue);
    uint64_t WaitQueueLen(wait_queue_t *queue);

    static inline void WaitQueueInit(wait_queue_t *wait_queue)
    {
        list_init(&wait_queue->wait_list);
        SpinLockInit(&wait_queue->lock);
    }
#endif