#ifndef OS_DIRENT_H
    #define OS_DIRENT_H

    #include<lib/type.h>

    #define DIR_NAME_LEN 128

    typedef int dir_t;

    typedef struct 
    {
        size_t size;
        uint32_t time;
        uint32_t date;
        mode_t attr;
        char name[DIR_NAME_LEN];
    }dirent_t;

    #define DIRENT_RDONLY  0x01
    #define DIRENT_HIDDEN  0x02
    #define DIRENT_SYSTEM  0x04
    #define DIRENT_DIR     0x08
    #define DIRENT_ARCHIVE 0x10
    #define DIRENT_BLOCK   0x20
    #define DIRENT_CHAR    0x40
    
    int SysCloseDir(dir_t dir);
    dir_t SysOpenDir(const char *path);
    int SysReadDir(dir_t dir,dirent_t *dirent);
    int SysRewindDir(dir_t dir);
#endif