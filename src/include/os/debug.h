#ifndef OS_DEBUG_H
    #define OS_DEBUG_H

    #include<lib/type.h>

    #define PRINT_EMEPG  "<0>"  //system fault 
    #define PRINT_ERR    "<1>"  //error condition
    #define PRINT_WARNNING "<2>"  //warnning condition
    #define PRINT_NOTICE "<3>"  //kernel message
    #define PRINT_INFO   "<4>"  //information
    #define PRINT_DEBUG  "<5>"  //debug message

    //debug flags max
    #define DEBUG_FLAGS_MAX 5

    int KPrint(const char *fmt,...);
    void DebugPutStr(const char *str);
    void Spin(char *functionname);
    void DumpVal(uint64_t val);
    void Panic(const char *fmt,...);
    
    #define print_fmt(fmt) fmt
#define emeprint(fmt, ...) \
    KPrint(PRINT_EMERG print_fmt(fmt), ##__VA_ARGS__)
#define errprint(fmt, ...) \
    KPrint(PRINT_ERR print_fmt(fmt), ##__VA_ARGS__)
#define warnprint(fmt, ...) \
    KPrint(PRINT_WARING print_fmt(fmt), ##__VA_ARGS__)
#define noteprint(fmt, ...) \
    KPrint(PRINT_NOTICE print_fmt(fmt), ##__VA_ARGS__)
#define infoprint(fmt, ...) \
    KPrint(PRINT_INFO print_fmt(fmt), ##__VA_ARGS__)
#define dbgprint(fmt, ...) \
    KPrint(PRINT_DEBUG fmt, ##__VA_ARGS__)
#define logprint(fmt, ...) \
    KPrint("file:%s line:%d: " PRINT_DEBUG print_fmt(fmt), __FILE__, __LINE__, ##__VA_ARGS__)
#endif