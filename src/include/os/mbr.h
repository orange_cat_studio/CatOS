#ifndef OS_MBR_H
#define OS_MBR_H

#include <lib/type.h>

// mbr parition max
#define MBR_PARTITION_MAX 4

// MBR partition  struct
struct mbr_partition
{
    uint8_t active;    // set bits7 active flags or bootable
    uint8_t start_chs[3];
    uint8_t type;     // partition type
    uint8_t end_chs[3];
    uint32_t lba;     // start lba address
    uint32_t sectors; // number of sectors in partition
} __attribute__((packed));

typedef struct mbr_partition mbr_partition_t;

// MBR struct
struct mbr
{
    uint8_t boot_code[440];
    uint32_t disk_id;
    uint16_t reserved;
    mbr_partition_t partition[4]; // partition table
    uint16_t magic;
} __attribute__((packed));

typedef struct mbr mbr_t;

// MBR partition tern size
#define MBR_PARTTERN_SIZE 4

// MBR partition table offset
#define MBR_PARTTABLE_OFF 446

#endif