#ifndef __OS_SOCKETCACHE_H
#define __OS_SOCKETCACHE_H

#include<arch/atomic.h>
#include<lib/list.h>

typedef struct
{
    list_t list;  //list
    int socket;   //socket
    atomic_t ref; //ref count
} socket_cache_t;

socket_cache_t *SocketCacheCreate(int socket);
int SocketCacheDestroy(socket_cache_t *socache);
int SocketCacheInc(socket_cache_t *socache);
int SocketCacheDec(socket_cache_t *socache);
socket_cache_t *SocketCacheFind(int socket);


#endif