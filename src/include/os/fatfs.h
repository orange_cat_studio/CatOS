// file:src/include/os/fatfs.h
// autor:jiangxinpeng
// time:2021.2.23
// copyright:(C) by jiangxinpeng,All right are reserved.

#ifndef FAT32_FAT32_H
#define FAT32_FAT32_H

#include <os/fsal.h>
#include <os/fat.h>
#include <os/fatfs.h>
#include<os/diskman.h>
#include <lib/list.h>

extern fsal_t fatfs_fsal;

#define MOUNT_DEVICE_DISK 0 // mount device is disk
#define MOUNT_DEVICE_VOL 1  // mount device is vol


typedef struct
{
    fatfs_t *fs[DISK_MAX];         // mount object
    int ref[DISK_MAX];             // ref count
    uint16_t mount_time[DISK_MAX]; // mount time
    uint16_t mount_data[DISK_MAX]; // mount data
} fatfs_extension_t;

typedef struct
{
    file_obj_t file;         // store file info
    dir_obj_t dir;           // store dir info
    char path[MAX_PATH_LEN+1]; // store file path
    char *dir_path;          // dir path
    char *temp_dir;          // temp dir
} fatfs_file_extension_t;

typedef struct
{
    dir_obj_t dir;
} fatfs_dir_extension_t;

extern fatfs_extension_t fatfs_extension;

#define PDRV_TO_PATH(pdrv) ((pdrv) + '0')
#define PATH_TO_PDRV(path) ((path) - '0')

#endif
