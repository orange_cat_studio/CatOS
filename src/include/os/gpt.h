#ifndef OS_GPT_H
#define OS_GPT_H

#include <os/mbr.h>
#include <lib/type.h>

// gpt support max partition
#define GPT_PARTION_MAX 128

typedef struct
{
    uint8_t signature[8];     // magic "EFI PART" (45h 46h 49h 20h 50h 41h 52h 54h)
    uint32_t revision;        // gpt revision
    uint32_t header_size;     // gpt header size
    uint32_t header_checksum; // CRC32 check sum
    uint32_t reserved0;
    uint64_t header_lba;     // header lba
    uint64_t backup_lba;     // gpt header backup lba
    uint64_t first_lba;      // first used partition lba
    uint64_t last_lba;       // last used partition lba
    uint8_t disk_guid[16];   // disk GUID
    uint64_t table_lba;      // partition table lba
    uint32_t entry_num;      // partition table entry number
    uint32_t entry_size;     // partition table entry size
    uint32_t table_checksum; // partition table checksum
    uint8_t reserved1[420];  // must be 0x00
} __attribute__((packed)) gpt_header_t;

typedef struct
{
    uint8_t type_guid[16]; // type guid fixed value
    uint8_t part_guid[16]; // partition guid
    uint64_t start_lba;    // partition start lba
    uint64_t end_lba;      // partition end lba
    uint64_t attr;         // partition attribute
    uint8_t name[72];      // partition name
} __attribute__((packed)) gpt_partition_t;

typedef struct
{
    mbr_t mbr;                                  // master boot reacord
    gpt_header_t header;                        // gpt partition table header
    gpt_partition_t partition[GPT_PARTION_MAX]; // partition table
} __attribute__((packed)) gpt_t;

#define GPT_ATTR_SYS (1 << 0)
#define GPT_ATTR_EFI (1 << 1)
#define GPT_ATTR_BOOT (1 << 2)
#define GPT_ATTR_READOLNY (1 << 60)
#define GPT_ATTR_HIDDEN (1 << 61)
#define GPT_ATTR_MOUNT (1 << 62)

#endif
