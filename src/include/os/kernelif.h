#ifndef OS_KERNELIF_H
#define OS_KERNELIF_H

#include <lib/type.h>
#include <sys/status.h>

int KFileOpen(const char *path, int flags);
int KFileClose(int fd);
int KFileRead(int fd, void *buffer, size_t bytes);
int KFileWrite(int fd, void *buffer, size_t bytes);
int KFileUnlink(const char *path);
int KFileStatus(const char *path, status_t *buff);
int KFileAccess(const char *path, int mode);
int KFileLSeek(int fd, offset_t off, int where);
int KFileMkDir(const char *path, mode_t mode);
int KFileRmDir(const char *path);
int KFileMkFs(const char *dev, char *fstype, int flags);
int KFileMount(const char *src, const char *targe, char *fstype, int flags);
int KFileUnMount(const char *src);
int KFileReadDir(int dir, char *buff);
int KFileRewind(int fd);
int KFileRewindDir(int fd);
int KFileOpenDir(const char *path);
int KFileCloseDir(int dir);
int KFileFTell(int dir);
int KFileGetCWD(char *path, uint64_t len);
int KFileReName(const char *path_old, const char *path_new);
#endif