#ifndef OS_FIFO_H
#define OS_FIFO_H

#include <os/fifoio.h>
#include <os/fifobuff.h>
#include <os/mutexlock.h>
#include <os/semaphore.h>
#include <os/task.h>

#define FIFO_FSAL_PATH "/fifofs"

typedef struct
{
    int handle;
} fifo_file_extension_t;

// name len max
#define FIFO_NAME_LEN 24

// size
#define FIFO_SIZE 4096

// number
#define FIFO_NUM 12

// flags
#define FIFO_IN_READ 0x01  // readable
#define FIFO_IN_WRITE 0x02 // writeable

#define FIFO_IS_BAD_ID(id) ((id) < 0 || (id) > (FIFO_NUM - 1))

// fifo struct
typedef struct
{
    int id;             // fifo id
    uint32_t flags;     // fifo flags (0-16 publib 16-24 reader 24-31 writer)
    fifobuff_t *buffer; // fifo buffer
    task_t *reader, *writer;
    atomic_t readref, writeref;
    mutexlock_t lock;
    char name[FIFO_NAME_LEN];
} fifo_t;

// fifofs fsal
extern fsal_t fifofs_fsal;

int FifoInit();
fifo_t *FifoAlloc(const char *name);
int FifoFree(fifo_t *fifo);
int FifoOpen(const char *name, uint32_t flags);
int FifoMake(const char *name, mode_t mode);
int FifoPut(int fifoid);
int FifoGet(const char *name, uint32_t flags);
int FifoRemove(const char *name);
int SysMkFifo(const char *name, mode_t mode);
void *FifoFsPathTranslate(const char *path, const char *checkpath);
int FifoIncRef(int id);
int FifoDecRef(int id);
int FifoWrite(int id, void *buff, size_t size);
int FifoRead(int id, void *buff, size_t size);
#endif