#ifndef OS_SEM_H
    #define OS_SEM_H

    #include<os/semaphore.h>

    #define SEM_NAME_LEN 32
    #define SEM_NUM_MAX 12

    typedef struct sem
    {
       uint64_t id;
       semaphore_t semaphore;
       char name[SEM_NAME_LEN]; //name
    }sem_t;

    int SemInit();
    sem_t *SemAlloc(char *name, int value);
    int SemFree(sem_t *sem);
    int SemGet(char *name, int value, int flags);
    int SemPut(int id);
    int SemDown(int id, int flags);
    int SemUp(int id);
    int SysSemGet(char *name, int value, int flags);
    int SysSemPut(uint64_t id);
    int SysSemDown(int id,int flags);
    int SysSemUp(uint64_t id);
#endif