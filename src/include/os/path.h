// file:src/include/os/path.h
// autor:jiangxinpeng
// time:2021.2.23
// uptime:2021.5.30
// copyright:(C) by jiangxinpeng,All right are reserved.

#ifndef OS_PATH_H
#define OS_PATH_H

#include <os/fsal.h>

// define system core path
#define SWAP_DIR_PATH "/swap"
#define ROOT_DIR_PATH "/root"
#define HOME_DIR_PATH "/home"
#define ACCOUNT_DIR_PATH "/account"
#define DEV_DIR_PATH "/dev"
#define SYS_DIR_PATH "/sys"
#define FIFO_DIR_PATH "/pipe"

// max of fsal path
#define FSAL_PATH_MAX 128

// max len of fsal path
#define FSAL_PATH_LEN 32

#define FSAL_BAD_PATH_IDX(idx) ((idx) < 0 || (idx) >= FSAL_PATH_MAX)
#define FSAL_PATH_IDX2PATH(idx) ((fsal_path_table) + idx)
#define FSAL_BAD_PATH(fpath) (!((fpath)->fsal && (fpath)->alpath))

#define MOUNT_FORMAT 0x01 // format disk while mount
#define MOUNT_DELAYED 0x2 // mount after delayed

typedef enum fsal_path_type
{
    FSAL_PATH_TYPE_PHYSIC = 1, // physic path
    FSAL_PATH_TYPE_VIRTUAL,    // virtual path
    FSAL_PATH_TYPE_DEVICE,     // device path
    FSAL_PATH_TYPE_NUM
} fsal_path_type_t;

typedef struct
{
    fsal_t *fsal;
    char path[FSAL_PATH_LEN];
    char alpath[FSAL_PATH_LEN];
    char devpath[FSAL_PATH_LEN];
} fsal_path_t;

extern fsal_path_t *fsal_path_table;

int FsalPathTableInit();
int FsalPathRemove(const char *path);
int FsalPathInsert(fsal_t *fsal, const char *devpath, const char *path, const char *alpath);
fsal_path_t *FsalPathAlloc();
int FsalPathFree(fsal_path_t *path);
fsal_path_t *FsalPathFindByType(fsal_path_type_t type, const char *path, int inmaster);
int FsalPathSwitch(fsal_path_t *fpath, char *new, char *cur);
void BuildPath(const char *path, char *out);
void AbsPath(const char *path, char *abspath);
void WashPath(char *path, char *new);
char *parse_path_after(char *path, char *out);
void FsalPathPrint();

#define FsalPathFind(alpath, inmaster) FsalPathFindByType(FSAL_PATH_TYPE_VIRTUAL, (alpath), (inmaster))
#define FsalPathFindDevice(devpath) FsalPathFindByType(FSAL_PATH_TYPE_DEVICE, (devpath), 0)
#endif