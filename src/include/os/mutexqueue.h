// file: include/os/mutexquueue.h
// autor: jiangxinpeng
// time: 2021.4.3
// copyright: (C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef OS_MUTEXQUEUE_H
#define OS_MUTEXQUEUE_H

#include <os/waitqueue.h>

#define MUTEX_QUEUE_NUM_MAX 32

#define MUTEX_QUEUE_USING (1 << 0)

#define MUTEX_QUEUE_IS_BAD(handle) ((handle) < 0 || (handle) > MUTEX_QUEUE_NUM_MAX)

// flags
#define MUTEX_QUEUE_TIMED (1 << 17) // wait times
#define MUTEX_QUEUE_ALL (1 << 16)   // wake all waiter

// mutex opeartor
#define MUTEX_QUEUE_ADD (1 << 0)
#define MUTEX_QUEUE_SUB (1 << 1)
#define MUTEX_QUEUE_INC (1 << 2)
#define MUTEX_QUEUE_DEC (1 << 3)
#define MUTEX_QUEUE_ONE (1 << 4)
#define MUTEX_QUEUE_ZERO (1 << 5)
#define MUTEX_QUEUE_SET (1 << 6)

#define MUTEX_QUEUE_OPMASK 0xFFFF // operator mask

typedef struct
{
    wait_queue_t wait_queue;
    uint32_t flags;
} mutex_queue_t;

extern mutex_queue_t *mutex_queue_list;

void __attribute__((optimize("O0"))) MutexQueueInit();
int SysMutexQueueAlloc();
int SysMutexQueueFree(int handle);
int SysMutexQueueWait(int handle, void *addr, uint32_t wqflags, uint32_t val);
int SysMutexQueueWake(int handle, void *addr, uint32_t wqflags, uint32_t value);
#endif