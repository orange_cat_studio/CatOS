#ifndef OS_DISKIO_H
#define OS_DISKIO_H

#include <lib/type.h>
#include <os/fat.h>
#include <os/fs.h>

typedef enum
{
    DSTU_NOINIT,  //driver no initilization
    DSTU_NODISK,  //no medium in the drive
    DSTU_PROTECT, //write protect
    DSTU_ERROR,   //error
    DSTU_OK,      //succeed
} disk_status_t;

#define CTRL_SYNC 0x00        //complete pending write process
#define GET_SECTOR_COUNT 0x01 //get media size
#define GET_SECTOR_SIZE 0x02  //get sector size
#define GET_BLOCK_SIZE 0x03   //get block size
#define CTRL_TRIM 0x04        //inform device that the data on the block of sectors no longer used

disk_status_t DiskInit(uint8_t pydrv);
disk_status_t DiskOpen(uint8_t pydrv);
disk_status_t DiskClose(uint8_t pydrv);
disk_status_t DiskWrite(uint8_t pdrv, uint8_t *buff, lba_t sec, uint32_t count);
disk_status_t DiskRead(uint8_t pdrv, uint8_t *buff, lba_t sec, uint32_t count);
disk_status_t DiskStatus(uint8_t pydrv);
disk_status_t DiskIoCtl(uint8_t pdrv, uint32_t cmd, void *arg);
#endif