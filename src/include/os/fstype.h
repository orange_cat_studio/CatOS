#ifndef FS_FSTYPE_H
    #define FS_FSTYPE_H

    #include<os/fsal.h>

    int FsTypeInit();
    void FsTypeRegister();
    void FsTypeUnRegister();
    fsal_t *FsTypeFind(char *name);
#endif