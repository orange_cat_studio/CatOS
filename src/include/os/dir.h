//file:src/include/os/dir.h
//autor:jiangxinpeng
//time:2021.2.23
//copyright:(C) by jiangxinpeng,All right are reserved.

#ifndef OS_DIR_H
#define OS_DIR_H

#include <os/fsal.h>

#define FSAL_DIR_MAX 128

#define FSAL_DIR_FREE 0
#define FSAL_DIR_USING 1

#define FSAL_DIR2IDX(dir) ((int)((dir)-fsal_dir_table))
#define FSAL_IDX2DIR(idx) ((fsal_dir_t *)(fsal_dir_table + idx))

#define FSAL_BAD_DIR_IDX(idx) ((idx) < 0 || (idx) >= FSAL_DIR_MAX)
#define FSAL_BAD_DIR(dir) (!((dir)->fsal) || !((dir)->extension))

typedef struct
{
    int flags;
    fsal_t *fsal;
    void *extension;
} fsal_dir_t;

extern fsal_dir_t *fsal_dir_table;

int FsalDirTableInit();
fsal_dir_t *FsalDirAlloc();
int FsalDirFree(fsal_dir_t *dir);

#endif