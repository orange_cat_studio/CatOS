#ifndef OS_KERNEL_H
    #define OS_KERNEL_H

    #include<lib/type.h>
    #include<lib/stddef.h>

    #ifndef KERNEL_NAME
    #define KERNEL_NAME "CatOS"     //kernel name
    #endif

    #ifndef KERNEL_VERSION
    #define KERNEL_VERSION "0.0.1"  //version
    #endif

    #ifndef KERENL_NAME_LEN 
    #define KERNEL_NAME_LEN 32      //kernel name len
    #endif


    //sys config name
    enum sysconfig
    {
        SC_HOST_NAME_LEN=0,
        SC_PAGESIZE,
        SC_OPENMAX,
        SC_TTY_NAME_LEN,
        SC_ARG_MAX,
        SC_CLK_TCK,
        SC_VERSION,
        SC_ARCH,
        SC_CPUWIDTH,
    };
    
    int SysConfig(int name);
    int SysGetHostName(char *name,size_t len);
    void *SysGetFireworkTable(char *table,int namelen,uint64_t *table_len);
#endif