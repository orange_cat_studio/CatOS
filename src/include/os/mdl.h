//file:include/os/mdl.h
//autor:jiangxinpeng
//time:2021.3.15
//copyright:(C) by jiangxinpeng,All right are reserved.

#ifndef OS_MDL_H
    #define OS_MDL_H

    #include<os/task.h>
    #include<lib/type.h>
    #include<lib/list.h>
    #include<lib/stddef.h>

    #define MDL_SIZE_MAX (3*MB)

    typedef struct 
    {
        list_t list;   //list object 
        task_t *task;  //current task 
        void *map_vbase;//map targe vbase
        void *start_vbase;//map source vbase
        uint64_t bytes_count;//map bytes count
        uint64_t bytes_offset;//map bytes count
    }mdl_t;

    #define MDL_GET_BYTES_COUNT(mdl)  ((mdl)->bytes_count)
    #define MDL_GET_BYTES_OFFSET(mdl) ((mdl)->bytes_offset)
    #define MDL_GET_MAP_VBASE(mdl)    ((mdl)->map_vbase)
    #define MDL_GET_START_VBASE(mdl)  ((mdl)->start_map)

    mdl_t *MdlAlloc(uint64_t vbase, uint64_t len, void *ioreq);
    void MdlFree(mdl_t *mdl);
#endif