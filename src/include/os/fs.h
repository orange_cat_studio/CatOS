// file:include/kernel/fs/fs.c
// autor:jiang xinpeng
// time:2020.12.23
// copyright:(C) 2020-2050 by jiang xinpeng,All right are reserved.

#ifndef KERNEL_FS_H
#define KERNEL_FS_H

#include <lib/type.h>
#include <os/memspace.h>
#include <os/fd.h>
#include <os/dirent.h>
#include <sys/status.h>
#include <sys/select.h>

void FsInit();
int SysOpen(char *path, int flags);
int SysOpenFifo(char *fifoname, int flags);
int SysOpenDev(char *path, int flags);
int SysClose(int fd);
int SysRead(int fd, void *buff, size_t bytes);
int SysWrite(int fd, void *buff, size_t bytes);
int SysIoCtl(int fd, int cmd, void *arg);
int SysFastIo(int fd, int cmd, void *arg);
int SysFastWrite(int fd, void *buff, size_t bytes);
int SysFastRead(int fd, void *buff, size_t bytes);
int SysCwd(char *buff, int size);
int SysMkfs(char *dev, char *fstype, uint32_t flags);
void *SysMmap(mmap_args_t *args);
int SysChDir(const char *path);
int SysAccess(const char *path, int mode);
int SysRename(const char *source, const char *targe);
int SysRmDir(const char *path, mode_t mode);
int SysMkDir(const char *path, mode_t mode);
int SysGetCwd(char *buff, int size);
int SysUnMount(char *path, uint32_t flags);
int SysMount(char *source, char *targe, char *fstype, uint32_t flags);
int SysFtruncate(int fd, offset_t off);
int SysUnlink(char *name);
int SysDup2(int oldfd, int newfd);
int SysPipe(int fd[2]);
int SysTell(int fd);
int SysRewind(int fd);
int SysFsync(int fd);

int SysProbeDev(const char *name, char *buff, size_t len);
int SysFcntl(int fd, int cmd, int64_t arg);
int SysDup(int oldfd);
int SysStatus(const char *path, status_t *status);
int SysRewindDir(dir_t dir);
int SysSelect(int max, fd_set_t *readfds, fd_set_t *writefds, fd_set_t *exceptfds, timeval_t *timeout);
int SysChmod(const char *path, mode_t mode);
int SysFChmod(int fd, mode_t mode);
int SysFStatus(int fd, status_t *status);
int SysLSeek(int fd, offset_t off, int whence);
int FsIfDecRef(int fd);
int FsIfIncRef(int fd);

#endif