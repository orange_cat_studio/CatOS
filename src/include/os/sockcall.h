#ifndef __OS_SOCKCALL_H
#define __OS_SOCKCALL_H

#include <os/config.h>

#if CONFIG_NET

#include <sys/socket.h>

typedef enum
{
    SOCKOP_SOCKET = 1,
    SOCKOP_BIND,
    SOCKOP_LISTEN,
    SOCKOP_CONNECT,
    SOCKOP_ACCEPT,
    SOCKOP_RECV,
    SOCKOP_SEND,
    SOCKOP_RECVFROM,
    SOCKOP_SENDTO,
    SOCKOP_SHUTDOWN,
    SOCKOP_SETSOCKOPT,
    SOCKOP_GETSOCKOPT,
    SOCKOP_SENDMSG,
    SOCKOP_RECVMSG,
    SOCKOP_GETPEERNAME,
    SOCKOP_GETSOCKNAME,
    SOCKOP_SOCKETPAIR,
    SOCKOP_NUM,
} sockcall_opt_type;

typedef struct
{
    struct
    {
        int domain;
        int type;
        int protocal;
    } socket;
    struct
    {
        int sock;
        struct sockaddr *my_addr;
        int addrlen;
    } bind;
    struct
    {
        int sock;
        struct sockaddr *serv_addr;
        int addrlen;
    } connect;
    struct
    {
        int sock;
        struct sockaddr *addr;
        int addrlen;
    } accept;
    struct
    {
        int sock;
        int backlog;
    } listen;
    struct
    {
        int sock;
        void *buff;
        int len;
        int flags;
    } recv;
    struct
    {
        int sock;
        void *buff;
        int len;
        int flags;
    } send;
    struct
    {
        int sock;
        void *buff;
        int len;
        int flags;
        struct sockaddr *from;
        int fromlen;
    } recvfrom;
    struct
    {
        int sock;
        void *buff;
        int len;
        int flags;
        struct sockaddr *to;
        int tolen;
    } sendto;
    struct
    {
        int sock;
        struct sockaddr *serv_addr;
        int addrlen;
    } getpeername;
    struct
    {
        int sock;
        struct sockaddr *my_addr;
        int addrlen;
    } getsockname;
    struct
    {
        int sock;
        int how;
    } shutdown;
    struct
    {
        int sock;
        int level;
        int optname;
        void *optval;
        int *optlen;
    } getsockopt;
    struct
    {
        int sock;
        int level;
        int optname;
        const void *optval;
        int optlen;
    } setsockopt;
} sock_param_t;

int SysSockcall(int sockop, sock_param_t *param);
#endif
#endif