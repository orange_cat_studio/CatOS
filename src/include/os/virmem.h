// file:src/include/os/virmem.h
// autor:jiangxinpeng
// time:2021.3.15
// copyright:(C) 2020-2050 by jiang xinpeng,All right are reserved.

#ifndef OS_VIRMEM_H
#define OS_VIRMEM_H

#include <arch/pymem.h>
#include <lib/list.h>
#include <lib/type.h>
#include <lib/stddef.h>

#define VIR_MEM_BASE DYNAMIC_MAP_MEM_VBASE
#define VIR_MEM_END DYNAMIC_MAP_MEM_END

typedef struct
{
    uint64_t addr;
    uint64_t size;
    list_t list;
} vir_mem_t;

void VirMemInit();
address_t VirBaseAlloc(size_t size);
int VirBaseFree(uint64_t vbase, uint64_t size);
void *VirMemDoAlloc(size_t size);
void *VirMemAlloc(size_t size);
int VirMemDoFree(vir_mem_t *vir);
int VirMemFree(void *ptr);
void *MemIoReMap(uint64_t pybase, size_t size);
int MemIoUnMap(void *vbase);
#endif