#ifndef OS_PTHREAD_H
#define OS_PTHREAD_H

#include <lib/type.h>
#include <os/semaphore.h>

// boot status
#define PTHREAD_CREATE_DETACHED 1 // detach
#define PTHREAD_CREATE_WAIT 0     // wait
#define PTHREAD_CANCEL_ENABLE 1   // enable cancel
#define PTHREAD_CANCEL_DISABLE 0  // disable cancel

// cancel type
#define PTHRED_CANCEL_ASYNHRONOUS 0
#define PTHREAD_CANCEL_DEFFERED 1

// system max pthread create number support
#define PTHREAD_NUM_MAX 64

typedef struct
{
    atomic_t thread_count; // thread count
} pthread_desc_t;

typedef struct
{
    int detachstatus; // pthread disptch status
    void *stackaddr;  // pthread stack address
    size_t stacksize; // pthread stack size
} pthread_attr_t;

void PthreadEntry(void *arg);
void PthreadDescriptExit(pthread_desc_t *pthread);
void PthreadDescriptInit(pthread_desc_t *pthread);

task_t *PthreadStart(task_func_t *fun, void *arg, pthread_attr_t *attr, void *thread_entry);
void PthreadExit(int status);
int PthreadJoin(pthread_t thread, void **thread_return);
int WaitOneHanggingThread(task_t *parent, pid_t pid, int *status);

pid_t SysThreadCreate(pthread_attr_t *attr, task_func_t *func, void *arg, void *thread_entry);
void SysThreadExit(int status);
int SysThreadSetCancelType(int type, int *oldtype);
int SysThreadSetCancelStatus(int status, int *oldstatus);
void SysThreadTestCancel();
int SysThreadCancel(pthread_t thread);
int SysThreadDetach(pid_t thread);
int SysThreadJoin(pthread_t thread, void **thread_return);

#endif