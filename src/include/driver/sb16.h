#ifndef DRIVER_SB16_H
#define DRIVER_SB16_H

#include <os/driver.h>
#include <os/dma.h>

#define DRIVER_NAME "sound blaster 16"
#define DRIVER_VERSION "0.1"
#define DEVICE_NAME "sb16"

// dma count
#define DMA_COUNT 10

// volume level
#define VOL_LOW 0x0
#define VOL_MID 0x7
#define VOL_HIGH 0xF

#define VOLPER_TO_VAl(percent) ((percent)*VOL_HIGH)

// dsp command
enum dsp_cmd
{
    // comamnd in dsp write port
    DSP_STOP_8BIT = 0xd0,
    DSP_STOP_16BIT = 0xd5,
    DSP_STOP_AFTER_8BIT = 0xda,
    DSP_STOP_AFTER_16BIT = 0xd9,
    DSP_PLAY_8BIT = 0xc0,
    DSP_PLAY_16BIT = 0xb0,
    DSP_PAUSE_8BIT = 0xd0,
    DSP_ENABLE_SPEAKE = 0xd1,
    DSP_DISABLE_SPEACK = 0xd3,
    DSP_RESUME_8BIT = 0xd4,
    DSP_PAUSE_16BIT = 0xd5,
    DSP_RESUME_16BIT = 0xd6,
    DSP_SET_TC = 0x40,      // set time constant
    DSP_SET_INRATE = 0x42,  // set input sample rate
    DSP_SET_OUTRATE = 0x41, // set output sample rate
    DSP_GET_VERSION = 0xe1,
    // command in dsp mixed port
    DSP_MASTER_VOLUME = 0x22,

};

// dsp mode
enum dsp_mode
{
    DSP_PLAY_AL = 0x06,
    DSP_PLAY_UNSIGNED = 0x00,
    DSP_PLAY_SIGNED = 0x10,
    DSP_PLAY_MONO = 0x00,
    DSP_PLAY_STEREO = 0x20,
};

// dsp register
#define DSP_MIXED 0x224      // dsp mixed port
#define DSP_MIXED_DATA 0x225 // dsp mixed data port
#define DSP_RESET 0x226
#define DSP_WRITE 0x22C
#define DSP_READ 0x22A
#define DSP_STATUS 0x22E // dsp read status
#define DSP_R_ACK 0x22F  // dsp 16bits int respond

#define SB16_SAMPLERATE 44100 //sample rate

typedef struct
{
    dma_region_t dma_region[DMA_COUNT];
    int date_len[DMA_COUNT];
    int index_w;
    int index_r;
    int major_version;
    int minor_version;
    uint32_t volume; // play volume
    uint32_t channel; //channel 0: mono 1: stero
    uint32_t sample_rate; //sample rate
    uint32_t block_size; //block size(bytes per sample)
    wait_queue_t waiter;
} device_extension_t;

#endif