#ifndef __DRIVER_VIEW_H
#define __DRIVER_VIEW_H

#ifdef __cplusplus
extern "C" {
#endif 

#include<driver/view/view.h>

#define DEVICE_NAME "view"
#define DRIVER_NAME "view-driver"
#define DRVIER_VER "0.1"

typedef struct __device_extension
{
    view_t *view;
    uint64_t flags;
}device_extension_t;

#ifdef __cplusplus
}
#endif 
#endif 