// file: drive/cpu.h
// autor: jiang xinpeng
// time:2021.8.7
// copyright: (C) by jiangxinpeng,All right are reserved.

#ifndef DRIVER_ACPI_H
#define DRIVER_ACPI_H

#include <lib/type.h>

#define DRIVER_NAME "processor"
#define DRIVER_VERSION "0.1"

#define DEVICE_NAME "cpu"

#define CPU_VENDOR_LEN 16
#define CPU_BRAND_LEN 50
#define CPU_FAMILY_LEN 50
#define CPU_MODEL_LEN 50

#define copy_family_string(dst, src) memcpy(dst, src, CPU_FAMILY_LEN);
#define copy_model_string(dst, src) memcpy(dst, src, CPU_MODEL_LEN);

typedef struct
{
    int cpu; // cpu ID
    char vendor[CPU_VENDOR_LEN];
    char brand[CPU_BRAND_LEN];
    char family_str[CPU_FAMILY_LEN];
    char model_str[CPU_MODEL_LEN];
    uint32_t family;
    uint32_t model;
    uint32_t type;
    uint32_t stepping;
    uint32_t max_cpuid;
    uint32_t max_cpuidext;
} device_extension_t;

#endif