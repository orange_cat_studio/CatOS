#ifndef DRIVER_USB_CORE_H
#define DRIVER_USB_CORE_H

#ifdef __cplusplus
extern "C" {
#endif

#include<driver/usb/hub.h>
#include<driver/usb/xhci.h>

#ifdef __cplusplus
}
#endif 

#endif 