#ifndef DRIVER_USB_XHCI_H
#define DRIVER_USB_XHCI_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <lib/type.h>
#include <driver/usb.h>

#define USB_XHCI_CMD_REG_RUNSTOP (1 << 0)
#define USB_XHCI_CMD_REG_HUBRESET (1 << 1)

#define USB_XHCI_CRCR_CMDSTOP (1 << 1)
#define USB_XHCI_CRCR_CMDABORT (1 << 2)
#define USB_XHCI_CRCR_CMDRUN (1 << 3)

#define USB_XHCI_PORT_STATUS_RESET (1 << 4)
#define USB_XHCI_PORT_STATUS_POWER (1 << 9)
#define USB_XHCI_PORT_STATUS_DEVICEREMOVE (1 <<)
#define USB_XHCI_PORT_STATUS_ENABLE (1 << 1)
#define USB_XHCI_PORT_STATUS_CURCON (1 << 0)

#define USB_XHCI_DB_TARGE_MASK 0xff
#define USB_XHCI_DB_TARGE_SHIFT 0
#define USB_XHCI_DB_STREAM_ID_MASK 0xffff
#define USB_XHCI_DB_STREAM_ID_SHIFT 16

    bool UsbPortConnect(uint32_t port, device_extension_t *extension);
    void UsbWritePortSC(uint32_t port, device_extension_t *extension, uint32_t data);
    uint32_t UsbReadPortSC(uint32_t port, device_extension_t *extension);
    void UsbWriteCommand(device_extension_t *extension, uint32_t cmd);

#ifdef __cplusplus
}
#endif

#endif