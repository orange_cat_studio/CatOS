#ifndef __DRIVER_USB_HUB_H
#define __DRIVER_USB_HUB_H

#include<driver/usb.h>

uint32_t UsbRegRead32(device_extension_t *extension, uint32_t off);
uint16_t UsbRegRead16(device_extension_t *extension, uint32_t off);
uint8_t UsbRegRead8(device_extension_t *extension, uint32_t off);
void UsbRegWrite32(device_extension_t *extension, uint32_t off, uint32_t data);
void UsbRegWrite16(device_extension_t *extension, uint32_t off, uint16_t data);
void UsbRegWrite8(device_extension_t *extension, uint32_t off, uint8_t data);




#endif