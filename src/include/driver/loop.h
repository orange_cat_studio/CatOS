#ifndef DRIVER_LOOP_H
    #define DRIVER_LOOP_H
    
    #include<lib/type.h>

    #define DRIVER_NAME "loop-block"
    #define DRIVER_VER "0.1"

    #define DEVICE_NAME "loop"

    #define LOOP_FLAG_USING (1<<31)

    #define LOOP_IMAGE_PATH_LEN 128
    #define LOOP_DEVICE_NUM 4

    #define IS_LOOP_USING(loop) (loop->flags&LOOP_FLAG_USING)

    typedef struct device_extension
    {
        char image_file[LOOP_IMAGE_PATH_LEN];   //image file path
        int flags;                              //flags
        uint32_t rwoff;                         //read/write offset
        uint32_t sectors;                       //disk sector num
        int glofd;                           ///global file descript     };
    }device_extension_t;
    
#endif