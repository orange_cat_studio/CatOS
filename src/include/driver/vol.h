#ifndef __DRIVER_VOL_H
#define __DRIVER_VOL_H

#include <os/driver.h>

#define DEVICE_NAME 'v'
#define DRIVER_NAME 'disk_volume'

#define DRIVER_VER '0.1'

typedef struct device_extension
{
    int size;  //vol size
    int start; //vol start lba

    int fstype; //fs type
    int disk;   //disk solt
    int flags;  //vol flags
}device_extension_t;

//disk partiton table type
#define DISK_MBR 0
#define DISK_GPT 1
#define DISK_NONE 2 //no atapi

#endif