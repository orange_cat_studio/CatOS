#ifndef __DRIVER_PTTY_H
#define __DRIVER_PTTY_H

#define DEVICE_NAME_MASTER "ptm"
#define DEVICE_NAME_SLAVE "pts"

#define DRIVER_NAME "pseudo-terminal"
#define DRIVER_VERSION "0.1"

typedef enum 
{
    PTTY_MASTER , //master endian
    PTTY_SLAVE,   //slave endian
}ptty_endian_t;

typedef enum
{
    PTTY_RDNOBLK=0x01,
    PTTY_WRNOBLK=0x02,
}ptty_noblk_t;

//ptty master num
#define PTM_NUM 8 

typedef struct __device_extension
{
    int locked; 
    int opened;
    device_object_t *other_object;
    int type;
    int device_id;
    pid_t pgrp;
    pipe_t *pipe_in;
    pipe_t *pipe_out;
    int flags;
}device_extension_t;


#endif 
