#ifndef __DRIVER_RAMDISK_H
#define __DRIVER_RAMDISK_H

#include <lib/type.h>
#include <lib/stddef.h>

#define DEVICE_NAME "ram0"

#define DRIVER_NAME "virtual-ramdisk"
#define DRIVER_VER "0.1"

// ram disk sector size (80MB)
#if 1
#define RAMDISK_SECTOR (20480 * RAMDISK_SEG_SEC)
#else 
#define RAMDISK_SECTOR RAMDISK_SEG_SEC*2048
#endif

#define RAMDISK_SEG_SEC 8 // sectors per segment

// ram disk segs (8 sectors)
#define RAMDISK_MAP_SEGS (RAMDISK_SECTOR / 8)
#define RAMDISK_SEG_SIZE (RAMDISK_SEG_SEC * SECTOR_SIZE)

typedef struct device_extension
{
    device_object_t *device;
    uint8_t *buffer;  // buffer
    uint64_t len;     // buffer length
    uint64_t sectors; // buffer sectors
    uint64_t rwoff;   // buffer offset
} device_extension_t;

#endif