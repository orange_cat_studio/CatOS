#ifndef DRIVER_PCSPEAKER_H
#define DRIVER_PCSPEAKER_H

#include <arch/x86.h>
#include <arch/pit.h>

#define DRIVER_NAME "pc-speker"
#define DRIVER_VERSION "0.1"
#define DEVICE_NAME "buzzer"

//programmable peripheral interface
#define PPI_KEYBOARD 0x60     //8255 Port A keyboard input/output buffer
#define PPI_OUTPUT 0x61       //8255 Port B output
#define PPI_INPUT 0x62        //8255 Port C input
#define PPI_COMMAND_MODE 0x63 //8255 command/mode control register

typedef struct
{
    uint8_t exist;           //device exist
    uint8_t open;            //device open
    device_object_t *device; //device object
    uint8_t status;          //buzzer work status
    uint32_t freqence;       //work freqence
} device_extension_t;

#endif