#ifndef DRIVER_KEYBOARD_H
#define DRIVER_KEYBOARD_H

#include <os/driver.h>

#define DEVICE_NAME "keyboard"
#define DRIVER_NAME "input-keyboard"
#define DRIVER_VER "0.1"

// define ps2 controller io port
#define PS2CTRL_IO_DATA_PORT 0X60
#define PS2CTRL_IO_STATUS_PORT 0X64
#define PS2CTRL_IO_CMD_PORT 0X64

// define command filed
// read first byte from internal ram
#define PS2CTRL_CMD_READCONFIGBYTE 0X20
// read n bytes from internal ram
#define PS2CTRL_CMD_READCONFIGNBYTE(n) (n - 1) + PS2CTRL_CMD_READBYTE
// write next byte to first byte of internal ram
#define PS2CTRL_CMD_WRITECONFIGBYTE 0x60
// write next byte to n bytes of internal ram
#define PS2CTRL_CMD_WRITECONFIGNBYTE(n) (n - 1) + PS2CTRL_CMD_WRITEBYTE
// disable second PS/2 port
#define PS2CTRL_CMD_DISSECONDPORT 0XA7
// able second PS/2 port
#define PS2CTRL_CMD_ENSECONDPORT 0xA8
// test second PS/2 port
#define PS2CTRL_CMD_TESTSECONDPORT 0XA9
// test PS/2 controller
#define PS2CTRL_CMD_TESTCONTROLLOER 0XAA
// test first PS/2 port
#define PS2CTRL_CMD_TESTFIRSTPORT 0XAB
// diagnostic dump
#define PS2CTRL_CMD_DGNSTCDUMP 0XAC
// disable first PS/2 port
#define PS2CTRL_CMD_DISFIRSTPORT 0XAD
// able first PS/2 port
#define PS2CTRL_CMD_ENFIRSTPORT 0XAE
// read controller input port
#define PS2CTRL_CMD_RDCTRLIN 0XC0
// copy bits 0 to 3 of input port to status bits 4 to 7
#define PS2CTRL_CMD_CPINTOSTU1 0XC1
// copy bits 4 to 7 of input port to status bits 4 to 7
#define PS2CTRL_CMD_CPINTOSTU2 0XC2
// read controller outport port
#define PS2CTRL_CMD_RDCTRLOUT 0xD0
// write next byte to controller output port
#define PS2CTRL_CMD_WRTNEXTBYTE 0XD1
// write next byte to first PS/2 port output buffer
#define PS2CTRL_CMD_WRITENEXTBYTE_TO_FIRSTPOSTOUTBUFF 0XD2
// write next byte to second PS/2 port output buffer
#define PS2CTRL_CMD_WRITENEXTBYTE_TO_SECONDPORTOUTBUFF 0XD3
// write next byte to second PS/2 port input buffer
#define PS2CTRL_CMD_WRITENEXTBYTE_TO_SECONDPORTINBUFF 0XD4
// pulse output line low for 6 ms
#define PS2CTRL_CMD_ENFULLPULSELINE 0xF0
// disable 0 pulse line
#define PS2CTRL_CMD_DISPULSELINE0 PS2CTRL_CMD_ENFULLPULSELINE | 0X01
// disable 1 pulse line
#define PS2CTRL_CMD_DISPULSELINE1 PS2CTRL_CMD_ENFULLPULSELINE | 0X02
// disable 2 pulse line
#define PS2CTRL_CMD_DISPULSELINE2 PS2CTRL_CMD_ENFULLPULSELINE | 0X04
// disable 3 pulse line
#define PS2CTRL_CMD_DISPULSELINE3 PS2CTRL_CMD_ENFULLPULSELINE | 0X08
// cpu reset
#define PS2CTLR_CMD_CPURESET 0xFE

// disable device scan
#define PS2_DEVICE_CMD_DISSCAN 0xF5
// enable device scan
#define PS2_DEVICE_CMD_ENSCAN 0xF4
// get device identify
#define PS2_DEVICE_CMD_IDENTIFY 0xF2
// device respond
#define PS2_DEVICE_CMD_RETURN_ACK 0xFA

// define PS/2 configuration filed mask
// enable first ps/2 port interrupt
#define PS2CTRL_CONFIG_FIRSTPORTINT 0x1
// enable second ps/2 port interrupt
#define PS2CTRL_CONFIG_SECONDPORTINT 0X2
// system flag bits if system passed POST,set
#define PS2CTRL_CONFIG_SYSTESTOK 0X4
// zero
#define PS2CTRL_CONFIG_ZERO0 0x8
// first ps/2 port clock
#define PS2CTRL_CONFIG_FIRSTPORTCLOCK 0x10
// second ps/2 port clock
#define PS2CTRL_CONFIG_SECONDPORTCLOCK 0x20
// translation second scancode to first scancode
#define PS2CTRL_CONFIG_SCANCODETRANSLATION 0x40
// zero
#define PS2CTRL_CONFIG_ZERO1 0X80

// ps2 controller status
#define PS2CTRL_STATUS_OUTFULL 0X1
#define PS2CTRL_STATUS_INFULL 0X2
#define PS2CTRL_STATUS_SYSFLAG 0X4
#define PS2CTRL_STATUS_CMD_DATA 0X8
#define PS2CTRL_STATUS_UNKNOW0 0x10
#define PS2CTRL_STATUS_UNKNOW1 0x20
#define PS2CTRL_STATUS_TIMOUTERR 0x40
#define PS2CTRL_STATUS_PARITYERR 0X80

// ps2 device type
#define PS2_TYPE_MOUSE 0    // ps2 mouse
#define PS2_TYPE_KEYBOARD 1 // ps2 keyboard
#define PS2_TYPE_NONE 2     // ps2 unknow device

// define keyboard command
#define KEYBOARD_CMD_SETLED 0xED
#define KEYBOARD_CMD_ECHO 0xEE
#define KEYBOARD_CMD_SETSCANCODESET 0XF0
#define KEYBOARD_CMD_KEYBOARDID 0XF2
#define KEYBOARD_CMD_SETTYPEMATICRATE 0XF3
#define KEYBOARD_CMD_SETDELAY 0XF3
// data used to set led
#define KEYBOARD_DATA_SETLED_SCROLLLOCK 0X1
#define KEYBOARD_DATA_SETLED_NUMBERLOCK 0X2
#define kEYBOARD_DATA_SETLED_CAPSLOCK 0X4
// data used to set scancode set
#define KEYBOARD_DATA_SETSCANCOESET_GETSCANCONDESET 0X1
#define KEYBOARD_DATA_SETSCANCOESET_SCANCODESET1 0X2
#define KEYBOARD_DATA_SETSCANCOESET_SCANCODESET2 0X4
#define KEYBOARD_DATA_SETSCANCOESET_SCANCODESET3 0X8
// set typematic rate

// mask
#define KEYBOARD_KEY_MASK 0x1FF
#define KEYBOARD_FLAG_BREAK_MASK 0x80 // key break mask
#define KEYBOARD_FLAG_EXT 0x0100      // normal function keys

// special key
#define KEYBOARD_ESC (0x1 + KEYBOARD_FLAG_EXT)        // esc
#define KEYBOARD_TAB (0x2 + KEYBOARD_FLAG_EXT)        // tab
#define KEYBOARD_ENTER (0x03 + KEYBOARD_FLAG_EXT)     // enter
#define KEYBOARD_BACKSPACE (0x04 + KEYBOARD_FLAG_EXT) // backspace

// shift,ctrl,alt
#define KEYBOARD_SHIFT_L (0x05 + KEYBOARD_FLAG_EXT) // L shift
#define KEYBOARD_SHIFT_R (0x06 + KEYBOARD_FLAG_EXT) // R shitf
#define KEYBOARD_CTRL_L (0x07 + KEYBOARD_FLAG_EXT)  // L ctrl
#define KEYBOARD_CTRL_R (0x08 + KEYBOARD_FLAG_EXT)  // R ctrl
#define KEYBOARD_ALT_L (0x09 + KEYBOARD_FLAG_EXT)   // L alt
#define KEYBOARD_ALT_R (0x0A + KEYBOARD_FLAG_EXT)   // R alt

#define KEYBOARD_GUI_L (0x0b + KEYBOARD_FLAG_EXT) // L gui
#define KEYBOARD_GUI_R (0x0c + KEYBOARD_FLAG_EXT) // R gui
#define KEYBOARD_APPS (0x0d + KEYBOARD_FLAG_EXT)  // APPS

// lock keys
#define KEYBOARD_CAPS_LOCK (0x0e + KEYBOARD_FLAG_EXT)   // caps lock
#define KEYBOARD_NUM_LOCK (0x0f + KEYBOARD_FLAG_EXT)    // number lock
#define KEYBOARD_SCROLL_LOCK (0x10 + KEYBOARD_FLAG_EXT) // scroll lock

// function keys
#define KEYBOARD_F1 (0x11 + KEYBOARD_FLAG_EXT)  // F1
#define KEYBOARD_F2 (0x12 + KEYBOARD_FLAG_EXT)  // F2
#define KEYBOARD_F3 (0x13 + KEYBOARD_FLAG_EXT)  // F3
#define KEYBOARD_F4 (0x14 + KEYBOARD_FLAG_EXT)  // F4
#define KEYBOARD_F5 (0x15 + KEYBOARD_FLAG_EXT)  // F5
#define KEYBOARD_F6 (0x16 + KEYBOARD_FLAG_EXT)  // F6
#define KEYBOARD_F7 (0x17 + KEYBOARD_FLAG_EXT)  // F7
#define KEYBOARD_F8 (0x18 + KEYBOARD_FLAG_EXT)  // F8
#define KEYBOARD_F9 (0x19 + KEYBOARD_FLAG_EXT)  // F9
#define KEYBOARD_F10 (0x1A + KEYBOARD_FLAG_EXT) // F10
#define KEYBOARD_F11 (0x1B + KEYBOARD_FLAG_EXT) // F11
#define KEYBOARD_F12 (0x1C + KEYBOARD_FLAG_EXT) // F12

// control pad
#define KEYBOARD_PRINTSCREEN (0x1D + KEYBOARD_FLAG_EXT) // print screen
#define KEYBOARD_PAUSEBREAK (0x1E + KEYBOARD_FLAG_EXT)  // pause/break
#define KEYBOARD_INSERT (0x1F + KEYBOARD_FLAG_EXT)      // insert
#define KEYBOARD_DELETE (0x20 + KEYBOARD_FLAG_EXT)      // delete
#define KEYBOARD_HOME (0x21 + KEYBOARD_FLAG_EXT)        // home
#define KEYBOARD_END (0x22 + KEYBOARD_FLAG_EXT)         // end
#define KEYBOARD_PAGEUP (0x23 + KEYBOARD_FLAG_EXT)      // page up
#define KEYBOARD_PAGEDOWN (0x24 + KEYBOARD_FLAG_EXT)    // page down
#define KEYBOARD_UP (0x25 + KEYBOARD_FLAG_EXT)          // up
#define KEYBOARD_DOWN (0x26 + KEYBOARD_FLAG_EXT)        // down
#define KEYBOARD_LEFT (0x27 + KEYBOARD_FLAG_EXT)        // left
#define KEYBOARD_RIGHT (0x28 + KEYBOARD_FLAG_EXT)       // right

// acpi power
#define KEYBOARD_POWER (0x29 + KEYBOARD_FLAG_EXT) // Power
#define KEYBOARD_SLEEP (0x2A + KEYBOARD_FLAG_EXT) // Sleep
#define KEYBOARD_WAKE (0x2B + KEYBOARD_FLAG_EXT)  // WakeUp

// num pad
#define KEYBOARD_PAD_SLASH (0x2C + KEYBOARD_FLAG_EXT) /* / */
#define KEYBOARD_PAD_STAR (0x2D + KEYBOARD_FLAG_EXT)  /* * */
#define KEYBOARD_PAD_MINUS (0x2E + KEYBOARD_FLAG_EXT) /* - */
#define KEYBOARD_PAD_PLUS (0x2F + KEYBOARD_FLAG_EXT)  /* + */
#define KEYBOARD_PAD_ENTER (0x30 + KEYBOARD_FLAG_EXT) /* Enter */
#define KEYBOARD_PAD_DOT (0x31 + KEYBOARD_FLAG_EXT)   /* . */
#define KEYBOARD_PAD_0 (0x32 + KEYBOARD_FLAG_EXT)     /* 0 */
#define KEYBOARD_PAD_1 (0x33 + KEYBOARD_FLAG_EXT)     /* 1 */
#define KEYBOARD_PAD_2 (0x34 + KEYBOARD_FLAG_EXT)     /* 2 */
#define KEYBOARD_PAD_3 (0x35 + KEYBOARD_FLAG_EXT)     /* 3 */
#define KEYBOARD_PAD_4 (0x36 + KEYBOARD_FLAG_EXT)     /* 4 */
#define KEYBOARD_PAD_5 (0x37 + KEYBOARD_FLAG_EXT)     /* 5 */
#define KEYBOARD_PAD_6 (0x38 + KEYBOARD_FLAG_EXT)     /* 6 */
#define KEYBOARD_PAD_7 (0x39 + KEYBOARD_FLAG_EXT)     /* 7 */
#define KEYBOARD_PAD_8 (0x3A + KEYBOARD_FLAG_EXT)     /* 8 */
#define KEYBOARD_PAD_9 (0x3B + KEYBOARD_FLAG_EXT)     /* 9 */
#define KEYBOARD_PAD_UP KEYBOARD_PAD_8                /* Up */
#define KEYBOARD_PAD_DOWN KEYBOARD_PAD_2              /* Down */
#define KEYBOARD_PAD_LEFT KEYBOARD_PAD_4              /* Left */
#define KEYBOARD_PAD_RIGHT KEYBOARD_PAD_6             /* Right */
#define KEYBOARD_PAD_HOME KEYBOARD_PAD_7              /* Home */
#define KEYBOARD_PAD_END KEYBOARD_PAD_1               /* End */
#define KEYBOARD_PAD_PAGEUP KEYBOARD_PAD_9            /* Page Up */
#define KEYBOARD_PAD_PAGEDOWN KEYBOARD_PAD_3          /*Page Down */
#define KEYBOARD_PAD_INS KEYBOARD_PAD_0               /* Ins */
#define KEYBOARD_PAD_MID KEYBOARD_PAD_5               /* Middle key */
#define KEYBOARD_PAD_DELETE KEYBOARD_PAD_DOT          /* Delete */

// multimedia
#define KEYBOARD_NEXT_TRACK (0x2F + KEYBOARD_FLAG_EXT)     /* Next Track */
#define KEYBOARD_PREVIOUS_TRACK (0x30 + KEYBOARD_FLAG_EXT) /* Previous Track */
#define KEYBOARD_MUTE (0x31 + KEYBOARD_FLAG_EXT)           /* Mute */
#define KEYBOARD_PLAYPAUSE (0x32 + KEYBOARD_FLAG_EXT)      /* Play Pause */
#define KEYBOARD_STOP (0x33 + KEYBOARD_FLAG_EXT)           /* Stop */

// ps2 controller config
#define PS2_CONFIG (PS2CTRL_CONFIG_SYSTESTOK | PS2CTRL_CONFIG_FIRSTPORTINT | PS2CTRL_CONFIG_SECONDPORTINT | PS2CTRL_CONFIG_SCANCODETRANSLATION)

// keyboard return code
enum keyboard_return_code
{
    KEYBOARD_RET_ACK = 0xFA,
};

enum keyboard_port
{
    KEYBOARD_WRITE_DATA_PORT = 0x60,
    KEYBOARD_READ_DATA_PORT = 0x60,
    PS2CTRL_CMD_PORT = 0x64,
    PS2CTRL_STATUS_PORT = 0x64,
};

static const uint16_t map_table[] =
    {
        KEYBOARD_PAUSEBREAK, KEY_PAUSE,
        KEYBOARD_UP, KEY_UP,
        KEYBOARD_DOWN, KEY_DOWN,
        KEYBOARD_LEFT, KEY_LEFT,
        KEYBOARD_RIGHT, KEY_RIGHT,
        KEYBOARD_BACKSPACE, KEY_BACKSPACE,
        KEYBOARD_TAB, KEY_TAB,
        KEYBOARD_INSERT, KEY_INSERT,
        KEYBOARD_HOME, KEY_HOME,
        KEYBOARD_END, KEY_END,
        KEYBOARD_ENTER, KEY_ENTER,
        KEYBOARD_PAGEUP, KEY_PAGEUP,
        KEYBOARD_PAGEDOWN, KEY_PAGEDOWN,
        KEYBOARD_F1, KEY_F1,
        KEYBOARD_F2, KEY_F2,
        KEYBOARD_F3, KEY_F3,
        KEYBOARD_F4, KEY_F4,
        KEYBOARD_F5, KEY_F5,
        KEYBOARD_F6, KEY_F6,
        KEYBOARD_F7, KEY_F7,
        KEYBOARD_F8, KEY_F8,
        KEYBOARD_F9, KEY_F9,
        KEYBOARD_F10, KEY_F10,
        KEYBOARD_F11, KEY_F11,
        KEYBOARD_ESC, KEY_ESCAPE,
        KEYBOARD_F12, KEY_F12,
        ' ', KEY_SPACE,
        '!', KEY_EXCLAIM,
        '"', KEY_QUOTEDBL,
        '#', KEY_HASH,
        '$', KEY_DOLLAR,
        '%', KEY_PERSENT,
        '&', KEY_AMPERSAND,
        '\'', KEY_QUOTE,
        '(', KEY_LEFTPAREN,
        ')', KEY_RIGHTPAREN,
        '*', KEY_ASTERISK,
        '+', KEY_PLUS,
        ',', KEY_COMMA,
        '-', KEY_MINUS,
        '.', KEY_PERIOD,
        '/', KEY_SLASH,
        '0', KEY_0,
        '1', KEY_1,
        '2', KEY_2,
        '3', KEY_3,
        '4', KEY_4,
        '5', KEY_5,
        '6', KEY_6,
        '7', KEY_7,
        '8', KEY_8,
        '9', KEY_9,
        ':', KEY_COLON,
        ';', KEY_SEMICOLON,
        '<', KEY_LESS,
        '=', KEY_EQUALS,
        '>', KEY_GREATER,
        '?', KEY_QUESTION,
        '@', KEY_AT,
        'A', KEY_A,
        'B', KEY_B,
        'C', KEY_C,
        'D', KEY_D,
        'E', KEY_E,
        'F', KEY_F,
        'G', KEY_G,
        'H', KEY_H,
        'I', KEY_I,
        'J', KEY_J,
        'K', KEY_K,
        'L', KEY_L,
        'M', KEY_M,
        'N', KEY_N,
        'O', KEY_O,
        'P', KEY_P,
        'Q', KEY_Q,
        'R', KEY_R,
        'S', KEY_S,
        'T', KEY_T,
        'U', KEY_U,
        'V', KEY_V,
        'W', KEY_W,
        'X', KEY_X,
        'Y', KEY_Y,
        'Z', KEY_Z,
        '[', KEY_LEFTSQUAREBRACKET,
        '\\', KEY_BACKSLASH,
        ']', KEY_RIGHTSQUAREBRACKET,
        '^', KEY_CARET,
        '_', KEY_UNDERSCRE,
        '`', KEY_BACKQUOTE,
        'a', KEY_a,
        'b', KEY_b,
        'c', KEY_c,
        'd', KEY_d,
        'e', KEY_e,
        'f', KEY_f,
        'g', KEY_g,
        'h', KEY_h,
        'i', KEY_i,
        'j', KEY_j,
        'k', KEY_k,
        'l', KEY_l,
        'm', KEY_m,
        'n', KEY_n,
        'o', KEY_o,
        'p', KEY_p,
        'q', KEY_q,
        'r', KEY_r,
        's', KEY_s,
        't', KEY_t,
        'u', KEY_u,
        'v', KEY_v,
        'w', KEY_w,
        'x', KEY_x,
        'y', KEY_y,
        'z', KEY_z,
        '{', KEY_LEFTBRACKET,
        '|', KEY_VERTICAL,
        '}', KEY_RIGHTBRACKET,
        '~', KEY_TILDE,
        KEYBOARD_DELETE, KEY_DELETE,
        KEYBOARD_PAD_0, KEY_KP0,
        KEYBOARD_PAD_1, KEY_KP1,
        KEYBOARD_PAD_2, KEY_KP2,
        KEYBOARD_PAD_3, KEY_KP3,
        KEYBOARD_PAD_4, KEY_KP4,
        KEYBOARD_PAD_5, KEY_KP5,
        KEYBOARD_PAD_6, KEY_KP6,
        KEYBOARD_PAD_7, KEY_KP7,
        KEYBOARD_PAD_8, KEY_KP8,
        KEYBOARD_PAD_9, KEY_KP9,
        KEYBOARD_PAD_DOT, KEY_KP_PERIOD,
        KEYBOARD_PAD_SLASH, KEY_KP_DIVIDE,
        KEYBOARD_PAD_STAR, KEY_KP_MULTIPLY,
        KEYBOARD_PAD_MINUS, KEY_KP_MINUS,
        KEYBOARD_PAD_PLUS, KEY_KP_PLUS,
        KEYBOARD_PAD_ENTER, KEY_KP_ENTER,
        KEYBOARD_PAD_ENTER, KEY_KP_EQUALS,
        KEYBOARD_NUM_LOCK, KEY_NUMLOCK,
        KEYBOARD_CAPS_LOCK, KEY_CAPSLOCK,
        KEYBOARD_SCROLL_LOCK, KEY_SCROLLOCK,
        KEYBOARD_SHIFT_R, KEY_RSHIFT,
        KEYBOARD_SHIFT_L, KEY_LSHIFT,
        KEYBOARD_CTRL_R, KEY_RCTRL,
        KEYBOARD_CTRL_L, KEY_LCTRL,
        KEYBOARD_ALT_R, KEY_RALT,
        KEYBOARD_ALT_L, KEY_LALT,
        KEYBOARD_PRINTSCREEN, KEY_PRINT,
        KEYBOARD_PAUSEBREAK, KEY_BREAK,
        KEYBOARD_MUTE, KEY_MUTE,
        KEYBOARD_STOP, KEY_STOP,
        KEYBOARD_WAKE, KEY_WAKE,
        KEYBOARD_POWER, KEY_POWER,
        KEYBOARD_NEXT_TRACK, KEY_NEXTTRACK,
        KEYBOARD_PREVIOUS_TRACK, KEY_PROVIOUSTRACK,
        KEYBOARD_SLEEP, KEY_SLEEP,
        KEYBOARD_PLAYPAUSE, KEY_PLAYPAUSE};

// number of scan code
#define MAX_SCAN_CODE_NUM 0x80
// number of columes in keymap
#define KEYMAP_COLS 3

/* Keymap for US MF-2 ext-> */
static uint32_t kbd_keymap[MAX_SCAN_CODE_NUM * KEYMAP_COLS] = {

    /* scan-code			!Shift		Shift		E0 XX	*/
    /* ==================================================================== */
    /* 0x00 - none		*/ 0, 0, 0,
    /* 0x01 - ESC		*/ KEYBOARD_ESC, KEYBOARD_ESC, 0,
    /* 0x02 - '1'		*/ '1', '!', 0,
    /* 0x03 - '2'		*/ '2', '@', 0,
    /* 0x04 - '3'		*/ '3', '#', 0,
    /* 0x05 - '4'		*/ '4', '$', 0,
    /* 0x06 - '5'		*/ '5', '%', 0,
    /* 0x07 - '6'		*/ '6', '^', 0,
    /* 0x08 - '7'		*/ '7', '&', 0,
    /* 0x09 - '8'		*/ '8', '*', 0,
    /* 0x0A - '9'		*/ '9', '(', 0,
    /* 0x0B - '0'		*/ '0', ')', 0,
    /* 0x0C - '-'		*/ '-', '_', 0,
    /* 0x0D - '='		*/ '=', '+', 0,
    /* 0x0E - BS		*/ KEYBOARD_BACKSPACE, KEYBOARD_BACKSPACE, 0,
    /* 0x0F - TAB		*/ KEYBOARD_TAB, KEYBOARD_TAB, 0,
    /* 0x10 - 'q'		*/ 'q', 'Q', 0,
    /* 0x11 - 'w'		*/ 'w', 'W', 0,
    /* 0x12 - 'e'		*/ 'e', 'E', 0,
    /* 0x13 - 'r'		*/ 'r', 'R', 0,
    /* 0x14 - 't'		*/ 't', 'T', 0,
    /* 0x15 - 'y'		*/ 'y', 'Y', 0,
    /* 0x16 - 'u'		*/ 'u', 'U', 0,
    /* 0x17 - 'i'		*/ 'i', 'I', 0,
    /* 0x18 - 'o'		*/ 'o', 'O', 0,
    /* 0x19 - 'p'		*/ 'p', 'P', KEYBOARD_NEXT_TRACK,
    /* 0x1A - '['		*/ '[', '{', KEYBOARD_PREVIOUS_TRACK,
    /* 0x1B - ']'		*/ ']', '}', 0,
    /* 0x1C - CR/LF		*/ KEYBOARD_ENTER, KEYBOARD_ENTER, 0,
    /* 0x1D - l. Ctrl	*/ KEYBOARD_CTRL_L, KEYBOARD_CTRL_L, KEYBOARD_CTRL_R,
    /* 0x1E - 'a'		*/ 'a', 'A', 0,
    /* 0x1F - 's'		*/ 's', 'S', 0,
    /* 0x20 - 'd'		*/ 'd', 'D', KEYBOARD_MUTE,
    /* 0x21 - 'f'		*/ 'f', 'F', 0,
    /* 0x22 - 'g'		*/ 'g', 'G', KEYBOARD_PLAYPAUSE,
    /* 0x23 - 'h'		*/ 'h', 'H', 0,
    /* 0x24 - 'j'		*/ 'j', 'J', KEYBOARD_STOP,
    /* 0x25 - 'k'		*/ 'k', 'K', 0,
    /* 0x26 - 'l'		*/ 'l', 'L', 0,
    /* 0x27 - ';'		*/ ';', ':', 0,
    /* 0x28 - '\''		*/ '\'', '"', 0,
    /* 0x29 - '`'		*/ '`', '~', 0,
    /* 0x2A - l. SHIFT	*/ KEYBOARD_SHIFT_L, KEYBOARD_SHIFT_L, 0,
    /* 0x2B - '\'		*/ '\\', '|', 0,
    /* 0x2C - 'z'		*/ 'z', 'Z', 0,
    /* 0x2D - 'x'		*/ 'x', 'X', 0,
    /* 0x2E - 'c'		*/ 'c', 'C', 0,
    /* 0x2F - 'v'		*/ 'v', 'V', 0,
    /* 0x30 - 'b'		*/ 'b', 'B', 0,
    /* 0x31 - 'n'		*/ 'n', 'N', 0,
    /* 0x32 - 'm'		*/ 'm', 'M', 0,
    /* 0x33 - ','		*/ ',', '<', 0,
    /* 0x34 - '.'		*/ '.', '>', 0,
    /* 0x35 - '/'		*/ '/', '?', KEYBOARD_PAD_SLASH,
    /* 0x36 - r. SHIFT	*/ KEYBOARD_SHIFT_R, KEYBOARD_SHIFT_R, 0,
    /* 0x37 - '*'		*/ KEYBOARD_PAD_STAR, '*', 0,
    /* 0x38 - ALT		*/ KEYBOARD_ALT_L, KEYBOARD_ALT_L, KEYBOARD_ALT_R,
    /* 0x39 - ' '		*/ ' ', ' ', 0,
    /* 0x3A - caps_lock	*/ KEYBOARD_CAPS_LOCK, KEYBOARD_CAPS_LOCK, 0,
    /* 0x3B - F1		*/ KEYBOARD_F1, KEYBOARD_F1, 0,
    /* 0x3C - F2		*/ KEYBOARD_F2, KEYBOARD_F2, 0,
    /* 0x3D - F3		*/ KEYBOARD_F3, KEYBOARD_F3, 0,
    /* 0x3E - F4		*/ KEYBOARD_F4, KEYBOARD_F4, 0,
    /* 0x3F - F5		*/ KEYBOARD_F5, KEYBOARD_F5, 0,
    /* 0x40 - F6		*/ KEYBOARD_F6, KEYBOARD_F6, 0,
    /* 0x41 - F7		*/ KEYBOARD_F7, KEYBOARD_F7, 0,
    /* 0x42 - F8		*/ KEYBOARD_F8, KEYBOARD_F8, 0,
    /* 0x43 - F9		*/ KEYBOARD_F9, KEYBOARD_F9, 0,
    /* 0x44 - F10		*/ KEYBOARD_F10, KEYBOARD_F10, 0,
    /* 0x45 - num_lock	*/ KEYBOARD_NUM_LOCK, KEYBOARD_NUM_LOCK, 0,
    /* 0x46 - ScrLock	*/ KEYBOARD_SCROLL_LOCK, KEYBOARD_SCROLL_LOCK, 0,
    /* 0x47 - Home		*/ KEYBOARD_PAD_HOME, '7', KEYBOARD_HOME,
    /* 0x48 - CurUp		*/ KEYBOARD_PAD_UP, '8', KEYBOARD_UP,
    /* 0x49 - PgUp		*/ KEYBOARD_PAD_PAGEUP, '9', KEYBOARD_PAGEUP,
    /* 0x4A - '-'		*/ KEYBOARD_PAD_MINUS, '-', 0,
    /* 0x4B - Left		*/ KEYBOARD_PAD_LEFT, '4', KEYBOARD_LEFT,
    /* 0x4C - MID		*/ KEYBOARD_PAD_MID, '5', 0,
    /* 0x4D - Right		*/ KEYBOARD_PAD_RIGHT, '6', KEYBOARD_RIGHT,
    /* 0x4E - '+'		*/ KEYBOARD_PAD_PLUS, '+', 0,
    /* 0x4F - End		*/ KEYBOARD_PAD_END, '1', KEYBOARD_END,
    /* 0x50 - Down		*/ KEYBOARD_PAD_DOWN, '2', KEYBOARD_DOWN,
    /* 0x51 - PgDown	*/ KEYBOARD_PAD_PAGEDOWN, '3', KEYBOARD_PAGEDOWN,
    /* 0x52 - Insert	*/ KEYBOARD_PAD_INS, '0', KEYBOARD_INSERT,
    /* 0x53 - Delete	*/ KEYBOARD_PAD_DOT, '.', KEYBOARD_DELETE,
    /* 0x54 - Enter		*/ KEYBOARD_PAD_ENTER, KEYBOARD_ENTER, KEYBOARD_ENTER,
    /* 0x55 - ???		*/ 0, 0, 0,
    /* 0x56 - ???		*/ 0, 0, 0,
    /* 0x57 - F11		*/ KEYBOARD_F11, KEYBOARD_F11, 0,
    /* 0x58 - F12		*/ KEYBOARD_F12, KEYBOARD_F12, 0,
    /* 0x59 - ???		*/ 0, 0, 0,
    /* 0x5A - ???		*/ 0, 0, 0,
    /* 0x5B - ???		*/ 0, 0, KEYBOARD_GUI_L,
    /* 0x5C - ???		*/ 0, 0, KEYBOARD_GUI_R,
    /* 0x5D - ???		*/ 0, 0, KEYBOARD_APPS,
    /* 0x5E - ???		*/ 0, 0, KEYBOARD_POWER,
    /* 0x5F - ???		*/ 0, 0, KEYBOARD_SLEEP,
    /* 0x60 - ???		*/ 0, 0, 0,
    /* 0x61 - ???		*/ 0, 0, 0,
    /* 0x62 - ???		*/ 0, 0, 0,
    /* 0x63 - ???		*/ 0, 0, KEYBOARD_WAKE,
    /* 0x64 - ???		*/ 0, 0, 0,
    /* 0x65 - ???		*/ 0, 0, 0,
    /* 0x66 - ???		*/ 0, 0, 0,
    /* 0x67 - ???		*/ 0, 0, 0,
    /* 0x68 - ???		*/ 0, 0, 0,
    /* 0x69 - ???		*/ 0, 0, 0,
    /* 0x6A - ???		*/ 0, 0, 0,
    /* 0x6B - ???		*/ 0, 0, 0,
    /* 0x6C - ???		*/ 0, 0, 0,
    /* 0x6D - ???		*/ 0, 0, 0,
    /* 0x6E - ???		*/ 0, 0, 0,
    /* 0x6F - ???		*/ 0, 0, 0,
    /* 0x70 - ???		*/ 0, 0, 0,
    /* 0x71 - ???		*/ 0, 0, 0,
    /* 0x72 - ???		*/ 0, 0, 0,
    /* 0x73 - ???		*/ 0, 0, 0,
    /* 0x74 - ???		*/ 0, 0, 0,
    /* 0x75 - ???		*/ 0, 0, 0,
    /* 0x76 - ???		*/ 0, 0, 0,
    /* 0x77 - ???		*/ 0, 0, 0,
    /* 0x78 - ???		*/ 0, 0, 0,
    /* 0x78 - ???		*/ 0, 0, 0,
    /* 0x7A - ???		*/ 0, 0, 0,
    /* 0x7B - ???		*/ 0, 0, 0,
    /* 0x7C - ???		*/ 0, 0, 0,
    /* 0x7D - ???		*/ 0, 0, 0,
    /* 0x7E - ???		*/ 0, 0, 0,
    /* 0x7F - ???		*/ 0, 0, 0};

#define DEVICE_BUFFER_SIZE 64

enum keyboard_flag
{
    KEYBOARD_FLAG_KEY_MASK = 0x1FF, /* 键值的mask值 */
    KEYBOARD_FLAG_SHIFT_L = 0x0200, /* Shift key			*/
    KEYBOARD_FLAG_SHIFT_R = 0x0400, /* Shift key			*/
    KEYBOARD_FLAG_CTRL_L = 0x0800,  /* Control key			*/
    KEYBOARD_FLAG_CTRL_R = 0x1000,  /* Control key			*/
    KEYBOARD_FLAG_ALT_L = 0x2000,   /* Alternate key		*/
    KEYBOARD_FLAG_ALT_R = 0x4000,   /* Alternate key		*/
    KEYBOARD_FLAG_PAD = 0x8000,     /* keys in num pad		*/
    KEYBOARD_FLAG_NUM = 0x10000,    /* number lock		*/
    KEYBOARD_FLAG_CAPS = 0x20000,   /* capslock		*/
    KEYBOARD_FLAG_BREAK = 0x40000,  /* Break Code   */
};

typedef struct
{
    device_object_t *devobj;
    device_handle_t keyboard;
    int flags;
    int open;
    uint8_t r_shift;
    uint8_t l_shift;
    uint8_t r_ctrl;
    uint8_t l_ctrl;
    uint8_t r_alt;
    uint8_t l_alt;
    uint8_t code_with_e0;
    uint8_t colume;
    uint8_t capslock;
    uint8_t numlock;
    uint8_t scrolllock;
    uint8_t led;  // led
    uint8_t irq;  // hardware irq
    uint8_t id;   // ps2 port
    uint8_t type; // ps2 device type
    fifo_io_t fifoio;
    input_event_buff_t eventbuff;
    uint8_t exist; // device exist
} device_extension_t;

//#define DEBUG_KEYBOARD

#endif