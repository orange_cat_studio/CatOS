// file:lib\stddef.h
// autor:jiang xinpeng
// time:2020.12.29
// copyright:(C) 2020-2050 by jiang xinpeng,All right are reserved.

#ifndef STD_STDDEF_H
#define STD_STDDEF_H

#include "type.h"
#include "const.h"
#include "math.h"
#include <arch/arch.h>

#define NULL ((void *)(address_t)0)

// get member offset in assume type struct

#define offsetof(TYPE, MEMBER) ((uintptr_t)(&((TYPE *)0)->MEMBER))

// according member point,struct type,member name,we get to owner point
#define container_of(ptr, type, member) (                 \
    {                                                     \
        const typeof(((type *)0)->member) *_mptr = (ptr); \
        (type *)((char *)_mptr - offsetof(type, member)); \
    })

#define IN_RANGE(x, a, b) (((x) > (a)) && ((x) < (b)))
#define OUT_RANGE(x, a, b) (((x) < (a)) || ((x) > (b)))

#ifndef PTYPE
#if __ARCH == X86
#define PTYPE(p) ((void *)(address_t)(p))
#define ADDR(p) ((address_t)(p))
#else
#define PTYPE(p) ((void *)(address_t)(p))
#define ADDR(p) ((address_t)(p))
#endif
#endif

#define likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)

#ifndef _BOOL
#define true 1
#define false 0
#endif

static inline uint64_t do_div64(uint64_t num1, uint32_t num2)
{
    uint64_t res = num1;
    do_div(&res, num2);
    return res;
}

#define SECTOR SECTOR_SIZE
#endif