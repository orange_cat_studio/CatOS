#ifndef LIB_LIMIT_H
#define LIB_LIMIT_H

#define CHAR_BIT 8
#define SCHAR_MIN (-SCHAR_MAX - 1)
#define SCHAR_MAX __SCHAR_MAX__
#define UCHAR_MAX (SCHAR_MAX * 2 + 1)

#ifndef CHAR_UNSIGNED
#define CHAR_MIN SCHAR_MIN
#define CHAR_MAX SCHAR_MAX
#else
#define CHAR_MIN 0
#define CHAR_MAX UCHAR_MAX
#endif

#define MB_LEN_MAX 5

#define SHRT_MIN (-SHRT_MAX - 1)
#define SHRT_MAX __SHRT_MAX__
#define USHRT_MAX (SHRT_MAX * 2 + 1)

#define INT_MIN (-INT - 1)
#define INT_MAX __INT_MAX__
#define UINT_MAX (INT_MAX * 2 + 1)

#define LONG_MIN (-LONG_MAX - 1)
#define LONG_MAX __LONG_MAX__
#define ULONG_MAX (LONG_MAX * 2 + 1)

#define LLONG_MIN (-LLONG_MAX - 1)
#define LLONG_MAX __LONG_LONG_MAX__
#define ULLONG_MAX (LLONG_MAX * 2 + 1)

#endif