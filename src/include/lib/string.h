//file:lib\string.h
//autor:jiang xinpeng
//time:2020.12.21
//copyright:(C) 2020-2050 by jiang xinpeng,All right are reserved.

#ifndef LIB_STRING_H
#define LIB_STRING_H

#include "type.h"

typedef struct _string
{
    uint32_t len;
    uint32_t max_len;
    char *text;
} string_t;

#define STRING_LEN_MAX 128

int strcmp(const char *src, const char *targe);
char *strcat(char *targe, const char *src);
char *strstr(const char *src, const char *match);
char *strcpy(char *targe, const char *src);
const char *strchr(const char *targe, int src);
const char *strrchr(const char *str, int c);
int strlen(const char *src);
int strncmp(const char *s1, const char *s2, int n);
char *strncat(char *dst, const char *src, int n);
char *strncpy(char *_dst, const char *_src, int n);

void string_init(string_t *string);
int string_new(string_t *string, const char *text, uint32_t maxlen);
void string_del(string_t *string);
void string_empty(string_t *string);
void string_copy(string_t *string, char *text);

void *memset(void *src, uint8_t value, uint32_t size);
void *memcpy(void *targe, const void *src, uint32_t size);
int memcmp(const void *src, const void *targe, uint32_t len);
#endif