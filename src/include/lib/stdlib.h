#ifndef LIB_STDLIB_H
#define LIB_STDLIB_H

#include "stddef.h"

// rand max
#define RAND_MAX 0x7fff
#define MB_CUR_MAX 1

#define EXIT_FAILURE 1 // failed exit status
#define EXIT_SUCCESS 0 // success exit status

void srand(uint64_t seed);
int rand();

void srandom(uint64_t seed);
int random();

char *itoa(int num, int base, char *str, int prefix);
int atoi(const char *str);

void abort();
#endif