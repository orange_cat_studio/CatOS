//file:include\lib\ctype.h
//autor:jiang xinpeng
//time:2020.12.8
//copyright:(C) 2020-2050 by Jiang xinpeng,All right are reserved

#ifndef LIB_CTYPE_H
#define LIB_CTYPE_H

int isspace(char ch);
int isalnum(char ch);
int isalpha(char ch);
int isdigit(char ch);
int isxdight(char ch);
int issuper(char ch);
int isspace(char ch);
#endif