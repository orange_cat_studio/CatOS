#ifndef LIB_ASSERT_H
#define LIB_ASSERT_H

//config asserto
#define CONFIG_ASSERT 1

#if CONFIG_ASSERT
#define assert(exp) ((exp) ? (void)0 : assertion_failure(#exp, __FILE__, __BASE_FILE__, __LINE__))

#endif

void assertion_failure(char *exp, char *file, char *basefile, int line);
#endif