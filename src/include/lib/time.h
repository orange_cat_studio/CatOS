#ifndef TIME
    #define TIME

    #include<lib/type.h>

    typedef struct tm{
            int tm_sec;     //second
            int tm_min;     //minute
            int tm_hour;    //hour
            int tm_mday;    //days from this month
            int tm_mon;     //month
            int tm_year;    //year
            int tm_wday;    //days from sunday
            int tm_yday;    //days from this year start(1.1)
            int tm_isdast;  
    }tm_t;

    double difftime(time_t time_end,time_t time_beg);       //different between end and begin time
    time_t time(time_t *arg);                               //return current time
    clock_t clock(void);                                    //return cpu time
#endif