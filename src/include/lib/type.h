// file: lib/type.h
// autor: jiang xinpeng
// time:2021.1.17
// update:2021.10.7
// copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef LIB_TYPE_H
#define LIB_TYPE_H

#include <arch/arch.h>
#include <lib/stdint.h>
#include <arch/const.h>

// os data type define
typedef void (*task_func_t)(void *); // task func
typedef uint64_t gate_des_t;         // gate descript
typedef int32_t pid_t;
typedef uint32_t dma_addr_t; // dma address
typedef struct task task_t;
typedef uint64_t clock_t;
typedef uint32_t address_t;
typedef int8_t cpuid_t;
typedef uint32_t flags_t;
typedef uint32_t time_t;
typedef uint32_t irqno_t;
typedef int32_t offset_t;
typedef void *buffer_t;
typedef uint64_t clock_type_t;
typedef uint32_t size_t;
typedef uint64_t sector_t;
typedef uint32_t mode_t;
typedef uint64_t inode_t;
typedef uint64_t nlink_t;
typedef uint64_t dev_t;
typedef uint64_t uid_t;
typedef uint64_t gid_t;
typedef uint64_t blkcnt_t;
typedef uint64_t blksize_t;
typedef uint32_t lba_t;
typedef uint64_t kobjid_t; // kernel object id
typedef uint64_t pthread_t;
typedef struct fsal fsal_t; // previous declare fsal
typedef struct task task_t; // previous declare task

// os specific type define
typedef char CHAR;
typedef uint16_t WORD;
typedef uint32_t DWORD;
typedef uint8_t BYTE;
typedef uint16_t WCHAR;
typedef CHAR TCHAR;

typedef uint8_t u8_t;
typedef uint16_t u16_t;
typedef uint32_t u32_t;
typedef uint64_t u64_t;

#if WORDSZ == 2
typedef int32_t intptr_t;
typedef uint32_t uintptr_t;
#else
typedef int64_t intptr_t;
typedef uint64_t uintptr_t;
#endif

#define PRIVATE static
#define PUBLIC extern

#endif