//file:src/include/lib/unistd.h
//autor:jiangxinpeng
//time:2021.2.28
//copyright:(C) by jiangxinpeng,All right are reserved.

#ifndef LIB_UNISTD_H
    #define LIB_UNISTD_H

    #include<lib/type.h>

    //seek
    #define SEEK_SET  0
    #define SEEK_CUR  1
    #define SEEK_END  2

    //file access
    #define R_OK      0
    #define W_OK      1
    #define X_OK      2
    #define F_OK      3

#endif