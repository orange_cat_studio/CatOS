//file:include/lib/const.inc
//autor:jiangxinpeng
//time:2020.1.2
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef LIB_CONST_H
    #define LIB_CONST_H

    #include<arch/const.h>
    
    #define SECTOR_SIZE 512
    #define BLOCK_SIZE  1024
    #define ATAPI_SECTOR_SIZE 2048
    
#endif