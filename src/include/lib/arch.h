//file:include/lib/arch.h
//autor:jiang xinpeng
//time:2020.12.7
//uptime: 2021.10.29
//copyright:(C) 2020-2050 by Jiang xinpeng,All right are reserved

#ifndef _LIB_ARCH_H
#define _LIB_ARCH_H

//include arch all header
#include <arch/arch.h>

#if __ARCH == X86
//include x86 arch header 
#include <arch/ards.h>
#include <arch/pymem.h>
#include <arch/page.h>
#include <arch/x86.h>
#include <arch/segment.h>
#include <arch/address.h>
#include <arch/config.h>
#include<arch/const.h>
#include <arch/console.h>
#include <arch/gate.h>
#include <arch/eflags.h>
#include <arch/interrupt.h>
#include <arch/io.h>
#include <arch/memory.h>
#include <arch/mem_pool.h>
#include <arch/memio.h>
#include <arch/cpu.h>
#include <arch/debug.h>
#include <arch/descript.h>
#include <arch/nmi.h>
#include <arch/cmos.h>
#include <arch/tss.h>
#else 
//include other arch header 
#include <arch/ards.h>
#include <arch/pymem.h>
#include <arch/page.h>
#include <arch/x86.h>
#include <arch/segment.h>
#include <arch/address.h>
#include <arch/config.h>
#include <arch/console.h>
#include <arch/gate.h>
#include <arch/eflags.h>
#include <arch/interrupt.h>
#include <arch/io.h>
#include <arch/memory.h>
#include <arch/mem_pool.h>
#include <arch/memio.h>
#include <arch/cpu.h>
#include <arch/debug.h>
#include <arch/descript.h>
#include <arch/nmi.h>
#include <arch/cmos.h>
#include <arch/tss.h>
#endif
#endif