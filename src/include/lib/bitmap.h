#ifndef LIB_BITMAP_H
    #define LIB_BITMAP_H
    
    #include "type.h"

    #define BITS_PER_BYTE 8

    //define bitmap struct
    typedef struct bitmap
    {
        uint64_t length;
        uint8_t *bits;
    }bitmap_t;

    
    void init_bitmap(bitmap_t *bitmap);
    bool bitmap_test(bitmap_t *bitmap,uint32_t index);
    void bitmap_set(bitmap_t *bitmap,uint8_t value,uint32_t index);
    void bitmap_traversal(bitmap_t *bitmap,uint32_t len,uint32_t pos);
    bool bitmap_find(bitmap_t *bitmap,uint32_t index);
    uint32_t bitmap_used(bitmap_t *bitmap);
    uint32_t bitmap_unused(bitmap_t *bitmap);
    int bitmap_find_free(bitmap_t *bitmap);
    int bitmap_find_nfree(bitmap_t *bitmap, uint64_t len);
    uint8_t bitmap_findbyte(bitmap_t *bitmap, uint32_t index);

#endif