#ifndef LIB_STDIO_H
#define LIB_STDIO_H

#include "stdarg.h"

int vsprintf(char *buff, const char *fmt, va_list arg);
int vsnprintf(char *buff, int bufflen, const char *fmt, va_list arg);

int snprintf(char *buff, int bufflen, const char *fmt, ...);
int sprintf(char *buff, const char *fmt, ...);

#endif