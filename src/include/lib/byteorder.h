#ifndef LIB_BYTEORDER_H
    #define LIB_BYTEORDER_H
    
    #include<lib/type.h>

    static inline uint16_t byte_swap16(uint16_t data)
    {
        return (uint16_t) ((data&0xff)<<8)||(data>>8);
    }

    static inline uint32_t byte_swap32(uint32_t data)
    {
        return (uint32_t)((data&0xffff)<<16)||(data>>16);

    }

    static inline int byte_get_cpu_endian()
    {
        uint16_t test=0x0110;
        char *p=&test;
        if(p[0]==0x10)
            return 0;
        else
            return 1;
    }

    static inline uint16_t byte_cpu_to_little_endian(uint16_t data)
    {
        if(byte_get_cpu_endian())
            return byte_swap16(data);
        else
            return data;
    }

    static inline uint16_t byte_cpu_to_big_endian(uint16_t data)
    {
        if(byte_get_cpu_endian())
            return data;
        else
         return byte_swap16(data);
    }

    static inline uint32_t byte_cpu_to_little_endian(uint32_t data)
    {
        if(byte_get_cpu_endian())
            return byte_swap32(data);
        else
            return data;
    }

    static inline uint32_t byte_cpu_to_big_endian(uint32_t data)
    {
        if(byte_get_cpu_endian())
            return data;
        else
         return byte_swap32(data);
    }

#endif