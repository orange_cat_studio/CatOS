// file:include/lib/stdarg.h
// autor:jiangxinpeng
// time:2021.3.12
// copyright:(C) by jiangxinpeng,All right are reserved.
#ifndef LIB_STDARG_H
#define LIB_STDARG_H

#include <lib/string.h>

typedef char *va_list;

#define va_start(arg, format) (void)(arg = (void *)((&(format)) + sizeof((format))))
#define va_arg(arg, type) (*(type *)(((arg) += sizeof(type)) - sizeof(type)))
#define va_end(arg) (arg = (void *)0)

#endif