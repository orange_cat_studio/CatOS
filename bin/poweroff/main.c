#include <sys/syscall.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define POWER_OFF 0

int main(int argc, char **argv)
{
    printf("ready to power off!\n");
    return syscall0(int, SYS_POWEROFF);
}