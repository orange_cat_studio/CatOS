#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/sys.h>

#define CPU_LEN 64
#define CPU_DEV "/dev/cpu0"

int main(int argc, char **argv)
{
    int fd = open(CPU_DEV, O_RDONLY);
    if (fd < 0)
    {
        fprintf(stderr, "cpuinfo: open cpu device failed!\n");
        return -1;
    }
    char buff[CPU_LEN];
    if (read(fd, buff, CPU_LEN) < 0)
    {
        fprintf(stderr, "cpuinfo: read cpu device failed!\n");
        return -1;
    }
    printf("cpuinfo:\n");
    printf("%s\n", buff);
    return 0;
}