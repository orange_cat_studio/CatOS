#include <sys/proc.h>
#include <sys/status.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static void print_help()
{
    printf("proc: show process info\n");
    printf("option:\n");
    printf("    -a: show system process\n");
    printf("    -h: show help\n");
}

int main(int argc, char **argv)
{
    tstatus_t ts;
    int all=0;
    int num = 0;

    printf("task status list\n");

    if (argc > 1)
    {
        char *p = argv[1];
        if (*p == '-')
        {
            p++;
            switch (*p)
            {
            case 'a':
                all = 1;
                break;
            case 'h':
                print_help();
                return 0;
            default:
                fprintf(stderr, "proc: unknow argument!\n");
                return -1;
            }
        }
        else
        {
            fprintf(stderr, "proc: unknow argument!\n");
            return -1;
        }
    }

    printf("| UID | GID | PID | PPID | PGID | STAT | PRO | TICKS |   NAME  |\n");
    while (!tstatus(&ts, &num))
    {
        if (!all) // only show user process
            if (ts.ts_ppid == -1)
                continue;
        printf("%5d %5d %5d %5d %5d %5d %5d %5d %10s\n", (uint32_t)ts.ts_uid, (uint32_t)ts.ts_gid, (uint32_t)ts.ts_pid, (uint32_t)ts.ts_ppid, (uint32_t)ts.ts_pgid, (uint32_t)ts.ts_status, (uint32_t)ts.ts_priority, (uint32_t)ts.ts_runticks, ts.ts_name);
    }

    return 0;
}