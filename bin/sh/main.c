// file: userapp/sh/main.c
// autor: jiangxinpeng
// time: 2021.12.28
// copyright: (C) 2020-2050 by jiangxinpeng,All right are reserved.

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <ctype.h>
#include <pwd.h>
#include <environ.h>
#include <sys/ioctl.h>
#include <sys/exception.h>
#include <sys/sys.h>
#include <sys/proc.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/config.h>
#include <sys/walltime.h>
#include "sh.h"
#include "buildin_cmd.h"
#include "user.h"

static int sh_stdin_backup;
static int sh_stdout_backup;
static int sh_stderr_backup;

userinfo_t cur_user; // current user info

char cwd_cache[MAX_PATH_LEN + 1] = {0};
char cmd_line[MAX_CMD_LEN + 1] = {0};
char *cmd_argv[MAX_ARG_NUM] = {NULL};

// buildin command table
buildin_cmd_t buildin_cmd_table[] = {
    {"exit", buildin_cmd_exit, "close shell and exit"},
    {"cls", buildin_cmd_cls, "clean screen"},
    {"arch", buildin_cmd_arch, "show arch info "},
    {"cwd", buildin_cmd_cwd, "print current work direcotr"},
    {"cd", buildin_cmd_cd, "changed current work director"},
    {"shinfo", buildin_cmd_shinfo, "show shell info"},
    {"who", buildin_cmd_who, "show current shell user info"},
    {"help", buildin_cmd_help, "show help list"},
};

static void print_prompt()
{
    // printf cwd and current login user
    printf("\n%s", cwd_cache);
}

int sh_exit(int ret, int relation)
{
    close(sh_stdin_backup);
    close(sh_stdout_backup);
    close(sh_stderr_backup);
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    if (relation)
    {
        pid_t ppid = getppid();
        if (ppid > 0)
            excsend(EXC_CODE_USER, ppid);
    }
    exit(ret);
}

static void sh_exit_handler(uint32_t code)
{
    sh_exit(code, 0);
}

static void sh_strip_blank(char *cmd)
{
    strrtrim(cmd);
    strtrim(cmd);
}

static void readline(char *buff, uint32_t count)
{
    int len = 0;
    char *pos = buff;

    while (len < count)
    {
        if (read(STDIN_FILENO, pos, 1) < 0)
            continue;

        if (*pos == '\n') // next line
        {
            *pos = '\0';
            break;
        }
        else
        {
            if (*pos == '\b') // backspace
            {
                *pos = '\0';
                // had data just del a front char
                if (pos > buff)
                {
                    *(--pos) = '\0';
                }
                else
                {
                    len = 0;
                }
            }
            else
            {
                // next char
                len++;
                pos++;
            }
        }
    }
}

static int do_buildin_cmd(int cmd_argc, char **cmd_argv)
{
    int cmd_num = ARRAY_SIZE(buildin_cmd_table);
    buildin_cmd_t *cmd_ptr;
    int i = 0;

    for (i = 0; i < cmd_num; i++)
    {
        cmd_ptr = &buildin_cmd_table[i];

        if (cmd_ptr->cmd_name[0] == 0)
            return -1;

        char *src = cmd_ptr->cmd_name, *dst = cmd_argv[0];
        for (; *src == *dst; src++, dst++)
        {
            if (*src == '\0')
                break;
        }
        uint8_t cmp = *src - *dst;

        if (!cmp)
        {
            if (cmd_ptr->cmd_func(cmd_argc, cmd_argv))
                printf("buildin cmd %s failed!\n", cmd_argv[0]);

            return 0;
        }
    }
    return -1;
}

static void cmd_exit(int code)
{
    exit(-1);
}

static int exec_cmd(int argc, char **argv)
{
    int status = 0;

    // enable term exception
    exccatch(EXC_CODE_TERM, cmd_exit);

    if (do_buildin_cmd(argc, argv))
    {
        argv[argc] = NULL;
        int pid;

        pid = fork();
        // fork failed
        if (pid == -1)
        {
            printf("sh: do fork failed!\n");
            return -1;
        }
        else
        {
            if (pid > 0) // father
            {
                // wait child exit
                pid = waitpid(-1, &status, 0);
                tstatus(NULL, NULL);
            }
            else // child
            {
                pid = getpid();
                // do exec cmd
                status = execv(argv[0], argv);
                if (status < 0)
                {
                    printf("sh: bad command %s\n", argv[0]);
                    return -1;
                }
            }
        }

        update_cwdcache();
    }
    return 0;
}

static int cmd_parse(char *cmd_str, char **argv, char token)
{
    if (!cmd_str)
        return -1;
    int arg_idx = 0;
    while (arg_idx < MAX_ARG_NUM)
    {
        argv[arg_idx] = NULL;
        arg_idx++;
    }
    char *next = cmd_str;
    int argc = 0;
    while (*next)
    {
        // trip the token char
        while (*next == token)
        {
            next++;
        }

        // if last argv with space
        if (*next == 0)
            break;

        // save argv addr
        argv[argc] = next;

        // goto the end of argv
        while (*next && *next != token)
        {
            next++;
        }

        // no end argv string  and write eof in end of string
        if (*next)
        {
            *next = '\0';
            next++;
        }
        else
        {
            *next = '\0';
            argc++;
            break;
        }

        if (argc >= MAX_ARG_NUM)
            return -1;

        argc++; // next argv
    }
    return argc;
}

void print_logo()
{
    char buff[32];
    memset(buff, 0, 32);

    if (getver(buff, 32) < 0) // get os version
    {
        printf("OS version info get error!\n");
        strcpy(buff, "Unknow");
    }

    printf("----------------------------------------------------\n");
    printf("| welcome to CatOS shell.version %s.                 |\n", SHELL_VERSION);
    printf("| copyright (C) 2020-2050 by jianxinpeng.            |\n");
    printf("| OS Info: %s                                        |\n", buff);
    printf("----------------------------------------------------\n");
    printf("please input 'help' cmd to show all list\n");
}

void update_cwdcache()
{
    memset(cwd_cache, 0, MAX_PATH_LEN);
    char buff[32] = {0};
    memset(buff, 0, 32);
    account_name(buff, 32);
    // make
    strcat(cwd_cache, "[");
    strcat(cwd_cache, buff);
    getcwd(buff, 32);
    strcat(cwd_cache, "###");
    strcat(cwd_cache, buff);
    strcat(cwd_cache, "]");
}

// when start shell need login an user
static int userlogin()
{
    char user[32];
    char passwd[64];

    printf("user:");
    readline("%s", user);
    printf("password:");
    readline("%s", passwd);
    if (login(user, passwd) < 0)
    {
        printf("login failed!\n");
        return -1;
    }
    return 0;
}

// get current shell user info
void get_curuserinfo()
{
    // if (!cur_user.user_name)
    // {
    //     passwd_t *entry;
    //     cur_user.uid = getuid(); // get task uid
    //     if (cur_user.uid < 0)
    //     {
    //         // need login to continue
    //         if (userlogin() < 0)
    //             return;
    //     }
    //     entry = getpwbyuid(cur_user.uid);
    //     if (!entry)
    //         return;
    //     cur_user.user_name = entry->pw_name;
    //     cur_user.shell = entry->pw_shell;
    //     cur_user.home = entry->pw_dir;
    //     cur_user.gid = entry->pw_gid;
    //     cur_user.uid = entry->pw_uid;
    // }
}

int main(int argc, char **argv)
{
    char *cmd_str = NULL;
    walltime_t time;

    // get user info
    get_curuserinfo();

    if (argc >= 3)
    {
        if (!strcmp(argv[1], "-c"))
        {
            cmd_str = argv[2];
        }
    }

    // backup handle
    sh_stdin_backup = dup(0);
    sh_stdout_backup = dup(1);
    sh_stderr_backup = dup(2);

    // update cwd
    update_cwdcache();

    // init exception
    exccatch(EXC_CODE_USER, sh_exit_handler);
    excblock(EXC_CODE_TERM);
    excblock(EXC_CODE_INT);

    // quiet exec cmd and return
    if (cmd_str)
    {
        argc = cmd_parse(cmd_str, cmd_argv, ' ');
        if (argc < 0)
        {
            fprintf(stderr, "sh: number of argument exceed %d\n", argc);
            return -1;
        }
        // pipe exec
        if (exec_cmd(argc, cmd_argv))
        {
            fprintf(stderr, "sh: exec cmd %s failed!\n", cmd_argv[0]);
            return -1;
        }
        return 0;
    }

    // show logo
    print_logo();
    // show time
    walltime(&time);
    printf("time: %d.%d.%d %d:%d:%d\n", (uint32_t)time.year, (uint32_t)time.month, (uint32_t)time.day, (uint32_t)time.hour, (uint32_t)time.minute, (uint32_t)time.second);

    // start receive and exec cmd
    while (1)
    {
        memset(cmd_line, 0, MAX_CMD_LEN);
        memset(cmd_argv, 0, 16 * sizeof(char *));

        // print prompt
        print_prompt();
        // read cmdline
        readline(cmd_line, MAX_CMD_LEN);

        if (*cmd_line == '\0')
            continue;

        // strip space
        sh_strip_blank(cmd_line);

        if (*cmd_line == '\0')
            continue;

        argc = cmd_parse(cmd_line, cmd_argv, ' ');
        if (argc <= 0)
        {
            fprintf(stderr, "sh: number of argument exceed %d!\n", argc);
            continue;
        }
        // exec cmd
        if (exec_cmd(argc, cmd_argv))
        {
            fprintf(stderr, "sh: exec cmd %s failed!\n", cmd_argv[0]);
        }
    }
    return 0;
}