// file: userapp/sh/buildin_cmd.c
// autor: jiangxinpeng
// time: 2021.12.28
// copyright: (C) 2020-2050 by jiangxinpeng,All right are reserved.

#include "buildin_cmd.h"
#include "sh.h"
#include "user.h"
#include <stdio.h>
#include <sys/arch.h>
#include <sys/config.h>
#include <sys/syscall.h>
#include <sys/ioctl.h>
#include <sys/sys.h>
#include <pwd.h>
#include <unistd.h>

// cwd cache info
extern char cwd_cache[MAX_PATH_LEN+1];
// current user info
extern userinfo_t cur_user;

int buildin_cmd_exit(int argc, char **argv)
{
    printf("sh: exit sh!\n");
    sh_exit(0, 1);
    return 0;
}

int buildin_cmd_shinfo(int argc, char **argv)
{
    print_logo();
    return 0;
}

int buildin_cmd_cls(int argc, char **argv)
{
    if (argc != 1)
    {
        printf("cls: no argument support");
        return -1;
    }
    printf("clean screen\n");
    ioctl(STDIN_FILENO, TTYIO_CLEAR, NULL);
    return 0;
}

int buildin_cmd_help(int argc, char **argv)
{
    if (argc != 1)
    {
        printf("help: no argument support!\n");
        return 0;
    }
    printf("All support buildin command list!\n");
    // list all buildin cmd
    for (int i = 0; i < MAX_CMD_NUM; i++)
    {
        if (buildin_cmd_table[i].cmd_name[0] == 0)
            return 0;
        printf("%s-----%s\n", buildin_cmd_table[i].cmd_name, buildin_cmd_table[i].cmd_help);
    }
    printf("system buildcmd all have %d. input cmd name to show detail help\n", MAX_CMD_NUM);
    return 0;
}

int buildin_cmd_cd(int argc, char **argv)
{
    if (argc > 2)
    {
        printf("cd: cmd format failed!\n");
        return -1;
    }
    // no assign argument
    if (argc == 1)
    {
        printf("%s\n", cwd_cache); // cwd
        return 0;
    }
    char *path = argv[1];
    if (chdir(path))
    {
        printf("cd: no path %s or no permission to access!\n", path);
        return -1;
    }
    // update cwd
    update_cwdcache();
    return 0;
}

int buildin_cmd_cwd(int argc, char **argv)
{
    if (argc != 1)
    {
        printf("cwd: no argument support!\n");
        return -1;
    }
    // get cwd
    char path[MAX_PATH_LEN+1];
    getcwd(path, MAX_PATH_LEN);
    printf("%s\n", path);
    return 0;
}

int buildin_cmd_arch(int argc, char **argv)
{
    if (argc != 1)
    {
        printf("ver: no argument support!\n");
        return -1;
    }

    printf("%s %s\n", SYS_NAME, SYS_VER);
    printf("arch:%s platform:%s host:%s\n", SYS_ARCH, SYS_PLATFORM, SYS_HOST);
    printf("oem: %s\n", SYS_RESERVED);
    return 0;
}

int buildin_cmd_who(int argc, char **argv)
{
    if (argc != 1)
    {
        printf("who: command no support argument!\n");
        return -1;
    }

    printf("%s\n", cur_user.user_name);
    return 0;
}

int buildin_cmd_login(int argc, char **argv)
{
    char user[32];
    char password[128];
    int try;

    if (argc >= 3)
    {
        printf("login: too many argument!\n");
        return -1;
    }

    if (argc == 1)
    {

        // try count is 3
        while (try < 3)
        {
            printf("user:");
            gets(user);
            printf("passward:");
            gets(password);

            // login user
            if (login(user, password) < 0)
            {
                try++;
                printf("login failed!\n");
            }
            else
            {
                // update user info
                get_curuserinfo();
                return 0;
            }
        }

        if (try >= 3)
        {
            printf("try count above limit!\n");
            return -1;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        if (argc == 3)
        {
            // direct login
            if (login(argv[1], argv[2]) < 0)
            {
                printf("login failed! please check user and password!\n");
                return -1;
            }
        }
        else
        {
            printf("Usage: login [name] [password]");
            return 0;
        }
    }
    return 0;
}

int buildin_cmd_logout(int argc, char **argv)
{
    if (argc != 1)
    {
        printf("logout: no support arguemnt!\n");
        return -1;
    }

    // no login
    if (!cur_user.user_name)
    {
        printf("no user login!\n");
        return -1;
    }

    // logout user
    if (logout(cur_user.user_name) < 0)
    {
        printf("logout failed!\n");
        return -1;
    }
    printf("user %s logout!\n", cur_user.user_name);
    return 0;
}