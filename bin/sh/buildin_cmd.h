#ifndef  __BUILDIN_CMD_H
#define __BUILDIN_CMD_H

#ifdef __cplusplus
extern "C" {
#endif 

int buildin_cmd_exit(int argc, char **argv);
int buildin_cmd_cls(int argc, char **argv);
int buildin_cmd_help(int argc, char **argv);
int buildin_cmd_cd(int argc, char **argv);
int buildin_cmd_cwd(int argc, char **argv);
int buildin_cmd_arch(int argc, char **argv);
int buildin_cmd_shinfo(int argc, char **argv);
int buildin_cmd_who(int argc,char **argv);
int buildin_cmd_login(int argc, char **argv);
int buildin_cmd_logout(int argc, char **argv);


#ifdef __cplusplus
}
#endif
#endif