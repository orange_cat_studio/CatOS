#ifndef __SH_USER_H
#define __SH_USER_H

#include<types.h>

typedef struct
{
    uid_t uid, euid;
    gid_t gid, egid;
    char *user_name;
    char *shell;
    char *home;
} userinfo_t;

#endif