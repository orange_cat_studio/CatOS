
#ifndef _SH_H
#define _SH_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stddef.h>

#define SHELL_NAME "CatOS Shell"
#define SHELL_VERSION "0.1"

#define MAX_CMD_LEN 256
#define MAX_HELP_LEN 64

#define MAX_ARG_NUM 16
#define MAX_CMD_NUM 32

    typedef int (*cmd_func_t)();

    typedef struct buildin_cmd
    {
        char cmd_name[MAX_CMD_LEN];
        cmd_func_t cmd_func;
        char cmd_help[MAX_HELP_LEN];
    } buildin_cmd_t;

    extern buildin_cmd_t buildin_cmd_table[MAX_CMD_NUM];

    int main(int argc, char **argv);
    int sh_exit(int ret, int relation);
    void print_logo();
    void update_cwdcache();
    void get_curuserinfo();

#ifdef __cplusplus
}
#endif
#endif