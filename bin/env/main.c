#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/list.h>
#include <sys/environ.h>

env_t *p = NULL;
env_t environ;

int main(int argc, char **argv)
{
    env_t buff;

    if (argc > 1)
    {
        fprintf(stderr, "[env] command syntax error!\n");
        return -1;
    }

    do
    {
        p = env(p, &environ);
        if (!p)
            return -1;
        printf("env name %s ---> env value %s\n", environ.name, environ.val);
    } while (p);
    
    return 0;
}