#include<stdio.h>
#include<stdlib.h>
#include<stddef.h>
#include<sys/swap.h>

int main(int argc, char** argv)
{
    if (argc != 1)
    {
        fprintf(stderr, "swapmem: no support argument!\n");
        return -1;
    }
    
    swap_info_t swap;
    if (SwapMem(&swap) < 0)
    {
        fprintf(stderr, "swapmem: get swap mem info error!\n");
        return -1;
    }

    printf("-----swap mem info-------\n");
    printf(" total mem: %6d\n", swap.total_size);
    printf(" free mem:  %6d\n", swap.free_size);
    printf(" used mem:  %6d\n", swap.used_size);
}