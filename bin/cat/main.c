#include <stdio.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/status.h>

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        fprintf(stderr, "cat: argument error!\n");
        return -1;
    }

    const char *path = argv[1];
    int fd = open(path, O_RDONLY); // open
    if (fd < 0)
    {
        fprintf(stderr, "cat: file %s open failed! may be no exist or error!\n", path);
        return -1;
    }

    status_t fstu;
    stat(path, &fstu); // get status
    uint32_t size = fstu.st_size;
    // file size check
    if (size <= 0)
        return 0;

    uint32_t buffsz = 0;
    if (size < 5 * MB)
        buffsz = fstu.st_size;
    else
        buffsz = 3 * MB;

    while (size)
    {
        // malloc buffer
        void *buff = malloc(buffsz);
        if (!buff)
        {
            fprintf(stderr, "cat: mem no enough!\n");
            return -1;
        }
        int bytes = read(fd, buff, fstu.st_size); // read file
        int i = 0;
        // output
        while (bytes--)
        {
            putchar(((char *)buff)[i++]);
        }
        free(buff);
        size -= buffsz;
    }
    close(fd); // close
    return 0;
}
