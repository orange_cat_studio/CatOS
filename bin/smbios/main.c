#include <sys/sys.h>
#include <stdio.h>
#include <stdlib.h>
#include "smbios.h"

#define PAGE_HIGH_BASE 0x80000000

smbios_t *smbios = NULL;
uint64_t len = 0;

smbios_struct_header_t *smbios_get_table(smbios_t *smbios, int type)
{
    uint8_t *p = (uint8_t *)(smbios->table_point + PAGE_HIGH_BASE);
    smbios_struct_header_t *header;
    uint32_t limit = (uint32_t)((uint32_t)smbios->table_total_len + (uint32_t)p);
    int i;
    uint32_t count = smbios->table_num;

    if (!smbios)
        return NULL;

    for (i = 0; i < count; i++)
    {
        header = (smbios_struct_header_t *)p;

        if ((uint32_t)p >= limit)
        {
            printf("[smbios] Smbios struct table limit fault!\n");
            return NULL;
        }

        if (header->length)
        {
            if (header->type == type)
            {
                printf("[smbios] Smbios struct table [%d] found! type:%d len:%d handler:%d\n", type, header->type, header->length, header->handler);
                return header;
            }
        }

        // goto next table
        p += header->length;
        while ((uint32_t)p <= limit)
        {
            if (*p == 0x00 && *(p + 1) == 0x00)
            {
                p += 2;
                break;
            }
            p++;
        }
    }

    printf("[smbios] No found targe smbios struct table! type:%d\n", type);
    return NULL;
}

char *smbios_getver(char *ver, int len)
{
    uint8_t major = smbios->major_version;
    uint8_t minor = smbios->minor_version;
    uint8_t buff[32];

    memset(ver, 0, len);

    strcpy(ver, itoa(major, 10, buff, 0));
    strcat(ver, ".");
    strcat(ver, itoa(minor, 10, buff, 0));

    return ver;
}

char *smbios_getstr(smbios_struct_header_t *header)
{
    uint8_t *p = (uint8_t *)header;

    if (header->length)
        return p + header->length;
}

uint32_t smbios_getstrlen(smbios_struct_header_t *header)
{
    uint8_t *p = (uint8_t *)header;

    if (header->length)
    {
        p += header->length;

        uint8_t *t = p;
        uint32_t limit = smbios->table_point + PAGE_HIGH_BASE + smbios->table_total_len;
        while ((uint32_t)t < limit)
        {
            if (*t == 0x00 && *(t + 1) == 0x00)
            {
                t += 2;
                return t - (p + 1);
            }
            t++;
        }
    }
    return 0;
}

char *smbios_findstr(smbios_struct_header_t *header, int idx)
{
    uint32_t len = smbios_getstrlen(header);  // get totol string length of struct table
    uint8_t *p = smbios_getstr(header); // get start of string

    if (!idx)
        return NULL;

    while (p < p + len)
    {
        if (idx == 1)
        {
            return p;
        }

        if (*p == '\0')
        {
            idx--;
        }
        p++;
    }
    return NULL;
}

void smbios_system_enclousure_print()
{
    smbios_struct_header_t *h = smbios_get_table(smbios, System_Enclosure);
    smbios_table_system_enclosure_classic_t *enclosure = h;
    if (!h)
        return;

    printf("System Enclosure or Classic Info\n");
    printf("-----------------------------------------------\n");
    printf("type: %x\n", enclosure->enclosure_type);
    printf("version: %s\n", smbios_findstr(h, enclosure->version) ?: "NULL");
    printf("Serial num: %s\n", smbios_findstr(h, enclosure->serial_num) ?: "NULL");
    printf("Asset Tag: %s\n", smbios_findstr(h, enclosure->asset_tag) ?: "NULL");
    printf("Bootup Status: %x\n", enclosure->bootup_status);
    printf("Power Status: %x\n", enclosure->power_status);
    printf("Thermal Status: %x\n", enclosure->thermal_status);
    printf("Security Status: %x\n", enclosure->security_status);
    printf("Oem: %x\n", enclosure->oem);
    printf("Height: %d\n", enclosure->height);
    printf("Number Of Power Cord: %d\n", enclosure->num_of_power);
}

void smbios_bios_print()
{
    smbios_struct_header_t *h = smbios_get_table(smbios, BIOS_Info);
    smbios_table_bios_t *bios = h;

    if (!h)
        return;
    printf("BIOS Info\n");
    printf("-----------------------------------------------\n");
    printf("Vendor: %s\n", smbios_findstr(h, bios->vendor) ?: "NULL");
    printf("Version: %s\n", smbios_findstr(h, bios->version) ?: "NULL");
    printf("Start addr: %x\n", (uint32_t)bios->start_addr);
    printf("Release data: %s\n", smbios_findstr(h, bios->release_date) ?: "NULL");
    printf("ROM size: %d\n", (uint32_t)bios->rom_size);
    printf("support: %x\n", (uint32_t)bios->support);
    printf("Bios extension: %x\n", bios->extension[0]);
    printf("Bios extension2: %x\n", bios->extension[1]);
    printf("system major: %d\n", (uint32_t)bios->sysbios_major_release);
    printf("system minor: %d\n", (uint32_t)bios->sysbios_minor_release);
    printf("Emabedded major: %d\n", (uint32_t)bios->ctrlfw_major_release);
    printf("Emabedded minjor: %d\n", (uint32_t)bios->ctrlfw_minor_release);
    printf("-------------------------------------------------\n");
}

void smbios_system_print()
{
    smbios_struct_header_t *h = smbios_get_table(smbios, System_Info);
    smbios_table_syteminfo_t *system = h;
    if (!h)
        return;
    printf("System Info\n");
    printf("----------------------------------------------\n");
    printf("Manufacturer: %s\n", smbios_findstr(h, system->manufacturer) ?: "NULL");
    printf("Product: %s\n", smbios_findstr(h, system->product_name) ?: "NULL");
    printf("Version: %s\n", smbios_findstr(h, system->version) ?: "NULL");
    printf("Serial num: %s\n", smbios_findstr(h, system->serial_number) ?: "NULL");
    printf("UUID:");
    for (int i = 0; i < 16; i++)
    {
        printf("%x", (uint32_t)system->UUID[i]);
    }
    printf("\n");
    printf("WakeUP: %x\n", system->wakeup_type);
    printf("SKU number: %s\n", smbios_findstr(h, system->SKU_number) ?: "NULL");
    printf("Family: %s\n", smbios_findstr(h, system->family) ?: "NULL");
    printf("----------------------------------------------\n");
}

void smbios_basebroad_print()
{
    smbios_struct_header_t *h = smbios_get_table(smbios, Baseboard_Info);
    smbios_table_baseboard_info_t *baseboard = h;
    if (!h)
        return;
    printf("BaseBoard Info\n");
    printf("--------------------------------------------\n");
    printf("Manufacturer: %s\n", smbios_findstr(h, baseboard->manufacturer) ?: "NULL");
    printf("Product: %s\n", smbios_findstr(h, baseboard->product) ?: "NULL");
    printf("Version: %s\n", smbios_findstr(h, baseboard->version) ?: "NULL");
    printf("Serial num: %s\n", smbios_findstr(h, baseboard->serial_number) ?: "NULL");
    printf("Asset Tag: %s\n", smbios_findstr(h, baseboard->asset_tag) ?: "NULL");
    printf("Feature: %x\n", baseboard->feature_flags);
    printf("Location in Chassis: %s\n", smbios_findstr(h, baseboard->location_chassic) ?: "NULL");
    printf("Chassis: %x\n", baseboard->chassic_handle);
    printf("BoardType: %d\n", baseboard->board_type);
    printf("---------------------------------------------\n");
}

void smbios_dump()
{
    char ver[8];

    printf("----------smbios-------------\n");
    printf("ver: %s\n", smbios_getver(ver, 8));
    printf("max size: %d\n", smbios->max_size);
    printf("smbios revision: %x\n", smbios->smbios_revision);
    printf("table length: %d,table point: %x,table num: %d\n", smbios->table_total_len, smbios->table_point, smbios->table_num);
    printf("bcd revision: %x\n", smbios->bcd_revision);
    smbios_bios_print();
    smbios_basebroad_print();
    smbios_system_print();
    smbios_system_enclousure_print();
}

int main()
{
    smbios = get_fireware("SMBIOS", strlen("SMBIOS"), &len);
    if (!smbios)
    {
        printf("[smbios] system smbios get err\n");
        return -1;
    }
    smbios_dump();
}