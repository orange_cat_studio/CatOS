#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <unistd.h>



int main(int argc, char **argv)
{
    int ret = -1;
    char *path = argv[1];
    if (argc != 2)
    {
        fprintf(stderr, "mkdir: argument error!\n");
        return -1;
    }
    if (!mkdir(path, 0))
    {
        printf("mkdir: dir %s make successful!\n", path);
        return 0;
    }
    fprintf(stderr, "mkdir: make dir %s failed。\n", path);
    return -1;
}