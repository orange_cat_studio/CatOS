#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <unistd.h>

int main(int argc, char **argv)
{
    if (argc == 1)
    {
        fprintf(stderr, "touch: please input filename!\n");
        return -1;
    }

    if (argc > 2)
    {
        fprintf(stderr, "touch: argument error!\n");
        return -1;
    }

    const char *path = argv[1];
    int fd = open(path, O_CREATE | O_RDWR);
    if (fd < 0)
    {
        fprintf(stderr, "touch: file %s error!\n", path);
        return -1;
    }
    printf("touch: operator ok\n");
    close(fd);
    return 0;
}
