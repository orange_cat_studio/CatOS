#include <sys/dir.h>
#include <types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/dir.h>

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        printf("find: no assign arg\n");
        return -1;
    }

    if (argc == 2)
    {
        char *file = argv[1];
        if (open(file, O_RDONLY) < 0)
        {
            printf("find: no found file\n");
        }
        else
        {
            printf("find: file exist");
        }
    }
    return 0;
}