//file: userapp/echo/main.c
//autor: jiangxinpeng
//time: 2021.12.27
//copyright: (C) 2020-2050 by jiangxinpeng,All right are reserved.

#include <ctype.h>
#include <types.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include "echo.h"

int main(int argc, char *argv[])
{
    if (argc == 1)
        return 0;

    char *p = argv[1];
    uint8_t no_line = 0;
    uint8_t trope = 0;
    uint8_t pos = 1; //message position

    if (*p == '-')
    {
        p++;
        switch (*p)
        {
        case 'n': //new line
            no_line = 1;
            break;
        case 'h': //help
            printf("usage: echo [option][message]\n");
            printf("option:\n");
            printf("-n   No new line after ouput\n");
            printf("-v   Print version\n");
            printf("-h   Print help page\n");
            printf("-e   Escape a string\n");
            break;
        case 'v': //version
            printf("echo version: %s\n", ECHO_VERSION);
            break;
        case 'e': //escape
            trope = 1;
            break;
        default:
            break;
        }
        p++;
        pos++;
    }
    int i;
    for (i = pos; i < argc; i++)
    {
        if (trope)
        {
            p = argv[i];
            while (*p)
            {
                if (*p == '\\')
                {
                    switch (*(p + 1))
                    {
                    case 'n':
                        p += 2;
                        putchar('\n');
                        break;
                    case 'b':
                        p += 2;
                        putchar('\b');
                        break;
                    case 'c':
                        p += 2;
                        no_line = 1;
                        break;
                    case 't':
                        p += 2;
                        putchar('\t');
                        break;
                    case '\\':
                        p += 2;
                        putchar('\\');
                        break;
                    default:
                        putchar(*p++);
                        break;
                    }
                }
                else
                {
                    putchar(*p++); //output direct
                }
            }
        }
        else //output direct
        {
            printf("%s", argv[i]);
        }
        //output a space to spilt
        if (i < argc - 1)
            putchar(' ');
    }
    if (!no_line)
        putchar('\n'); //new line
    return 0;
}