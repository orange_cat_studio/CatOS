#ifndef _ECHO_H
    #define _ECHO_H

    #ifdef __cplusplus
    extern "C" {
    #endif
    

    #define ECHO_VERSION "0.1"

    int main(int argc,char *argv[]);

    #ifdef __cplusplus
    }
    #endif
#endif