#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <unistd.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/udev.h>
#include <sys/sys.h>
#include <sys/status.h>
#include <sys/ioctl.h>

#define LOOPBACK_NAME "loopback"

static int loop_device_setup(char *dev, char *file)
{
    if (!dev || !file)
        return -1;
    int fd = open(dev, O_RDWR);
    if (fd < 0)
    {
        fprintf(stderr, "mount: device %s not exist or no permission!\n", dev);
        return -1;
    }
    // do ioctl to setup
    if (ioctl(fd, DISKIO_SETUP, file) < 0)
    {
        fprintf(stderr, "mount: set device %s with file %s failed!\n", dev, file);
        close(fd);
        return -1;
    }
    close(fd);
    return 0;
}

static int loop_device_setdown(char *dev)
{
    if (!dev)
        return -1;
    int fd = open(dev, O_RDWR);
    if (fd < 0)
    {
        fprintf(stderr, "mount: device %s not exist or no perssion!\n", dev);
        return -1;
    }
    if (ioctl(fd, DISKIO_SETDOWN, NULL))
    {
        fprintf(stderr, "mount: set device %s down failed!\n", dev);
        close(fd);
        return -1;
    }
    return 0;
}

static void help()
{
    printf("mount:  mount [ -t fstype] [-o opt] device dir\n");
    printf("opt:\n");
    printf("  -t: fstype\n");
    printf("      FAT12\n");
    printf("      FAT16\n");
    printf("      FAT32\n");
    printf("  -h: help\n");
    printf("  -o: loop device\n");
}

int main(int argc, char *argv[])
{
    char *arg_fs=NULL;
    char *arg_opt=NULL;
    char *arg_device=NULL;
    char *arg_dir=NULL;
    int res;

    while ((res = getopt(argc, argv, "hvt:o:")) != -1)
    {
        switch (res)
        {
        case 'h':
            help();
            return 0;
        case 't':
            arg_fs = optarg;
            break;
        case 'o':
            arg_opt = optarg;
            break;
        default:
            fprintf(stderr, "mount: unknow opt!\n");
            break;
        }
    }

    // get device and dir path
    if (argv[optind])
    {
        arg_device = argv[optind];
        optind++;
        if (argv[optind])
            arg_dir = argv[optind];
    }

    // no assign fs
    if (!arg_fs)
        arg_fs = "auto";

    char loop_dev[32];
    if (arg_opt)
    {
        // loop
        if (!strcmp(arg_opt, "loop"))
        {
            char devname[32];
            if (probedev("loop", devname, 32) < 0)
            {
                fprintf(stderr, "mount: probe loop device failed!\n");
                return -1;
            }
            strcpy(loop_dev, "/dev/");
            strcat(loop_dev, devname);
        }
        else
        {
            // config loop path
            if (!strncmp(arg_opt, LOOPBACK_NAME, strlen(LOOPBACK_NAME)))
            {
                char *config_device = arg_opt + strlen(LOOPBACK_NAME);
                if (*config_device == '=')
                {
                    config_device++;
                    strcpy(loop_dev, config_device);
                }
            }
            else
            {
                fprintf(stderr, "mount: invalid operator!\n");
            }
        }

        char *file_path = arg_device;
        // check file exist
        if (access(file_path, F_OK) < 0)
        {
            fprintf(stderr, "mount: file %s no exit!\n", file_path);
            return -1;
        }
        // set loop device
        if (loop_device_setup(loop_dev, file_path) < 0)
            return -1;
        arg_device = loop_dev; // point loop device
    }

    
    if (!arg_device || !arg_dir)
    {
        fprintf(stderr, "mount: no assign mount object!\n");
        return -1;
    }

    // mount device
    printf("mount: device %s path %s fs %s\n", arg_device, arg_dir, arg_fs);
    if (mount(arg_device, arg_dir, arg_fs, 0) < 0)
    {
        fprintf(stderr, "mount: device:%s dir:%s fs:%s failed!\n", arg_device, arg_dir, arg_fs);
        return -1;
    }
    return 0;
}
