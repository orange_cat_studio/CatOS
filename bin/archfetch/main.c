#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <const.h>
#include <sys/proc.h>
#include <sys/sys.h>
#include <sys/sys.h>
#include <sys/vmm.h>
#include "archfetch.h"

#define CPU_DEV "/dev/cpu0"
#define CPU_LEN 64

char *cat_logo[] =
    {
        "----------------------------------------",
        "|    ^      ^                          |",
        "|   ———————————                        |",
        "|  /           \\                      |",
        "| |  *    *     |                      |",
        "| |     ^       |                      |",
        "| \\   ~~~     / ——————————\\          |",
        "|   ----------/               \\       |",
        "|            |                 | ____  |",
        "|            |                 | ____ )|",
        "|            \\                /       |",
        "|              ———————————————         |",
        "|               | |      | |           |",
        "|              <__|     <__|           |",
        " ---------------------------------------",
};

static void print_logo()
{
    int logo_len = sizeof(cat_logo) / sizeof(cat_logo[0]);
    int i;
    for (i = 0; i < logo_len; i++)
    {
        printf("%s\n", cat_logo[i]);
    }
}

static void print_ver()
{
    char buff[SYS_VER_LEN];
    getver(buff, SYS_VER_LEN);
    printf("os: %s\n", buff);
}

static int print_mem()
{
    mstatus_t ms;
    mstate(&ms);
    char *msg = "mem";
    // GB
    if (ms.mst_total / GB > 1)
    {
        printf("%s: %d GB / %d GB\n", msg, (uint32_t)(ms.mst_used / GB), (uint32_t)(ms.mst_total / GB));
    }
    else
    {
        // MB
        printf("%s: %lld MB / %lld MB\n", msg, (uint32_t)(ms.mst_used / MB),(uint32_t)(ms.mst_total / MB));
    }
}

static int print_cpuinfo()
{
    int fd = open(CPU_DEV, O_RDONLY);
    if (fd < 0)
    {
        close(fd);
        return -1;
    }
    char buff[CPU_LEN];
    if (read(fd, buff, CPU_LEN) < 0)
    {
        close(fd);
        return -1;
    }
    printf("cpu: %s\n", buff);
    close(fd);
    return 0;
}

int main(int argc, char **argv)
{
    print_logo(cat_logo);
    printf("-------------------------\n");
    print_ver();
    print_cpuinfo();
    print_mem();
    printf("--------------------------\n");
    return 0;
}
