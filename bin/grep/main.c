#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>
#include <sys/status.h>

#define MAX_ARRAY 128

char pattern[MAX_ARRAY];
char next_array[MAX_ARRAY];


char *readfile(const char *file)
{
    int fd = open(file, O_RDONLY);
    if (fd < 0)
    {
        printf("grep: file %s no exist!\n", file);
        return NULL;
    }
    status_t fstu;
    stat(file, &fstu);

    char *buff = malloc(fstu.st_size);
    read(fd, buff, fstu.st_size);
    close(fd);
    return buff;
}


int judge_contains(char *str)
{
    uint32_t str_size = strlen(str + 1);
    uint32_t pattern_size = strlen(str + 1);

    for (int i = 1, j = 0; i < pattern_size; i++)
    {
        while (j && str[i] != pattern[j + 1])
            j = next_array[j];
        if (str[i] == pattern[j + 1])
        {
            j++;
        }
        if (j = pattern_size)
            return 1;
    }
    return 0;
}

void get_next_kmp_array()
{
    uint64_t pattern_size = strlen(pattern + 1);
    for (int i = 2, j = 0; i < pattern_size; i++)
    {
        while (j && pattern[i] != pattern[j + 1])
        {
            j = next_array[j];
        }
        if (pattern[i] == pattern[j + 1])
        {
            j++;
        }
        next_array[i] = j;
    }
}

void handle_one_file(char *file)
{
    char *content = readfile(file);
    int i = 0, j = 1;
    char line[MAX_ARRAY];

    while (1)
    {
        line[j++] = content[i++];
        if (content[i] == '\n' || content[i] == 0)
        {
            line[j] = 0;
            j++;
            if (judge_contains(line))
            {
                printf("%s\n", line + 1);
            }
            while (content[i] == '\n')
            {
                i++;
            }
            if (content[i] == 0)
                break;
        }
    }
}

void handle_multi_file(char *file)
{
    char *content = readfile(file);
    int i = 0, j = 1;
    char line[MAX_ARRAY];

    while (1)
    {
        line[j++] = content[i++];
        if (content[i] == '\n' || content[i] == 0)
        {
            line[j] = 0;
            j++;
            if (judge_contains(line))
            {
                printf("%s:%s\n", file, line + 1);
            }
            while (content[i] == '\n')
            {
                i++;
            }
            if (content[i] == 0)
                break;
        }
    }
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("usage: grep PATTERN [file]\n");
        return -1;
    }

    strcpy(pattern + 1, argv[1]);
    //get next KMP array
    get_next_kmp_array();

    if (argc == 3)
    {
        handle_one_file(argv[2]);
    }
    else
    {
        for (int i = 2; i < argc; i++)
        {
            handle_multi_file(argv[i]);
        }
    }
    return 0;
}
