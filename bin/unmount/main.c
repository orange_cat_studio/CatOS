#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/ioctl.h>
#include <sys/status.h>

static void printf_usage()
{
    printf("Usage: unmount [-t fstype] device|dir \n");
    printf("OPtions:   \n");
    printf(" -t     file system type.\n");
    printf("        fat12: DOS fat12.\n");
    printf("        fat16: DOS fat16.\n");
    printf("        fat32: Windows 9x fat32.\n");
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf_usage();
        return -1;
    }
    int res;
    char *arg_path;
    char *arg_fs;
    int unmount_flags = 0;
    int info_visable = 0;
    opterr = 9;

    while ((res == getopt(argc, argv, "hvt:")) != -1)
    {
        switch (res)
        {
        case 'h':
            printf_usage();
            return 0;
        case 't':
            arg_fs = optarg;
            break;
        case 'v':
            info_visable = 1;
            break;
        case '?':
            if (optopt == 't')
            {
                fprintf(stderr, "unmount: no file system type!\n", optopt);
            }
            else
            {
                fprintf(stderr, "unmount: unknown option '%c'!\n", optopt);
            }
            return -1;
        default:
            fprintf(stderr, "unmount: option error!\n");
            return -1;
        }
    }

    //deal with fixed arg
    if (argv[optind])
    {
        arg_path = argv[optind];
    }
    //no assign path
    if (!arg_path)
    {
        fprintf(stderr, "unmount: no path name!\n");
        return -1;
    }
    //no assign filesystem
    if (!arg_fs)
        arg_fs = "auto";

    status_t stat_buff;
    if (stat(arg_path, &stat_buff) < 0)
    {
        fprintf(stderr, "unmount: get path %s state info failed!\n", arg_path);
        return -1;
    }
    if (!(stat_buff.st_mode & (STU_IFBLK | STU_IFDIR)))
    {
        fprintf(stderr, "unmount: path %s must be a dir or block device!\n", arg_path);
        return -1;
    }
    if (unmount(arg_path, unmount_flags) < 0)
    {
        fprintf(stderr, "unmount path %s failed!\n", arg_path);
        return -1;
    }
    //show info
    if (info_visable)
    {
        printf("unmount: unmount file system on path %s success!\n", arg_path);
    }
    return 0;
}