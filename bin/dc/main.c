#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/ioctl.h>

#define VERISION "0.1"
#define DEVFS_PATH "/dev/"
#define COPYRIGHT_INFO "copyright 2020-2050 by jiangxinpeng,all right reserved\n"

char *input_path = NULL;
char *output_path = NULL;
int count = 0;
int block = 0;

#define BLOCKSIZE 512

static void show_help()
{
    printf("dc: -i input -o output -b block_size -c count\n");
}

static void copy_data(char *input_path, char *output_path, int block, int count)
{
    if (!input_path || !output_path)
        return;
    if (count == 0)
    {
        printf("dc: no assign copy count\n");
        return;
    }
    if (block == 0) // used default block size
        block = BLOCKSIZE;

    printf("---start copy block %d count %d\n", block, count);

    // open source and targe file
    int handle_i = open(input_path, O_RDONLY);
    if (handle_i < 0)
        printf("dc: open input file error\n");

    int handle_o = open(output_path, O_RDWR|O_TRUNC);
    if (handle_o < 0)
        printf("dc: open output file error\n");

    if (handle_i < 0 || handle_o < 0)
        return;

    int total_read = 0, total_write = 0;

    char *buff = malloc(block);
    if (!buff)
        return;

    for (int i = 0; i < count; i++)
    {
        int reads = read(handle_i, buff, block);
        if (reads < 0)
            return;
        int writes = write(handle_o, buff, block);
        if (writes < 0)
            return;
        total_read += reads;
        total_write += writes;
    }
    close(handle_i);
    close(handle_o);
    printf("opertor ok,reads %d bytes,write %d bytes\n", total_read, total_write);
}

int main(int argc, char **argv)
{
    if (argc == 1)
    {
        printf("dc: no assign argument\n");
    }
    else
    {
        int res = -1;
        char *argstr = NULL;

        // regonize argument
        while ((res = getopt(argc, argv, "hvi:o:b:c:")) != -1)
        {
            switch (res)
            {
            case 'h':
                show_help();
                return;
            case 'v':
                printf("version: %s\n", VERISION);
                return;
            case 'i':
                input_path = (char *)optarg;
                break;
            case 'o':
                output_path = (char *)optarg;
                break;
            case 'b':
                argstr = (char *)optarg;
                if (!argstr || !argstr[0])
                {
                    printf("dc: block argument unregonized\n");
                    return;
                }
                block = atoi(argstr);
                break;
            case 'c':
                argstr = (char *)optarg;
                if (!argstr || !argstr[0])
                {
                    printf("dc: count argument unregonized\n");
                    return;
                }
                count = atoi(argstr);
            default:
                break;
            }
        }

        if (!input_path || !output_path)
        {
            printf("please assign device\n");
            return;
        }

        printf("dc input %s output %s block %d count %d\n", input_path, output_path, block, count);

        copy_data(input_path, output_path, block, count);
    }
}