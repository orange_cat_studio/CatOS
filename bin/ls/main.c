#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <types.h>
#include <dirent.h>
#include <sys/time.h>
#include <sys/walltime.h>
#include <sys/status.h>

static void __ls(char *path, int detail)
{
    DIR *dir = opendir(path);
    if (!dir)
    {
        printf("ls: path %s no found or no access permission!\n", path);
        return;
    }
    printf("read dir\n");
    rewinddir(dir);

    dirent_t *dirent;
    char subpath[MAX_PATH_LEN+1];
    status_t fstu;
    char type;
    char attrR, attrW, attrX;

    do
    {
        if (!(dirent = readdir(dir)))
            break;

        if (detail) // list all attr
        {
            if (dirent->d_attr & DIRENT_DIR)
            {
                type = 'd';
            }
            else
            {
                if (dirent->d_attr & DIRENT_BLOCK)
                {
                    type = 'b';
                }
                else
                {
                    if (dirent->d_attr & DIRENT_CHAR)
                        type = 'c';
                    else
                        type = '-';
                }
            }

            // full path
            memset(subpath, 0, MAX_PATH_LEN);
            buildpath(path, subpath);
            strcat(subpath, "/");
            strcat(subpath, dirent->d_name);

            memset(&fstu, 0, sizeof(status_t));
            if (stat(subpath, &fstu))
            {
                printf("ls: get file %s status failed!\n", subpath);
                continue;
            }

            if (fstu.st_mode & STU_IREAD)
            {
                attrR = 'r';
            }
            else
            {
                attrR = '-';
            }

            if (fstu.st_mode & STU_IWRITE)
            {
                attrW = 'w';
            }
            else
            {
                attrW = '-';
            }

            if (fstu.st_mode & STU_IEXEC)
            {
                attrX = 'x';
            }
            else
            {
                attrX = '-';
            }
            printf("|type|attr|date|time|size|name|\n");
            printf("%c%c%c%c %04d/%02d/%02d %02d:%02d:%02d %d %s\n", type, attrR, attrW, attrX, WALLTIME_RD_YEAR(fstu.st_mtime >> 16), WALLTIME_RD_MON(fstu.st_mtime >> 16), WALLTIME_RD_DAY(fstu.st_mtime >> 16), WALLTIME_RD_HOUR(fstu.st_mtime & 0xffff), WALLTIME_RD_MIN(fstu.st_mtime & 0xffff), WALLTIME_RD_SEC(fstu.st_mtime & 0xffff), (uint32_t)fstu.st_size, dirent->d_name);
        }
        else // normal mode
        {
            if (dirent->d_attr & DIRENT_DIR)
            {
                type = 'd';
            }
            else
            {
                if (dirent->d_attr & DIRENT_BLOCK)
                {
                    type = 'b';
                }
                else
                {
                    if (dirent->d_attr & DIRENT_CHAR)
                        type = 'c';
                    else
                        type = '-';
                }
            }
            printf("%c %s\n", type, dirent->d_name);
        }
    } while (1);
}

int main(int argc, char **argv)
{
    if (argc == 1)
    {
        printf("[ls] current dir\n");
        __ls(".", 1); // ls current dir
    }
    else
    {
        char *arg_path = NULL;
        int detail = 0;
        int arg_idx = 1;

        while (arg_idx < argc)
        {
            if (argv[arg_idx][0] == '-') // opt
            {
                char *opt = &argv[arg_idx][1];
                if (*opt)
                {
                    switch (*opt)
                    {
                    case 'l':
                        detail = 1;
                        break;
                    case 'h':
                        printf("usage: ls [opt] [dir]\n");
                        printf("opt list: \n");
                        printf("  -l printf all info detail.\n");
                        printf("  -h printf help list.\n");
                        printf("When no assign opt,just show current dir.\n");
                        break;
                    }
                }
            }
            else // dir
            {
                if (!arg_path)
                {
                    arg_path = argv[arg_idx];
                }
            }
            arg_idx++; // next arg
        }

        if (!arg_path)
        {
            __ls(".", detail); // ls current dir
        }
        else
        {
            __ls(arg_path, detail); // ls path
        }
    }
    return 0;
}