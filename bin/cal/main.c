#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <sys/status.h>
#include <sys/walltime.h>

// day
const char month_day[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

// month
static const char *month_name[] = {
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"};

int is_leep(int year)
{
    if (!(year % 4) && year % 100)
        return 1;
    if (!(year % 400))
        return 1;
    return 0;
}

int get_week(int year, int month, int day)
{
    int c, y, week;
    if (month == 1 || month == 2) // is month 1 or 2
    {
        year--;
        month += 12;
    }
    c = year / 100;
    y = year - c * 100;
    week = (c / 4) - 2 * c + (y + y / 4) + (13 * (month + 1) / 5) + day - 1;
    while (week < 0)
    {
        week += 7;
    }
    week %= 7;
    return week;
}

void display(int year, int month)
{
    int day, week_first, i;
    day = month_day[month] + (month == 2 ? is_leep(year) : 0);
    week_first = get_week(year, month, 1);
    printf("    %10s    %d \n", month_name[month - 1], year);
    printf("Su  Mo  Tu  We  Th  Fr  Sa\n");
    for (i = 0; i < week_first; i++) // goto the first day of week
        printf("    ");
    for (i = 1; i <= day; i++)
    {
        printf("%4d", i);
        week_first++;
        if (week_first >= 7) // next weeks
        {
            printf("\n");
            week_first = 0;
        }
    }
}

int main(int argc, char **argv)
{
    if (argc < 1)
    {
        fprintf(stderr, "cal: too few argument!\n");
        return -1;
    }

    int year, month;
    walltime_t wt;
    if (argc == 1) // no argument show current date
    {
        walltime(&wt);
        year = wt.year;
        month = wt.month;
        display(year, month);
    }
    else
    {
        // one argument show assiant date
        char *p = argv[1];
        char *q;
        if (*p == '-')
        {
            p++;
            switch (*p)
            {
            case 'd': // -d option
                if (argc < 3)
                {
                    fprintf(stderr, "cal: -d format error!\n");
                    return -1;
                }

                p = argv[2];        // get year
                q = strchr(p, '-'); // get month
                if (!q)
                {
                    fprintf(stderr, "cal: -d format error! must be year-month!\n");
                    return -1;
                }
                *q++ = '\0';
                // check year
                if (isdigitstr(p))
                {
                    year = atoi(p);
                    if (year < 0)
                    {
                        fprintf(stderr, "cal: year must > 0!\n");
                        return -1;
                    }
                }

                // check month
                if (isdigitstr(q))
                {
                    month = atoi(q);
                    if (month < 1 || month > 12)
                    {
                        fprintf(stderr, "cal: month must in [1-12] \n");
                        return -1;
                    }
                }
                display(year, month);
                break;
            case 'h':
                printf("usage: cal [option] [year-month] ...\n");
                printf("option:\n");
                printf("    -d: get a month of a year");
                printf("    -h: get help\n");
                break;
            default:
                fprintf(stderr, "cal: option error!\n");
                break;
            }
        }
        else
        {
            if (isdigitstr(p))
            {
                year = atoi(p);
                if (year < 0)
                {
                    fprintf(stderr, "cal: year must > 0!\n");
                    return -1;
                }
                for (month = 1; month < 12; month++)
                {
                    display(year, month);
                }
            }
            else
            {
                fprintf(stderr, "cal: year val error!\n");
                return -1;
            }
        }
    }
    return 0;
}