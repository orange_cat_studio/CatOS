#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <environ.h>

int main(int argc, char **argv)
{
    char *p;
    int overwrite = 0;


    if (argc == 1)
    {
        printf("env: too few argument!\n");
        printf("usage: setenv [-o] env_name env_value\n");
        return -1;
    }

    if (argc > 4)
    {
        printf("env: argument error!\n");
        return -1;
    }

    if ((p = argv[1]))
    {
        // option
        if (*p == '-')
        {
            p++;
            switch (*p)
            {
            case 'o': // overwrite old env value
                overwrite = 1;
                printf("[env] update env\n");
                break;
            default:
                break;
            }
        }
    }

    char *name = NULL, *val = NULL;
    if (argc == 3 || argc == 4)
    {
        if (argc == 3)
        {
            // update or create env
            name = argv[1];
            val = argv[2];
        }
        else
        {
            name = argv[2];
            val = argv[3];
        }

        if (getenv(name))
        {
            if (overwrite)
            {
                setenv(name, val, 1);
                printf("env: update env success\n");
            }
            else
            {
                printf("env: env value %s exist!\n", name);
                return -1;
            }
        }
        else
        {
            char *buff;
            buff = (char *)malloc(strlen(name) + strlen(val) + strlen("=") + 2);
            strcpy(buff, name);
            strcat(buff, "=");
            strcat(buff, val);
            putenv(buff, 1);
            free(buff);
            printf("env: set env %s end\n",name);
        }
    }
    else
    {
        char *name = argv[1]; // show assiant env value
        char *val = NULL;
        val = getenv(name);
        printf("%s=%s\n", name, val);
    }
}

