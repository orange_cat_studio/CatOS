#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/status.h>

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        fprintf(stderr, "move: command syntax err!\n");
        return -1;
    }
    if (!strcmp(argv[1], ".") || !strcmp(argv[1], ".."))
    {
        fprintf(stderr, "move: src and dst path error!\n");
        return -1;
    }

    if (!strcmp(argv[1], argv[2]))
    {
        fprintf(stderr, "move: src and dst file same!\n");
        return -1;
    }
    int src_fd = open(argv[1], O_RDONLY);
    if (src_fd < 0)
    {
        fprintf(stderr, "move: src file no exist or permission no enough!\n");
        return -1;
    }
    int dst_fd = open(argv[2], O_WRONLY | O_CREATE | O_TRUNC);
    if (dst_fd < 0)
    {
        fprintf(stderr, "move: create dst file failed!\n");
        close(src_fd);
        return -1;
    }

    status_t fstu;
    if (stat(argv[1], &fstu) < 0)
    {
        fprintf(stderr, "move: get file %s info failed!\n", argv[1]);
        close(src_fd);
        close(dst_fd);
        return -1;
    }

    char *buff = malloc(fstu.st_size);
    if (!buff)
    {
        fprintf(stderr, "mem no enough!\n");
        close(src_fd);
        close(dst_fd);
        return -1;
    }

    char *p = buff;
    int size = fstu.st_size;
    int reads;

    int chunk = (size & 0xffff) - 1; // 64kb

    if (!chunk)
    {
        chunk = 0xffff;
        size -= 0xffff;
    }

    // copy
    while (size > 0)
    {
        reads = read(src_fd, p, chunk);
        if (reads < 0)
            goto failed;
        if (write(dst_fd, p, reads) < 0)
        {
            goto failed;
        }
        p += chunk;
        size -= 0xffff;
        chunk = 0xffff;
    }
    // copy mode
    chmod(argv[2], fstu.st_mode);

    free(buff);
    close(src_fd);
    close(dst_fd);

    if (remove(argv[1]) < 0)
    {
        fprintf(stderr, "move: src file del failed!\n");
        remove(argv[1]);
    }
    return 0;

failed:
    fprintf(stderr, "move: move file %s failed!\n", argv[1]);
    free(buff);
end:
    close(src_fd);
    close(dst_fd);
    return -1;
}