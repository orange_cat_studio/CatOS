#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("rename: command syntax error!\n");
        printf("rename old_name new_name\n");
        return -1;
    }
    //path name cannot be '.' or '..'
    if (!strcmp(argv[1], ".") || !strcmp(argv[1], ".."))
    {
        printf("rename: path can't be '.' or '..'\n");
        return -1;
    }
    if (!strcmp(argv[2], ".") || !strcmp(argv[2], ".."))
    {
        printf("rename: path can't be '.' or '..'\n");
        return -1;
    }
    char srcpath[MAX_PATH_LEN+1];
    char dstpath[MAX_PATH_LEN+1];
    buildpath(argv[1],srcpath);
    buildpath(argv[2],dstpath);
    if (!rename(srcpath, dstpath))
    {
        printf("rename: file %s to %s change ok!\n", argv[1], argv[2]);
        return 0;
    }
    else
    {
        printf("rename: %s to %s failed!\n", argv[1], argv[2]);
        return -1;
    }
    return 0;
}
