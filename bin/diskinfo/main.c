#include <sys/syscall.h>
#include <sys/ioctl.h>
#include <sys/sys.h>
#include <sys/disk.h>
#include <sys/udev.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <const.h>
#include <stddef.h>
#include <ctype.h>

#define DEVICE_PATH "/dev"

disk_t *disk_info[DISK_SOLT_NUM] = {"unknow"};
int disknum = 0;

static uint64_t DiskSize(char *name)
{
    uint64_t size = 0;

    char devname[32];

    memset(devname, 0, 32);
    strcat(devname, "/dev/");
    strcat(devname, name);

    int handle = open(devname, 0);
    if (handle < 0)
        return -1;
    if (ioctl(handle, DISKIO_GETSIZE, &size) < 0)
        return -1;
    close(handle);
    return size;
}

static void DiskAdd(char *name)
{
    disk_t *disk;

    // alloc buffer
    disk = (disk_t *)malloc(sizeof(disk_t));
    if (!disk)
        return -1;
    disk_info[disknum] = disk;
    if (disk_info_get(name, disk) < 0)
    {
        printf("disk info get failed!\n");
        return -1;
    }
    disknum++;
}

static int DiskScan()
{
    devent_t *p=NULL;
    devent_t dev;
    disk_t *disk=NULL;

    p = NULL;
    do
    {
        if (scandev(p, DEVICE_TYPE_DISK, &dev) < 0) // scan disk failed
        {
            break;
        }
        DiskAdd(dev.de_name); // add disk device
        p = &dev;
    } while (p);

    p = NULL;
    do
    {
        if (scandev(p, DEVICE_TYPE_VIRTUAL_DISK, &dev) < 0)
        {
            break;
        }
        DiskAdd(dev.de_name); // add vdisk device
        p = &dev;
    } while (p);

    p = NULL;
    do
    {
        if (scandev(p, DEVICE_TYPE_VOL, &dev) < 0)
        {
            break;
        }
        DiskAdd(dev.de_name);
        p = &dev;
    } while (p);
    return 0;
}

static void Disprintf()
{
    int i;
    disk_t *disk;

    for (i = 0; i < disknum; i++)
    {
        disk = disk_info[i];
        if (disk == NULL)
            break;

        printf("disk %s info:\n", disk->virname);
        printf("disk name: %s \n", disk->devtern.de_name);
        printf("disk id: %d  disk size: %d\n", disk->id, DiskSize(disk->devtern.de_name));
        printf("\n");
    }
}

int main(int argc, char **argv)
{
    if (DiskScan() < 0)
    {
        printf("disk scan failed!\n");
        return -1;
    }

    Disprintf();
    return 0;
}