#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/status.h>

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        fprintf(stderr, "copy: command syntax error!\n");
        return -1;
    }

    if (!strcmp(argv[1], ".") || !strcmp(argv[1], ".."))
    {
        fprintf(stderr, "copy: path err!\n");
        return -1;
    }

    if (!strcmp(argv[1], argv[2]))
    {
        fprintf(stderr, "copy: srd and dst path same,no can copy!\n");
        return -1;
    }

    int src_fd = open(argv[1], O_RDONLY);
    if (src_fd < 0)
    {
        fprintf(stderr, "copy: src file %s no found or perssion no enough!\n", argv[1]);
        return -1;
    }
    int dst_fd = open(argv[2], O_WRONLY | O_CREATE | O_TRUNC);
    if (dst_fd < 0)
    {
        fprintf(stderr, "copy: dst file %s create faild!\n", argv[2]);
        close(src_fd);
        return -1;
    }

    // get file status info
    status_t fstu;
    if (stat(argv[1], &fstu) < 0)
    {
        fprintf(stderr, "copy: src file %s get file info failed!\n", argv[1]);
        close(src_fd);
        close(dst_fd);
        return -1;
    }

    // buffer malloc
    char *buff = malloc(fstu.st_size);
    if (!buff)
    {
        fprintf(stderr, "copy: mem size no enough!\n");
        close(src_fd);
        close(dst_fd);
        return -1;
    }

    char *p = buff;
    int size = fstu.st_size;
    int reads;

    int chunk = (size & 0xffff) + 1; // 64kb

    // chunk is 0,set block size
    if (!chunk)
    {
        chunk = 0xffff;
    }

    // copy data
    while (size > 0)
    {
        reads = read(src_fd, p, chunk);
        if (reads < 0)
            goto failed;
        if (write(dst_fd, p, chunk) < 0)
            goto failed;
        // next block
        p += chunk;
        size -= chunk;
        chunk = 0xffff;
    }

    // copy mode
    chmod(argv[2], fstu.st_mode);

    // free buff
    free(buff);

    close(src_fd);
    close(dst_fd);
    return 0;

failed:
    fprintf(stderr, "copy: copy data failed!\n");
    free(buff);
end:
    close(src_fd);
    close(dst_fd);
    return -1;
}