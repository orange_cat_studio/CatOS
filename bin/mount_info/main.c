#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>


int main()
{
    mount_t *mount=NULL;
    uint32_t total=0;

    total=mount_info(NULL,0);
    if(!total)
    {
        printf("[mount_info] no mount info\n");
        return 0;
    }
    printf("[mount_info] max support %d mount points\n",total);

    mount=malloc(total*sizeof(mount_t));
    if(!mount)
    {
        printf("[mount_info] mem no enough\n");
        return -1;
    }
    uint32_t count=mount_info(mount,total);
    if(count<0)
    {
        printf("[mount_info] get mount point error\n");
        return -1;
    }
    printf("[mount_info] exist mount points count %d\n",count);
    mount_t *mt=NULL;
    for(int i=0;i<count;i++)
    {
        mt=mount+i;
        printf("%d: dev %s mount %s path %s\n",i,mt->devpath,mt->abspath,mt->path);
    }

    return 0;
}
