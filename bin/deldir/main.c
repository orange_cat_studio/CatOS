#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        fprintf(stderr, "rmdir: argument error!\n");
        return -1;
    }
    else
    {
        if (!rmdir(argv[1]))
            printf("rmdir: remove %s success!\n", argv[1]);
        else
        {
            fprintf(stderr, "rmdir: remove %s failed!\n", argv[1]);
            return -1;
        }
    }
    return 0;
}
