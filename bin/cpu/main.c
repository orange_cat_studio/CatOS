#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/udev.h>
#include <sys/ioctl.h>
#include <sys/syscall.h>
#include <sys/sys.h>

#define DEV_PATH "/dev"

static void get_cpuinfo(char *name)
{
    printf("cpu: %s\n", name);
    char path[MAX_PATH_LEN+1];
    memset(path,0,MAX_PATH_LEN+1);
    strcpy(path,"/dev/");
    strcat(path,name);

    int fd = open(path, 0);
    if (fd < 0)
        return -EINVAL;
    char vendor[16];
    memset(vendor,0,16);
    if (ioctl(fd, CPUIO_GETVENDOR, vendor) < 0)
        return -1;
    char brand[16];
    memset(brand,0,16);
    if (ioctl(fd, CPUIO_GETBRAND, brand) < 0)
        return -1;
    char family[32];
    memset(family,0,32);
    if (ioctl(fd, CPUIO_GETFAMILY, family) < 0)
        return -1;
    uint64_t cur_frequery=0;
    if (ioctl(fd, CPUIO_GETCURFREQUERY, &cur_frequery) < 0)
        return -1;
    uint64_t frequery=0;
    if (ioctl(fd, CPUIO_GETFREQUERY, &frequery) < 0)
        return -1;
    uint64_t max_frequery=0;
    if (ioctl(fd, CPUIO_GETMAXFREQUERY, &max_frequery) < 0)
        return -1;

    printf("vendor:%s family:%s\n", vendor, family);
    printf("brand:%s\n",brand);
    printf("cur frequery:%d frequery:%d max_frequery:%d\n",(uint32_t)cur_frequery,(uint32_t)frequery,(uint32_t)max_frequery);
}

void cpuinfo_scan()
{
    devent_t *p;
    devent_t dev;
    uint8_t num = 0;

    p = NULL;
    do
    {
        if (scandev(p, DEVICE_TYPE_PROCESS, &dev) < 0)
            return;
        num++;
        get_cpuinfo(dev.de_name);
        p=&dev;
    } while (p);
    printf("cpu num: %d\n", num);
}

void main()
{
    printf("----CPU INFO VIEW------\n");
    cpuinfo_scan();
}