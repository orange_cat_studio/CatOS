#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<getopt.h>
#include<sys/ioctl.h>

#define VERSION_STR "0.1"

static void printf_usage()
{
    printf("loinfo [-a|h|v] [loop device]\n");
    printf("loinfo -a: show all loop\n");
    printf("loinfo loop device: show targe loop device\n");
    printf("verison %s\n",VERSION_STR);
}

static int loop_info(char *loop)
{
    char buff[MAX_PATH_LEN];
    memset(buff,0,MAX_PATH_LEN+1);
    strcpy(buff,"/dev/");
    strcat(buff,loop);

    int handle=open(buff,O_RDONLY);
    if(handle<0)
        return -1;

    printf("loinfo: loop %s open ok\n",loop);

    uint32_t sector=0;
    if(ioctl(handle,DISKIO_GETSIZE,&sector)<0)
    {
        printf("loop device info ioctl failed\n");
        return -1;
    }   
    printf("loop %s sectors %d\n",loop,sector);
    close(handle);
}

int main(int argc,char **argv)
{
    if(argc<2)
    {
        printf("[loinfo] no argument");
        printf_usage();
        return 0;
    }
    int res=0;
    int all=0;
    while((res=getopt(argc,argv,"ha"))!=-1)
    {
        switch (res)
        {
        case 'h':
            printf_usage();
            return 0;
            break;
        case 'a':
            all=1;
            break;
        default:
            break;
        }
    }

    char *loop=NULL;

    if(argv[optind])
    {
        loop=argv[optind];
    }

    if(all==1)
    {
        printf("no suport\n");
    }
    else 
    {
        if(loop==NULL)
        {
            printf("loinfo: no input device name\n");
            return -1;
        }
        loop_info(loop); //get loop devcie info
    }   
}
