#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <unistd.h>
#include <getopt.h>

static void print_usage()
{
    printf("Usage: mkfs [-t fstype] device\n");
    printf("Option: \n");
    printf("-t filesystem type\n");
    printf("support: fat12,fat16,fat32\n");
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        print_usage();
        return -1;
    }
    int result=-1;
    char *arg_device=NULL;
    char *arg_fs=NULL;
    int mkfs_flags = 0;
    int info_visiable = 0;

    while ((result = getopt(argc, argv, "hvt:"))!=-1)
    {
        switch (result)
        {
        case 'h':
            print_usage();
            return 0;
            break;
        case 't':
            arg_fs = optarg;
            break;
        case 'v':
            info_visiable = 1;
            break;
        case '?':
            if (optopt == 't')
            {
                fprintf(stderr, "mkfs: no file system type!\n");
            }
            else
            {
                fprintf(stderr, "mkfs: unknown option!\n");
            }
            return -1;
        default:
            break;
        }
    }

    if (argv[optind])
    {
        arg_device = argv[optind];
    }

    if (!arg_device)
    {
        fprintf(stderr, "mkfs: device error!\n");
        return -1;
    }
    //auto switch fs
    if (!arg_fs)
        arg_fs = "auto";

    printf("mkfs on device %s fstype %s\n",arg_device,arg_fs);
    if (mkfs(arg_device, arg_fs, mkfs_flags) < 0)
    {
        fprintf(stderr, "mkfs: device:%s file sytem:%s failed!\n", arg_device, arg_fs);
        return -1;
    }
    if (info_visiable)
    {
        printf("mkfs: make filesystem %s on disk %s success!\n", arg_fs, arg_device);
    }
    return 0;
}
