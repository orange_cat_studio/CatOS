#include <environ.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(int argc, char **argv)
{
    if (argc > 2)
    {
        fprintf(stderr, "[unsetenv] too much argument!\n");
        return -1;
    }

    char *name = argv[1];
    char *p=name;
    while (*p) //env name invalid
    {
        if (*p == '=')
        {
            fprintf(stderr, "[unsetenv] env name %s error!\n");
            return -1;
        }
        p++;
    }
    int ret= unsetenv(name);
    if(ret<0)
    {
        printf("unsetenv: unable to remove %s\n",name);
    }
    printf("unsetenv: operator ok\n");
}
