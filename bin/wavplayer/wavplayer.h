#ifndef __WAVPLAYER_H
#define __WAVPLAYER_H

typedef struct riff_block
{
    char riff[4]; //"riff"
    uint32_t riffsize;
    char wave[4]; //"wave"
} riff_block_t;

typedef struct fmt_block
{
    char fmt[4]; //"fmt"
    uint32_t fmtsize;
    uint16_t formatTag;     // encode foramt (1:PCM)
    uint16_t channels;      // channels (1: single channels 2: two channels)
    uint32_t samplesPerSec; // sample per sec
    uint32_t bytesPerSec;   // transmit bytes per6 sec(channels*samplesPerSec*bitPerSample/8)
    uint16_t blockAlign;    // bytes per sample(channels*bitsPerSample/8)
    uint16_t bitsPerSample; // bits per sample
} fmt_block_t;

typedef struct fact_block
{
    char fact[4];
    uint32_t factSize;
    uint32_t factData;
} fact_block_t;

typedef struct data_block
{
    char data[4];
    uint32_t dataSize;
} data_block_t;

typedef struct file_header
{
    riff_block_t riff_block;
    fmt_block_t fmt_block;
    //fact_block_t fact_block;
    data_block_t data_block;
} file_header_t;

#define SOUND_DEVICE "/dev/sb16"

#define WAVE_SIZE (1024 * 32)
#define BUFF_SIZE (WAVE_SIZE * 1)

#endif