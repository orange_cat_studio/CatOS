#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>
#include <sys/arch.h>
#include <sys/ioctl.h>
#include <sys/status.h>
#include "wavplayer.h"

int sound_fd = -1;

int read_wav_file(const char *file)
{
    file_header_t file_header;
    FILE *fp = fopen(file, "rb");
    if (!fp)
    {
        printf("wav file open err\n");
        return -1;
    }

    int play_size = 0;

    // read file header
    fread(&file_header, sizeof(file_header_t), 1, fp);

    if (strncmp(file_header.riff_block.riff, "RIFF", 4))
    {
        fprintf(stderr, "wav file invalid format!\n");
        goto exit;
    }

    int channels = file_header.fmt_block.channels;
    int blocksize = file_header.fmt_block.blockAlign;
    int samplePerSec = file_header.fmt_block.samplesPerSec;
    int bytesPerSec = file_header.fmt_block.bytesPerSec;
    int bitsPerSample = file_header.fmt_block.bitsPerSample;
    int data_size = file_header.data_block.dataSize;
    int total_time = data_size / bytesPerSec;
    int free_time = total_time;
    int samples = data_size / blocksize;

    if ((file_header.fmt_block.bytesPerSec != channels * samplePerSec * bitsPerSample / 8) || (file_header.fmt_block.blockAlign != channels * bitsPerSample / 8))
    {
        printf("wav file format error!\n");
        return -1;
    }

    if (file_header.fmt_block.formatTag != 1) // no PCM encode
    {
        printf("wav no support no PCM encode!\n");
        return -1;
    }

    printf("fmtsize: %d\n", file_header.fmt_block.fmtsize);
    printf("ecode: %d\n", file_header.fmt_block.formatTag);
    printf("samplePerSec: %d\n", file_header.fmt_block.samplesPerSec);
    printf("bytesPerSec: %d\n", file_header.fmt_block.bytesPerSec);
    printf("blocksize: %d\n", file_header.fmt_block.blockAlign);
    printf("channels: %d\n", file_header.fmt_block.channels);
    printf("file size: %d\n", file_header.riff_block.riffsize + 8);
    printf("samples: %d\n",samples);
    printf("total time:%d free time: %d\n", total_time, free_time);

    if (channels == 1)
    {
        while (1)
        {
        }
    }
    else
    {
        if (channels == 2)
        {
            while (1)
            {
            }
        }
    }

exit:
    printf("wavplayer: play size %d\n", play_size);
    fclose(fp);
    return 0;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("[wavplayer] please assign wave file\n");
        return -1;
    }
    sound_fd = open(SOUND_DEVICE, 0);
    if (sound_fd < 0)
    {
        printf("[wavplayer] open sound device err\n");
        return -1;
    }
    if (read_wav_file(argv[1]) < 0)
    {
        printf("[wavplayer] play err\n");
    }
    close(sound_fd);
    return 0;
}
