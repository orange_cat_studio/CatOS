//file: userapp/data/date.c
//autor:jiangxinpneg
//time:2021.12.27
//copyright:(C) by jiangxinpeng,All right are reserved.

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#include <sys/walltime.h>

int main(int argc, char **argv)
{
    tm_t tm;

    if (argc != 1)
    {
        fprintf(stderr, "date: no support argument!\n");
        return -1;
    }
    walltime_t wt;
    walltime(&wt);
    walltime_switch(&wt, &tm);
    printf("%s\n", asctime(&tm));

    return 0;
}


