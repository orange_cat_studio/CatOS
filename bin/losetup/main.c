#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/ioctl.h>
#include <sys/udev.h>

void printf_usage()
{
    printf("losetup [-d] [device] [file]\n");
    printf("   -d:  delete file link to device\n");
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        printf_usage();
        return -1;
    }
    int result;
    int *arg_device = NULL;
    int *arg_file = NULL;
    int is_delete_device = 0;

    opterr = 0;

    while ((result = getopt(argc, argv, "hd")) != -1)
    {
        switch (result)
        {
        case 'h':
            printf_usage();
            return 0;
        case 'd':
            is_delete_device = 1;
            break;
        case '?':
            fprintf(stderr, "losetup: unknown option '%c'\n", optopt);
            return -1;
        default:
            fprintf(stderr, "losetup: option error!\n");
            return -1;
        }
    }

    if(argv[optind])
    {
        arg_device=argv[optind];    //device
        optind++;
        if(argv[optind]) //file
        {
            arg_file=argv[optind];
        }
    }
            
    if(arg_device==NULL)
    {
        fprintf(stderr,"losetup: no device");
        return -1;
    }

    int fd=open(arg_device,O_RDWR);
    if(fd<0)
    {
        fprintf(stderr,"losetup: device %s not exist or no permission to access!\n",arg_device);
        return -1;
    }

    if(!is_delete_device)   //no delete device
    {
        if(arg_file==NULL)
        {
            fprintf(stderr,"losetup: no file!\n");
            close(fd);
            return -1;
        }
        if(access(arg_file,F_OK)<0)
        {
            fprintf(stderr,"losetup: file %s no exist or no permission to access!\n",arg_file);
            close(fd);
            return -1;
        }

        if(ioctl(fd,DISKIO_SETUP,arg_file)<0)
        {
            fprintf(stderr,"losetup: set device %s with file %s failed!\n",arg_device,arg_file);
            close(fd);
            return -1;
        }
        printf("losetup: set device %s with file %s success!\n",arg_device,arg_file);
    }
    else 
    {
        //delete device
        if(ioctl(fd,DISKIO_SETDOWN,NULL)<0)
        {
            fprintf(stderr,"losetup: device %s set down error!\n",arg_device);
            close(fd);
            return -1;
        }
        printf("losetup: delete device %s success!\n",arg_device);
    }

    close(fd);
    return 0;
}
