#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    if (argc != 2)
        fprintf(stderr, "rm: no argument support!\n");
    else
    {
        if (!remove(argv[1]))
        {
            printf("rm: delete %s ok!\n", argv[1]);
            return 0;
        }
        else
        {
            fprintf(stderr, "rm: delete %s ok!\n", argv[1]);
            return -1;
        }
    }
    return -1;
}
