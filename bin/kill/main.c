#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <ctype.h>
#include <string.h>

#define MAX_SUPPORT_SIG_NUM 32

// signal table
char *signal_table[MAX_SUPPORT_SIG_NUM][2] = {
    {"NULL", "NULL"},
    {"SIGUSR1", "USR1"},
    {"SIGINT", "INT"},
    {"SIGKILL", "KILL"},
    {"SIGTRAP", "TRAP"},
    {"SIGABRT", "ABRT"},
    {"SIGBUS", "BUS"},
    {"SIGSEGV", "SEGV"},
    {"SIGFPE", "FPE"},
    {"SIGILL", "ILL"},
    {"SIGPIPE", "PIPE"},
    {"SIGSTKFLT", "STKFLT"},
    {"SIGALRM", "ALRM"},
    {"SIGTERM", "TERM"},
    {"SIGCOUNT", "CONT"},
    {"SIGSTOP", "STOP"},
    {"SIGTTIN", "TTIN"},
    {"SIGTTOUT", "TTOUT"},
    {"SIGSYS", "SYS"},
    {"SIGIO", "IO"},
    {"SIGHUP", "HUP"},
    {"SIGHUP", "WINCH"},
    {"SIGVTALRM", "VTALRM"},
    {"SIGPROF", "PROF"},
    {"SIGQUIT", "QUIT"},
};

// find signal
int find_signal_in_table(char *name)
{
    int idx=0;
    int signo = -1;

    for (idx = 0; idx < MAX_SUPPORT_SIG_NUM; idx++)
    {
        if (!strcmp(signal_table[idx][0], name) || !strcmp(signal_table[idx][1], name))
        {
            signo = idx;
            break;
        }
    }

    if (signo != -1)
        return signo;
    return 0;
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("usage: kill [-l [siname] | -signo | -siname] [pid]\n");
        return;
    }

    int signo = SIGTERM; // default send SIGTERM
    int pid = 0;
    char *p = NULL;
    // no list signal
    bool list_signal = false;
    // whether had argument
    bool has_option = false;
    bool pid_negative = false;

    int idx = 1;

    while (idx < argc)
    {
        if (argv[idx][0] == '-' && argv[idx][1] != '\0' && idx == 1) // had options
        {
            has_option = true;
            p = (char *)(argv[idx] + 1);

            if (*p == 'l' && p[1] == '\0') // -l option
            {
                list_signal = true;
            }
            else
            {
                // signal value
                if (isdigitstr((const char *)p)) // translate signo to value
                {
                    signo = atoi(p);
                    if (signo < 1 || signo >= MAX_SUPPORT_SIG_NUM)
                    {
                        printf("kill: signal %s[%d] no support!\n", p, signo);
                        return -1;
                    }
                }
                else
                {
                    // signal string
                    signo = find_signal_in_table(p);
                    if (signo <= 0)
                    {
                        printf("kill: signal %s not found!\n", p);
                        return -1;
                    }
                }
            }
        }
        else
        {
            // process pid
            if (argv[idx][0] && idx == 1)
            {
                p = (char *)argv[idx];
                if (*p == '-') // negative
                {
                    pid_negative = true;
                    p++;
                }
                if (isdigitstr((const char *)p))
                {
                    pid = atoi(p);
                    if (pid_negative)
                        pid = -pid;
                }
                else
                {
                    printf("kill: process id %s error", p);
                    return -1;
                }
            }
            else
            {
                // two argument is process pid
                if (argv[idx][0] && idx == 2)
                {
                    p = (char *)argv[idx];

                    if (list_signal) // has option
                    {
                        if (isdigitstr(p))
                            signo = atoi(p);
                        // find signo number by name string
                        signo = find_signal_in_table(p);
                        if (signo <= 0)
                        {
                            printf("kill: signal %s not found!\n");
                            return -1;
                        }
                    }
                    else
                    {
                        if (*p == '-')
                        {
                            pid_negative = true;
                            p++;
                        }
                        if (isdigitstr((const char *)p))
                        {
                            pid = atoi(p);
                            if (pid_negative)
                                pid = -pid;
                        }
                        else
                        {
                            printf("kill: process id %s must be number!\n", p);
                            return -1;
                        }
                    }
                }
            }
        }
        idx++;
    }

    if (has_option) // had option
    {
        if (list_signal) // list siganal
        {
            if (argc == 2)
            {
                // list all signal
                printf("all support signal list\n");
                printf(" 1) SIGUSR1      2) SIGINT       3) SIGKILL      4) SIGTRAP      5) SIGABRT     \n");
                printf(" 6) SIGBUS       7) SIGSEGV      8) SIGFPE       9) SIGKILL     10) SIGPIPE     \n");
                printf("11) SIGSTKFLT   12) SIGALRM     13) SIGTERM     14) SIGCHLD     15) SIGCONT     \n");
                printf("16) SIGSTOP     17) SIGTTIN     18) SIGTTOU     19) SIGSYS      20) SIGIO       \n");
                printf("21) SIGHUP      22) SIGWINCH    23) SIGVTALRM   24) SIGPROF     25) SIGQUIT     \n");
            }
            else
            {
                // direct list signal value
                printf("%d", signo);
            }
        }
        else
        {
            if (argc == 2)
            {
                printf("kill: please order proccess id!\n");
                return -1;
            }
            else
            {
                printf("send signo %d to pid %d\n", signo, pid);
                if (kill(pid, signo) < 0) // kill process
                {
                    printf("kill: kill pid %d failed", pid);
                    return -1;
                }
            }
        }
    }
    else
    {
        // no options
        printf("send signo %d to pid %d\n", signo, pid);
        if (kill(pid, signo) < 0) // kill process
        {
            printf("kill: kill pid %d failed!\n", pid);
            return -1;
        }
    }
    return 0;
}