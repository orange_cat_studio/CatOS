#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/sys.h>
#include <sys/time.h>


static void readline(char *buff, uint32_t count)
{
    int len = 0;
    char *pos = buff;

    while (len < count)
    {
        if (read(STDIN_FILENO, pos, 1) < 0)
            continue;

        if (*pos == '\n') // next line
        {
            *pos = '\0';
            break;
        }
        else
        {
            if (*pos == '\b') // backspace
            {
                *pos = '\0';
                // had data just del a front char
                if (pos > buff)
                {
                    *(--pos) = '\0';
                    len--;
                }
                else
                {
                    len = 0;
                }
            }
            else
            {
                // next char
                len++;
                pos++;
            }
        }
    }
    *pos='\0';
}

int main(int argc, char **argv)
{
    if (argc > 2)
    {
        printf("chpwd: too many argument!\n");
        return -1;
    }

    char *name;
    char password_old[64];
    char password_new[64];
    char cur_name[32];

    // get current user name
    if (account_name(cur_name, 32) < 0)
    {
        printf("chpwd: no login any user!\n");
        return -1;
    }

    // no admin user cannot to change password
    if (strcmp(cur_name, "admin") != 0)
    {
        printf("chpwd: no permission to product operator!\n");
        return -1;
    }

    // get username
    if (argc == 1)
    {
        char buff[32];
        printf("username:");
        gets(buff);
        name = buff;
    }
    else
    {
        name = argv[1];
        uint32_t len=strlen(name);
    }

    // get password
    printf("input old password:");
    readline(password_old,64);
    printf("input new password:");
    readline(password_new,64);

    // change password
    if (account_chpwd(name, password_old, password_new) < 0)
    {
        printf("chage user %s password failed, please check source password is valid!\n", name);
        return -1;
    }
    printf("chpwd: update %s passwd ok\n", name);
    return 0;
}