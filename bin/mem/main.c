#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/vmm.h>
#include <const.h>

int main(int argc, char **argv)
{
    if (argc > 1)
    {
        fprintf(stderr, "mem: no arguments support!\n");
        return -1;
    }
    mstatus_t mem;
    mstate(&mem);
    printf("         TOTAL           USED          FREE\n");
    printf("%14uG%14uG%14uG\n", (uint32_t)mem.mst_total / GB, (uint32_t)mem.mst_used / GB, (uint32_t)mem.mst_free / GB);
    printf("%14uM%14uM%14uM\n", (uint32_t)mem.mst_total / MB, (uint32_t)mem.mst_used / MB, (uint32_t)mem.mst_free / MB);
    return 0;
}