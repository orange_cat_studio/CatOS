# env var
ENV_CFLAGS	:= -march=i386 -fno-builtin -w -fno-PIE -m32 -std=gnu99 -fno-stack-protector -fno-omit-frame-pointer -fno-strict-aliasing -g 

# kernel name & version
ENV_CFLAGS	+= -DKERNEL_NAME=\"CATOS\" -DKERNEL_VERSION=\"0.1.9\"

ENV_AFLAGS	:= -f elf -g
ENV_LDFLAGS	:= -no-pie -g

ENV_USER_LD_SCRIPT	:= -T ../libs/libc/arch/x86/user.ld

# MacOS special
ifeq ($(shell uname),Darwin)
	ENV_LD		:=  i386-elf-ld -m elf_i386
else
	ENV_LD		:=  ld -m elf_i386
endif

ENV_AS		:= nasm