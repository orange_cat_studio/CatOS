#!/bin/bash
echo [qemu-img] create vhd file
qemu-img create -f vpc -o size=512M ./develop/img/CatOS.vhd
echo [modprobe]enable nbd mod
modprobe nbd
echo [qemu-nbd]conect vhd image to nbd0
qemu-nbd -f vpc -c /dev/nbd0  ./develop/img/CatOS.vhd   
echo [fdisk]create primary on /dev/nbd0
fdisk /dev/nbd0 << EOF
n
p
1


w
EOF
echo [mkfs]install fat32 filesystem to vdisk
mkfs.fat -F 32 /dev/nbd0p1 -n CatOS 
echo [mount]mount /dev/nbd0p1 to /mnt/
mount -w /dev/nbd0p1 /mnt
