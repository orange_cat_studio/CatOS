#ifndef __LIBC_LOCALE_H
    #define __LIBC_LOCALE_H

    #ifdef __cplusplus
    extern "C"{
    #endif

    char *setlocale(int category,const char *locale);

    #ifdef __cplusplus
    }
    #endif
#endif