#ifndef __LIBC_CTYPE_H
    #define __LIBC_CTYPE_H

    #ifdef __cplusplus
    extern "C" {
    #endif 

    int isspace(char ch);
    int isalnum(char ch);
    int isdigit(char ch);
    int isxdight(char ch);
    int isalpha(char ch);
    int tolower(int c);
    int toupper(int c);
    int isdigitstr(const char *str);
    int isgraph(int ch);
    int islower(int ch);
    int isupper(int ch);
    int ispunct(int ch);
    int isprint(int ch);

    #ifdef __cplusplus
    }
    #endif
#endif