#ifndef __LIBC_ENVIRON_H
#define __LIBC_ENVIRON_H

#include <types.h>

int clearenv();
char *getenv(const char *name);
int putenv(const char *str, int overwrite);
int setenv(const char *name, const char *val, int overwrite);
int unsetenv(const char *name);

#endif