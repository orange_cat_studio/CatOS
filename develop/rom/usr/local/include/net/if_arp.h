#ifndef __LIBC_NET_IF_ARP_H
#define __LIBC_NET_IF_ARP_H

#ifdef __cplusplus
extern "C" {
#endif

#define ARPHDR_ETHER     1  //ehernet 10Mbps
#define ARPHDR_LOOPBACK 2  //lookback device

#ifdef __cplusplus
}
#endif 
#endif 