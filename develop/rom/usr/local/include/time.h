#ifndef __LIBC_TIME_H
    #define __LIBC_TIME_H

    #ifdef __cplusplus
    extern "C"
    {
    #endif

    #include <types.h>
    #include <sys/time.h>
    #include <sys/walltime.h>

    clock_t clock();
    char *asctime(const tm_t *tm);
    char *ctime(const char *t);
    tm_t *gmtime(const time_t *time);
    time_t mktime(tm_t *tm);
    double difftime(time_t t1, time_t t0);
    tm_t *localtime(const time_t *t);
    int64_t __tm_to_secs(const tm_t *tm);
    int __secs_to_tm(int64_t t, tm_t *tm);

    #ifdef __cplusplus
    }
    #endif
#endif