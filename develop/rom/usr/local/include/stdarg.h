//file:libc/include/stdarg.h
//autor:jiangxinpeng
//time:2021.8.14
//copyright:(C) by jiangxinpeng,All right are reserved.

#ifndef __LIBC_STDARG_H
#define __LIBC_STDARG_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <string.h>

    typedef char *va_list;

#define va_start(arg, format) (void)(arg = (void *)(&(format)) + sizeof((format)))
#define va_arg(arg, type) (*(type *)(((arg) += sizeof(type)) - sizeof(type)))
#define va_end(arg) (arg = (void *)0)
//copy variable args
#define va_copy(d, s) __builtin_va_copy(d, s)

#ifdef __cplusplus
}
#endif
#endif