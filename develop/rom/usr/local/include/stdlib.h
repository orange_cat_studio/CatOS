//file: libs/libc/include/stdlib.h
//autor: jiangxinpeng
//time: 2021.12.24
//copyright: (C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef __LIBC_STDLIB_H
#define __LIBC_STDLIB_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "stddef.h"
#include "stdint.h"
#include "types.h"
#include "exit.h"


//rand max
#define RAND_MAX 0x7FFF

    typedef struct
    {
        int quot;
        int rem;
    } div_t;

    typedef struct
    {
        int64_t quot;
        int64_t rem;
    } ldiv_t;

    typedef struct
    {
        int64_t quot;
        int64_t rem;
    } lldiv_t;

    div_t div(int num, int den);
    ldiv_t ldiv(int64_t num, int64_t den);
    lldiv_t lldiv(int64_t num, int64_t den);
    int srand(uint64_t seed);
    int rand();
    int srandom(uint64_t seed);
    int random();
    int atoi(const char *nptr);
    int64_t atol(const char *nptr);
    int64_t atoll(const char *nptr);
    void atexit(void (*func)(void));
    void __atexit_callback();
    void qsort(void *aa, size_t n, size_t es, int (*cmp)(const void *, const void *));
    char *realpath(const char *path, char *resolved_path);
    intmax_t strntoimax(const char *nptr, char **endptr, int base, size_t n);
    uintmax_t strntoumax(const char *nptr, char **endptr, int base, size_t n);
    double strtod(const char *nptr, char **endptr);
    float strtof(const char *nptr, char **endptr);
    intmax_t strtol(const char *nptr, char **endptr, int base);
    long long strtoll(const char *nptr, char **endptr, int base);
    void abort(void);

    void *malloc(size_t size);
    void free(void *ptr);
    void *realloc(void *oldp, size_t size);
    void *memalign(size_t boundary, size_t size);
    void *calloc(int num, size_t size);

    static inline int abs(int i)
    {
        return i > 0 ? i : -1;
    }

#ifdef __cplusplus
}
#endif
#endif