#ifndef _LIBC_FCNTL_H
    #define _LIBC_FCNTL_H

    #ifdef __cplusplus
    extern "C" {
    #endif

    #ifndef F_DUPFD 
    #define F_DUPFD 0
    #endif

    #ifndef F_GETFD 
    #define F_GETFD 1
    #endif

    #ifndef F_SETFD
    #define F_SETFD 2
    #endif

    #ifndef F_GETFL
    #define F_GETFL 3
    #endif

    #ifndef  F_SETFL 
    #define F_SETFL 4
    #endif

    #ifndef FD_NCLOSEEEXC 
    #define FD_NCLOSEEXEC 0
    #endif

    #ifndef FD_CLOSEEXEC 
    #define FD_CLOSEEXEC 1
    #endif

    //file open
    #define O_RDONLY    0x01
    #define O_WRONLY    0x02
    #define O_RDWR      0x04
    #define O_CREATE     0x08
    #define O_TRUNC     0x10
    #define O_APPEND    0x20
    #define O_EXEC      0x40
    #define O_TEXT      0x80
    #define O_BINARY    0x100
    #define O_NONBLOCK  0x200

    int fcntl(int fd,int cmd,...);
    
    #ifdef __cplusplus
    }
    #endif

#endif