#ifndef __LIBC_NETDB_H
#define __LIBC_NETDB_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <sys/socket.h>

    typedef struct hostent
    {
        char *h_name;       // host name
        char **h_aliases;   // host aliased list
        int h_addrtype;     // address type
        int h_length;       // address length
        char **h_addr_list; // host address list
    } hostent_t;

    typedef struct servent
    {
        char *s_name;     // server name
        char **s_aliases; // server aliases
        int s_port;       // server port
        char *s_proto;    // protocol type
    } servent_t;

    typedef struct addrinfo
    {
        int ai_flags;             // address flags
        int ai_family;            // address family
        int ai_socktype;          // server type(SOCK_STREAM or SCOK_DGRAM)
        int ai_protocol;          // adress protocol
        socklen_t ai_addrlen;     // socket address len
        char *ai_canonname;       // host canonname
        sockaddr_t *ai_addr;      // socket address
        struct addrinfo *ai_next; // point to next addrinfo
    } addrinfo_t;

#ifdef __cplusplus
}
#endif

#endif