//file:lib\string.h
//autor:jiang xinpeng
//time:2020.12.21
//copyright:(C) 2020-2050 by jiang xinpeng,All right are reserved.

#ifndef __LIBC_STRING_H
    #define __LIBC_STRING_H

    #ifdef __cplusplus
    extern "C"
    {
    #endif

    #include "types.h"

    #define STRING_LEN_MAX 128

    int strcmp(const char *src, const char *targe);
    char *strcat(char *targe, const char *src);
    char *strstr(const char *targe, const char *src);
    char *strcpy(char *targe, const char *src);
    char *strchr(const char *targe, char src);
    char *strrchr(const char *str, char c);
    int strlen(const char *str);
    int strpos(const char *str, char ch);
    char *strncat(char *dst, const char *src, int n);
    char *strncpy(char *_dst, const char *_src, int n);
    int strncmp(const char *s1, const char *s2, int n);
    char *strdup(const char *s);
    int strcasecmp(const char *s1, const char *s2);

    void *memset(void *src, uint8_t value, uint32_t size);
    void *memcpy(void *targe, const void *src, uint32_t size);
    int memcmp(const void *src, const void *targe, uint32_t len);

    void *memchr(const void *s, int c, size_t n);

    //cut all sapce from right
    static void strrtrim(char *str)
    {
        char *p = str + strlen(str) - 1;
        while (*p == ' ')
        {
            *p = '\0';
            p--;
        }
    }

    //cut all space from left
    static void strtrim(char *str)
    {
        char *p = str;

        while (*p != '\0')
        {
            *str++ = *p++;
        }
        *str = '\0';
    }

    #ifdef __cplusplus
    }
    #endif
#endif