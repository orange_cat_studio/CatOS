//file: libs/libc/include/malloc.h
//autor: jiangxinpeng
//time: 2021.12.31
//copyright: (C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef __LIBC_MALLOC_H
    #define __LIBC_MALLOC_H

    #ifdef __cplusplus
    extern "C"{
    #endif

    #include<types.h>

    void *realloc(void *oldp, size_t size);
    void *malloc(size_t size);
    void free(void *ptr);
    void *memalign(size_t boundary, size_t size);
    void *calloc(int num, size_t size);
    
    #ifdef __cplusplus
    }
    #endif
#endif