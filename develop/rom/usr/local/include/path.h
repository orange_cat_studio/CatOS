#ifndef __LIBC_PATH_H
    #define __LIBC_PATH_H

    #ifdef __cplusplus
    extern "C"{
    #endif


    char *dirname(char *path);
    char *basename(char *path);


    #ifdef __cplusplus
    }
    #endif

#endif