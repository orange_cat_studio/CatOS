#ifndef SYS_EXCEPTION_H
    #define SYS_EXCPTION_H

    #include <types.h>

    typedef void (*exception_handler_t)();

    enum exc_code
    {
        EXC_CODE_UNKNOW = 0,
        EXC_CODE_USER,
        EXC_CODE_INT,
        EXC_CODE_ILL,      //invalid instruction
        EXC_CODE_TRAP,     //tracer/breakpoint trap
        EXC_CODE_ABORT,    //stoping process
        EXC_CODE_BUS,      //mem apccess error
        EXC_CODE_SEGV,     //invaild mem reference
        EXC_CODE_FPE,      //fpu error
        EXC_CODE_FINALHIT, //
        EXC_CODE_PIPE,     //pipe close
        EXC_CODE_STKFLT,   //coprocess stack fault
        EXC_CODE_ALRM,     //real timer expired
        EXC_CODE_TERM,     //end proccess
        EXC_CODE_CHLD,     //abort or stopping sub process
        EXC_CODE_CONT,     //if stopping just continue
        EXC_CODE_STOP,     //assert stopping
        EXC_CODE_TTIN,     //terminal in
        EXC_CODE_TTOU,     //terminal write
        EXC_CODE_SYS,      //invalid syscall
        EXC_CODE_DEVICE,   //device error
        EXC_CODE_MAX       //exc code max
    };

    int excsend(pid_t pid, uint32_t code);
    int excraise(uint32_t code);
    int excmask(uint32_t mask);
    int exccatch(uint32_t code, exception_handler_t handler);
    int excblock(uint32_t code);
    int excunblock(uint32_t code);
    void *exchandler(uint32_t code);
    static int __excblock(uint32_t code, uint32_t status);
#endif