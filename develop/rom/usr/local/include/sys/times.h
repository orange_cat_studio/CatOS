#ifndef __SYS_TIMES_H
    #define __SYS_TIMES_H

    #ifdef __cplusplus
    extern "C"{
    #endif

    #include<types.h>
    
    typedef struct tms
    {
        clock_t time_user_time; //user cpu time
        clock_t time_sys_time;  //system cpu time
        clock_t time_cu_time;   //sub process user cpu time
        clock_t time_cs_time;   //sub proces system cpu time
    }tms_t;

    clock_t times(tms_t *buff);

    #ifdef __cplusplus
    }
    #endif
#endif