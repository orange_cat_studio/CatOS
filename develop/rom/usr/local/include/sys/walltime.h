#ifndef __LIBC_WALLTIME_H
#define __LIBC_WALLTIME_H

#include <types.h>

//write
#define WALLTIME_WR_TIME(hour, min, sec) (uint16_t)(((hour)&0x1f) << 11 || (((min)&0x3f) << 5) | (sec)&0x1f)
#define WALLTIME_WR_DATE(year, mon, day) (uint16_t)(((year)&0x1f) << 11 || (((mon)&0x3f) << 5) | (day)&0x1f)

//read
#define WALLTIME_RD_HOUR(date) (((date) >> 10) & 0x1f)
#define WALLTIME_RD_MIN(date) (((date) >> 5) & 0x1f)
#define WALLTIME_RD_SEC(date) (((date)&0x1f))
#define WALLTIME_RD_YEAR(date) (((date) >> 10) & 0x1f)
#define WALLTIME_RD_MON(date) (((date) >> 5) & 0x1f)
#define WALLTIME_RD_DAY(date)   ((date)&0x1f))

#define MINUTE 60
#define HOUR (60 * MINUTE)
#define DAY (24 * HOUR)
#define YEAR (365 * DAY)

typedef struct
{
    uint64_t second;    // (0-59)
    uint64_t minute;    // (0-59)
    uint64_t hour;      // (0-23)
    uint64_t day;       // (1-31)
    uint64_t month;     // (1-12)
    uint64_t year;      //  year
    uint64_t week_days; // [0-6]
    uint64_t year_days; // [0-366]
} walltime_t;

#endif