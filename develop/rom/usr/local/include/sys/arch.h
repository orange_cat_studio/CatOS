#ifndef __SYS_ARCH_H
#define __SYS_ARCH_H

#define __ARCH_X86 0
#define __ARCH_X64 1

//system arch micro 
#define __ARCH __ARCH_X86

#define __ARCH_X86_STR "x86"
#define __ARCH_X64_STR "X86_64"
#endif