#ifndef __SYS_SPINLOCK_H
    #define __SYS_SPINLOCK_H

    #ifdef __cplusplus
    extern "C"{
    #endif

    typedef struct 
    {
        volatile int lock;
    }spinlock_t;

    #define __SPIN_COUNT 10

    int spin_lock_init(spinlock_t *lock);
    int spin_lock(spinlock_t *lock);
    int spin_trylock(spinlock_t *lock);
    int spin_unlock(spinlock_t *lock);
    int spin_is_lock(spinlock_t *lock);

    #ifdef __cplusplus
    }
    #endif
#endif