#ifndef __SYS_DISK_H
#define __SYS_DISK_H

#include <sys/udev.h>
#include<sys/list.h>
#include<arch/atomic.h>

//max disk solt num
#define DISK_SOLT_NUM 10
//partition support max 
#define DISK_PARITION_MAX 256

//partition info (logical disk)
typedef struct
{
    int id;                        //partition id
    char virname[DEVICE_NAME_LEN+1]; //partition name
    devent_t devtern;              //device entry info
    int disk_solt;                 //disk solt
    int solt;                      //partition solt
    int handle;                    //handle
    atomic_t ref;                  //reference
    list_t list;                   //parition list
} partition_t;

//disk info
typedef struct
{
    list_t list;                               //disk list
    int id;                                    //disk id
    char virname[DEVICE_NAME_LEN+1];             //disk name
    partition_t *partition[DISK_PARITION_MAX]; //disk parition
    int solt;                                  //solt position
    devent_t devtern;                          //device entry info
    int handle;                                //source handle
    atomic_t ref;                              //reference
    int vol_num;                               //disk volume current count
} disk_t;

int disk_info_get(char *name,disk_t *buff);
int vol_info_get(char *name,partition_t *buff);



#endif