#ifndef __LIBC_MUTEXQUEU_H
#define __LIBC_MUTEXQUEUE_H

#include <types.h>

//flags
#define MUTEX_QUEUE_TIMED (1 << 17) //wait times
#define MUTEX_QUEUE_ALL (1 << 16)   //wake all waiter

//mutex opeartor
#define MUTEX_QUEUE_ADD (1 << 0)
#define MUTEX_QUEUE_SUB (1 << 1)
#define MUTEX_QUEUE_INC (1 << 2)
#define MUTEX_QUEUE_DEC (1 << 3)
#define MUTEX_QUEUE_ONE (1 << 4)
#define MUTEX_QUEUE_ZERO (1 << 5)
#define MUTEX_QUEUE_SET (1 << 6)

#define MUTEX_QUEUE_OPMASK 0xFFFF //operator mask 

int mutex_queue_alloc();
int mutex_queue_free(int handle);
int mutex_queue_wait(int handle, void *addr, uint32_t wqfalgs, uint64_t value);
int mutex_queue_wake(int handle, void *addr, uint32_t wqflags, uint64_t value);

#endif