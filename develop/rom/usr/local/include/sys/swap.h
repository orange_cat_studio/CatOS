#ifndef __SYS_SWAP_H
#define __SYS_SWAP_H

#ifdef __cplusplus
extern "C" {
#endif 

#include<types.h>

typedef struct
{
    uint64_t total_size;
    uint64_t free_size;
    uint64_t used_size;
}swap_info_t;

int SwapMem(swap_info_t *swap_info);

#ifdef __cplusplus
}
#endif 

#endif 