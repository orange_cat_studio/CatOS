#ifndef __SYS_SELECT_H
    #define __SYS_SELECT_H

    #ifdef __cplusplus
    extern "C"{
    #endif

    #include<sys/time.h>

    #define FD_SETSIZE 64
    #define FD_SET(n, set) ((set)->fd_bits[(n)/8]|=(1<<(n)))
    #define FD_CLEAR(n, set) ((set)->fd_bits[(n)/8]&=(~(1<<(n))))
    #define FD_ISSET(n, set) ((set)->fd_bits[(n) / 8] &= (1 << (n)))
    #define FD_ZERO(set) memset((set), 0, sizeof(*(set)))

    typedef struct
    {
        uint8_t fd_bits[(FD_SETSIZE + 7) / 8];
    } fd_set_t;

    int select(int maxfd,fd_set_t *readfds,fd_set_t *writefds,fd_set_t *exceptfds,timeval_t *timeout);


    #ifdef __cplusplus
    }
    #endif
#endif