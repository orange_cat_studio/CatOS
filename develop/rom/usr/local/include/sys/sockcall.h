#ifndef _SYS_SOCKETCALL_H
#define _SYS_SOCKETCALL_H

#include "socket.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define SOCKOP_SOCKET 0
#define SOCKET_BIND 1
#define SOCKET_CONNECT 2
#define SOCKET_ACCEPT 3
#define SOCKET_RECV 4
#define SOCKET_LISTEN 5
#define SOCKET_RECV_FROM 6
#define SOCKET_SEND 7
#define SOCKET_SEND_TO 8
#define SOCKET_SHUTDOWN 9
#define SOCKET_GETSOCKNAME 10
#define SOCKET_GETPEERNAME 11
#define SOCKET_GETSOCKOP 12
#define SOCKET_SETSOCKOP 13

    typedef struct
    {
        struct
        {
            int domain;
            int type;
            int protocol;
        } socket;
        struct
        {
            int sock;
            struct sockaddr *my_addr;
            int addrlen;
        } bind;
        struct
        {
            int sock;
            struct sockaddr *serv_addr;
            int addrlen;
        } connect;
        struct
        {
            int sock;
            struct sockaddr *addr;
            int *addrlen;
        } accept;
        struct
        {
            int sock;
            int backlog;
        } listen;
        struct
        {
            int sock;
            void *buff;
            int len;
            int flags;
        } recv;
        struct
        {
            int sock;
            void *buff;
            int len;
            int flags;
            struct sockaddr *from;
            socklen_t *fromlen;
        } recvfrom;
        struct
        {
            int sock;
            const void *buff;
            int len;
            int flags;
        } send;
        struct
        {
            int sock;
            const void *buff;
            int len;
            int flags;
            const struct sockaddr *to;
            socklen_t tolen;
        } sendto;
        struct
        {
            int sock;
            int how;
        } shutdown;
        struct
        {
            int sock;
            struct sockaddr *serv_addr;
            socklen_t *addrlen;
        } getpeername;
        struct
        {
            int sock;
            struct sockaddr *my_addr;
            socklen_t *addr_len;
        } getsockname;
        struct
        {
            int sock;
            int level;
            int optname;
            void *optval;
            socklen_t *optlen;
        } getsockopt;
        struct
        {
            int sock;
            int level;
            int optname;
            const void *optval;
            socklen_t optlen;
        } setsockopt;

    } sock_param_t;

    int sockcall(int sockop, sock_param_t *param);

#ifdef __cplusplus
}
#endif

#endif