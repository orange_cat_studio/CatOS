//file:libc/include/sizes.h
//autor:jiangxinpeng
//time:2021.8.14
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef __LIB_SIZES_H
    #define __LIB_SIZES_H

    #ifdef __cplusplus
    extern "C"{
    #endif

    #define SZ_16  (0x10)
    #define SZ_256 (0x100)
    #define SZ_512 (0x200)

    #define SZ_1K  (0x400)
    #define SZ_4K  (0x1000)
    #define SZ_8K  (0x2000)
    #define SZ_16K (0x4000)
    #define SZ_32K (0x8000)
    #define SZ_64K (0x10000)
    #define SZ_128K (0x20000)
    #define SZ_256K (0x40000)
    #define SZ_512K (0x80000)
    
    #define SZ_1M (0x100000)
    #define SZ_2M (0x200000)
    #define SZ_4M (0x400000)
    #define SZ_8M (0x800000)
    #define SZ_16M (0x1000000)
    #define SZ_32M (0x2000000)
    #define SZ_64M (0x4000000)
    #define SZ_128M (0x8000000)
    #define SZ_256M (0x10000000)
    #define SZ_512M (0x20000000)

    #define SZ_1G (0x100000000)
    #define SZ_2G (0x200000000)

    #ifdef __cplusplus
    }
    #endif
#endif