#ifndef __LIBC_X86_ATOMIC_H
#define __LIBC_X86_ATOMIC_H

#ifdef __cplusplus
extern "C"
{
#endif

    static void inline mem_atomic_add(int *a, int b)
    {

        __asm__ __volatile__("addl %1,(%0)"
                             :
                             : "a"(a), "b"(b)
                             : "memory");
    }

    static void inline mem_atomic_sub(int *a, int b)
    {
        __asm__ __volatile__("subl %1,(%0)"
                             :
                             : "a"(a), "b"(b)
                             : "memory");
    }

    static void inline mem_atomic_inc(int *a)
    {
        __asm__ __volatile__("inc (%0)"
                             :
                             : "a"(a)
                             : "memory");
    }

    static inline int mem_atomic_dec(int *a)
    {

        __asm__ __volatile__("dec %0"
                             :
                             : "a"(a)
                             : "memory");
    }

    static inline void mem_atomic_and(int *a, int b)
    {
        __asm__ __volatile__("and %1,(%0)"
                             :
                             : "a"(a), "b"(b)
                             : "memory");
    }

    static inline void mem_atomic_or(int *a, int b)
    {
        __asm__ __volatile__("or %1, (%0)"
                             :
                             : "a"(a), "b"(b)
                             : "memory");
    }

    static inline void mem_atomic_set(int *a, int b)
    {
        __asm__ __volatile__("mov %1,(%0)"
                             :
                             : "a"(a), "b"(b)
                             : "memory");
    }

    static inline void mem_atomic_xchg(int *a, int b)
    {
        __asm__ __volatile__("xchg %1,(%0)" ::"a"(a), "b"(b)
                             : "memory");
    }

#ifdef __cplusplus
}
#endif
#endif