#ifndef __LIBC_X86_CONST_H
#define __LIBC_X86_CONST_H

#ifdef __cplusplus
extern "C"
{
#endif

//page size
#define PG_SIZE 4096
#define PG_ALIGN(x) ((x + (PG_SIZE - 1)) & PAGE_SIZE)

//in 32bits cpu,word size is 2.in 64bits cpu is 4
#define _WORDSZ 2

#ifdef __cplusplus
}
#endif
#endif