#ifndef __LIBC_ARCH_ATOMIC_H
    #define __LIBC_ARCH_ATOMIC_H

    #ifdef __cplusplus
    extern "C"
    {
    #endif

    #include "config.h"
    #include "xchg.h"

    //atomic var
    typedef struct
    {
        int value;
    } atomic_t;

    #define atomic_get(atomic) ((atomic)->value)
    #define atomic_set(atomic, val) ((atomic)->value = (val))

    #define ATOMIC_INIT(val) (val)

    static inline void atomic_add(atomic_t *atomic, int value)
    {
        mem_atomic_add(&atomic->value, value);
    }

    static inline void atomic_sub(atomic_t *atomic, int value)
    {
        mem_atomic_sub(&atomic->value, value);
    }

    static inline void atomic_inc(atomic_t *atomic)
    {
        mem_atomic_inc(&atomic->value);
    }

    static inline void atomic_dec(atomic_t *atomic)
    {
        mem_atomic_dec(&atomic->value);
    }

    static inline void atomic_set_mask(atomic_t *atomic, int mask)
    {
        mem_atomic_or(&atomic->value, mask);
    }

    static inline void atomic_clear_mask(atomic_t *atomic, int mask)
    {
        mem_atomic_and(&atomic->value, ~mask);
    }

    #define atomic_xchg(v, new) (test_and_set(&((v)->value), new))

    #ifdef __cplusplus
    }
    #endif
#endif