#ifndef __LIBC_PWD_H
    #define __LIBC_PWD_H

    #include<types.h>

    #ifdef __cplusplus
    extern "C" {
    #endif

    typedef struct passwd
    {
       char *pw_name;       //user name
       char *pw_password;   //user password
       uid_t pw_uid;       //user id
       gid_t pw_gid;       //group id
       char *pw_gecos;      
       char *pw_dir;        //home dir
       char *pw_shell;      //shell path
    }passwd_t;
    
    int getpw(uid_t uid,char *buff);
    void setpwent();
    int endpwent();
    int getpw(uid_t uid, char *buff);
    passwd_t *getpwent();
    passwd_t *getpwbyname(const char *name);
    passwd_t *getpwbyuid(uid_t uid);

    #ifdef __cplusplus
    }
    #endif

#endif