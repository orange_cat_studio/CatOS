#ifndef __LIBC_GETOPT_H
#define __LIBC_GEtOPT_H

extern char *optarg;
extern int optind;
extern int opterr;
extern int optopt;

typedef struct option
{
    const char *name;
    int has_arg;
    int *flag;
    int val;
} option_t;

#define no_argument 0
#define required_argument 1
#define optional_argument 2

int getopt(int argc, char *const *argv, const char *option);
int getopt_long(int argc, char *const *argv, const char *options, option_t *long_options, int *idx);
int getopt_long_only(int argc, char *const *argv, const char *options, const option_t *long_options, int *idx);

#endif