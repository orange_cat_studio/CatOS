//file: libc/include/termios.h
//autor:jiangxinpeng
//time: 2021.12.27
//copyright:(C) 2020-2050 by jiangxinpeng,All right are reserved.

#ifndef __LIBC_TERMIOS_H
    #define __LIBC_TERMIOS_H

    #include <sys/ioctl.h>

    #ifdef __cplusplus
    extern "C"
    {
    #endif

    #define NCC 8 //termios control char group length

    typedef struct termios
    {
        uint64_t c_iflags; //input mode flags
        uint64_t c_oflags; //output mode flags
        uint64_t c_cfalgs; //control mode flags
        uint64_t c_lflags; //local mode flags
        uint8_t c_line;    //line rate
        uint8_t c_cc[NCC]; //control char num group
    } termios_t;

    int tcsetpgrp(int fd, pid_t pgid);
    int tcgetattr(int fildes, termios_t *termios_p);
    char *tgoto(const char *cap, int col, int row);

    #ifdef __cplusplus
    }
    #endif

#endif